﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
/// <summary>
/// Summary description for Alib
/// </summary>
public class Alib
{
   

    // public static string conStr = "Server=103.21.59.175; Database=vikasardo; User ID=vikasardo;Password=Host@vikasardo123; Min Pool Size=100; Max Pool Size=1000;";
    public static string conStr = "Server=13.58.11.166; Database=vikasardo; User ID=vikasardo;Password=V15A5A1%2; Min Pool Size=10; Max Pool Size=20;";//Trusted_Connection=True";
    //public static string conStr = "Server=13.58.157.42; Database=vikasardo; User ID=arera_all_user;Password=HJHJ^**(%^rtyderd6DFF; Min Pool Size=10; Max Pool Size=20;";
 //   public static string conStr = "Server=182.50.133.110; Database=vikasa; User ID=vikasa;Password=Host@vikasa; Min Pool Size=100; Max Pool Size=1000;";//Trusted_Connection=True";
    /*public static int userId;
    public static string clientId = "fjdsfi";
    public static string remainder;
    public static string profile;
    public static string FormLoad;
    public static string companyuserId = "khjleu";
    public static string codeId;
    public static string ApplyJobeId;

    public static int RepID;
    public static string id;
    public static int AdminID;
    public static int ManagerID;
    public static bool search_flag = false;
    public static string a = "";
    public static bool update_flag = false;

    public static bool cat_executed = false;*/
    public Alib()
    {
    }

    public static string idGetAFieldByQuery(string qur)
    {
        string retStr = "";
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand command = new SqlCommand(qur, conn);

            SqlDataReader reader = null;
            conn.Open();
            reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                retStr = reader[0].ToString();
            }
            conn.Close();
        }
        catch
        {

        }
        return retStr.Trim();
    }
    public static DateTime idGetADateByQuery(string qur)
    {
        DateTime retDate = new DateTime();
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand command = new SqlCommand(qur, conn);

            SqlDataReader reader = null;
            conn.Open();
            reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                retDate = (DateTime)reader[0];
            }
            conn.Close();
        }
        catch
        {

        }
        return retDate;
    }
    public static string idGetAFieldByQuery(string qur, string conStr)
    {
        string retStr = "";
        try
        {
            SqlConnection conn = new SqlConnection(conStr);
            SqlCommand command = new SqlCommand(qur, conn);

            SqlDataReader reader = null;
            conn.Open();
            reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                retStr = reader[0].ToString();
            }
            conn.Close();
        }
        catch
        {

        }
        return retStr.Trim();
    }
    public static bool idHasRows(string qur)
    {
        bool retBool = false;
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand command = new SqlCommand(qur, conn);
            conn.Open();
            SqlDataReader reader;
            reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                retBool = true;
            }
            conn.Close();
        }
        catch
        {

        }
        return retBool;

    }
    public static void idExecute(string qur)
    {
        SqlConnection conn = new SqlConnection(conStr);
        SqlCommand cmd = new SqlCommand();

        cmd.Connection = conn;
        conn.Open();
        cmd.CommandText = qur;
        cmd.ExecuteNonQuery();
        conn.Close();

    }
    public static void idExecute(string qur, string conStr)
    {
        SqlConnection conn = new SqlConnection(conStr);
        SqlCommand cmd = new SqlCommand();
        while (true)
        {
            try
            {


                cmd.Connection = conn;
                conn.Open();
                cmd.CommandText = qur;
                cmd.ExecuteNonQuery();
                break;
            }
            catch
            {

            }
            conn.Close();

        }
    }
    public static void idInsertInto(string TableName, object conStr, params string[] paramStr)
    {
        SqlConnection conn = new SqlConnection(conStr.ToString());


        string qur = "insert into " + TableName + "(";
        for (int i = 0; i < paramStr.Length; i = i + 2)
        {
            if (paramStr[i] == ":nc:" || paramStr[i] == ":null:")
                i++;
            qur += "" + paramStr[i] + ",";
        }
        qur = qur.Trim(',');
        qur += ") values (";
        for (int i = 0; i < paramStr.Length; i = i + 2)
        {
            if (paramStr[i] == ":nc:")
            {
                i++;
                qur += paramStr[i + 1] + ",";
            }
            else if (paramStr[i] == ":null:")
            {
                i++;
                qur += "null,";
            }
            else if (paramStr[i + 1] == ":null:")
                qur += "NULL,";
            else
            {

                qur += "'" + paramStr[i + 1].Replace('\'', '`') + "',";
            }
        }
        qur = qur.Trim(',');
        qur += ")";
        try
        {
            conn.Open();
            SqlCommand command = new SqlCommand(qur, conn);
            command.ExecuteNonQuery();
        }
        catch
        { }

        conn.Close();

    }
    public static void idInsertInto(string TableName, params string[] paramStr)
    {
        SqlConnection conn = new SqlConnection(Alib.conStr);


        string qur = "insert into " + TableName + "(";
        for (int i = 0; i < paramStr.Length; i = i + 2)
        {
            if (paramStr[i] == ":nc:" || paramStr[i] == ":null:")
                i++;
            qur += "" + paramStr[i] + ",";
        }
        qur = qur.Trim(',');
        qur += ") values (";
        for (int i = 0; i < paramStr.Length; i = i + 2)
        {
            if (paramStr[i] == ":nc:")
            {
                i++;
                qur += paramStr[i + 1] + ",";
            }
            else if (paramStr[i] == ":null:")
            {
                i++;
                qur += "null,";
            }
            else if (paramStr[i + 1] == ":null:")
                qur += "NULL,";
            else
            {

                qur += "'" + paramStr[i + 1].Replace('\'', '`') + "',";
            }
        }
        qur = qur.Trim(',');
        qur += ")";
        try
        {
            conn.Open();
            SqlCommand command = new SqlCommand(qur, conn);
            command.ExecuteNonQuery();
        }
        catch
        { }

        conn.Close();

    }
    public static int idUpdateTable(string TableName, string Condition, params string[] paramStr)
    {
        int val = 0;
        SqlConnection conn = new SqlConnection(Alib.conStr);

        string qur = "update " + TableName + " set ";
        for (int i = 0; i < paramStr.Length; i = i + 2)
        {
            if (paramStr[i] == ":nc:")
            {
                i++;
                qur += "" + paramStr[i] + "=" + paramStr[i + 1] + ",";
            }
            else if (paramStr[i] == ":null:")
            {
                i++;
                if (paramStr[i + 1] == null)
                    qur += "" + paramStr[i] + "=null,";
                else
                    qur += "" + paramStr[i] + "='" + paramStr[i + 1] + "',";
            }
            else if (paramStr[i + 1] == ":null:")
                qur += "" + paramStr[i] + "=NULL,";
            else
                qur += "" + paramStr[i] + "='" + paramStr[i + 1].Replace('\'', '`') + "',";
        }
        qur = qur.Trim(',');
        qur += " Where " + Condition;
        conn.Open();

        SqlCommand command = new SqlCommand(qur, conn);
        val = command.ExecuteNonQuery();

        conn.Close();
        return val;
    }
    public static DateTime idGetServerDateTime()
    {
        DateTime dt = new DateTime();
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = new SqlCommand("sahaya.idGetDateTime", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            reader.Read();
            dt = (DateTime)reader[0];
            conn.Close();
        }
        catch
        {
        }
        return dt;
    }

    public static DataSet getData(string query)
    {
        SqlConnection conn = new SqlConnection(Alib.conStr);
        //SqlCommand command = new SqlCommand(qur, conn);
        SqlDataAdapter adp = new SqlDataAdapter(query, conn);
        DataSet ds = new DataSet();
        conn.Open();
        adp.Fill(ds);
        return ds;
    }

    public static string getSingleValue(string query)
    {
        SqlConnection conn = new SqlConnection(Alib.conStr);
        //SqlDataReader dr = null;
        SqlCommand cmd = new SqlCommand(query, conn);
        conn.Open();
        var result = cmd.ExecuteScalar();
        if (result != null)
            return result.ToString();
        else
            return string.Empty;
    }

    public static SqlDataReader idReturnReader(string qur)
    {
        SqlConnection conn = new SqlConnection(Alib.conStr);
        SqlCommand command = new SqlCommand(qur, conn);
        conn.Open();
        SqlDataReader reader;
        reader = command.ExecuteReader();
        return reader;
    }


    public static System.Net.Mail.MailMessage mail { get; set; }

    public static bool idDeleteRows(string TableName, string condition)
    {
        bool isUpdated = false;
        SqlConnection conn = new SqlConnection(Alib.conStr);

        string qur = "delete " + TableName + " where " + condition;
        conn.Open();

        SqlCommand command = new SqlCommand(qur, conn);
        int result = command.ExecuteNonQuery();
        if (result > 0)
            isUpdated = true;
        else
            isUpdated = false;
        conn.Close();
        return isUpdated;
    }

    public static void idInsertIntoSeqVal(string TableName, string colname, string seqname, params string[] paramStr)
    {
        SqlConnection conn = new SqlConnection(Alib.conStr);
        string qur = "insert into " + TableName + "(" + colname + ",";
        for (int i = 0; i < paramStr.Length; i = i + 2)
        {
            if (paramStr[i] == ":nc:" || paramStr[i] == ":null:")
                i++;
            qur += "" + paramStr[i] + ",";
        }
        qur = qur.Trim(',');
        qur += ") values (NEXT VALUE FOR " + seqname + ",";
        for (int i = 0; i < paramStr.Length; i = i + 2)
        {
            if (paramStr[i] == ":nc:")
            {
                i++;
                qur += paramStr[i + 1] + ",";
            }
            else if (paramStr[i] == ":null:")
            {
                i++;
                qur += "null,";
            }
            else if (paramStr[i + 1] == ":null:")
                qur += "NULL,";
            else
            {
                qur += "'" + paramStr[i + 1].Replace('\'', '`') + "',";
            }
        }
        qur = qur.Trim(',');
        qur += ")";
        try
        {
            conn.Open();
            SqlCommand command = new SqlCommand(qur, conn);
            command.ExecuteNonQuery();
        }
        catch
        { }
        conn.Close();
    }
}

