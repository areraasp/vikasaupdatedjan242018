using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Arr
/// </summary>
public class Arr
{
    public Arr()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
public class regions
{
    public string regionID { get; set; }
    public string regionName { get; set; }
}
public class districts
{
    public string districtID { get; set; }
    public string districtName { get; set; }
}
public class education
{
    public string eduID { get; set; }
    public string eduName { get; set; }
}
public class loanpurpose
{
    public string ID { get; set; }
    public string purpose { get; set; }
}
public class occupation
{
    public string occID { get; set; }
    public string occName { get; set; }
}
public class govtCard
{
    public string govtID { get; set; }
    public string govtName { get; set; }
}
public class getActiveSHG
{
    public string shgid { get; set; }
    public string name { get; set; }
    public string type { get; set; }
    public string region { get; set; }
    public string district { get; set; }
    public string savingAccount { get; set; }
    public string meetingDate { get; set; }
    public string totalMember { get; set; }
    public string meetingPlace { get; set; }
    public string meetingTime { get; set; }
    public string formationDate { get; set; }
    public string savingAccOpenDate { get; set; }
    public string status { get; set; }
    public string village { get; set; }
    public string branch { get; set; }
}
public class getInactiveSHG
{
    public string shgid { get; set; }
    public string name { get; set; }
    public string type { get; set; }
    public string village { get; set; }
    public string taluka { get; set; }
    public string region { get; set; }
    public string district { get; set; }
    public string savingAccount { get; set; }
    public string meetingDate { get; set; }
    public string annualmeetDate { get; set; }
    public string annualIncome { get; set; }
    public string expenses { get; set; }
    public string rid { get; set; }
    public string totalMember { get; set; }
    public string meetingPlace { get; set; }
    public string meetingTime { get; set; }
    public string formationDate { get; set; }
    public string savingAccOpenDate { get; set; }
    public string cdeid { get; set; }
    public string status { get; set; }
    public string reason { get; set; }
    public string branch { get; set; }
    public string totalmembshg { get; set; }
}
public class getRejectedSHG
{
    public string shgid { get; set; }
    public string name { get; set; }
    public string type { get; set; }
    public string region { get; set; }
    public string district { get; set; }
    public string savingAccount { get; set; }
    public string meetingDate { get; set; }
    public string totalMember { get; set; }
    public string meetingPlace { get; set; }
    public string meetingTime { get; set; }
    public string formationDate { get; set; }
    public string savingAccOpenDate { get; set; }
    public string reason { get; set; }
    public string status { get; set; }
}
public class getSHGDetails
{
    public string name { get; set; }
    public string region { get; set; }
    public string district { get; set; }
    public string formationDate { get; set; }
    public string accNum { get; set; }
}
public class getMembers
{
    public string memberID { get; set; }
    public string name { get; set; }
    public string fatherORhusband { get; set; }
    public string dob { get; set; }
    public string gender { get; set; }
    public string adhaarNum { get; set; }
    public string bankname { get; set; }
    public string savingAccNum { get; set; }
    public string nominee { get; set; }
    public string maritalStatus { get; set; }
    public string education { get; set; }
    public string caste { get; set; }
    public string occupation { get; set; }
    public string typeOfHome { get; set; }
    public string propertyType { get; set; }
    public string govtCard { get; set; }
    public string mobile { get; set; }
    public string pan { get; set; }
    public string membershipFee { get; set; }
    public string sizeofFamily { get; set; }
    public string eldermale { get; set; }
    public string elderfemale { get; set; }
    public string childmale { get; set; }
    public string childfemale { get; set; }
    public string photo { get; set; }
    public string signature { get; set; }
    public string cdeid { get; set; }
    public string shgid { get; set; }
    public string status { get; set; }
    public string annualincome { get; set; }
    public string address { get; set; }
    public string savingDetails { get; set; }
    public string internalLending { get; set; }
    public string externalLending { get; set; }
}
public class getRepresentative
{
    public string memberID { get; set; }
    public string name { get; set; }
    public string memberActionID { get; set; }
}
public class getTask
{
    public string taskID { get; set; }
    public string name { get; set; }
    public string taskName { get; set; }
    public string time { get; set; }
    public string date { get; set; }
    public string status { get; set; }
    public string branch { get; set; }
    public string shglist { get; set; }
    public string taskby { get; set; }
    public string groupName { get; set; }
    public string enterDetails { get; set; }
    public string villageName { get; set; }   
}
public class cdogetTask
{
    public string taskID { get; set; }
    public string taskName { get; set; }
    public string name { get; set; }
    public string time { get; set; }
    public string date { get; set; }
    public string status { get; set; }
    public string branch { get; set; }
    public string shglist { get; set; }
    public string taskby { get; set; }
    public string groupName { get; set; }
    public string enterDetails { get; set; }
    public string villageName { get; set; } 
}

//for sepa
public class getMembersIE
{
    public string name { get; set; }
    public string memberID { get; set; }
    public string savings { get; set; }
    public List<Internallending> lending { get; set; }
    public string loanRepBalance { get; set; }
    public string penalty { get; set; }
}
public class Internallending
{
    public string internallendingid { get; set; }
    public string principalamountbal { get; set; }
    public string interestamountbal { get; set; }
    public string count { get; set; }
}

//-------For last payment---------//

public class getMembersIElastpayments
{
    public string name { get; set; }
    public string memberID { get; set; }
    public string savings { get; set; }
    public string lastsavings { get; set; }
    public List<Internallendinglastpayments> lending { get; set; }
    public string loanRepBalance { get; set; }
    public string lastloanRepBalance { get; set; }
    public string penalty { get; set; }
    public string lastpenalty { get; set; }
}
public class Internallendinglastpayments
{
    public string internallendingid { get; set; }
    public string principalamountbal { get; set; }
    public string lastprincipalamountbal { get; set; }
    public string interestamountbal { get; set; }
    public string lastinterestamountbal { get; set; }
    public string count { get; set; }
}


public class getMOM
{
    public string date { get; set; }
    public string place { get; set; }
    public string noOfPerson { get; set; }
    public string income { get; set; }
    public string expense { get; set; }
}
public class getSHgsList
{
    public string id { get; set; }
    public string name { get; set; }
    public string savingAcc { get; set; }
    public string branchName { get; set; }
    public string village { get; set; }
}

public class getBankBranch
{
    public string id { get; set; }
    public string name { get; set; }
}
public class getLoanType
{
    public string id { get; set; }
    public string name { get; set; }
}
public class getMembersIELastpayments
{
    public string savings { get; set; }
    public string internalBal { get; set; }
    public string loanRepay { get; set; }
    public string penalty { get; set; }
}
public class NullgetMembersIE
{
    public string name { get; set; }
    public string memberID { get; set; }
    public string savings { get; set; }
    public string internallendingid { get; set; }
    public string principalamountbal { get; set; }
    public string interestamountbal { get; set; }
    public string count { get; set; }
    public string loanRepBalance { get; set; }
    public string penalty { get; set; }
}

//-----------------------------------------------------Praveen's Work----------------------------------------------------------------//

public class NewProposals
{
    public string loanrefid { get; set; }
    public string memberid { get; set; }
    public string name { get; set; }
    public string momdate { get; set; }
    public string shgid { get; set; }
    public string cdeid { get; set; }
    public string amount { get; set; }
    public string reason { get; set; }
    public string bankbrach { get; set; }
    public string split_type { get; set; }
    public string shgloanamount { get; set; }
    public string aadharnum { get; set; }
    public string shgname { get; set; }
    public string loaenqid { get; set; }
}
public class NewProposalEnqs
{
    public string loanrefID { get; set; }
    public string shgid { get; set; }
    public string cdeid { get; set; }
    public string reason { get; set; }
    public string split_type { get; set; }
    public string shgloanamount { get; set; }
    public string shgname { get; set; }
    public string loaenqid { get; set; }
    public string bankbranch { get; set; }
    public string loantype { get; set; }
    public string status { get; set; }
    public string shgaccnum { get; set; }
    public string reportimage { get; set; }
    public string photoimage { get; set; }
    public string proposaldate { get; set; }
	public string isSancRejected { get; set; }
    //  public List<NewProposalEnqMembs> memberslist { get; set; }
}
public class NewProposalEnqdetails
{
    public string loanrefID { get; set; }
    public string shgid { get; set; }
    public string cdeid { get; set; }
    public string reason { get; set; }
    public string split_type { get; set; }
    public string shgloanamount { get; set; }
    public string shgname { get; set; }
    public string loaenqid { get; set; }
    public string bankbranch { get; set; }
    public string loantype { get; set; }
    public string status { get; set; }
    public string loan_number { get; set; }
    public string approvedloanamount { get; set; }
    public string emi { get; set; }
    public string shgaccnum { get; set; }
    public string rejreason { get; set; }
    public string rejby { get; set; }
    public string proposaldate { get; set; }
    public string sanctionedDate { get; set; }
    public string termsnconditions { get; set; }



    //  public List<NewProposalEnqMembs> memberslist { get; set; }
}
public class NewProposalEnqdetailsRole
{
    public string loanrefID { get; set; }
    public string shgid { get; set; }
    public string cdeid { get; set; }
    public string reason { get; set; }
    public string split_type { get; set; }
    public string shgloanamount { get; set; }
    public string shgname { get; set; }
    public string loaenqid { get; set; }
    public string bankbranch { get; set; }
    public string loantype { get; set; }
    public string status { get; set; }
    public string loan_number { get; set; }
    public string approvedloanamount { get; set; }
    public string emi { get; set; }
    public string shgaccnum { get; set; }
    public string rejby { get; set; }
    public string rejreason { get; set; }
    //  public List<NewProposalEnqMembs> memberslist { get; set; }
}
public class NewProposalEnqMembs
{
    public string membid { get; set; }
    public string membname { get; set; }
    public string aadharnum { get; set; }
    public string amount { get; set; }
    public string shgaccnum { get; set; }
    public string proposaldate { get; set; }
}

public class TotalEnqDetails
{
    public List<NewProposalEnqs> enqmaindetails { get; set; }
    public List<NewProposalEnqMembs> membinenqs { get; set; }
}

public class CaseStudy
{
    public string membid { get; set; }
    public string membname { get; set; }
    public string activityname { get; set; }
    public string date { get; set; }
    public string imagename { get; set; }
    public string cdeid { get; set; }
    public string shgid { get; set; }
}

public class addmomdetails
{
    public string memberid { get; set; }
    public string name { get; set; }
    public string savings { get; set; }
    public string totalsavings { get; set; }
    public string lendbal { get; set; }
    public string lendemi { get; set; }
    public string interest { get; set; }
    public string lending { get; set; }
    public string lendpending { get; set; }
    public string bankloan { get; set; }
    public string bankemi { get; set; }
    public string bankloanpending { get; set; }
    public string penalty { get; set; }
    public string newmeetinglending { get; set; }
    public string cash { get; set; }
    public string totalpenalty { get; set; }
    public string totalcollections { get; set; }
    public string meetings { get; set; }
}
public class MomMainDetails
{
    public string date { get; set; }
    public string place { get; set; }
    public string persons { get; set; }
    public string shgid { get; set; }
    public string cdeid { get; set; }
}

public class MomInfo
{
    public MomMainDetails mommaindetails { get; set; }
    public List<addmomdetails> momdetails { get; set; }
    public Savings saving { get; set; }
    public Expenses expense { get; set; }
}

public class MomList
{
    public string date { get; set; }
    public string place { get; set; }
    public string persons { get; set; }
    public Savings saving { get; set; }
    public Expenses expense { get; set; }
}

public class Savings
{
    public string shginbank { get; set; }
    public string shginhand { get; set; }
    public string shgsavings { get; set; }
    public string shgtotsavings { get; set; }
    public string shgtotlendrepay { get; set; }
    public string shgtotlendinterest { get; set; }
    public string shgtotintlending { get; set; }
    public string shgtotbankemi { get; set; }
    public string shgtotpenalty { get; set; }
    public string shgtotbankinterest { get; set; }
    public string shgtotgrants { get; set; }
    public string shgotherssavings { get; set; }
    public string shgtotcollection { get; set; }
    public string shgincomerefundfromrid { get; set; }
    public string membershipFee { get; set; }
}
public class Expenses
{
    public string shgexpcash { get; set; }
    public string shgexpcheque { get; set; }
    public string shgexpdepositetosavingsacc { get; set; }
    public string shgexpdepositetobankacc { get; set; }
    public string shgexpcashinhand { get; set; }
    public string shgexpamountinbanksavings { get; set; }
    public string shgexpmembersavingsrepayment { get; set; }
    public string shgexpothers { get; set; }
    public string shgexptotal { get; set; }
    public string shgexprid { get; set; }
    public string shgexp { get; set; }
}

public class MemberProfile
{
    public string memberid { get; set; }
    public string name { get; set; }
    public string totalsavings { get; set; }
    public string lending { get; set; }
    public string lendpending { get; set; }
    public string bankloan { get; set; }
    public string bankloanpending { get; set; }
    public string image { get; set; }
}

public class getCDE
{
    public string cdeid { get; set; }
    public string employment { get; set; }
    public string cdename { get; set; }
    public string cdephone { get; set; }
    public string cdemail { get; set; }
    public string cdoid { get; set; }
    public string cdmid { get; set; }
    public string cdephoto { get; set; }
    public string action { get; set; }
    public string SHG { get; set; }
}
public class getrejectedSHGbyCDE
{
    public string shgid { get; set; }
    public string name { get; set; }
    public string type { get; set; }
    public string village { get; set; }
    public string taluka { get; set; }
    public string region { get; set; }
    public string district { get; set; }
    public string savingAccount { get; set; }
    public string meetingDate { get; set; }
    public string annualmeetingDate { get; set; }
    public string annualIncome { get; set; }
    public string totalMember { get; set; }
    public string meetingPlace { get; set; }
    public string meetingTime { get; set; }
    public string formationDate { get; set; }
    public string savingAccOpenDate { get; set; }
    public string reason { get; set; }
    public string branch { get; set; }
    public string status { get; set; }
    public string rejectedby { get; set; }
    public string rejectedbyname { get; set; }
}

public class getrejectedSHGbyCDO
{
    public string shgid { get; set; }
    public string name { get; set; }
    public string type { get; set; }
    public string village { get; set; }
    public string taluka { get; set; }
    public string region { get; set; }
    public string district { get; set; }
    public string savingAccount { get; set; }
    public string meetingDate { get; set; }
    public string annualmeetingDate { get; set; }
    public string annualIncome { get; set; }
    public string totalMember { get; set; }
    public string meetingPlace { get; set; }
    public string meetingTime { get; set; }
    public string formationDate { get; set; }
    public string savingAccOpenDate { get; set; }
    public string reason { get; set; }
    public string branch { get; set; }
    public string status { get; set; }
    public string rejectedby { get; set; }
    public string rejectedbyname { get; set; }
}

public class getrejectedSHGbyCDM
{
    public string shgid { get; set; }
    public string name { get; set; }
    public string type { get; set; }
    public string village { get; set; }
    public string taluka { get; set; }
    public string region { get; set; }
    public string district { get; set; }
    public string savingAccount { get; set; }
    public string meetingDate { get; set; }
    public string annualmeetingDate { get; set; }
    public string annualIncome { get; set; }
    public string totalMember { get; set; }
    public string meetingPlace { get; set; }
    public string meetingTime { get; set; }
    public string formationDate { get; set; }
    public string savingAccOpenDate { get; set; }
    public string reason { get; set; }
    public string branch { get; set; }
    public string status { get; set; }
    public string rejectedby { get; set; }
    public string rejectedbyname { get; set; }
}
public class getactiveSHGCDO
{
    public string shgid { get; set; }
     public string cdeID { get; set; }
    public string name { get; set; }
    public string type { get; set; }
    public string village { get; set; }
    public string taluka { get; set; }
    public string region { get; set; }
    public string district { get; set; }
    public string savingAccount { get; set; }
    public string meetingDate { get; set; }
    public string annualmeetDate { get; set; }
    public string annualIncome { get; set; }
    public string totalMember { get; set; }
    public string meetingPlace { get; set; }
    public string meetingTime { get; set; }
    public string formationDate { get; set; }
    public string savingAccOpenDate { get; set; }
    public string status { get; set; }
    public string reason { get; set; }
    public string branch { get; set; }
    public string totalmembshg { get; set; }
}
public class getCDO
{
    public string cdoid { get; set; }
    public string employment { get; set; }
    public string cdoname { get; set; }
    public string cdophone { get; set; }
    public string cdomail { get; set; }
    public string cdmid { get; set; }
    public string cdophoto { get; set; }
    public string action { get; set; }
    public string CDE { get; set; }
}
public class cdmgetTask
{
    public string taskID { get; set; }
    public string name { get; set; }
    public string taskName { get; set; }
    public string time { get; set; }
    public string date { get; set; }
    public string status { get; set; }
    public string branch { get; set; }
    public string shglist { get; set; }
    public string taskby { get; set; }
    public string groupName { get; set; }
    public string enterdetails { get; set; }
    public string villageName { get; set; }
}
public class getcdetasks
{
    public string id { get; set; }
    public string tasks { get; set; }
}

public class getcdotasks
{
    public string id { get; set; }
    public string tasks { get; set; }
}
public class getcdmtasks
{
    public string id { get; set; }
    public string tasks { get; set; }
}


///--------------------------------------OLD SHGS--------------------------------------///

public class getoldSHgsList
{
    public string id { get; set; }
    public string name { get; set; }
    public string savingAcc { get; set; }
    public string branchName { get; set; }
    public string members { get; set; }
    public string gender { get; set; }
    public string village { get; set; }
    public string loanaccnum { get; set;}
    public string status { get; set; }


}

public class getOldSHgsLoan
{
    public string loanDate { get; set; }
    public string emi { get; set; }
    public string term { get; set; }
    public string termover { get; set; }
    public string targetRecover { get; set; }
    public string loanAmt { get; set; }
    public string receivedAmount { get; set; }
    public string loanBal { get; set; }
   
    //public string presentRecover { get; set; }
    //public string totalRecover { get; set; }
    //public string balRecover { get; set; }
}

public class getAddedOldSHgsLoanlist
{
    public string shgname { get; set; }
    public string sbacc { get; set; }
    public string branch { get; set; }
    public string gender { get; set; }
    public string members { get; set; }
    public string loandate { get; set; }
    public string emi { get; set; }
    public string term { get; set; }
    public string loanamt { get; set; }
    public string targetrecovery { get; set; }
    public string prevloanbalance { get; set; }
    public string presentrecovery { get; set; }
    public string totalrecovery { get; set; }
    public string balancerecovery { get; set; }
    public string meetingDate { get; set; }
    public string termOver { get; set; }
    public string village { get; set; }
}
public class getCDELeave
{
    public string leaveid { get; set; }
    public string cdeid { get; set; }
    public string type { get; set; }
    public string from { get; set; }
    public string to { get; set; }
    public string days { get; set; }
    public string name { get; set; }
}


// ---------------------------------------------------------------------05th October----------------------------------------------------------------------------//

//-------------------For Shgs-----------------------------//

public class SHGs_Due_Irregular_NPA
{
    public string shgid { get; set; }
    public string name { get; set; }
    public string branchName { get; set; }
    public string receivedAmount { get; set; }
    public string balance { get; set; }
}


public class SHGSactioned
{
    public static int shgId { get; set; }
}

public class StaffCDE
{
    public static int cdeId;
}
