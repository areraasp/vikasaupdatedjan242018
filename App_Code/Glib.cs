﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for Glib
/// </summary>
public static class Glib
{
    public static void LoadRequest(string query, GridView gv)
    {
        try
        {
            SqlConnection con = new SqlConnection(Alib.conStr);
            con.Open();
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gv.DataSource = ds;
            gv.DataBind();
            con.Close();
        }
        catch
        {

        }
    }
    /// <summary>
    /// For Binding  Value to Dropdown list 
    /// </summary>
    /// <param name="List">name of the DropDownList</param> 
    /// <param name="query">Sql query that you want to fetch data from database</param>
    /// <param name="name">Name ot the DropDownList name field</param>
    /// <param name="name">Value to the DropDownList  Value field</param>
    /// data for updating>/param>
    public static void LoadDDL(DropDownList List, string query, string name, string val)
    {
        SqlConnection con = null;
        try
        {
            if (Alib.idHasRows(query))
            {
                con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                List.DataSource = ds;
                List.DataTextField = name;
                List.DataValueField = val;
                List.DataBind();
                List.Items.Insert(0, new ListItem("--Select--", "10000000"));
            }
        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            if (con != null)
            {
                con.Close();
            }
        }
    }
    public static void LoadLst(ListBox List, string query, string name, string val)
    {
        SqlConnection con = null;
        try
        {
            if (Alib.idHasRows(query))
            {
                con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                List.DataSource = ds;
                List.DataTextField = name;
                List.DataValueField = val;
                List.DataBind();
            }
        }
        catch 
        {
        }
        finally
        {
            if (con != null)
            {
                con.Close();
            }
        }
    }

    /// <summary>
    /// For feaching first row first column value i.e. it will return single value and ignore other values 
    /// </summary>
    public static bool idHasRows(string qur)
    {
        bool retBool = false;
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand command = new SqlCommand(qur, conn);
            conn.Open();
            string result = (string)command.ExecuteScalar();
            if (result != null && result != "")
            {
                retBool = true;
            }
            conn.Close();
        }
        catch
        {

        }
        return retBool;
    }

    public static string[] Slice(List<string> source, int index, int length)
    {
        string[] slice = new string[length];
        source.CopyTo(index, slice, 0, length);
        return slice;
    }
}