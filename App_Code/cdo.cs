﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for cdo
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class cdo : System.Web.Services.WebService
{

    public cdo()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    SqlConnection Conn = null;
    SqlCommand Cmd;
    SqlDataReader rdr = null;
    private static SqlConnection con = null;
    private static SqlCommand cmd = null;
    private static SqlDataAdapter da = null;
    private static DataSet ds = null;

    //--------------------------------------------------------------CDO Sign Up----------------------------------------------------------------//
    [WebMethod(Description = "This is for CDO Sign Up ")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdo_signup(string phone)
    {
        string status = "";
        string msg = "OTP to sign up is:";
        string cdoid = "";
        string cdoname = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (Alib.idHasRows("SELECT * FROM [vikasardo].[vikasa].[cdo] WHERE cdophone='" + phone + "' "))
            {
                cdoid = Alib.getSingleValue("select cdoid from [vikasardo].[vikasa].[cdo] where cdophone='" + phone + "'");
                cdoname = Alib.getSingleValue("select cdoname from [vikasardo].[vikasa].[cdo] where cdophone='" + phone + "'");
                string cdoimg = "http://test.vikasardo.org/images/" + Alib.getSingleValue("select cdophoto from [vikasardo].[vikasa].[cdo] where cdophone='" + phone + "'");
                Random r = new Random();
                int tmp = r.Next(10000, 99999);
                WebClient Client = new WebClient();
                string bu = "http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + string.Concat(msg, tmp) + "&route=0&from=VIKASA&to=" + phone;
                Stream data = Client.OpenRead(bu);
                StreamReader reader = new StreamReader(data);
                string s = reader.ReadToEnd();
                data.Close();
                reader.Close();
                Alib.idExecute("Update [vikasardo].[vikasa].[cdo] set otp='" + tmp.ToString() + "' where cdophone='" + phone + "' ");
                status = "{\"isSuccess\":\"true\",\"cdoid\":\"" + cdoid + "\",\"otp\":\"" + tmp + "\",\"phone\":\"" + phone + "\",\"name\":\"" + cdoname + "\",\"image\":\"" + cdoimg + "\"}";
            }
            else
            {
                status = "{\"isSuccess\":\"false\",\"messageTitle\":\"invalid number\",\"messageDescription\":\"please enter valid number\"}";
            }
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"status\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //--------------------------------------------------------------CDM Sign Up----------------------------------------------------------------//
    [WebMethod(Description = "This is for CDM Sign Up ")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdm_signup(string phone)
    {
        string status = "";
        string msg = "OTP to sign up is:";
        string cdmid = "";
        string cdmname = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (Alib.idHasRows("SELECT * FROM [vikasardo].[vikasa].[cdm] WHERE cdmphone='" + phone + "' "))
            {
                cdmid = Alib.getSingleValue("select cdmid from [vikasardo].[vikasa].[cdm] where cdmphone='" + phone + "'");
                cdmname = Alib.getSingleValue("select cdmname from [vikasardo].[vikasa].[cdm] where cdmphone='" + phone + "'");
                string cdmimg = "http://test.vikasardo.org/images/" + Alib.getSingleValue("select cdmphoto from [vikasardo].[vikasa].[cdm] where cdmphone='" + phone + "'");
                Random r = new Random();
                int tmp = r.Next(10000, 99999);
                WebClient Client = new WebClient();
                string bu = "http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + string.Concat(msg, tmp) + "&route=0&from=VIKASA&to=" + phone;
                Stream data = Client.OpenRead(bu);
                StreamReader reader = new StreamReader(data);
                string s = reader.ReadToEnd();
                data.Close();
                reader.Close();
                Alib.idExecute("Update [vikasardo].[vikasa].[cdm] set otp='" + tmp.ToString() + "' where cdmphone='" + phone + "' ");
                status = "{\"isSuccess\":\"true\",\"cdmid\":\"" + cdmid + "\",\"otp\":\"" + tmp + "\",\"phone\":\"" + phone + "\",\"name\":\"" + cdmname + "\",\"image\":\"" + cdmimg + "\"}";
            }
            else
            {
                status = "{\"isSuccess\":\"false\",\"messageTitle\":\"invalid number\",\"messageDescription\":\"please enter valid number\"}";
            }
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"status\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //------------------------------------------------------------Add Task-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Add Task")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void add_task(string cdoid, string name, string taskname, string date, string time, string branch, string shglist, string taskby, string groupName, string enterDetails, string villageName)
    {
        string[] fromdate = date.Split('/');

        //--------date----------------//
        int first = Convert.ToInt32(fromdate[0].Count());
        string first1 = fromdate[0].ToString();
        if (first == 1)
        {
            first1 = "0" + first1;
        }

        //-----------month---------------//
        int second = Convert.ToInt32(fromdate[1].Count());
        string second1 = fromdate[1].ToString();
        if (second == 1)
        {
            second1 = "0" + second1;
        }

        //------------yesr--------------//
        int third = Convert.ToInt32(fromdate[2].Count());
        string third1 = fromdate[2].ToString();
        if (third == 2)
        {
            third1 = "20" + third1;
        }
        date = first1 + "/" + second1 + "/" + third1;
        string status = "";
        string taskID = "";
        string action = "0";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (!Alib.idHasRows("SELECT * FROM [vikasardo].[testvikasa].[cdotasks] WHERE cdoid='" + cdoid + "' and taskname='" + taskname + "' and date='" + date + "' and time='" + time + "' "))
            {
                taskID = Alib.getSingleValue("INSERT INTO [vikasardo].[testvikasa].[cdotasks] (cdoid, taskname, date, time, status, branch, shglist, taskby, name,groupName,enterDetails,villageName) Output Inserted.cdotaskid values('" + cdoid + "','" + taskname + "','" + date + "', '" + time + "', '" + action + "', '" + branch + "', '" + shglist + "', '" + taskby + "', '" + name + "','" + groupName + "','" + enterDetails + "','" + villageName + "')");
                status = "{\"message\":\"success\", \"taskID\":\"" + taskID + "\"}";
            }
            else
            {
                // Alib.idExecute("UPDATE representative set name='" + name + "', memberID='" + memberID + "' WHERE shgID='" + shgID + "' and  memberActionID='" + memberActionID + "'");
                status = "{\"message\":\"Already Existing\"}";
            }

            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //------------------------------------------------------------Get Task-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get Task")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_task(string cdoid, string date)
    {
        string s = "";
        List<cdogetTask> rg = new List<cdogetTask>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            string[] fromdate = date.Split('/');

            //--------date----------------//
            int first = Convert.ToInt32(fromdate[0].Count());
            string first1 = fromdate[0].ToString();
            if (first == 1)
            {
                first1 = "0" + first1;
            }

            //-----------month---------------//
            int second = Convert.ToInt32(fromdate[1].Count());
            string second1 = fromdate[1].ToString();
            if (second == 1)
            {
                second1 = "0" + second1;
            }

            //------------yesr--------------//
            int third = Convert.ToInt32(fromdate[2].Count());
            string third1 = fromdate[2].ToString();
            if (third == 2)
            {
                third1 = "20" + third1;
            }
            date = first1 + "/" + second1 + "/" + third1;

            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[testvikasa].[cdotasks] where cdoid='" + cdoid + "' and date='" + date + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new cdogetTask { taskID = reader["cdotaskid"].ToString(), name = reader["name"].ToString(), taskName = reader["taskName"].ToString(), date = reader["date"].ToString(), time = reader["time"].ToString(), status = reader["status"].ToString(), branch = reader["branch"].ToString(), shglist = reader["shglist"].ToString(), taskby = reader["taskby"].ToString(), groupName = reader["groupName"].ToString(), enterDetails = reader["enterDetails"].ToString(), villageName = reader["villageName"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Get Actions-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get Task Actions")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_task_action(string taskid, string action)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (!Alib.idHasRows("SELECT * FROM [vikasardo].[testvikasa].cdotasks WHERE cdotaskid='" + taskid + "' and status='" + action + "' "))
            {
                Alib.idExecute("UPDATE [vikasardo].[testvikasa].cdotasks set status='" + action + "' WHERE cdotaskid='" + taskid + "'");
                status = "{\"message\":\"success\"}";
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.Write(status);
            }
            else
            {
                status = "{\"message\":\"Already Exist\"}";
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.Write(status);
            }
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //------------------------------------------------------------Get Inactive SHG'S in CDO-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Inactive SHGs in CDO")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_inactive_shgCDO(string cdoid)
    {
        string s = "";
        List<getInactiveSHG> rg = new List<getInactiveSHG>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[shg] where status in (1,2) and cdeid in (select cdeid from [vikasardo].[vikasa].[cde] where cdoid='" + cdoid + "')", conn);
            conn.Open();
            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    string shgid = reader["shgid"].ToString();
                    rg.Add(new getInactiveSHG
                    {
                        shgid = shgid,
                        name = reader["name"].ToString(),
                        type = reader["type"].ToString(),
                        village = reader["village"].ToString(),
                        taluka = reader["taluka"].ToString(),
                        region = reader["region"].ToString(),
                        district = reader["district"].ToString(),
                        savingAccount = reader["savingAccount"].ToString(),
                        meetingDate = reader["meetingDate"].ToString(),
                        annualmeetDate = reader["annualmeetDate"].ToString(),
                        annualIncome = reader["annualIncome"].ToString(),
                        expenses = reader["expenses"].ToString(),
                        rid = reader["rid"].ToString(),
                        meetingPlace = reader["meetingPlace"].ToString(),
                        meetingTime = reader["meetingTime"].ToString(),
                        formationDate = reader["formationDate"].ToString(),
                        savingAccOpenDate = reader["savingAccOpenDate"].ToString(),
                        cdeid = reader["cdeid"].ToString(),
                        totalmembshg = reader["totalMember"].ToString(),
                        status = reader["status"].ToString(),
                        reason = reader["reason"].ToString(),
                        branch = reader["branch"].ToString(),
                        totalMember = Alib.idGetAFieldByQuery("select count(memberID) from [vikasardo].[vikasa].members where shgid='" + shgid + "' and cdeid in (select cdeid from [vikasardo].[vikasa].[cde] where cdoid='" + cdoid + "')")
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Get Inactive SHG'S in CDM-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Inactive SHGs in CDM")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_inactive_shgCDM(string cdmid)
    {
        string s = "";
        List<getInactiveSHG> rg = new List<getInactiveSHG>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[shg] where status in (2,3) and cdeid in (select cdeid from [vikasardo].[vikasa].cde where cdmid='" + cdmid + "')", conn);
            conn.Open();
            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    string shgid = reader["shgid"].ToString();
                    rg.Add(new getInactiveSHG
                    {
                        shgid = shgid,
                        name = reader["name"].ToString(),
                        type = reader["type"].ToString(),
                        village = reader["village"].ToString(),
                        taluka = reader["taluka"].ToString(),
                        region = reader["region"].ToString(),
                        district = reader["district"].ToString(),
                        savingAccount = reader["savingAccount"].ToString(),
                        meetingDate = reader["meetingDate"].ToString(),
                        annualmeetDate = reader["annualmeetDate"].ToString(),
                        annualIncome = reader["annualIncome"].ToString(),
                        expenses = reader["expenses"].ToString(),
                        rid = reader["rid"].ToString(),
                        meetingPlace = reader["meetingPlace"].ToString(),
                        meetingTime = reader["meetingTime"].ToString(),
                        formationDate = reader["formationDate"].ToString(),
                        savingAccOpenDate = reader["savingAccOpenDate"].ToString(),
                        cdeid = reader["cdeid"].ToString(),
                        totalmembshg = reader["totalMember"].ToString(),
                        status = reader["status"].ToString(),
                        reason = reader["reason"].ToString(),
                        branch = reader["branch"].ToString(),
                        totalMember = Alib.idGetAFieldByQuery("select count(memberID) from [vikasardo].[vikasa].members where shgid='" + shgid + "' and cdeid in (select cdeid from [vikasardo].[vikasa].[cde] where cdmid='" + cdmid + "')")
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------SHG Approval from CDO-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Approval SHG from CDO")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void shg_approval_fromCDO(string shgid)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            Alib.idExecute("UPDATE [vikasardo].[vikasa].[shg] set status='2' WHERE shgid='" + shgid + "'");
            status = "{\"message\":\"Approval sent to CDM\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);

            //----------------FCM Notification Starts--------------//

            //string cdmid = Alib.idGetAFieldByQuery("Select m.fcmid from [vikasardo].[vikasa].[cdm] m inner join [vikasardo].[vikasa].[cde] e on e.cdmid=m.cdmid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + shgid + "'");
            //string BrowserAPIKey = "AIzaSyByrnlMNpCI6gaOlqr7KIj0C5El2vVttBE";

            //string subject = "SHG_CDM";
            //string message = "CDO has approved new SHG please check it and approve";
            //if (cdmid != "")
            //{
            //    string postData = "{ \"registration_ids\": [\"" + cdmid + "\"], \"data\": {\"subject\":\"" + subject + "\",\"message\":\"" + message + "\"}}";
            //  CDMSendGCMNotification(BrowserAPIKey, postData);
            //}

            // -----------------FCM Notification Ends---------------------//


            // -----------------Message Send Starts-----------------------//

            string cdmnum = Alib.idGetAFieldByQuery("Select m.cdmphone from [vikasardo].[vikasa].[cdm] m inner join [vikasardo].[vikasa].[cde] e on e.cdmid=m.cdmid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + shgid + "'");
            string shgname = Alib.idGetAFieldByQuery("Select name from [vikasardo].[vikasa].[shg] where shgid ='" + shgid + "'");
            string cdoname = Alib.idGetAFieldByQuery("Select o.cdoname from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[cde] e on e.cdoid=o.cdoid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + shgid + "'");
            string cdename = Alib.idGetAFieldByQuery("Select e.cdename from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shg] s on e.cdeid= s.cdeid where shgid ='" + shgid + "'");
            string wish = "Dear Sir/Madam,\n I am Requesting you to approve the SHG below mentioned,\n " + shgname + " from " + cdename + ",\n Regards,\n " + cdoname;
            SqlConnection con = null;
            SqlDataReader rdr = null;
            try
            {
                WebClient Client = new WebClient();
                Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + cdmnum);
                Stream data = Client.OpenRead(uri);
                StreamReader reader = new StreamReader(data);
                string s = reader.ReadToEnd();
                data.Close();
                reader.Close();
            }
            catch (SqlException ex)
            {
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                    con.Dispose();
                }
                if (rdr != null)
                    rdr.Dispose();
            }

            string cdenum = Alib.idGetAFieldByQuery("Select e.cdephone from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shg] s on e.cdeid = s.cdeid where shgid ='" + shgid + "'");
            string shgname1 = Alib.idGetAFieldByQuery("Select name from [vikasardo].[vikasa].[shg] where shgid ='" + shgid + "'");
            string cdoname1 = Alib.idGetAFieldByQuery("Select o.cdoname from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[cde] e on e.cdoid=o.cdoid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + shgid + "'");
            string cdename1 = Alib.idGetAFieldByQuery("Select e.cdename from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shg] s on e.cdeid = s.cdeid where shgid ='" + shgid + "'");
            string wish1 = "Dear " + cdename1 + ",\n I have approved your requested SHG- " + shgname1 + " send to CDM for further verification,\n Regards,\n " + cdoname1;
            SqlConnection con1 = null;
            SqlDataReader rdr1 = null;
            try
            {
                WebClient Client = new WebClient();
                Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish1 + "&route=0&from=VIKASA&to=" + cdenum);
                Stream data = Client.OpenRead(uri);
                StreamReader reader = new StreamReader(data);
                string s = reader.ReadToEnd();
                data.Close();
                reader.Close();
            }
            catch (SqlException ex)
            {
            }
            finally
            {
                if (con1 != null)
                {
                    con1.Close();
                    con1.Dispose();
                }
                if (rdr1 != null)
                    rdr1.Dispose();
            }
            //-----------------Message Send Ends-------------------------//
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //------------------------------------------------------------SHG Approval from CDM-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Approval SHG from CDM")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void shg_approval_fromCDM(string shgid)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            Alib.idExecute("UPDATE [vikasardo].[vikasa].[shg] set status='3' WHERE shgid='" + shgid + "'");
            status = "{\"message\":\"Approval sent to HO\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);


            // -----------------Message Send Starts-----------------------//

            string honum = Alib.idGetAFieldByQuery("Select num from [vikasardo].[vikasardo].[HO]");
            string cdmname = Alib.idGetAFieldByQuery("Select m.cdmname from [vikasardo].[vikasa].[cdm] m inner join [vikasardo].[vikasa].[cde] e on e.cdmid=m.cdmid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + shgid + "'");
            string shgname = Alib.idGetAFieldByQuery("Select name from [vikasardo].[vikasa].[shg] where shgid ='" + shgid + "'");
            string cdoname = Alib.idGetAFieldByQuery("Select o.cdoname from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[cde] e on e.cdoid=o.cdoid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + shgid + "'");
            string cdename = Alib.idGetAFieldByQuery("Select e.cdename from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shg] s on e.cdeid= s.cdeid where shgid ='" + shgid + "'");
            string wish = "Dear Sir/Madam,\n I am Requesting you to approve the SHG below mentioned,\n " + shgname + " from CDE- " + cdename + " and CDO- " + cdoname + ",\n Regards,\n " + cdmname;
            SqlConnection con = null;
            SqlDataReader rdr = null;
            try
            {
                WebClient Client = new WebClient();
                Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + honum);
                Stream data = Client.OpenRead(uri);
                StreamReader reader = new StreamReader(data);
                string s = reader.ReadToEnd();
                data.Close();
                reader.Close();
            }
            catch (SqlException ex)
            {
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                    con.Dispose();
                }
                if (rdr != null)
                    rdr.Dispose();
            }

            string cdonum = Alib.idGetAFieldByQuery("Select o.cdophone from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[cde] e on e.cdoid=o.cdoid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + shgid + "'");
            string cdmname1 = Alib.idGetAFieldByQuery("Select m.cdmname from [vikasardo].[vikasa].[cdm] m inner join [vikasardo].[vikasa].[cde] e on e.cdmid=m.cdmid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + shgid + "'");
            string shgname1 = Alib.idGetAFieldByQuery("Select name from [vikasardo].[vikasa].[shg] where shgid ='" + shgid + "'");
            string cdoname1 = Alib.idGetAFieldByQuery("Select o.cdoname from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[cde] e on e.cdoid=o.cdoid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + shgid + "'");
            // string cdename1 = Alib.idGetAFieldByQuery("Select e.cdename from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shg] s on e.cdeid= s.cdeid where shgid ='" + shgid + "'");
            string wish1 = "Dear " + cdoname1 + ",\n I have approved your requested SHG- " + shgname1 + " sent to HO for further verification,\n Regards,\n " + cdmname1;
            SqlConnection con1 = null;
            SqlDataReader rdr1 = null;
            try
            {
                WebClient Client = new WebClient();
                Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish1 + "&route=0&from=VIKASA&to=" + cdonum);
                Stream data = Client.OpenRead(uri);
                StreamReader reader = new StreamReader(data);
                string s = reader.ReadToEnd();
                data.Close();
                reader.Close();
            }
            catch (SqlException ex)
            {
            }
            finally
            {
                if (con1 != null)
                {
                    con1.Close();
                    con1.Dispose();
                }
                if (rdr1 != null)
                    rdr1.Dispose();
            }

            //-----------------Message Send Ends-------------------------//
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //------------------------------------------------------------Get CDE'S-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get CDEs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_cdes(string cdoid)
    {
        string s = "";
        List<getCDE> rg = new List<getCDE>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;

            cmd = new SqlCommand("Select e.cdeid, e.employment, e.cdename, e.cdephone, e.cdemail, e.cdoid, e.cdmid, e.cdephoto, count(*) as SHGs from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shg] s on cast(s.cdeid as varchar(50)) = cast(e.cdeid as varchar(50)) where e.cdoid='" + cdoid + "' group by e.cdeid, e.employment, e.cdename, e.cdephone, e.cdemail, e.cdoid, e.cdmid, e.cdephoto", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getCDE { cdeid = reader["cdeid"].ToString(), employment = reader["employment"].ToString(), cdename = reader["cdename"].ToString(), cdephone = reader["cdephone"].ToString(), cdemail = reader["cdemail"].ToString(), cdoid = reader["cdoid"].ToString(), cdmid = reader["cdmid"].ToString(), cdephoto = "http://test.vikasardo.org/images/" + reader["cdephoto"].ToString(), SHG = reader["SHGs"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-----------------------------------------------------Get CDO's Proposals----------------------------------------------------------------//

    [WebMethod(Description = "This is to Get CDO's Proposals")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getcdo_newenquiries(string cdoid)
    {
        string s = "";
        List<NewProposalEnqs> np = new List<NewProposalEnqs>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            //cmd = new SqlCommand("select top 1 id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num from shgexternalloansplit where cdeid='" + cdeid + "'", conn);
            cmd = new SqlCommand("SELECT id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from [vikasardo].[vikasa].shgexternalloansplit where status in(1,3,5) and id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and cdoid='" + cdoid + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqs { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }


    //-------------------------------------------------------------CDO Proposal Approve----------------------------------------------------------//
    [WebMethod(Description = "This is for CDO Proposal Approve")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdo_approvalfornewenqs(string cdoid, string loanenqnum, string reportimage, string photoimage)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            //for repimg
            byte[] imageBytes = Convert.FromBase64String(reportimage);
            MemoryStream ms = new MemoryStream(imageBytes);
            string repimg = RandomNo.Random.GenerateIdentifier(28) + ".png";
            FileStream fs = new FileStream(Server.MapPath("~/Reportimages/" + repimg), FileMode.Create);
            ms.WriteTo(fs);

            //for photoimg
            byte[] imageBytes1 = Convert.FromBase64String(photoimage);
            MemoryStream ms1 = new MemoryStream(imageBytes1);
            string photoimg = RandomNo.Random.GenerateIdentifier(28) + ".png";
            FileStream fs1 = new FileStream(Server.MapPath("~/Photoimages/" + photoimg), FileMode.Create);
            ms1.WriteTo(fs1);

            ms.Close();
            fs.Close();
            fs.Dispose();

            ms1.Close();
            fs1.Close();
            fs1.Dispose();

            Alib.idExecute("update [vikasardo].[vikasa].[shgexternalloansplit] set status='3',approvedstate='3',reportimage='" + repimg + "',photoimage='" + photoimg + "' where cdoid='" + cdoid + "' and loan_enq_num='" + loanenqnum + "'");
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";


            //---------------------FCM Strats------------------//
            //string cdmid = Alib.idGetAFieldByQuery("Select m.fcmid from [vikasardo].[vikasa].[cdm] m inner join [vikasardo].[vikasa].[cdo] o on o.cdmid = o.cdmid where cdoid ='" + cdoid + "'");
            //string BrowserAPIKey = "AIzaSyByrnlMNpCI6gaOlqr7KIj0C5El2vVttBE";

            //string subject = "ENQ_CDM";
            //string message = "CDO has approved new Enquiry please check it and approve";
            //if (cdmid != "")
            //{
            //    string postData = "{ \"registration_ids\": [\"" + cdmid + "\"], \"data\": {\"subject\":\"" + subject + "\",\"message\":\"" + message + "\"}}";
            //  CDMSendGCMNotification(BrowserAPIKey, postData);
            //}
            //-------------------FCM Ends-------------------//

            //-----------------Message Send Starts-----------------------//

            string cdmnum = Alib.idGetAFieldByQuery("Select m.cdmphone from [vikasardo].[vikasa].[cdm] m inner join [vikasardo].[vikasa].[cdo] o on o.cdmid=m.cdmid where cdoid ='" + cdoid + "'");
            string cdoname = Alib.idGetAFieldByQuery("Select cdoname from [vikasardo].[vikasa].[cdo] where cdoid ='" + cdoid + "'");
            string shgname = Alib.idGetAFieldByQuery("Select s.name from [vikasardo].[vikasa].[shg] s inner join [vikasardo].[vikasa].[shgexternalloansplit] sp on sp.shgid=s.shgid where loan_enq_num ='" + loanenqnum + "'");
            string cdename = Alib.idGetAFieldByQuery("Select e.cdename from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shgexternalloansplit] sp on sp.cdeid=e.cdeid where loan_enq_num ='" + loanenqnum + "'");

            string wish = "Dear Sir/Madam,\n I am Requesting you to approve the Proposal of " + shgname + ", from " + cdename + ", \n Regards,\n " + cdoname;
            SqlConnection con = null;
            SqlDataReader rdr = null;
            try
            {
                WebClient Client = new WebClient();
                Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + cdmnum);
                Stream data = Client.OpenRead(uri);
                StreamReader reader1 = new StreamReader(data);
                string s = reader1.ReadToEnd();
                data.Close();
                reader1.Close();
            }
            catch (SqlException ex)
            {
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                    con.Dispose();
                }
                if (rdr != null)
                    rdr.Dispose();
            }

            string cdenum = Alib.idGetAFieldByQuery("Select e.cdephone from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shgexternalloansplit] sp on sp.cdeid=e.cdeid where loan_enq_num ='" + loanenqnum + "'");
            string wish1 = "Dear " + cdename + ",\n I have approved your requested Proposal of " + shgname + ", sent to CDM for further verification,\n Regards,\n " + cdoname;
            SqlConnection con1 = null;
            SqlDataReader rdr1 = null;
            try
            {
                WebClient Client = new WebClient();
                Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish1 + "&route=0&from=VIKASA&to=" + cdenum);
                Stream data = Client.OpenRead(uri);
                StreamReader reader1 = new StreamReader(data);
                string s = reader1.ReadToEnd();
                data.Close();
                reader1.Close();
            }
            catch (SqlException ex)
            {
            }
            finally
            {
                if (con1 != null)
                {
                    con1.Close();
                    con1.Dispose();
                }
                if (rdr1 != null)
                    rdr1.Dispose();
            }

            //-----------------Message Send Ends-------------------------//
        }
        catch(Exception ex)
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}"+ ex.Message;
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //-------------------------------------------------------------CDO Proposal Reject----------------------------------------------------------//
    [WebMethod(Description = "This is for CDO Proposal Reject")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdo_rejectfornewenqs(string cdoid, string loanenqnum, string rejreason)
    {

        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("update [vikasardo].[vikasa].[shgexternalloansplit] set status='4',rejreason='" + rejreason + "',rejbyid='" + cdoid + "',rejbyrole='CDO' where cdoid='" + cdoid + "' and loan_enq_num='" + loanenqnum + "'");
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //-----------------------------------------------------Get CDM's Proposals----------------------------------------------------------------//

    [WebMethod(Description = "This is to Get CDM's Proposals")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getcdm_newenquiries(string cdmid)
    {
        string s = "";
        List<NewProposalEnqs> np = new List<NewProposalEnqs>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            //cmd = new SqlCommand("select top 1 id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num from shgexternalloansplit where cdeid='" + cdeid + "'", conn);
            cmd = new SqlCommand("SELECT id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,reportimage,photoimage from [vikasardo].[vikasa].shgexternalloansplit where status in(3,5) and id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and cdmid='" + cdmid + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqs { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), reportimage = "http://test.vikasardo.org/Reportimages/" + reader["reportimage"].ToString(), photoimage = "http://test.vikasardo.org/Photoimages/" + reader["photoimage"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-------------------------------------------------------------CDM Proposal Approve----------------------------------------------------------//
    [WebMethod(Description = "This is for CDM Proposal Approve")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdm_approvalfornewenqs(string cdmid, string loanenqnum)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("update [vikasardo].[vikasa].[shgexternalloansplit] set status='5',approvedstate='5' where cdmid='" + cdmid + "' and loan_enq_num='" + loanenqnum + "'");
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";

            //------------------------------Msg Starts-------------------------//
            string honum = Alib.idGetAFieldByQuery("Select num from [vikasardo].[vikasardo].[HO]");
            string cdmname = Alib.idGetAFieldByQuery("Select cdmname from [vikasardo].[vikasa].[cdm] where cdmid ='" + cdmid + "'");
            string shgname = Alib.idGetAFieldByQuery("Select s.name from [vikasardo].[vikasa].[shg] s inner join [vikasardo].[vikasa].[shgexternalloansplit] sp on sp.shgid= s.shgid where loan_enq_num ='" + loanenqnum + "'");
            string cdoname = Alib.idGetAFieldByQuery("Select o.cdoname from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[shgexternalloansplit] sp on sp.cdoid= o.cdoid where loan_enq_num ='" + loanenqnum + "'");
            string cdename = Alib.idGetAFieldByQuery("Select e.cdename from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shgexternalloansplit] sp on sp.cdeid= e.cdeid where loan_enq_num ='" + loanenqnum + "'");
            string wish = "Dear HO Panel,\n I am Requesting you to approve the proposal of SHG- " + shgname + " from CDE- " + cdename + " and CDO- " + cdoname + ",\n Regards,\n " + cdmname;
            SqlConnection con = null;
            SqlDataReader rdr = null;
            try
            {
                WebClient Client = new WebClient();
                Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + honum);
                Stream data = Client.OpenRead(uri);
                StreamReader reader = new StreamReader(data);
                string s = reader.ReadToEnd();
                data.Close();
                reader.Close();
            }
            catch (SqlException ex)
            {

            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                    con.Dispose();
                }
                if (rdr != null)
                    rdr.Dispose();
            }

            string cdonum = Alib.idGetAFieldByQuery("Select o.cdophone from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[shgexternalloansplit] sp on sp.cdoid= o.cdoid where loan_enq_num ='" + loanenqnum + "'");
            string wish1 = "Dear " + cdoname + ",\n I have approved your requested Proposal of " + shgname + ", sent to HO for further verification,\n Regards,\n " + cdmname;
            SqlConnection con1 = null;
            SqlDataReader rdr1 = null;
            try
            {
                WebClient Client = new WebClient();
                Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish1 + "&route=0&from=VIKASA&to=" + cdonum);
                Stream data = Client.OpenRead(uri);
                StreamReader reader1 = new StreamReader(data);
                string s = reader1.ReadToEnd();
                data.Close();
                reader1.Close();
            }
            catch (SqlException ex)
            {
            }
            finally
            {
                if (con1 != null)
                {
                    con1.Close();
                    con1.Dispose();
                }
                if (rdr1 != null)
                    rdr1.Dispose();
            }

            //-----------------------Msg Ends--------------------//
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //-------------------------------------------------------------CDM Proposal Reject----------------------------------------------------------//
    [WebMethod(Description = "This is for CDM Proposal Reject")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdm_rejectfornewenqs(string cdmid, string loanenqnum, string rejreason)
    {

        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("update [vikasardo].[vikasa].[shgexternalloansplit] set status='6',rejreason='" + rejreason + "',rejbyid='" + cdmid + "',rejbyrole='CDM' where cdmid='" + cdmid + "' and loan_enq_num='" + loanenqnum + "'");
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }
    //PRAVEENS WORK FROM JUNE 19

    //-------------------------------------------------------------CDO Sanctioned List----------------------------------------------------------//
    [WebMethod(Description = "This is for CDO Sanctioned List")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getcdo_sanctionedlist(string cdoid)
    {

        string s = "";
        List<NewProposalEnqdetails> np = new List<NewProposalEnqdetails>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            //  cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,loan_number,approvedloanamount,emi,shgaccnum from [vikasardo].[vikasa].shgexternalloansplit where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and cdeid='" + cdeid + "' and approvedstate='1' and status='4'", conn);
            cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,loan_number,approvedloanamount,emi,shgaccnum,sanctionedDate,termsnconditions from [vikasardo].[vikasa].shgexternalloansplit where status in(11,12,13) and id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num)  and cdoid='" + cdoid + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqdetails { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), loan_number = reader["loan_number"].ToString(), emi = reader["emi"].ToString(), approvedloanamount = reader["approvedloanamount"].ToString(), shgaccnum = reader["shgaccnum"].ToString(), sanctionedDate = reader["sanctionedDate"].ToString(), termsnconditions = reader["termsnconditions"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-------------------------------------------------------------CD0 Approval for Sanctioned List----------------------------------------------------------//
    [WebMethod(Description = "This is for CD0 Approval for Sanctioned List")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdoapproval_forsanctionedlist(string cdoid, string loanenqnum)
    {

        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("update [vikasardo].[vikasa].[shgexternalloansplit] set status='12' where cdoid='" + cdoid + "' and loan_enq_num='" + loanenqnum + "'");
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

     //-------------------------------------------------------------Reject for Sanctioned List----------------------------------------------------------//
    [WebMethod(Description = "This is for Reject for Sanctioned List(Use This for both CDO,CDM)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void reject_forsanctionedlist(string rejbyrole, string rejreason, string rejbyid, string loanenqnum, string loanrefid)
    {

        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        int sancRejected = Convert.ToInt32(Alib.idGetAFieldByQuery("Select [isSancRejected] FROM [vikasa].[shgexternalloansplit] where loan_enq_num='" + loanenqnum + "'"));

        string rejSanctioned = "50";
        int isSancRej = 2;
        if (sancRejected == 0)
        {
            rejSanctioned = "7";
            isSancRej = 1;
        }

        try
        {
            Alib.idExecute("update [vikasardo].[vikasa].[shgexternalloansplit] set status='" + rejSanctioned + "',rejreason='" + rejreason + "',rejbyid='" + rejbyid + "',rejbyrole='" + rejbyrole + "' where referenceID='" + loanrefid + "' and loan_enq_num='" + loanenqnum + "'");
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
            Alib.idExecute("update [vikasa].[shgexternalloansplit] set [isSancRejected] = "+ isSancRej + " where loan_enq_num='" + loanenqnum + "'");
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        


        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }



    //-------------------------------------------------------------CDM Sanctioned List----------------------------------------------------------//
    [WebMethod(Description = "This is for CDM Sanctioned List")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getcdm_sanctionedlist(string cdmid)
    {

        string s = "";
        List<NewProposalEnqdetails> np = new List<NewProposalEnqdetails>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            //  cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,loan_number,approvedloanamount,emi,shgaccnum from [vikasardo].[vikasa].shgexternalloansplit where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and cdeid='" + cdmid + "' and approvedstate='1' and status='4'", conn);
            cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,loan_number,approvedloanamount,emi,shgaccnum,sanctionedDate,termsnconditions from [vikasardo].[vikasa].shgexternalloansplit where status in(12,13) and id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num)  and cdmid='" + cdmid + "'", conn);

            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqdetails { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), loan_number = reader["loan_number"].ToString(), emi = reader["emi"].ToString(), approvedloanamount = reader["approvedloanamount"].ToString(), shgaccnum = reader["shgaccnum"].ToString(), sanctionedDate = reader["sanctionedDate"].ToString(), termsnconditions = reader["termsnconditions"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-------------------------------------------------------------CDM Approval for Sanctioned List----------------------------------------------------------//
    [WebMethod(Description = "This is for CDM Approval for Sanctioned List")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdmapproval_forsanctionedlist(string cdmid, string loanenqnum, string disbimage, string meetingimage)
    {

        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {

            //for disb img
            byte[] imageBytes = Convert.FromBase64String(disbimage);
            MemoryStream ms = new MemoryStream(imageBytes);
            string dimg = RandomNo.Random.GenerateIdentifier(28) + ".png";
            FileStream fs = new FileStream(Server.MapPath("~/Disburseimages/" + dimg), FileMode.Create);
            ms.WriteTo(fs);

            //for meeting img
            byte[] imageBytes1 = Convert.FromBase64String(meetingimage);
            MemoryStream ms1 = new MemoryStream(imageBytes1);
            string mimg = RandomNo.Random.GenerateIdentifier(28) + ".png";
            FileStream fs1 = new FileStream(Server.MapPath("~/Meetingimages/" + mimg), FileMode.Create);
            ms1.WriteTo(fs1);

            ms.Close();
            fs.Close();
            fs.Dispose();

            ms1.Close();
            fs1.Close();
            fs1.Dispose();

            Alib.idExecute("update [vikasardo].[vikasa].[shgexternalloansplit] set status='13' where cdmid='" + cdmid + "' and loan_enq_num='" + loanenqnum + "'");
            Alib.idExecute("update [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] set status='13',disburseimg='" + dimg + "',meetingimg='" + mimg + "' where cdmid='" + cdmid + "' and loan_enq_num='" + loanenqnum + "'");
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //-----------------------------------------------------Get Finalized List----------------------------------------------------------------//
    [WebMethod(Description = "This is to Get CDO Finalized List(Finally Approved By HO)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getfinalizedcdo_sanctionedlist(string cdoid)
    {
        string s = "";
        List<NewProposalEnqdetails> np = new List<NewProposalEnqdetails>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            // cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from shgexternalloansplit where cdeid='" + cdeid + "' and approvedstate='1' and status='2'", conn);
            cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,loan_number,approvedloanamount,emi,shgaccnum,sanctionedDate,termsnconditions from [vikasardo].[vikasa].shgexternalloansplit where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and cdoid='" + cdoid + "' and status='14'", conn);

            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqdetails { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), loan_number = reader["loan_number"].ToString(), emi = reader["emi"].ToString(), approvedloanamount = reader["approvedloanamount"].ToString(), shgaccnum = reader["shgaccnum"].ToString(), sanctionedDate = reader["sanctionedDate"].ToString(), termsnconditions = reader["termsnconditions"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

   //-----------------------------------------------------Get Rejected List----------------------------------------------------------------//
    [WebMethod(Description = "This is to Get CDO Rejected List(Finally Rejected By HO)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getrejectedcdo_sanctionedlist(string cdoid)
    {
        string s = "";
        List<NewProposalEnqdetails> np = new List<NewProposalEnqdetails>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            // cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from shgexternalloansplit where cdeid='" + cdeid + "' and approvedstate='1' and status='2'", conn);
            cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,loan_number,approvedloanamount,emi,shgaccnum from [vikasardo].[vikasa].shgexternalloansplit where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and cdoid='" + cdoid + "'  and status='12' and isSancRejected = 0", conn);

            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqdetails { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), loan_number = reader["loan_number"].ToString(), emi = reader["emi"].ToString(), approvedloanamount = reader["approvedloanamount"].ToString(), shgaccnum = reader["shgaccnum"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }
    //-----------------------------------------------------Get Finalized List----------------------------------------------------------------//
    [WebMethod(Description = "This is to Get CDM Finalized List(Finally Approved By HO)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getfinalizedcdm_sanctionedlist(string cdmid)
    {
        string s = "";
        List<NewProposalEnqdetails> np = new List<NewProposalEnqdetails>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            // cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from shgexternalloansplit where cdeid='" + cdeid + "' and approvedstate='1' and status='2'", conn);
            cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,loan_number,approvedloanamount,emi,shgaccnum,sanctionedDate,termsnconditions from [vikasardo].[vikasa].shgexternalloansplit where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and cdmid='" + cdmid + "'  and status='14'", conn);

            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqdetails { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), loan_number = reader["loan_number"].ToString(), emi = reader["emi"].ToString(), approvedloanamount = reader["approvedloanamount"].ToString(), shgaccnum = reader["shgaccnum"].ToString(), sanctionedDate = reader["sanctionedDate"].ToString(), termsnconditions = reader["termsnconditions"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

     //-----------------------------------------------------Get CDM Rejected List----------------------------------------------------------------//
    [WebMethod(Description = "This is to Get CDM Rejected List(Finally Rejected By HO)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getrejectedcdm_sanctionedlist(string cdmid)
    {
        string s = "";
        List<NewProposalEnqdetails> np = new List<NewProposalEnqdetails>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            // cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from shgexternalloansplit where cdeid='" + cdeid + "' and approvedstate='1' and status='2'", conn);
            cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,loan_number,approvedloanamount,emi,shgaccnum from [vikasardo].[vikasa].shgexternalloansplit where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and cdmid='" + cdmid + "'  and status='20' and isSancRejected = 0", conn);

            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqdetails { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), loan_number = reader["loan_number"].ToString(), emi = reader["emi"].ToString(), approvedloanamount = reader["approvedloanamount"].ToString(), shgaccnum = reader["shgaccnum"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-----------------------------------------------------Get CDM Rejected List----------------------------------------------------------------//
    [WebMethod(Description = "This is to Get Rejected List(For both CDM,CDO)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getrejected_sanctionedlist()
    {
        string s = "";
        List<NewProposalEnqdetailsRole> np = new List<NewProposalEnqdetailsRole>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            // cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from shgexternalloansplit where cdeid='" + cdeid + "' and approvedstate='1' and status='2'", conn);
            cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,loan_number,approvedloanamount,emi,shgaccnum,rejbyrole from [vikasardo].[vikasa].shgexternalloansplit where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and status='20'", conn);

            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqdetailsRole { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), loan_number = reader["loan_number"].ToString(), emi = reader["emi"].ToString(), approvedloanamount = reader["approvedloanamount"].ToString(), shgaccnum = reader["shgaccnum"].ToString(), rejby = reader["rejbyrole"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }
    //-----------------------------------------------------Get CDM Rejected List----------------------------------------------------------------//
    [WebMethod(Description = "This is to Get Rejected List(For CDO)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getcdo_rejectedlist(string cdoid)
    {
        string s = "";
        List<NewProposalEnqdetailsRole> np = new List<NewProposalEnqdetailsRole>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            // cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from shgexternalloansplit where cdeid='" + cdeid + "' and approvedstate='1' and status='2'", conn);
            cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,loan_number,approvedloanamount,emi,shgaccnum,rejbyrole,rejreason from [vikasardo].[vikasa].shgexternalloansplit where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and status in(4,6,8,20)", conn);

            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqdetailsRole { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), loan_number = reader["loan_number"].ToString(), emi = reader["emi"].ToString(), approvedloanamount = reader["approvedloanamount"].ToString(), shgaccnum = reader["shgaccnum"].ToString(), rejby = reader["rejbyrole"].ToString(), rejreason = reader["rejreason"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-----------------------------------------------------Get CDM Rejected List----------------------------------------------------------------//
    [WebMethod(Description = "This is to Get Rejected List(For CDM)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getcdm_rejectedlist(string cdmid)
    {
        string s = "";
        List<NewProposalEnqdetailsRole> np = new List<NewProposalEnqdetailsRole>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            // cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from shgexternalloansplit where cdeid='" + cdeid + "' and approvedstate='1' and status='2'", conn);
            cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,loan_number,approvedloanamount,emi,shgaccnum,rejbyrole,rejreason from [vikasardo].[vikasa].shgexternalloansplit where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and status in(4,6,8,20)", conn);

            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqdetailsRole { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), loan_number = reader["loan_number"].ToString(), emi = reader["emi"].ToString(), approvedloanamount = reader["approvedloanamount"].ToString(), shgaccnum = reader["shgaccnum"].ToString(), rejby = reader["rejbyrole"].ToString(), rejreason = reader["rejreason"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------SHG rejection CDE----------(20-6-17)--------------//
    [WebMethod(Description = "This is for CDE Rejection")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cde_rejection(string shgid, string reason, string cdeid, string name)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("Update [vikasardo].[vikasa].[shg] set status='11', reason='" + reason + "', rejectedby='" + cdeid + "', rejectedbyname='" + name + "' where shgid='" + shgid + "'");
            status = "{\"message\":\"CDE rejected\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //------------------------SHG rejection CDO----------(20-6-17)--------------//
    [WebMethod(Description = "This is for CDO Rejection")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdo_rejection(string shgid, string reason, string cdoid, string name)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("Update [vikasardo].[vikasa].[shg] set status='22', reason='" + reason + "', rejectedby='" + cdoid + "', rejectedbyname='" + name + "' where shgid='" + shgid + "'");
            status = "{\"message\":\"CDO rejected\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //------------------------SHG rejection CDM----------(20-6-17)--------------//
    [WebMethod(Description = "This is for CDM Rejection")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdm_rejection(string shgid, string reason, string cdmid, string name)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("Update [vikasardo].[vikasa].[shg] set status='33', reason='" + reason + "', rejectedby='" + cdmid + "', rejectedbyname='" + name + "' where shgid='" + shgid + "'");
            status = "{\"message\":\"CDM rejected\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //------------------------get rejected SHG by CDE----------(20-6-17)--------------//
    [WebMethod(Description = "This is for get rejected SHG by CDE")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getrejectedshg_CDE(string cdeid)
    {
        string s = "";
        List<getrejectedSHGbyCDE> np = new List<getrejectedSHGbyCDE>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;


            cmd = new SqlCommand("SELECT s.shgid,s.name,s.type,s.village,s.taluka,s.region,s.district,s.savingAccount,s.meetingDate,s.annualmeetDate,s.annualIncome,s.totalMember,s.meetingPlace,s.meetingTime,s.formationDate,s.savingAccOpenDate,s.branch,s.status,s.reason,s.rejectedby,s.rejectedbyname from [vikasardo].[vikasa].[shg] s inner join [vikasardo].[vikasa].[cde] e on s.cdeid = e.cdeid where convert(int, e.cdeid)='" + cdeid + "' and status in (11,22,33,5)", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new getrejectedSHGbyCDE
                    {
                        shgid = reader["shgid"].ToString(),
                        name = reader["name"].ToString(),
                        type = reader["type"].ToString(),
                        village = reader["village"].ToString(),
                        taluka = reader["taluka"].ToString(),
                        region = reader["region"].ToString(),
                        district = reader["district"].ToString(),
                        savingAccount = reader["savingAccount"].ToString(),
                        meetingDate = reader["meetingDate"].ToString(),
                        annualmeetingDate = reader["annualmeetDate"].ToString(),
                        annualIncome = reader["annualIncome"].ToString(),
                        totalMember = reader["totalMember"].ToString(),
                        meetingPlace = reader["meetingPlace"].ToString(),
                        meetingTime = reader["meetingTime"].ToString(),
                        formationDate = reader["formationDate"].ToString(),
                        savingAccOpenDate = reader["savingAccOpenDate"].ToString(),
                        reason = reader["reason"].ToString(),
                        branch = reader["branch"].ToString(),
                        status = reader["status"].ToString(),
                        rejectedby = reader["rejectedby"].ToString(),
                        rejectedbyname = reader["rejectedbyname"].ToString()
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------get rejected SHG by CDO----------(20-6-17)--------------//
    [WebMethod(Description = "This is for get rejected SHG by CDO")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getrejectedshg_CDO(string cdoid)
    {
        string s = "";
        List<getrejectedSHGbyCDO> np = new List<getrejectedSHGbyCDO>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            //cmd = new SqlCommand("select top 1 id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num from shgexternalloansplit where cdeid='" + cdeid + "'", conn);
            cmd = new SqlCommand("SELECT s.shgid,s.name,s.type,s.village,s.taluka,s.region,s.district,s.savingAccount,s.meetingDate,s.annualmeetDate,s.annualIncome,s.totalMember,s.meetingPlace,s.meetingTime,s.formationDate,s.savingAccOpenDate,s.branch,s.status,s.reason,s.rejectedby,s.rejectedbyname from [vikasardo].[vikasa].[shg] s inner join [vikasardo].[vikasa].[cde] e on s.cdeid = e.cdeid where e.cdoid='" + cdoid + "' and status in (11,22,33,5)", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new getrejectedSHGbyCDO
                    {
                        shgid = reader["shgid"].ToString(),
                        name = reader["name"].ToString(),
                        type = reader["type"].ToString(),
                        village = reader["village"].ToString(),
                        taluka = reader["taluka"].ToString(),
                        region = reader["region"].ToString(),
                        district = reader["district"].ToString(),
                        savingAccount = reader["savingAccount"].ToString(),
                        meetingDate = reader["meetingDate"].ToString(),
                        annualmeetingDate = reader["annualmeetDate"].ToString(),
                        annualIncome = reader["annualIncome"].ToString(),
                        totalMember = reader["totalMember"].ToString(),
                        meetingPlace = reader["meetingPlace"].ToString(),
                        meetingTime = reader["meetingTime"].ToString(),
                        formationDate = reader["formationDate"].ToString(),
                        savingAccOpenDate = reader["savingAccOpenDate"].ToString(),
                        reason = reader["reason"].ToString(),
                        branch = reader["branch"].ToString(),
                        status = reader["status"].ToString(),
                        rejectedby = reader["rejectedby"].ToString(),
                        rejectedbyname = reader["rejectedbyname"].ToString()
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------get rejected SHG by CDM----------(20-6-17)--------------//
    [WebMethod(Description = "This is for get rejected SHG by CDM")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getrejectedshg_CDM(string cdmid)
    {
        string s = "";
        List<getrejectedSHGbyCDM> np = new List<getrejectedSHGbyCDM>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            //cmd = new SqlCommand("select top 1 id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num from shgexternalloansplit where cdeid='" + cdeid + "'", conn);
            cmd = new SqlCommand("SELECT s.shgid,s.name,s.type,s.village,s.taluka,s.region,s.district,s.savingAccount,s.meetingDate,s.annualmeetDate,s.annualIncome,s.totalMember,s.meetingPlace,s.meetingTime,s.formationDate,s.savingAccOpenDate,s.branch,s.status,s.reason,s.rejectedby,s.rejectedbyname from [vikasardo].[vikasa].[shg] s inner join [vikasardo].[vikasa].[cde] e on s.cdeid = e.cdeid where e.cdmid='" + cdmid + "' and status in (11,22,33,5)", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new getrejectedSHGbyCDM
                    {
                        shgid = reader["shgid"].ToString(),
                        name = reader["name"].ToString(),
                        type = reader["type"].ToString(),
                        village = reader["village"].ToString(),
                        taluka = reader["taluka"].ToString(),
                        region = reader["region"].ToString(),
                        district = reader["district"].ToString(),
                        savingAccount = reader["savingAccount"].ToString(),
                        meetingDate = reader["meetingDate"].ToString(),
                        annualmeetingDate = reader["annualmeetDate"].ToString(),
                        annualIncome = reader["annualIncome"].ToString(),
                        totalMember = reader["totalMember"].ToString(),
                        meetingPlace = reader["meetingPlace"].ToString(),
                        meetingTime = reader["meetingTime"].ToString(),
                        formationDate = reader["formationDate"].ToString(),
                        savingAccOpenDate = reader["savingAccOpenDate"].ToString(),
                        reason = reader["reason"].ToString(),
                        branch = reader["branch"].ToString(),
                        status = reader["status"].ToString(),
                        rejectedby = reader["rejectedby"].ToString(),
                        rejectedbyname = reader["rejectedbyname"].ToString()
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }
    //------------------------------------------------------------Get active SHG'S in CDO-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for active SHGs in CDO")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_active_shgCDO(string cdoid)
    {
        string s = "";
        List<getactiveSHGCDO> rg = new List<getactiveSHGCDO>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[shg] where status ='4' and cdeid in (select cdeid from [vikasardo].[vikasa].[cde] where cdoid='" + cdoid + "')", conn);
            conn.Open();
            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    string shgid = reader["shgid"].ToString();
                    rg.Add(new getactiveSHGCDO
                    {
                        shgid = shgid,
                        name = reader["name"].ToString(),
                        type = reader["type"].ToString(),
                        village = reader["village"].ToString(),
                        taluka = reader["taluka"].ToString(),
                        region = reader["region"].ToString(),
                        district = reader["district"].ToString(),
                        savingAccount = reader["savingAccount"].ToString(),
                        meetingDate = reader["meetingDate"].ToString(),
                        annualmeetDate = reader["annualmeetDate"].ToString(),
                        annualIncome = reader["annualIncome"].ToString(),
                        meetingPlace = reader["meetingPlace"].ToString(),
                        meetingTime = reader["meetingTime"].ToString(),
                        formationDate = reader["formationDate"].ToString(),
                        savingAccOpenDate = reader["savingAccOpenDate"].ToString(),
                        cdeID = reader["cdeid"].ToString(),
                        totalmembshg = reader["totalMember"].ToString(),
                        status = reader["status"].ToString(),
                        reason = reader["reason"].ToString(),
                        branch = reader["branch"].ToString(),
                        totalMember = Alib.idGetAFieldByQuery("select count(memberID) from [vikasardo].[vikasa].members where shgid='" + shgid + "' and cdeid in (select cdeid from [vikasardo].[vikasa].[cde] where cdoid='" + cdoid + "')")
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Get active SHG'S in CDM-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for active SHGs in CDM")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_active_shgCDM(string cdmid)
    {
        string s = "";
        List<getactiveSHGCDO> rg = new List<getactiveSHGCDO>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[shg] where status ='4' and cdeid in (select cdeid from [vikasardo].[vikasa].cde where cdmid='" + cdmid + "')", conn);
            conn.Open();
            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    string shgid = reader["shgid"].ToString();
                    rg.Add(new getactiveSHGCDO
                    {
                        shgid = shgid,
                        name = reader["name"].ToString(),
                        type = reader["type"].ToString(),
                        village = reader["village"].ToString(),
                        taluka = reader["taluka"].ToString(),
                        region = reader["region"].ToString(),
                        district = reader["district"].ToString(),
                        savingAccount = reader["savingAccount"].ToString(),
                        meetingDate = reader["meetingDate"].ToString(),
                        annualmeetDate = reader["annualmeetDate"].ToString(),
                        annualIncome = reader["annualIncome"].ToString(),
                        meetingPlace = reader["meetingPlace"].ToString(),
                        meetingTime = reader["meetingTime"].ToString(),
                        formationDate = reader["formationDate"].ToString(),
                        savingAccOpenDate = reader["savingAccOpenDate"].ToString(),
                        cdeID = reader["cdeid"].ToString(),
                        totalmembshg = reader["totalMember"].ToString(),
                        status = reader["status"].ToString(),
                        reason = reader["reason"].ToString(),
                        branch = reader["branch"].ToString(),
                        totalMember = Alib.idGetAFieldByQuery("select count(memberID) from [vikasardo].[vikasa].members where shgid='" + shgid + "' and cdeid in (select cdeid from [vikasardo].[vikasa].[cde] where cdmid='" + cdmid + "')")
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }
    //------------------------------------------------------------Get CDO'S-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get CDOs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_cdo(string cdmid)
    {
        string s = "";
        List<getCDO> rg = new List<getCDO>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;

            cmd = new SqlCommand("Select o.cdoid, o.employment, o.cdoname, o.cdophone, o.cdomail, o.cdmid, o.cdophoto, count(*) as CDEs from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[cde] e on cast(o.cdoid as varchar(50)) = cast(e.cdoid as varchar(50)) where o.cdmid='" + cdmid + "' group by o.cdoid, o.employment, o.cdoname, o.cdophone, o.cdomail, o.cdmid, o.cdophoto", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getCDO { cdoid = reader["cdoid"].ToString(), employment = reader["employment"].ToString(), cdoname = reader["cdoname"].ToString(), cdophone = reader["cdophone"].ToString(), cdomail = reader["cdomail"].ToString(), cdmid = reader["cdmid"].ToString(), cdophoto = "http://test.vikasardo.org/images/" + reader["cdophoto"].ToString(), CDE = reader["CDEs"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Add CDM Task-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for CDM Add Task")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void add_CDMtask(string cdmid, string name, string taskname, string date, string time, string branch, string shglist, string taskby, string groupName, string enterDetails, string villageName)
    {
        string[] fromdate = date.Split('/');

        //--------date----------------//
        int first = Convert.ToInt32(fromdate[0].Count());
        string first1 = fromdate[0].ToString();
        if (first == 1)
        {
            first1 = "0" + first1;
        }

        //-----------month---------------//
        int second = Convert.ToInt32(fromdate[1].Count());
        string second1 = fromdate[1].ToString();
        if (second == 1)
        {
            second1 = "0" + second1;
        }

        //------------yesr--------------//
        int third = Convert.ToInt32(fromdate[2].Count());
        string third1 = fromdate[2].ToString();
        if (third == 2)
        {
            third1 = "20" + third1;
        }
        date = first1 + "/" + second1 + "/" + third1;

        string status = "";
        string taskID = "";
        string action = "0";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (!Alib.idHasRows("SELECT * FROM [vikasardo].[vikasardo].[cdmtasks] WHERE cdmid='" + cdmid + "' and taskname='" + taskname + "' and date='" + date + "' and time='" + time + "' "))
            {
                taskID = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasardo].[cdmtasks] (cdmid, taskname, date, time, status, branch, shglist, taskby, name, groupName, enterDetails, villageName) Output Inserted.cdmtaskid values('" + cdmid + "','" + taskname + "','" + date + "', '" + time + "', '" + action + "', '" + branch + "', '" + shglist + "', '" + taskby + "', '" + name + "','" + groupName + "', '" + enterDetails + "', '" + villageName + "')");
                status = "{\"message\":\"success\", \"taskID\":\"" + taskID + "\"}";
            }
            else
            {
                // Alib.idExecute("UPDATE representative set name='" + name + "', memberID='" + memberID + "' WHERE shgID='" + shgID + "' and  memberActionID='" + memberActionID + "'");
                status = "{\"message\":\"Already Existing\"}";
            }

            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //------------------------------------------------------------Get CDM Task-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for CDM Get Task")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_CDMtask(string cdmid, string date)
    {
        string s = "";
        List<cdmgetTask> rg = new List<cdmgetTask>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            string[] fromdate = date.Split('/');
            //--------date----------------//
            int first = Convert.ToInt32(fromdate[0].Count());
            string first1 = fromdate[0].ToString();
            if (first == 1)
            {
                first1 = "0" + first1;
            }

            //-----------month---------------//
            int second = Convert.ToInt32(fromdate[1].Count());
            string second1 = fromdate[1].ToString();
            if (second == 1)
            {
                second1 = "0" + second1;
            }

            //------------yesr--------------//
            int third = Convert.ToInt32(fromdate[2].Count());
            string third1 = fromdate[2].ToString();
            if (third == 2)
            {
                third1 = "20" + third1;
            }
            date = first1 + "/" + second1 + "/" + third1;
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasardo].[cdmtasks] where cdmid='" + cdmid + "' and date='" + date + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new cdmgetTask { taskID = reader["cdmtaskid"].ToString(), name = reader["name"].ToString(), taskName = reader["taskName"].ToString(), date = reader["date"].ToString(), time = reader["time"].ToString(), status = reader["status"].ToString(), branch = reader["branch"].ToString(), shglist = reader["shglist"].ToString(), taskby = reader["taskby"].ToString(), groupName = reader["groupName"].ToString(), enterdetails = reader["enterdetails"].ToString(), villageName = reader["villageName"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }


    //------------------------------------------------------------Get CDM Actions-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for CDM Get Task Actions")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_CDMtask_action(string taskid, string action)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (!Alib.idHasRows("SELECT * FROM [vikasardo].[vikasardo].[cdmtasks] WHERE cdmtaskid='" + taskid + "' and status='" + action + "' "))
            {
                Alib.idExecute("UPDATE [vikasardo].[vikasardo].[cdmtasks] set status='" + action + "' WHERE cdmtaskid='" + taskid + "'");
                status = "{\"message\":\"success\"}";
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.Write(status);
            }
            else
            {
                status = "{\"message\":\"Already Exist\"}";
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.Write(status);
            }
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    public bool ValidateServerCertificate(
                                          object sender,
                                          X509Certificate certificate,
                                          X509Chain chain,
                                          SslPolicyErrors sslPolicyErrors)
    {
        return true;
    }

    private string CDESendGCMNotification(string apiKey, string postData, string postDataContentType = "application/json")
    {
        ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateServerCertificate);
        apiKey = "AIzaSyB2ssGd1Id4T2AlANre3bM-2lAk5Yy0CkA";

        //  MESSAGE CONTENT
        byte[] byteArray = Encoding.UTF8.GetBytes(postData);


        //  CREATE REQUEST
        HttpWebRequest Request = (HttpWebRequest)WebRequest.Create("https://fcm.googleapis.com/fcm/send");
        Request.Method = "POST";
        Request.KeepAlive = false;
        Request.ContentType = postDataContentType;
        Request.Headers.Add(string.Format("Authorization: key={0}", apiKey));
        Request.ContentLength = byteArray.Length;

        Stream dataStream = Request.GetRequestStream();
        dataStream.Write(byteArray, 0, byteArray.Length);
        dataStream.Close();


        //  SEND MESSAGE
        try
        {
            WebResponse Response = Request.GetResponse();
            HttpStatusCode ResponseCode = ((HttpWebResponse)Response).StatusCode;
            if (ResponseCode.Equals(HttpStatusCode.Unauthorized) || ResponseCode.Equals(HttpStatusCode.Forbidden))
            {
                var text = "Unauthorized - need new token";
            }
            else if (!ResponseCode.Equals(HttpStatusCode.OK))
            {
                var text = "Response from web service isn't OK";
            }

            StreamReader Reader = new StreamReader(Response.GetResponseStream());
            string responseLine = Reader.ReadToEnd();
            Reader.Close();

            return responseLine;
        }
        catch (Exception e)
        {
        }
        return "error";
    }
    private string CDOSendGCMNotification(string apiKey, string postData, string postDataContentType = "application/json")
    {
        ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateServerCertificate);
        apiKey = "AIzaSyCFcaERhLpp8xUaqAmIEPfW8v0HvqlK9YI";

        //  MESSAGE CONTENT
        byte[] byteArray = Encoding.UTF8.GetBytes(postData);


        //  CREATE REQUEST
        HttpWebRequest Request = (HttpWebRequest)WebRequest.Create("https://fcm.googleapis.com/fcm/send");
        Request.Method = "POST";
        Request.KeepAlive = false;
        Request.ContentType = postDataContentType;
        Request.Headers.Add(string.Format("Authorization: key={0}", apiKey));
        Request.ContentLength = byteArray.Length;

        Stream dataStream = Request.GetRequestStream();
        dataStream.Write(byteArray, 0, byteArray.Length);
        dataStream.Close();


        //  SEND MESSAGE
        try
        {
            WebResponse Response = Request.GetResponse();
            HttpStatusCode ResponseCode = ((HttpWebResponse)Response).StatusCode;
            if (ResponseCode.Equals(HttpStatusCode.Unauthorized) || ResponseCode.Equals(HttpStatusCode.Forbidden))
            {
                var text = "Unauthorized - need new token";
            }
            else if (!ResponseCode.Equals(HttpStatusCode.OK))
            {
                var text = "Response from web service isn't OK";
            }

            StreamReader Reader = new StreamReader(Response.GetResponseStream());
            string responseLine = Reader.ReadToEnd();
            Reader.Close();

            return responseLine;
        }
        catch (Exception e)
        {
        }
        return "error";
    }
    private string CDMSendGCMNotification(string apiKey, string postData, string postDataContentType = "application/json")
    {
        ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateServerCertificate);
        apiKey = "AIzaSyByrnlMNpCI6gaOlqr7KIj0C5El2vVttBE";

        //  MESSAGE CONTENT
        byte[] byteArray = Encoding.UTF8.GetBytes(postData);


        //  CREATE REQUEST
        HttpWebRequest Request = (HttpWebRequest)WebRequest.Create("https://fcm.googleapis.com/fcm/send");
        Request.Method = "POST";
        Request.KeepAlive = false;
        Request.ContentType = postDataContentType;
        Request.Headers.Add(string.Format("Authorization: key={0}", apiKey));
        Request.ContentLength = byteArray.Length;

        Stream dataStream = Request.GetRequestStream();
        dataStream.Write(byteArray, 0, byteArray.Length);
        dataStream.Close();


        //  SEND MESSAGE
        try
        {
            WebResponse Response = Request.GetResponse();
            HttpStatusCode ResponseCode = ((HttpWebResponse)Response).StatusCode;
            if (ResponseCode.Equals(HttpStatusCode.Unauthorized) || ResponseCode.Equals(HttpStatusCode.Forbidden))
            {
                var text = "Unauthorized - need new token";
            }
            else if (!ResponseCode.Equals(HttpStatusCode.OK))
            {
                var text = "Response from web service isn't OK";
            }

            StreamReader Reader = new StreamReader(Response.GetResponseStream());
            string responseLine = Reader.ReadToEnd();
            Reader.Close();

            return responseLine;
        }
        catch (Exception e)
        {
        }
        return "error";
    }
    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void search(int type)
    {
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        List<util> list = new List<util>();

        StringBuilder qry = new StringBuilder();
        if (type == 1)
        {
            qry.Append("SELECT *,'cde' as state FROM [vikasardo].[vikasa].[cde] where lat is not null");
            try
            {
                using (Conn = new SqlConnection(Alib.conStr))
                {
                    using (Cmd = new SqlCommand(qry.ToString(), Conn))
                    {
                        Conn.Open();
                        rdr = Cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                list.Add(new util { state = rdr["state"].ToString(), lat = rdr["lat"].ToString(), lon = rdr["lon"].ToString(), name = rdr["cdename"].ToString(), contact = rdr["cdephone"].ToString(), date = rdr["date"].ToString() });
                            }
                        }
                    }
                }
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string jsonString = serializer.Serialize(list);
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.Write(jsonString);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (Conn != null)
                {
                    Conn.Close();
                }
                if (rdr != null)
                {
                    rdr.Close();
                }
            }

        }
        if (type == 2)
        {
            qry.Append("SELECT *,'cdo' as state FROM [vikasardo].[vikasa].[cdo] where lat is not null");
            try
            {
                using (Conn = new SqlConnection(Alib.conStr))
                {
                    using (Cmd = new SqlCommand(qry.ToString(), Conn))
                    {
                        Conn.Open();
                        rdr = Cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                list.Add(new util { state = rdr["state"].ToString(), lat = rdr["lat"].ToString(), lon = rdr["lon"].ToString(), name = rdr["cdoname"].ToString(), contact = rdr["cdophone"].ToString(), date = rdr["date"].ToString() });
                            }
                        }
                    }
                }
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string jsonString = serializer.Serialize(list);
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.Write(jsonString);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (Conn != null)
                {
                    Conn.Close();
                }
                if (rdr != null)
                {
                    rdr.Close();
                }
            }
        }
        if (type == 3)
        {
            qry.Append("SELECT *,'cdm' as state FROM [vikasardo].[vikasa].[cdm] where lat is not null");
            try
            {
                using (Conn = new SqlConnection(Alib.conStr))
                {
                    using (Cmd = new SqlCommand(qry.ToString(), Conn))
                    {
                        Conn.Open();
                        rdr = Cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                list.Add(new util { state = rdr["state"].ToString(), lat = rdr["lat"].ToString(), lon = rdr["lon"].ToString(), name = rdr["cdmname"].ToString(), contact = rdr["cdmphone"].ToString(), date = rdr["date"].ToString() });
                            }
                        }
                    }
                }
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string jsonString = serializer.Serialize(list);
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.Write(jsonString);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (Conn != null)
                {
                    Conn.Close();
                }
                if (rdr != null)
                {
                    rdr.Close();
                }
            }
        }
        //if (type == 4)
        //    qry.Append("SELECT *, 'cdm' as state FROM [vikasardo].[vikasa].[cdm] union select *,'cdo' as state from [vikasardo].[vikasa].[cdo] union select *,'cde' as state from [vikasardo].[vikasa].[cde]");

    }
}
