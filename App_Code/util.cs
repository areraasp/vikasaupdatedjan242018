﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for util
/// </summary>
public class util
{
    public string lat { get; set; }
    public string state { get; set; }
    public string lon { get; set; }
    
    public string name { get; set; }
    public string contact { get; set; }
    public string date { get; set; }
}
public static class StringExtension
{
    public static string GetLast(this string source, int tail_length)
    {
        if (tail_length >= source.Length)
            return source;
        return source.Substring(source.Length - tail_length);
    }
}