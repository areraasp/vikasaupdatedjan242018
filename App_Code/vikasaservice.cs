using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for vikasaservice
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class vikasaservice : System.Web.Services.WebService
{
    SqlConnection Conn = null;
    SqlCommand Cmd;
    SqlDataReader rdr = null;
    private static SqlConnection con = null;
    private static SqlCommand cmd = null;
    private static SqlDataAdapter da = null;
    private static DataSet ds = null;

    public vikasaservice()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    //---------------------------------------------------------------Education--------------------------------------------------------------------------//
    [WebMethod(Description = "This is for Education")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_education()
    {
        string s = "";
        List<education> rg = new List<education>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[education]", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                rg.Add(new education { eduName = "Select Education" });
                while (reader.Read())
                {
                    rg.Add(new education { eduID = reader["id"].ToString(), eduName = reader["education"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //---------------------------------------------------------------Loan Purpose--------------------------------------------------------------------------//
    [WebMethod(Description = "This is for Loan Purpose")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_loanpurpose()
    {
        string s = "";
        List<loanpurpose> rg = new List<loanpurpose>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasardo].[loanPurpose]", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                rg.Add(new loanpurpose { purpose = "Select Loan Purpose" });
                while (reader.Read())
                {
                    rg.Add(new loanpurpose { ID = reader["id"].ToString(), purpose = reader["name"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Occupation-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Occupation")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_occupation()
    {
        string s = "";
        List<occupation> rg = new List<occupation>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[occupation]", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                rg.Add(new occupation { occName = "Select Occupation" });
                while (reader.Read())
                {
                    rg.Add(new occupation { occID = reader["id"].ToString(), occName = reader["occupation"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Govt Card-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Govt Card")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_govtCard()
    {
        string s = "";
        List<govtCard> rg = new List<govtCard>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[govtcard]", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                rg.Add(new govtCard { govtName = "Select Govt Card" });
                while (reader.Read())
                {
                    rg.Add(new govtCard { govtID = reader["id"].ToString(), govtName = reader["govtCard"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Regions-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Regions")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_regions()
    {
        string s = "";
        List<regions> rg = new List<regions>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[regions]", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                rg.Add(new regions { regionName = "Select Region" });
                while (reader.Read())
                {
                    rg.Add(new regions { regionID = reader["regionID"].ToString(), regionName = reader["name"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //---------------------------------------------------------------Districts-----------------------------------------------------------------//
    [WebMethod(Description = "This is for Districts")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_districts()
    {
        string s = "";
        List<districts> ds = new List<districts>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[districts]", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                ds.Add(new districts { districtName = "Select Districts" });
                while (reader.Read())
                {
                    ds.Add(new districts { districtID = reader["districtID"].ToString(), districtName = reader["name"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(ds);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //--------------------------------------------------------------CDE Sign Up----------------------------------------------------------------//
    [WebMethod(Description = "This is for CDE Sign Up ")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cde_signup(string phone)
    {
        string status = "";
        string msg = "OTP to sign up is:";
        string cdeid = "";
        string cdename = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (Alib.idHasRows("SELECT * FROM [vikasardo].[vikasa].[cde] WHERE cdephone='" + phone + "' "))
            {
                cdeid = Alib.getSingleValue("select cdeid from [vikasardo].[vikasa].[cde]  where cdephone='" + phone + "'");
                cdename = Alib.getSingleValue("select cdename from [vikasardo].[vikasa].[cde]  where cdephone='" + phone + "'");
                string cdeimg = "http://test.vikasardo.org/images/" + Alib.getSingleValue("select cdephoto from [vikasardo].[vikasa].[cde]  where cdephone='" + phone + "'");
                Random r = new Random();
                int tmp = r.Next(10000, 99999);
                WebClient Client = new WebClient();

                string bu = "http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + string.Concat(msg, tmp) + "&route=0&from=VIKASA&to=" + phone;
                Stream data = Client.OpenRead(bu);
                StreamReader reader = new StreamReader(data);
                string s = reader.ReadToEnd();
                data.Close();
                reader.Close();
                Alib.idExecute("Update [vikasardo].[vikasa].[cde] set otp='" + tmp.ToString() + "' where cdephone='" + phone + "' ");
                status = "{\"isSuccess\":\"true\",\"cdeid\":\"" + cdeid + "\",\"otp\":\"" + tmp + "\",\"phone\":\"" + phone + "\",\"name\":\"" + cdename + "\",\"image\":\"" + cdeimg + "\"}";
            }
            else
            {
                status = "{\"isSuccess\":\"false\",\"messageTitle\":\"invalid number\",\"messageDescription\":\"please enter valid number\"}";
            }
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"status\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
            //File.WriteAllText(@"ftp://vikasardo@13.58.11.166/logfile.txt", ex.Message);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //-------------------------------------------------------------SHG Registration----------------------------------------------------------//
    [WebMethod(Description = "This is for SHG Registration")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void shg_registration(string name, string type, string village, string taluka, string region, string district, string savingAcc, string meetingDate, string annualmeetingDate, string totalMem, string meetingPlace, string meetingTime, string formationDate, string savingAccOpenDate, string cdeid, string branch)
    {
        string shgID;
        string status1 = "";
        string status = "";
        string state = "0";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (Alib.idHasRows("SELECT savingAccount FROM [vikasardo].[vikasa].[shg] WHERE savingAccount='" + savingAcc + "'"))
            {
                status = "{\"message\":\"Saving Account Existing\"}";
            }
            else
            {
                if (!Alib.idHasRows("SELECT name FROM [vikasardo].[vikasa].[shg] WHERE name='" + name + "' and village='" + village + "' "))
                {
                    shgID = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].shg (name, type, village, taluka, region, district, savingAccount, meetingDate,annualmeetDate,annualIncome,expenses,rid, totalMember, meetingPlace, meetingTime, formationDate, savingAccOpenDate, cdeid, status, branch, loanstatus) Output Inserted.shgid values('" + name + "','" + type + "','" + village + "','" + taluka + "','" + region + "','" + district + "','" + savingAcc + "','" + meetingDate + "','" + annualmeetingDate + "','0','0','0','" + totalMem + "','" + meetingPlace + "','" + meetingTime + "','" + formationDate + "','" + savingAccOpenDate + "', '" + cdeid + "', '" + state + "','" + branch + "','0')");
                    status1 = "[{\"shgID\":\"" + shgID + "\",\"name\":\"" + name + "\"}]";
                    status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + status1 + "}";
                }
                else
                {
                    status = "{\"message\":\"Already Existing\"}";
                }
            }
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //-------------------------------------------------------------SHG Update----------------------------------------------------------//
    [WebMethod(Description = "This is for SHG Update")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void shg_update(string shgid, string name, string type, string village, string taluka, string region, string district, string savingAcc, string meetingDate, string annualmeetingDate, string totalMem, string meetingPlace, string meetingTime, string formationDate, string savingAccOpenDate, string cdeid, string branch)
    {

        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("update [vikasardo].[vikasa].[shg] set name='" + name + "', type='" + type + "',village='" + village + "',  taluka='" + taluka + "',region='" + region + "',  district='" + district + "',savingAccount='" + savingAcc + "', meetingDate='" + meetingDate + "', annualmeetDate='" + annualmeetingDate + "',totalMember='" + totalMem + "',meetingPlace='" + meetingPlace + "',meetingTime='" + meetingTime + "',formationDate='" + formationDate + "',savingAccOpenDate='" + savingAccOpenDate + "', cdeid='" + cdeid + "', branch='" + branch + "' where shgid='" + shgid + "'");
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //------------------------------------------------------------Get Active SHG'S-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Active SHGs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_active_shg(string cdeid)
    {
        string s = "";
        List<getActiveSHG> rg = new List<getActiveSHG>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select s.shgid as shgid, s.name as name, s.type as type, s.region as region, s.district as district, s.savingAccount as savingAccount, s.meetingDate as meetingDate, s.meetingPlace as meetingPlace, s.meetingTime as meetingTime, s.formationDate as formationDate, s.savingAccOpenDate as savingAccOpenDate, s.village, s.branch, s.status as status, count(*) as totmemb from [vikasardo].[vikasa].shg s inner join [vikasardo].[vikasa].members m on s.shgid=CONVERT(INT, CASE WHEN IsNumeric(CONVERT(VARCHAR(12), m.shgid)) = 1 then CONVERT(VARCHAR(12), m.shgid) else 0 End) where s.cdeid='" + cdeid + "' and s.status= 4 group by s.shgid, s.name, s.type, s.region, s.district, s.savingAccount, s.meetingDate, s.meetingPlace, s.meetingTime, s.formationDate, s.savingAccOpenDate, s.status, s.village, s.branch", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getActiveSHG
                    {
                        shgid = reader["shgid"].ToString(),
                        name = reader["name"].ToString(),
                        type = reader["type"].ToString(),
                        region = reader["region"].ToString(),
                        district = reader["district"].ToString(),
                        savingAccount = reader["savingAccount"].ToString(),
                        meetingDate = reader["meetingDate"].ToString(),
                        totalMember = reader["totmemb"].ToString(),
                        meetingPlace = reader["meetingPlace"].ToString(),
                        meetingTime = reader["meetingTime"].ToString(),
                        formationDate = reader["formationDate"].ToString(),
                        savingAccOpenDate = reader["savingAccOpenDate"].ToString(),
                        status = reader["status"].ToString(),
                        village = reader["village"].ToString(),
                        branch = reader["branch"].ToString()
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Get Inactive SHG'S-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Inactive SHGs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_inactive_shg(string cdeid)
    {
        string s = "";
        List<getInactiveSHG> rg = new List<getInactiveSHG>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[shg] where cdeid='" + cdeid + "' and status in (0,1)", conn);
            conn.Open();
            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    string shgid = reader["shgid"].ToString();
                    rg.Add(new getInactiveSHG
                    {
                        shgid = shgid,
                        name = reader["name"].ToString(),
                        type = reader["type"].ToString(),
                        village = reader["village"].ToString(),
                        taluka = reader["taluka"].ToString(),
                        region = reader["region"].ToString(),
                        district = reader["district"].ToString(),
                        savingAccount = reader["savingAccount"].ToString(),
                        meetingDate = reader["meetingDate"].ToString(),
                        annualmeetDate = reader["annualmeetDate"].ToString(),
                        annualIncome = reader["annualIncome"].ToString(),
                        expenses = reader["expenses"].ToString(),
                        rid = reader["rid"].ToString(),
                        meetingPlace = reader["meetingPlace"].ToString(),
                        meetingTime = reader["meetingTime"].ToString(),
                        formationDate = reader["formationDate"].ToString(),
                        savingAccOpenDate = reader["savingAccOpenDate"].ToString(),
                        cdeid = reader["cdeid"].ToString(),
                        totalmembshg = reader["totalMember"].ToString(),
                        status = reader["status"].ToString(),
                        reason = reader["reason"].ToString(),
                        branch = reader["branch"].ToString(),
                        totalMember = Alib.idGetAFieldByQuery("select count(memberID) from [vikasardo].[vikasa].members where shgid='" + shgid + "' and cdeid='" + cdeid + "'")
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Get Rejected SHG'S-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Rejected SHGs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_rejected_shg(string cdeid)
    {
        string s = "";
        List<getRejectedSHG> rg = new List<getRejectedSHG>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select s.shgid as shgid, s.name as name, s.type as type, s.region as region, s.district as district, s.savingAccount as savingAccount, s.meetingDate as meetingDate, s.meetingPlace as meetingPlace, s.meetingTime as meetingTime, s.formationDate as formationDate, s.savingAccOpenDate as savingAccOpenDate, s.reason as reason, s.status as status, count(*) as totmemb from [vikasardo].[vikasa].shg s inner join [vikasardo].[vikasa].members m on m.shgid=s.shgid where s.cdeid='" + cdeid + "' and s.status=5 group by s.shgid, s.name, s.type, s.region, s.district, s.savingAccount, s.meetingDate, s.meetingPlace, s.meetingTime, s.formationDate, s.savingAccOpenDate, s.reason, s.status", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getRejectedSHG { shgid = reader["shgid"].ToString(), name = reader["name"].ToString(), type = reader["type"].ToString(), region = reader["region"].ToString(), district = reader["district"].ToString(), savingAccount = reader["savingAccount"].ToString(), meetingDate = reader["meetingDate"].ToString(), totalMember = reader["totmemb"].ToString(), meetingPlace = reader["meetingPlace"].ToString(), meetingTime = reader["meetingTime"].ToString(), formationDate = reader["formationDate"].ToString(), savingAccOpenDate = reader["savingAccOpenDate"].ToString(), status = reader["status"].ToString(), reason = reader["reason"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------SHG Approval from CDE-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Approval SHG from CDE")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void shg_approval_fromCDE(string shgid)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            Alib.idExecute("UPDATE [vikasardo].[vikasa].[shg] set status='1' WHERE shgid='" + shgid + "'");
            status = "{\"message\":\"Approval sent to CDO\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);

            //-----------------FCM Notification Starts---------------------//

            //string cdoid = Alib.idGetAFieldByQuery("Select o.fcmid from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[cde] e on e.cdoid=o.cdoid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + shgid + "'");
            //string BrowserAPIKey = "AIzaSyA8CJ6sEkCmQoHxiHAIKn7D6z1WpRSOxfg";

            //string subject = "SHG_CDO";
            //string message = "CDE has created and approved new SHG please check it and approve";
            //if (cdoid != "")
            //{
            //  string postData = "{ \"registration_ids\": [\"" + cdoid + "\"], \"data\": {\"subject\":\"" + subject + "\",\"message\":\"" + message + "\"}}";
            // CDOSendGCMNotification(BrowserAPIKey, postData);
            //}

            //-----------------FCM Notification Ends---------------------//

            //-----------------Message Send Starts-----------------------//

            string cdonum = Alib.idGetAFieldByQuery("Select o.cdophone from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[cde] e on e.cdoid=o.cdoid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + shgid + "'");
            string shgname = Alib.idGetAFieldByQuery("Select name from [vikasardo].[vikasa].[shg] where shgid ='" + shgid + "'");
            string cdename = Alib.idGetAFieldByQuery("Select e.cdename from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shg] s on e.cdeid= s.cdeid where shgid ='" + shgid + "'");
            string wish = "Dear Sir/Madam,\n I am Requesting you to approve the SHG below mentioned,\n " + shgname + ",\n Regards,\n " + cdename;
            SqlConnection con = null;
            SqlDataReader rdr = null;
            try
            {
                WebClient Client = new WebClient();
                Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + cdonum);
                Stream data = Client.OpenRead(uri);
                StreamReader reader = new StreamReader(data);
                string s = reader.ReadToEnd();
                data.Close();
                reader.Close();
            }
            catch (SqlException ex)
            {
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                    con.Dispose();
                }
                if (rdr != null)
                    rdr.Dispose();
            }

            //-----------------Message Send Ends-------------------------//
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }


    //------------------------------------------------------------Get SHG Details-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for get SHG details")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_shg_details(string shgid)
    {
        string s = "";
        List<getSHGDetails> rg = new List<getSHGDetails>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[shg] where shgid='" + shgid + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getSHGDetails { name = reader["name"].ToString(), region = reader["region"].ToString(), district = reader["district"].ToString(), formationDate = reader["formationDate"].ToString(), accNum = reader["savingAccount"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Get Members List-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for get Members")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_members(string shgid)
    {
        string s = "";
        string memid = "";
        List<getMembers> rg = new List<getMembers>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[members] where shgid='" + shgid + "' order by memberID", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    memid = reader["memberID"].ToString();
                    rg.Add(new getMembers
                    {
                        memberID = reader["memberID"].ToString(),
                        name = reader["name"].ToString(),
                        fatherORhusband = reader["fatherORhusband"].ToString(),
                        dob = reader["dob"].ToString(),
                        gender = reader["gender"].ToString(),
                        adhaarNum = reader["adhaarNum"].ToString(),
                        bankname = reader["bankname"].ToString(),
                        savingAccNum = reader["savingAccNum"].ToString(),
                        nominee = reader["nominee"].ToString(),
                        maritalStatus = reader["maritalStatus"].ToString(),
                        education = reader["education"].ToString(),
                        caste = reader["caste"].ToString(),
                        occupation = reader["occupation"].ToString(),
                        typeOfHome = reader["typeOfHome"].ToString(),
                        propertyType = reader["propertyType"].ToString(),
                        govtCard = reader["govtCard"].ToString(),
                        mobile = reader["mobile"].ToString(),
                        pan = reader["pan"].ToString(),
                        membershipFee = reader["membershipFee"].ToString(),
                        sizeofFamily = reader["sizeofFamily"].ToString(),
                        eldermale = reader["eldermale"].ToString(),
                        elderfemale = reader["elderfemale"].ToString(),
                        childmale = reader["childmale"].ToString(),
                        childfemale = reader["childfemale"].ToString(),
                        photo = "http://test.vikasardo.org/images/" + reader["image"].ToString(),
                        signature = "http://test.vikasardo.org/images/" + reader["signature"].ToString(),
                        cdeid = reader["cdeid"].ToString(),
                        shgid = reader["shgid"].ToString(),
                        status = reader["status"].ToString(),
                        annualincome = reader["annualincome"].ToString(),
                        address = reader["address"].ToString(),
                        savingDetails = Alib.idGetAFieldByQuery("SELECT top 1 totalsavings from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and memberid='" + memid + "' order by id desc"),
                        internalLending = Alib.idGetAFieldByQuery("SELECT top 1 lendpending from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and memberid='" + memid + "' order by id desc"),
                        externalLending = Alib.idGetAFieldByQuery("SELECT top 1 bankloanpending from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and memberid='" + memid + "' order by id desc"),
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Delete Member-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Delete Member")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void delete_member(string shgid, string memberID, string cdeid)
    {
        string status = "";
        string bankloan = "";
        string internallending = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            bankloan = Alib.idGetAFieldByQuery("select top 1 bankloanpending from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and memberid='" + memberID + "' order by id desc");
            internallending = Alib.idGetAFieldByQuery("select top 1 lendpending from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and memberid='" + memberID + "' order by id desc");

            if (bankloan == "0" && internallending == "0")
            {
                Alib.idExecute("delete from [vikasardo].[vikasa].members WHERE shgid='" + shgid + "' and memberID='" + memberID + "'");
                Alib.idExecute("delete from [vikasardo].[vikasa].meeting WHERE shgid='" + shgid + "' and memberID='" + memberID + "' and id in ( select top 1 id from [vikasardo].[vikasa].meeting WHERE shgid='" + shgid + "' and cdeid='" + cdeid + "' and  memberID='" + memberID + "' order by id asc)");

                status = "{\"message\":\"Member has been deleted\"}";
            }
            else
            {
                status = "{\"message\":\"Member cannot be deleted\"}";
            }
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //------------------------------------------------------------Add Representative-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Add Representative")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void add_representative(string name, string memberID, string memberActionID, string shgID)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            int count = Convert.ToInt32(Alib.idGetAFieldByQuery("select count(repid) from [vikasardo].[vikasa].[representative] where shgID='" + shgID + "'"));

            if (Alib.idHasRows("SELECT * FROM [vikasardo].[vikasa].[representative] WHERE memberID='" + memberID + "'"))
            {
                status = "{\"message\":\"already exists\"}";
            }
            else if (!Alib.idHasRows("SELECT * FROM [vikasardo].[vikasa].[representative] WHERE shgID='" + shgID + "' and name='" + name + "' and memberID='" + memberID + "' and memberActionID='" + memberActionID + "'") && count < 2)
            {
                memberID = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].[representative] (name, memberID, memberActionID, shgID) Output Inserted.repID values('" + name + "','" + memberID + "','" + memberActionID + "', '" + shgID + "')");
                status = "{\"message\":\"success\"}";
            }
            else if (!Alib.idHasRows("SELECT * FROM [vikasardo].[vikasa].[representative] WHERE shgID='" + shgID + "' and name='" + name + "' and memberID='" + memberID + "' and memberActionID='" + memberActionID + "' ") && count <= 2)
            {
                Alib.idExecute("UPDATE [vikasardo].[vikasa].[representative] set name='" + name + "', memberID='" + memberID + "' WHERE shgID='" + shgID + "' and  memberActionID='" + memberActionID + "'");
                status = "{\"message\":\"success\"}";
            }
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //------------------------------------------------------------Get Representative-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get Representative")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_representative(string shgid)
    {
        string s = "";
        List<getRepresentative> rg = new List<getRepresentative>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].representative where shgID='" + shgid + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getRepresentative { memberID = reader["memberID"].ToString(), name = reader["name"].ToString(), memberActionID = reader["memberActionID"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Add Task-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Add Task")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void add_task(string cdeid, string name, string taskname, string date, string time, string branch, string shglist, string taskby, string groupName, string enterDetails, string villageName)
    {
        string[] fromdate = date.Split('/');

        //--------date----------------//
        int first = Convert.ToInt32(fromdate[0].Count());
        string first1 = fromdate[0].ToString();
        if (first == 1)
        {
            first1 = "0" + first1;
        }

        //-----------month---------------//
        int second = Convert.ToInt32(fromdate[1].Count());
        string second1 = fromdate[1].ToString();
        if (second == 1)
        {
            second1 = "0" + second1;
        }

        //------------yesr--------------//
        int third = Convert.ToInt32(fromdate[2].Count());
        string third1 = fromdate[2].ToString();
        if (third == 2)
        {
            third1 = "20" + third1;
        }
        date = first1 + "/" + second1 + "/" + third1;


        string status = "";
        string taskID = "";
        string action = "0";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (!Alib.idHasRows("SELECT * FROM [vikasardo].[vikasa].[tasks] WHERE cdeid='" + cdeid + "' and taskname='" + taskname + "' and date='" + date + "' and time='" + time + "' "))
            {
                taskID = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].[tasks](cdeid, taskname, date, time, status, branch, shglist, taskby, name, groupName, enterDetails, villageName) Output Inserted.taskid values('" + cdeid + "','" + taskname + "','" + date + "', '" + time + "', '" + action + "', '" + branch + "', '" + shglist + "', '" + taskby + "', '" + name + "' , '" + groupName + "', '" + enterDetails + "', '" + villageName + "')");
                status = "{\"message\":\"success\", \"taskID\":\"" + taskID + "\"}";
            }
            else
            {
                // Alib.idExecute("UPDATE representative set name='" + name + "', memberID='" + memberID + "' WHERE shgID='" + shgID + "' and  memberActionID='" + memberActionID + "'");
                status = "{\"message\":\"Already Existing\"}";
            }

            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //------------------------------------------------------------Get Task-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get Task")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_task(string cdeid, string date)
    {
        string s = "";
        List<getTask> rg = new List<getTask>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            string[] fromdate = date.Split('/');
            //--------date----------------//
            int first = Convert.ToInt32(fromdate[0].Count());
            string first1 = fromdate[0].ToString();
            if (first == 1)
            {
                first1 = "0" + first1;
            }

            //-----------month---------------//
            int second = Convert.ToInt32(fromdate[1].Count());
            string second1 = fromdate[1].ToString();
            if (second == 1)
            {
                second1 = "0" + second1;
            }

            //------------yesr--------------//
            int third = Convert.ToInt32(fromdate[2].Count());
            string third1 = fromdate[2].ToString();
            if (third == 2)
            {
                third1 = "20" + third1;
            }
            date = first1 + "/" + second1 + "/" + third1;
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[tasks] where cdeid='" + cdeid + "' and date='" + date + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getTask { taskID = reader["taskid"].ToString(), name = reader["name"].ToString(), taskName = reader["taskName"].ToString(), date = reader["date"].ToString(), time = reader["time"].ToString(), status = reader["status"].ToString(), branch = reader["branch"].ToString(), shglist = reader["shglist"].ToString(), taskby = reader["taskby"].ToString(), groupName = reader["groupName"].ToString(), enterDetails = reader["enterDetails"].ToString(), villageName = reader["villageName"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Get Actions-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get Task Actions")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_task_action(string taskid, string action)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (!Alib.idHasRows("SELECT * FROM [vikasardo].[vikasa].tasks WHERE taskid='" + taskid + "' and status='" + action + "' "))
            {
                Alib.idExecute("UPDATE [vikasardo].[vikasa].tasks set status='" + action + "' WHERE taskid='" + taskid + "'");
                status = "{\"message\":\"success\"}";
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.Write(status);
            }
            else
            {
                status = "{\"message\":\"Already Exist\"}";
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.Write(status);
            }
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }


    //-----------------------------------------------------------------Create MOM-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for create mom")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void create_mom(string shgid, string cdeid, string date, string meetingPlace, string noOfPerson, string momImage)
    {
        string status = "";
        string momid = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (!Alib.idHasRows("SELECT * FROM [vikasardo].[vikasa].mom WHERE shgid='" + shgid + "' and cdeid='" + cdeid + "' and date='" + date + "' and meetingPlace='" + meetingPlace + "' "))
            {
                byte[] imageBytes = Convert.FromBase64String(momImage);
                MemoryStream ms = new MemoryStream(imageBytes);
                string imgname = RandomNo.Random.GenerateIdentifier(28) + ".png";
                FileStream fs = new FileStream(Server.MapPath("~/images/" + imgname), FileMode.Create);
                ms.WriteTo(fs);
                ms.Close();
                fs.Close();
                fs.Dispose();

                momid = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].mom (shgid, cdeid, date, meetingPlace, noOfPerson, momImage) Output Inserted.momid values('" + shgid + "','" + cdeid + "','" + date + "', '" + meetingPlace + "', '" + noOfPerson + "', '" + imgname + "')");
                status = "{\"message\":\"success\", \"momID\":\"" + momid + "\"}";
            }
            else
            {
                // Alib.idExecute("UPDATE representative set name='" + name + "', memberID='" + memberID + "' WHERE shgID='" + shgID + "' and  memberActionID='" + memberActionID + "'");
                status = "{\"message\":\"Already Existing\"}";
            }

            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //----------------------------------------------------------------Get MOM List-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get MOM list")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_mom_list(string shgid, string cdeid)
    {
        string s = "";
        List<MomList> rg = new List<MomList>();
        MomList ml = new MomList();
        Savings sav = new Savings();
        Expenses exp = new Expenses();

        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            //cmd = new SqlCommand("select distinct date, place, persons, shginbank,shginhand,shgsavings,shgtotsavings,shgtotlendrepay,shgtotlendinterest,shgtotintlending,shgtotbankemi,shgtotpenalty,shgtotbankinterest,shgtotgrants,shgotherssavings,shgtotcollection,shgincomerefundfromrid,shgexpcash,shgexpcheque,shgexpdepositetosavingsacc,shgexpdepositetobankacc,shgexpcashinhand,shgexpamountinbanksavings,shgexpmembersavingsrepayment,shgexpothers,shgexptotal,shgexprid,membershipfee,shgexp from [vikasardo].[vikasa].[meeting] where cdeid='" + cdeid + "' and shgid='" + shgid + "' and date<>'0'", conn);

            cmd = new SqlCommand("select distinct convert(date, convert(varchar(30), date), 103) as Date, place,persons,shginbank,shginhand,shgsavings,shgtotsavings,shgtotlendrepay,shgtotlendinterest,shgtotintlending,shgtotbankemi,shgtotpenalty,shgtotbankinterest,shgtotgrants,shgotherssavings,shgtotcollection,shgincomerefundfromrid,shgexpcash,shgexpcheque,shgexpdepositetosavingsacc,shgexpdepositetobankacc,shgexpcashinhand,shgexpamountinbanksavings,shgexpmembersavingsrepayment,shgexpothers,shgexptotal,shgexprid,membershipfee,shgexp from [vikasardo].[vikasa].[meeting] where cdeid='" + cdeid + "' and shgid='" + shgid + "' and date<>'0' order by Date asc", conn);

            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    sav = new Savings { shginbank = reader["shginbank"].ToString(), shginhand = reader["shginhand"].ToString(), shgsavings = reader["shgsavings"].ToString(), shgtotsavings = reader["shgtotsavings"].ToString(), shgtotlendrepay = reader["shgtotlendrepay"].ToString(), shgtotlendinterest = reader["shgtotlendinterest"].ToString(), shgtotintlending = reader["shgtotintlending"].ToString(), shgtotbankemi = reader["shgtotbankemi"].ToString(), shgtotpenalty = reader["shgtotpenalty"].ToString(), shgtotbankinterest = reader["shgtotbankinterest"].ToString(), shgtotgrants = reader["shgtotgrants"].ToString(), shgotherssavings = reader["shgotherssavings"].ToString(), shgtotcollection = reader["shgtotcollection"].ToString(), shgincomerefundfromrid = reader["shgincomerefundfromrid"].ToString(), membershipFee = reader["membershipfee"].ToString() };
                    exp = new Expenses { shgexpcash = reader["shgexpcash"].ToString(), shgexpcheque = reader["shgexpcheque"].ToString(), shgexpdepositetosavingsacc = reader["shgexpdepositetosavingsacc"].ToString(), shgexpdepositetobankacc = reader["shgexpdepositetobankacc"].ToString(), shgexpcashinhand = reader["shgexpcashinhand"].ToString(), shgexpamountinbanksavings = reader["shgexpamountinbanksavings"].ToString(), shgexpmembersavingsrepayment = reader["shgexpmembersavingsrepayment"].ToString(), shgexpothers = reader["shgexpothers"].ToString(), shgexptotal = reader["shgexptotal"].ToString(), shgexprid = reader["shgexprid"].ToString(), shgexp = reader["shgexp"].ToString() };

                    //string[] dateData = reader["date"].ToString().Replace(" 12:00:00 AM", "").Split('-');
                    //string finalDate = dateData[0] + "-" + dateData[1] + "-" + dateData[2];

                    rg.Add(new MomList { date = reader["date"].ToString(), place = reader["place"].ToString(), persons = reader["persons"].ToString(), saving = sav, expense = exp });
                   // rg.Add(new MomList { date = finalDate, place = reader["place"].ToString(), persons = reader["persons"].ToString(), saving = sav, expense = exp });
                }
            }
            //
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }

        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Get All SHGs List-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All SHGs list")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_shgs_list(string cdeid)
    {
        string s = "";
        List<getSHgsList> rg = new List<getSHgsList>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[shg] where cdeid='" + cdeid + "' and status in('4','5')", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                rg.Add(new getSHgsList { name = "Select SHG", id = "shg Not Selected" });
                while (reader.Read())
                {
                    rg.Add(new getSHgsList { id = reader["shgid"].ToString(), name = reader["name"].ToString(), savingAcc = reader["savingAccount"].ToString(), branchName = reader["branch"].ToString(), village = reader["village"].ToString() });
                }
            }


            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);

            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" + jsonString2 + "}";

            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";

            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }


    //------------------------------------------------------------Get All SHGs List-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All SHGs list")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_shgs_list1(string cdeid)
    {
        string s = "";
        List<getSHgsList> rg = new List<getSHgsList>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            //  SqlCommand cmd = null;
            // SqlDataReader reader = null;
            //  cmd = new SqlCommand("select * from [vikasardo].[vikasa].[shg] where cdeid='" + cdeid + "' and loanstatus='0'", conn);
            //  conn.Open();
            //  reader = cmd.ExecuteReader();
            //  if (reader.HasRows)
            // {
            //    rg.Add(new getSHgsList { name = "Select SHG" });
            //   while (reader.Read())
            //  {
            //    rg.Add(new getSHgsList { id = reader["shgid"].ToString(), name = reader["name"].ToString(), savingAcc = reader["savingAccount"].ToString(), branchName = reader["branch"].ToString() });
            //  }
            //  }

            //---------------------------------
            SqlCommand command;
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet ds = new DataSet();
            int i = 0;
            string firstSql = null;
            string secondSql = null;

            firstSql = "select * from [vikasardo].[vikasa].[shg] where cdeid = '" + cdeid + "'  and loanstatus='0'";
            secondSql = "select oldshgid, Name_of_the_SHG, S_B_Acc_No, Branch_Name,Name_of_the_village, cdeid  from [vikasardo].[vikasardo].[oldshg] where cdeid='" + cdeid + "'";

            try
            {
                conn.Open();
                command = new SqlCommand(firstSql, conn);
                adapter.SelectCommand = command;
                adapter.Fill(ds, "First Table");

                adapter.SelectCommand.CommandText = secondSql;
                adapter.Fill(ds, "Second Table");

                adapter.Dispose();
                command.Dispose();
                conn.Close();

                //ds.Tables[0].Merge(ds.Tables[1]);

                //retrieve first table data 
                rg.Add(new getSHgsList { name = "Select SHG" });
                for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    rg.Add(new getSHgsList { id = ds.Tables[0].Rows[i].ItemArray[0].ToString(), name = ds.Tables[0].Rows[i].ItemArray[1].ToString(), savingAcc = ds.Tables[0].Rows[i].ItemArray[7].ToString(), branchName = ds.Tables[0].Rows[i].ItemArray[21].ToString(), village = ds.Tables[0].Rows[i].ItemArray[3].ToString() });
                }
                //retrieve second table data 
                for (i = 0; i <= ds.Tables[1].Rows.Count - 1; i++)
                {
                    rg.Add(new getSHgsList { id = ds.Tables[1].Rows[i].ItemArray[0].ToString(), name = ds.Tables[1].Rows[i].ItemArray[1].ToString(), savingAcc = ds.Tables[1].Rows[i].ItemArray[2].ToString(), branchName = ds.Tables[1].Rows[i].ItemArray[3].ToString(), village = ds.Tables[1].Rows[i].ItemArray[4].ToString() });
                }
            }
            catch (Exception ex)
            {
            }

            //---------------------------------


            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);

            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" + jsonString2 + "}";

            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";

            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }


    //------------------------------------------------------------Get All SHGs BankBranch List-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All SHGs BankBranch list")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_shgs_listBankBranch()
    {
        string s = "";
        List<getBankBranch> rg = new List<getBankBranch>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {

            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[bankBranch]", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getBankBranch { id = reader["bankbranchID"].ToString(), name = reader["name"].ToString() });
                }
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);

            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" + jsonString2 + "}";

            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";

            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Get All SHGs LoanType List-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All SHGs LoanType list")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_shgs_listLoanType()
    {
        string s = "";
        List<getLoanType> rg = new List<getLoanType>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[loanType]", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getLoanType { id = reader["loanTypeID"].ToString(), name = reader["name"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);

            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" + jsonString2 + "}";

            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";

            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------------Get shg id-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for get shg id")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_shg_id(string shgid)
    {
        string s = "";
        List<getActiveSHG> rg = new List<getActiveSHG>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[shg] where shgid='" + shgid + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getActiveSHG { shgid = reader["shgid"].ToString(), name = reader["name"].ToString(), type = reader["type"].ToString(), region = reader["region"].ToString(), district = reader["district"].ToString(), savingAccount = reader["savingAccount"].ToString(), meetingDate = reader["meetingDate"].ToString(), totalMember = reader["totalMember"].ToString(), meetingPlace = reader["meetingPlace"].ToString(), meetingTime = reader["meetingTime"].ToString(), formationDate = reader["formationDate"].ToString(), savingAccOpenDate = reader["savingAccOpenDate"].ToString(), status = reader["status"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Get Members Under SHGs-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for get Members")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_members_ONshgid(string shgid)
    {
        string s = "";
        List<getMembers> rg = new List<getMembers>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[members] where shgid='" + shgid + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getMembers { memberID = reader["memberID"].ToString(), name = reader["name"].ToString(), adhaarNum = reader["adhaarNum"].ToString(), mobile = reader["mobile"].ToString(), photo = "http://test.vikasardo.org/images/" + reader["image"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //---------------------------------------------------------------Income and Expenses---------------------------------------------------------------//

    //---------------------------------------------------------------------Add Members-----------------------------------------------------------//
    [WebMethod(Description = "This is for add Members")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void add_members(string name, string fatherORhusband, string dob, string gender, string adhaarNum, string address, string bankname, string savingAccNum, string nominee, string maritalStatus, string education, string caste, string occupation, string typeOfHome, string propertyType, string govtCard, string mobile, string pan, string sizeofFamily, string eldermale, string elderfemale, string childmale, string childfemale, string image, string signature, string cdeid, string shgid, string annualincome)
    {
        string memberID;
        //string savingsid = "";
        //string penalty = "";
        //string memberinternallending = "";
        //string internallendingid = "";
        //string extloanid = "";
        //string shgexternalloansplit = "";
        //string shgextloan = "";
        //string shgexternalloanrepayincome = "";
        //string memberinternallendingrepincome = "";
        string status = "";
        string status1 = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (!Alib.idHasRows("SELECT adhaarNum FROM [vikasardo].[vikasa].[members] WHERE adhaarNum='" + adhaarNum + "' "))
            {
                string imgname = "";
                string imgname1 = "";
                if (image != "")
                {
                    byte[] imageBytes = Convert.FromBase64String(image);
                    MemoryStream ms = new MemoryStream(imageBytes);
                    imgname = RandomNo.Random.GenerateIdentifier(28) + ".png";
                    FileStream fs = new FileStream(Server.MapPath("~/images/" + imgname), FileMode.Create);
                    ms.WriteTo(fs);
                    ms.Close();
                    fs.Close();
                    fs.Dispose();
                }

                if (signature != "")
                {
                    byte[] imageBytes1 = Convert.FromBase64String(signature);
                    MemoryStream ms1 = new MemoryStream(imageBytes1);
                    imgname1 = RandomNo.Random.GenerateIdentifier(28) + ".png";
                    FileStream fs1 = new FileStream(Server.MapPath("~/images/" + imgname1), FileMode.Create);
                    ms1.WriteTo(fs1);
                    ms1.Close();
                    fs1.Close();
                    fs1.Dispose();
                }
                //-----insert member table----------//
                memberID = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].[members] (name, fatherORhusband, dob, gender, adhaarNum ,bankname,savingAccNum ,nominee ,maritalStatus ,education ,caste ,occupation ,typeOfHome ,propertyType ,govtCard ,mobile, pan, sizeofFamily, eldermale, elderfemale, childmale, childfemale, image, signature, cdeid, shgid, status,annualincome, address) Output Inserted.memberID values('" + name + "','" + fatherORhusband + "','" + dob + "', '" + gender + "','" + adhaarNum + "','" + bankname + "','" + savingAccNum + "','" + nominee + "','" + maritalStatus + "','" + education + "','" + caste + "','" + occupation + "','" + typeOfHome + "','" + propertyType + "','" + govtCard + "','" + mobile + "','" + pan + "','" + sizeofFamily + "','" + eldermale + "','" + elderfemale + "','" + childmale + "','" + childfemale + "','" + imgname + "','" + imgname1 + "','" + cdeid + "','" + shgid + "', '0','" + annualincome + "', '" + address + "')");

                string check = Alib.idGetAFieldByQuery("select top 1 meetings from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc");
                if (check == "")
                {
                    Alib.idExecute("Insert into [vikasardo].[vikasa].[meeting] (memberid, name, date, place, persons, shgid, cdeid, savings, totalsavings, lendbal, lendemi, interest, lending, lendpending,bankloan,bankemi,bankloanpending,penalty,newmeetinglending,cash,totalpenalty,totalcollections,meetings,shginbank,shginhand,shgsavings,shgtotsavings,shgtotlendrepay,shgtotlendinterest,shgtotintlending,shgtotbankemi,shgtotpenalty,shgtotbankinterest,shgtotgrants,shgotherssavings,shgtotcollection, shgexpcash,shgexpcheque,shgexpdepositetosavingsacc,shgexpdepositetobankacc,shgexpcashinhand,shgexpamountinbanksavings,shgexpmembersavingsrepayment,shgexpothers,shgexptotal)VALUES('" + memberID + "','" + name + "','" + System.DateTime.Now.AddHours(5).AddMonths(30).ToString("dd/MM/yyyy") + "','0','0','" + shgid + "','" + cdeid + "','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')");
                }
                else
                {
                    int mid = 1 + Convert.ToInt32(Alib.idGetAFieldByQuery("select top 1 meetings from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc"));
                    int checkmid = mid - 1;
                    Alib.idExecute("Insert into [vikasardo].[vikasa].[meeting] (memberid, name, date, place, persons, shgid, cdeid, savings, totalsavings, lendbal, lendemi, interest, lending,lendpending,bankloan,bankemi,bankloanpending,penalty,newmeetinglending,cash,totalpenalty,totalcollections,meetings,shginbank,shginhand,shgsavings,shgtotsavings,shgtotlendrepay,shgtotlendinterest,shgtotintlending,shgtotbankemi,shgtotpenalty,shgtotbankinterest,shgtotgrants,shgotherssavings,shgtotcollection, shgexpcash,shgexpcheque,shgexpdepositetosavingsacc,shgexpdepositetobankacc,shgexpcashinhand,shgexpamountinbanksavings,shgexpmembersavingsrepayment,shgexpothers,shgexptotal)VALUES('" + memberID + "','" + name + "','" + System.DateTime.Now.AddHours(5).AddMonths(30).ToString("dd/MM/yyyy") + "','0','0','" + shgid + "','" + cdeid + "','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','" + checkmid + "','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')");
                }
                //Alib.idExecute("Insert into meeting (memberid,name,date,place,persons,shgid,cdeid,savings,totalsavings,lendbal,lendemi,interest,lending,lendpending,bankloan,bankemi,bankloanpending,penalty,newmeetinglending,cash,totalpenalty,totalcollections,meetings,shginbank,shginhand,shgsavings,shgtotsavings,shgtotlendrepay,shgtotlendinterest,shgtotintlending,shgtotbankemi,shgtotpenalty,shgtotbankinterest,shgtotgrants,shgotherssavings,shgtotcollection, shgexpcash,shgexpcheque,shgexpdepositetosavingsacc,shgexpdepositetobankacc,shgexpcashinhand,shgexpamountinbanksavings,shgexpmembersavingsrepayment,shgexpothers,shgexptotal)VALUES('" + memberID + "','" + name + "','" + System.DateTime.Now.ToString("dd/MM/yyyy") + "','0','0','" + shgid + "','" + cdeid + "','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','" + checkmid + "','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')");
                status1 = "{\"memberID\":\"" + memberID + "\",\"name\":\"" + name + "\"}";
                status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + status1 + "}";
            }
            else
            {
                status = "{\"message\":\"Already Existing\"}";
            }
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //----------------------------------------------------------------Get Members IE-----------------------------------------------------------------//

    [WebMethod(Description = "This is for Get members for IE")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_members_IE(string shgid, string cdeid, string momdate)
    {
        string s = "";
        List<getMembersIE> rg = new List<getMembersIE>();
        // List<Internallending> ld = new List<Internallending>();
        List<NullgetMembersIE> rg1 = new List<NullgetMembersIE>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            using (SqlConnection con = new SqlConnection(Alib.conStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("select  s.name as name, s.memberid as id, s.totamount as Saving_Amount, p.totamount as Penalty_Amount,el.amountbal as ExtSplit_Amount,s.incomesavingsid from [vikasardo].[vikasa].membersavings s " +
               "inner join [vikasardo].[vikasa].memberpenaltyincome p on p.incomesavingsid = s.incomesavingsid inner join [vikasardo].[vikasa].shgexternalloanrepayincome el on el.incomesavingsid=s.incomesavingsid  where s.incomesavingsid in (Select max(incomesavingsid) from [vikasardo].[vikasa].membersavings where momdate='" + momdate + "' and shgid=" + shgid + " and cdeid=" + cdeid + " group by memberid)", con))
                {
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        List<Internallending> lendlist = null;
                        List<getMembersIE> d = new List<getMembersIE>();
                        while (rdr.Read())
                        {
                            lendlist = new List<Internallending>();

                            using (SqlConnection con2 = new SqlConnection(Alib.conStr))
                            {
                                con2.Open();
                                using (SqlCommand cmd1 = new SqlCommand("Select internallendingid as Ref_ID, principalamountbal as IntLending_Amount,interestamountbal as Interest,count from [vikasardo].[vikasa].memberinternallendingrepincome where  incomesavingsid=" + rdr["incomesavingsid"].ToString(), con2))
                                {
                                    SqlDataReader rdr1 = cmd1.ExecuteReader();
                                    if (rdr1.HasRows)
                                    {
                                        while (rdr1.Read())
                                        {
                                            lendlist.Add(new Internallending() { internallendingid = rdr1["Ref_ID"].ToString(), principalamountbal = rdr1["IntLending_Amount"].ToString(), interestamountbal = rdr1["Interest"].ToString(), count = rdr1["count"].ToString() });
                                        }
                                    }
                                }
                            }

                            d.Add(new getMembersIE()
                            {
                                name = rdr["name"].ToString(),
                                memberID = rdr["id"].ToString(),
                                savings = rdr["Saving_Amount"].ToString(),
                                loanRepBalance = rdr["ExtSplit_Amount"].ToString(),
                                penalty = rdr["Penalty_Amount"].ToString(),
                                lending = lendlist
                            });
                        }

                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        string jsonString = serializer.Serialize(d);
                        HttpContext.Current.Response.ClearContent();
                        string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
                        HttpContext.Current.Response.Write(status);
                    }
                    else
                    {
                        using (SqlConnection con1 = new SqlConnection(Alib.conStr))
                        {
                            con1.Open();
                            using (SqlCommand cmd1 = new SqlCommand("select * from [vikasardo].[vikasa].members where shgid='" + shgid + "' and cdeid='" + cdeid + "'", con1))
                            {
                                SqlDataReader rdr1 = cmd1.ExecuteReader();
                                if (rdr1.HasRows)
                                {
                                    List<getMembersIE> d = new List<getMembersIE>();
                                    while (rdr1.Read())
                                    {
                                        d.Add(new getMembersIE()
                                        {
                                            name = rdr1["name"].ToString(),
                                            memberID = rdr1["memberid"].ToString(),
                                            savings = "0",
                                            loanRepBalance = "0",
                                            penalty = "0",
                                            lending = null
                                        });
                                    }
                                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                                    string jsonString = serializer.Serialize(d);
                                    HttpContext.Current.Response.ClearContent();
                                    string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
                                    HttpContext.Current.Response.Write(status);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //----------------------------------------------------------------Get Last Payment Members IE-----------------------------------------------------------------//

    [WebMethod(Description = "This is for Get last payment members for IE")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_lastpayment_IE(string shgid, string cdeid, string momdate, string memberid)
    {
        string s = "";
        List<getMembersIElastpayments> rg = new List<getMembersIElastpayments>();
        // List<Internallending> ld = new List<Internallending>();
        List<NullgetMembersIE> rg1 = new List<NullgetMembersIE>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            using (SqlConnection con = new SqlConnection(Alib.conStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("select  s.name as name, s.memberid as id, s.totamount as Saving_Amount, s.lastpay as Last_Savings, p.totamount as Penalty_Amount, p.lastpay as Last_Penalty, el.amountbal as ExtSplit_Amount, el.lastpay as Last_Loan, s.incomesavingsid from [vikasardo].[vikasa].membersavings s " +
                    "inner join [vikasardo].[vikasa].memberpenaltyincome p on p.incomesavingsid = s.incomesavingsid inner join [vikasardo].[vikasa].shgexternalloanrepayincome el on el.incomesavingsid=s.incomesavingsid where s.incomesavingsid in (Select max(incomesavingsid) from [vikasardo].[vikasa].membersavings where momdate='" + momdate + "' and shgid=" + shgid + " and cdeid=" + cdeid + " and memberid='" + memberid + "' group by memberid)", con))
                {
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        List<Internallendinglastpayments> lendlist = null;
                        List<getMembersIElastpayments> d = new List<getMembersIElastpayments>();
                        while (rdr.Read())
                        {
                            lendlist = new List<Internallendinglastpayments>();

                            using (SqlConnection con2 = new SqlConnection(Alib.conStr))
                            {
                                con2.Open();
                                using (SqlCommand cmd1 = new SqlCommand("Select internallendingid as Ref_ID, principalamountbal as IntLending_Amount,interestamountbal as Interest, lastprinciple, lastinterest, count from [vikasardo].[vikasa].memberinternallendingrepincome where incomesavingsid=" + rdr["incomesavingsid"].ToString(), con2))
                                {
                                    SqlDataReader rdr1 = cmd1.ExecuteReader();
                                    if (rdr1.HasRows)
                                    {
                                        while (rdr1.Read())
                                        {
                                            lendlist.Add(new Internallendinglastpayments() { internallendingid = rdr1["Ref_ID"].ToString(), principalamountbal = rdr1["IntLending_Amount"].ToString(), interestamountbal = rdr1["Interest"].ToString(), lastprincipalamountbal = rdr1["lastprinciple"].ToString(), lastinterestamountbal = rdr1["lastinterest"].ToString(), count = rdr1["count"].ToString() });
                                        }
                                    }
                                }
                            }

                            d.Add(new getMembersIElastpayments()
                            {
                                name = rdr["name"].ToString(),
                                memberID = rdr["id"].ToString(),
                                savings = rdr["Saving_Amount"].ToString(),
                                lastsavings = rdr["Last_Savings"].ToString(),
                                loanRepBalance = rdr["ExtSplit_Amount"].ToString(),
                                lastloanRepBalance = rdr["Last_Loan"].ToString(),
                                penalty = rdr["Penalty_Amount"].ToString(),
                                lastpenalty = rdr["Last_Penalty"].ToString(),
                                lending = lendlist
                            });
                        }

                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        string jsonString = serializer.Serialize(d);
                        HttpContext.Current.Response.ClearContent();
                        string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
                        HttpContext.Current.Response.Write(status);
                    }
                    else
                    {
                        using (SqlConnection con1 = new SqlConnection(Alib.conStr))
                        {
                            con1.Open();
                            using (SqlCommand cmd1 = new SqlCommand("select * from [vikasardo].[vikasa].members where shgid='" + shgid + "' and cdeid='" + cdeid + "'", con1))
                            {
                                SqlDataReader rdr1 = cmd1.ExecuteReader();
                                if (rdr1.HasRows)
                                {
                                    List<getMembersIElastpayments> d = new List<getMembersIElastpayments>();
                                    while (rdr1.Read())
                                    {
                                        d.Add(new getMembersIElastpayments()
                                        {
                                            name = rdr1["name"].ToString(),
                                            memberID = rdr1["memberid"].ToString(),
                                            savings = "0",
                                            lastsavings = "0",
                                            loanRepBalance = "0",
                                            lastloanRepBalance = "0",
                                            penalty = "0",
                                            lastpenalty = "0",
                                            lending = null
                                        });
                                    }
                                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                                    string jsonString = serializer.Serialize(d);
                                    HttpContext.Current.Response.ClearContent();
                                    string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
                                    HttpContext.Current.Response.Write(status);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-----------------------------------------------------Update IE----------------------------------------------------------------//
    [WebMethod(Description = "This is for edit members for IE")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void edit_members_IE(string shgid, string cdeid, string memberid, string name, string mom, string date, string savings, string totsavings, string rfid1, string rfid2, string internallend1, string internallend2, string internallendbal1, string internallendbal2, string interest1, string interest2, string interestbal1, string interestbal2, string penalty, string totpenalty, string extamt, string extamtbal)
    {
        string status = "";
        string rfid11 = "";
        string rfid12 = "";
        string dateid = "";
        string extloanid = "";
        string membersavingsid = "";
        string penaltyid = "";
        string internallendingid = "";
        string loanid = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            membersavingsid = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].membersavings (memberid, name, shgid, cdeid, momdate, date, amount, totamount, lastpay, mode) Output Inserted.incomegrantid values('" + memberid + "','" + name + "','" + shgid + "','" + cdeid + "','" + mom + "','" + date + "','" + savings + "','" + totsavings + "','" + savings + "','cash')");

            penaltyid = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].memberpenaltyincome (memberid,name,shgid,cdeid,momdate,date,amount,totamount,lastpay,mode,incomesavingsid) values('" + memberid + "','" + name + "','" + shgid + "','" + cdeid + "','" + mom + "','" + date + "','" + penalty + "','" + totpenalty + "','cash')");

            internallendingid = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].memberinternallendingrepincome (internallendingid,memberid,name,shgid,cdeid,date,momdate,principalamount,principalamountbal,interestamount,interestamountbal1,interestamountbal2,mode,count) values('" + rfid11 + "','" + rfid12 + "','" + memberid + "','" + name + "','" + shgid + "','" + cdeid + "','" + date + "','" + mom + "','" + internallend1 + "','" + internallend2 + "','" + internallendbal1 + "','" + internallendbal2 + "','" + interest1 + "','" + interest2 + "','" + interestbal1 + "','" + interestbal2 + "','cash','0')");

            loanid = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].shgexternalloanrepayincome (extloanid,memberid,name,momdate,shgid,cdeid,date,amount,amountbal,mode) values('" + extloanid + "','" + memberid + "','" + name + "','" + mom + "','" + shgid + "','" + cdeid + "','" + date + "','" + extamt + "','" + extamtbal + "','cash')");


            Alib.idExecute("Insert into [vikasardo].[vikasa].membersavings (memberid, name, shgid, cdeid, momdate, date, amount, totamount, lastpay, mode) values('" + memberid + "','" + name + "','" + shgid + "','" + cdeid + "','" + mom + "','" + date + "','" + savings + "','" + totsavings + "','cash')");
            Alib.idExecute("Update [vikasardo].[vikasa].membersavings set momdate='" + mom + "', date='" + date + "', mode='cash' where memberid='" + memberid + "' and shgid='" + shgid + "' and cdeid='" + cdeid + "'");

            Alib.idExecute("Insert into [vikasardo].[vikasa].memberpenaltyincome (memberid,name,shgid,cdeid,momdate,date,amount,totamount,mode) values('" + memberid + "','" + name + "','" + shgid + "','" + cdeid + "','" + mom + "','" + date + "','" + penalty + "','" + totpenalty + "','cash')");
            Alib.idExecute("Update [vikasardo].[vikasa].memberpenaltyincome set momdate='" + mom + "', date='" + date + "', mode='cash' where memberid='" + memberid + "' and shgid='" + shgid + "' and cdeid='" + cdeid + "'");

            Alib.idExecute("Insert into [vikasardo].[vikasa].memberinternallendingrepincome (internallendingid1,internallendingid2,memberid,name,shgid,cdeid,date,momdate,principalamount1,principalamount2,principalamountbal1,principalamountbal2,interestamount1,interestamount2,interestamountbal1,interestamountbal2,mode,count) values('" + rfid11 + "','" + rfid12 + "','" + memberid + "','" + name + "','" + shgid + "','" + cdeid + "','" + date + "','" + mom + "','" + internallend1 + "','" + internallend2 + "','" + internallendbal1 + "','" + internallendbal2 + "','" + interest1 + "','" + interest2 + "','" + interestbal1 + "','" + interestbal2 + "','cash','0')");
            Alib.idExecute("Update [vikasardo].[vikasa].memberinternallendingrepincome set momdate='" + mom + "', date='" + date + "', mode='cash' where memberid='" + memberid + "' and shgid='" + shgid + "' and cdeid='" + cdeid + "'");

            Alib.idExecute("Insert into [vikasardo].[vikasa].shgexternalloanrepayincome (extloanid,memberid,name,momdate,shgid,cdeid,date,amount,amountbal,mode) values('" + extloanid + "','" + memberid + "','" + name + "','" + mom + "','" + shgid + "','" + cdeid + "','" + date + "','" + extamt + "','" + extamtbal + "','cash')");
            Alib.idExecute("Update [vikasardo].[vikasa].shgexternalloanrepayincome set momdate='" + mom + "', date='" + date + "', mode='cash' where memberid='" + memberid + "' and shgid='" + shgid + "' and cdeid='" + cdeid + "'");

            status = "{\"message\":\"success\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "[{\"status\":\"fail\"}]";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Expense Add SHG Grant-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for SHG Grant")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void add_grant(string shgid, string cdeid, string grantdate, string momdate, string date, string amount, string sanctionedby, string mode)
    {
        string status = "";
        string grantID = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (!Alib.idHasRows("SELECT * FROM [vikasardo].[vikasa].shggrant WHERE shgid='" + shgid + "' and cdeid='" + cdeid + "' and grantdate='" + grantdate + "' and momdate='" + momdate + "' and date='" + date + "' and sanctionedby='" + sanctionedby + "' and amount='" + amount + "' "))
            {
                grantID = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].shggrant (shgid, cdeid, grantdate, momdate, date, amount, sanctionedby, mode) Output Inserted.incomegrantid values('" + shgid + "','" + cdeid + "', '" + grantdate + "','" + momdate + "', '" + date + "', '" + amount + "', '" + sanctionedby + "', '" + mode + "')");
                status = "{\"message\":\"success\", \"grantID\":\"" + grantID + "\"}";
            }
            else
            {
                status = "{\"message\":\"Already Existing\"}";
            }

            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //------------------------------------------------------------Expense Add shgbankinterest-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for SHG Bank Interest")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void add_bank_interest(string shgid, string cdeid, string bankdate, string momdate, string date, string amount, string particular, string mode)
    {
        string status = "";
        string bankinterestid = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (!Alib.idHasRows("SELECT * FROM [vikasardo].[vikasa].shgbankinterest WHERE shgid='" + shgid + "' and cdeid='" + cdeid + "' and bankdate='" + bankdate + "' and momdate='" + momdate + "' and date='" + date + "' and amount='" + amount + "' and particular='" + particular + "'"))
            {
                bankinterestid = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].shgbankinterest (shgid, cdeid, bankdate, momdate, date, amount, particular, mode) Output Inserted.bankinterestid values('" + shgid + "','" + cdeid + "', '" + bankdate + "', '" + momdate + "', '" + date + "', '" + amount + "', '" + particular + "', '" + mode + "')");
                status = "{\"message\":\"success\", \"bankinterestID\":\"" + bankinterestid + "\"}";
            }
            else
            {
                status = "{\"message\":\"Already Existing\"}";
            }

            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //------------------------------------------------------------Expense Add Other Exp-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Add Other Exp")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void add_otherExp(string shgid, string cdeid, string otherdate, string momdate, string date, string amount, string reason, string mode)
    {
        string status = "";
        string otherExp = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (!Alib.idHasRows("SELECT * FROM [vikasardo].[vikasa].shgotherexp WHERE shgid='" + shgid + "' and cdeid='" + cdeid + "' and otherdate='" + otherdate + "'and momdate='" + momdate + "' and date='" + date + "' and amount='" + amount + "' and reason='" + reason + "'"))
            {
                otherExp = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].shgotherexp (shgid, cdeid, otherdate, momdate, date, amount, reason, mode) Output Inserted.otherexpid values('" + shgid + "','" + cdeid + "', '" + otherdate + "', '" + momdate + "', '" + date + "', '" + amount + "', '" + reason + "', '" + mode + "')");
                status = "{\"message\":\"success\", \"otherExpID\":\"" + otherExp + "\"}";
            }
            else
            {
                status = "{\"message\":\"Already Existing\"}";
            }

            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //------------------------------------------------------------Income Add Other-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Add Other Income")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void add_otherInc(string shgid, string cdeid, string otherdate, string momdate, string date, string amount, string givenby, string mode)
    {
        string status = "";
        string otherInc = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            if (!Alib.idHasRows("SELECT * FROM [vikasardo].[vikasa].shgotherincome WHERE shgid='" + shgid + "' and cdeid='" + cdeid + "' and otherdate='" + otherdate + "' and momdate='" + momdate + "' and date='" + date + "' and amount='" + amount + "' and givenby='" + givenby + "'"))
            {
                otherInc = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].shgotherincome (shgid, cdeid, otherdate, momdate, date, amount, givenby, mode) Output Inserted.incomeotherid values('" + shgid + "','" + cdeid + "','" + otherdate + "', '" + momdate + "', '" + date + "', '" + amount + "', '" + givenby + "', '" + mode + "')");
                status = "{\"message\":\"success\", \"otherIncID\":\"" + otherInc + "\"}";
            }
            else
            {
                status = "{\"message\":\"Already Existing\"}";
            }

            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////PRAVEEN'S WORK STARTS FROM HERE/////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------Add Internal Lending------------------------------------------------------------------//
    [WebMethod(Description = "This is to Add Internal Lending")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void addinternal_Lending(string shgid, string memberID, string Amount, string EMI, string Reason, string Momdate, string Currentdate, string cdeid, string name, string intrest, string amountbal, string intrestbal, string mode)
    {
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        string rep = "";
        string intrlenid = "";
        string masterid = "";
        string internallendingid = "";
        string membersavingsid = "";
        string checkcount = "";
        string dateid = "";
        int count = 1 + Convert.ToInt32(Alib.idGetAFieldByQuery("select count(id) from [vikasardo].[vikasa].memberInternallendingexp where shgid='" + shgid + "' and memberid='" + memberID + "' and cdeid='" + cdeid + "' "));
        if (count < 3)
        {
            try
            {
                Random r = new Random();
                int tmp = r.Next(1000, 9999);
                dateid = System.DateTime.Now.Day.ToString() + "" + System.DateTime.Now.Month.ToString();
                intrlenid = "INT" + dateid + "" + tmp;

                masterid = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].memberInternallendingexp (internallendingid, memberid, name, shgid, cdeid, date, momdate, amount, interest, emi, reason, amountbal, interestbal, mode, count) Output Inserted.id values('" + intrlenid + "','" + memberID + "','" + name + "', '" + shgid + "', '" + cdeid + "','" + Currentdate + "', '" + Momdate + "', '" + Amount + "', '" + intrest + "','" + EMI + "','" + Reason + "', '" + amountbal + "', '" + intrestbal + "', '" + mode + "','" + count.ToString() + "')");

                string id = Alib.getSingleValue("select top 1 id from [vikasardo].[vikasa].meeting where memberid='" + memberID + "' and shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc");
                Alib.idExecute("update [vikasardo].[vikasa].meeting set lending='" + Amount + "',lendpending='" + Amount + "',lendbal='" + Amount + "' where id=(select top 1 id from [vikasardo].[vikasa].meeting where memberid='" + memberID + "' and shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc)");

                //-----------------------//

                SqlConnection conn = new SqlConnection(Alib.conStr);
                SqlCommand cmd = null;
                SqlDataReader reader = null;
                cmd = new SqlCommand("select top 1 * from [vikasardo].[vikasa].memberinternallendingrepincome where shgid='" + shgid + "' and memberid='" + memberID + "' and cdeid='" + cdeid + "' order by id desc", conn);
                conn.Open();
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        internallendingid = reader["internallendingid"].ToString();
                        checkcount = reader["count"].ToString();
                        membersavingsid = reader["incomesavingsid"].ToString();

                        if (internallendingid == "0" && checkcount == "0")
                        {
                            Alib.idExecute("Update [vikasardo].[vikasa].memberinternallendingrepincome set masterid='" + masterid + "', internallendingid='" + intrlenid + "', date='" + Currentdate + "', momdate='" + Momdate + "', principalamount='" + Amount + "', principalamountbal='" + amountbal + "',interestamount='" + intrest + "',interestamountbal='" + intrestbal + "', count='1' where masterid='" + memberID + "' and shgid='" + shgid + "' and cdeid='" + cdeid + "'");
                        }
                        else if (checkcount == "1")
                        {
                            Alib.idExecute("Insert into [vikasardo].[vikasa].memberinternallendingrepincome (masterid, internallendingid, memberid, name, shgid, cdeid, date, momdate, principalamount, principalamountbal, lastprinciple, interestamount, interestamountbal, lastinterest, mode, count, incomesavingsid) values('" + masterid + "','" + intrlenid + "', '" + memberID + "','" + name + "','" + shgid + "','" + cdeid + "','" + Currentdate + "','" + Momdate + "','" + Amount + "','" + amountbal + "','0','" + intrest + "','" + intrestbal + "','0','cash','2','" + membersavingsid + "'");
                        }

                    }
                }
                rep = "{\"status\":\"Success\",\"count\":" + count + "}";
            }
            catch
            {
                rep = "{\"status\":\"fail\"}";
            }
        }
        else
        {
            rep = "{\"status\":\"reached\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(rep);
    }


    //-----------------------------------------------------Add New Proposal----------------------------------------------------------------//
    [WebMethod(Description = "This is to Add New Proposal")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void addnew_proposal(string shgid, string shgname, string loanamount, string loantype, string reason, string bankbranch, string split_type, string cdeid, string membername, string memberid, string aadharnum, string amount, string count, string status, string shgaccnum, string proposalDate)
    {
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        string cdoid = Alib.idGetAFieldByQuery("select cdoid from [vikasardo].[vikasa].[cde] where cdeid='" + cdeid + "'");
        string cdmid = Alib.idGetAFieldByQuery("select cdmid from [vikasardo].[vikasa].[cde] where cdeid='" + cdeid + "'");

        List<string> membidlist = new List<string>();
        List<string> namelist = new List<string>();
        List<string> aadharlist = new List<string>();
        List<string> amountlist = new List<string>();

        string[] Amountarr = amount.Trim('"').Replace('[', ' ').Replace(']', ' ').Trim().TrimStart().Split(',');
        string[] Namearr = membername.Trim('"').Replace('[', ' ').Replace(']', ' ').Trim().TrimStart().Split(',');
        string[] Aadhararr = aadharnum.Trim('"').Replace('[', ' ').Replace(']', ' ').Trim().TrimStart().Split(',');
        string[] IDSarr = memberid.Trim('"').Replace('[', ' ').Replace(']', ' ').Trim().TrimStart().Split(',');

        string rep = "";
        string loan_enq_num = "";
        string dateid = "";
        Random r = new Random();
        int tmp = r.Next(1000, 9999);
        dateid = System.DateTime.Now.Day.ToString() + "" + System.DateTime.Now.Month.ToString();
        loan_enq_num = "LOANENQID" + dateid + "" + tmp;

        if (IDSarr != null)
        {
            foreach (string s in Amountarr)
                amountlist.Add(s);
            foreach (string s in Namearr)
                namelist.Add(s);
            foreach (string s in Aadhararr)
                aadharlist.Add(s);
            foreach (string s in IDSarr)
                membidlist.Add(s);
        }
        try
        {
            for (int j = 0; j < IDSarr.Length; j++)
            {
                Alib.idInsertInto("[vikasardo].[vikasa].shgexternalloansplit", "referenceID", "0", "memberid", IDSarr[j], "name", Namearr[j], "momdate", "momdate", "shgid", shgid, "cdeid", cdeid, "amount", Amountarr[j], "emi", "emi", "emidate", "emidate", "balance", "balance", "bankbrach", bankbranch, "split_type", split_type, "shgloanamount", loanamount, "aadharnum", Aadhararr[j], "shgname", shgname, "reason", reason, "status", status, "approvedstate", "0", "loan_number", "0", "approvedloanamount", "0", "loan_enq_num", loan_enq_num, "loantype", loantype, "shgaccnum", shgaccnum, "cdoid", cdoid, "cdmid", cdmid, "proposaldate", proposalDate);
            }

            rep = "{\"status\":\"Success\"}";
        }
        catch
        {
            rep = "{\"status\":\"fail\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(rep);
    }

    //-----------------------------------------------------Get Proposals----------------------------------------------------------------//

    [WebMethod(Description = "This is to Get Proposals")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_enquiryproposals(string cdeid)
    {
        string s = "";
        List<NewProposalEnqs> np = new List<NewProposalEnqs>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            //cmd = new SqlCommand("select top 1 id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num from shgexternalloansplit where cdeid='" + cdeid + "'", conn);
            cmd = new SqlCommand("SELECT id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,shgaccnum,proposaldate from [vikasardo].[vikasa].shgexternalloansplit where status in (0,1,3,5) and id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and cdeid='" + cdeid + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqs { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), shgaccnum = reader["shgaccnum"].ToString(), proposaldate = reader["proposaldate"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-------------------------------------------------------------CDE Approval For New Enqs----------------------------------------------------------//
    [WebMethod(Description = "This is for CDE Approval For New Enqs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdeapproval_fornewenqs(string cdeid, string loanenqnum)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("update [vikasardo].[vikasa].[shgexternalloansplit] set status='1',approvedstate='1' where cdeid='" + cdeid + "' and loan_enq_num='" + loanenqnum + "'");
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";

            //-----------------FCM Notification Starts---------------------//
            //---CDO----//
            //string cdoid = Alib.idGetAFieldByQuery("Select o.fcmid from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[cde] e on e.cdoid = o.cdoid where cdeid ='" + cdeid + "'");
            //string BrowserAPIKey = "AIzaSyA8CJ6sEkCmQoHxiHAIKn7D6z1WpRSOxfg";

            //string subject = "ENQ_CDO";
            //string message = "CDE has approved new Enquiry please check it and approve";
            //if (cdoid != "")
            //{
            //    string postData = "{ \"registration_ids\": [\"" + cdoid + "\"], \"data\": {\"subject\":\"" + subject + "\",\"message\":\"" + message + "\"}}";
            // CDOSendGCMNotification(BrowserAPIKey, postData);
            //  }
            //-----------------FCM Notification Ends---------------------//

            //-----------------Message Send Starts-----------------------//

            string cdonum = Alib.idGetAFieldByQuery("Select o.cdophone from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[cde] e on e.cdoid=o.cdoid where cdeid ='" + cdeid + "'");
            string shgname = Alib.idGetAFieldByQuery("Select s.name from [vikasardo].[vikasa].[shg] s inner join [vikasardo].[vikasa].[shgexternalloansplit] sp on sp.shgid = s.shgid where sp.cdeid ='" + cdeid + "' and sp.loan_enq_num='" + loanenqnum + "'");
            string cdename = Alib.idGetAFieldByQuery("Select cdename from [vikasardo].[vikasa].[cde] where cdeid ='" + cdeid + "'");
            string wish = "Dear Sir/Madam,\n I am Requesting you to approve the Proposal of " + shgname + ",\n Regards,\n " + cdename;
            SqlConnection con = null;
            SqlDataReader rdr = null;
            try
            {
                WebClient Client = new WebClient();
                Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + cdonum);
                Stream data = Client.OpenRead(uri);
                StreamReader reader1 = new StreamReader(data);
                string s = reader1.ReadToEnd();
                data.Close();
                reader1.Close();
            }
            catch (SqlException ex)
            {
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                    con.Dispose();
                }
                if (rdr != null)
                    rdr.Dispose();
            }
            //-----------------Message Send Ends-------------------------//
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //-------------------------------------------------------------CDE Reject For New Enqs----------------------------------------------------------//
    [WebMethod(Description = "This is for CDE Reject For New Enqs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdereject_fornewenqs(string cdeid, string loanenqnum)
    {

        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("update [vikasardo].[vikasa].[shgexternalloansplit] set status='2',approvedstate='0' where cdeid='" + cdeid + "' and loan_enq_num='" + loanenqnum + "'");
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //-----------------------------------------------------Get Proposal Members List----------------------------------------------------------------//

    [WebMethod(Description = "This is to Get Proposal Members List")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_proposalmemblist(string cdeid, string loan_enqid)
    {
        string s = "";
        List<NewProposalEnqMembs> np = new List<NewProposalEnqMembs>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            //cmd = new SqlCommand("select top 1 id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num from shgexternalloansplit where cdeid='" + cdeid + "'", conn);
            cmd = new SqlCommand("SELECT memberid,name,aadharnum,amount,shgaccnum, proposaldate from [vikasardo].[vikasa].shgexternalloansplit where cdeid='" + cdeid + "' and loan_enq_num='" + loan_enqid + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqMembs { membid = reader["memberid"].ToString(), membname = reader["name"].ToString(), aadharnum = reader["aadharnum"].ToString(), amount = reader["amount"].ToString(), shgaccnum = reader["accnum"].ToString(), proposaldate = reader["proposaldate"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }


    //-----------------------------------------------------Update Proposals----------------------------------------------------------------//
    [WebMethod(Description = "This is to Add New Proposal")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void update_proposal(string loan_enqid, string shgid, string shgname, string loanamount, string loantype, string reason, string bankbranch, string split_type, string cdeid, string membername, string memberid, string aadharnum, string amount, string count, string status, string shgaccnum)
    {
        string rep = string.Empty;
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        List<string> membidlist = new List<string>();
        List<string> namelist = new List<string>();
        List<string> aadharlist = new List<string>();
        List<string> amountlist = new List<string>();

        Alib.idExecute("delete from [vikasardo].[vikasa].shgexternalloansplit where loan_enq_num='" + loan_enqid + "' and cdeid='" + cdeid + "'");

        string[] Amountarr = amount.Trim('"').Replace('[', ' ').Replace(']', ' ').Trim().TrimStart().Split(',');
        string[] Namearr = membername.Trim('"').Replace('[', ' ').Replace(']', ' ').Trim().TrimStart().Split(',');
        string[] Aadhararr = aadharnum.Trim('"').Replace('[', ' ').Replace(']', ' ').Trim().TrimStart().Split(',');
        string[] IDSarr = memberid.Trim('"').Replace('[', ' ').Replace(']', ' ').Trim().TrimStart().Split(',');

        if (IDSarr != null)
        {
            foreach (string s in Amountarr)
                amountlist.Add(s);
            foreach (string s in Namearr)
                namelist.Add(s);
            foreach (string s in Aadhararr)
                aadharlist.Add(s);
            foreach (string s in IDSarr)
                membidlist.Add(s);
        }
        try
        {

            for (int j = 0; j < IDSarr.Length; j++)
            {
                Alib.idInsertInto("[vikasardo].[vikasa].shgexternalloansplit", "referenceID", "0", "memberid", IDSarr[j], "name", Namearr[j], "momdate", "momdate", "shgid", shgid, "cdeid", cdeid, "amount", Amountarr[j], "emi", "emi", "emidate", "emidate", "balance", "balance", "bankbrach", bankbranch, "split_type", split_type, "shgloanamount", loanamount, "aadharnum", Aadhararr[j], "shgname", shgname, "reason", reason, "approvedstate", "0", "loan_number", "0", "approvedloanamount", "approvedloanamount", "sactioned_split_type", "sactioned_split_type", "termsnconditions", "termsnconditions", "intrest_rate", "intrest_rate", "loan_enq_num", loan_enqid, "shgaccnum", shgaccnum);
            }
            rep = "{\"status\":\"Success\"}";

        }
        catch
        {
            rep = "{\"status\":\"fail\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(rep);
    }

    //-----------------------------------------------------Get Reference Proposal List(Approved With Ref.No.)----------------------------------------------------------------//
    [WebMethod(Description = "This is to Get Reference Proposal List(Approved With Ref.No.)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getreference_proposallist(string cdeid)
    {
        string s = "";
        List<NewProposalEnqs> np = new List<NewProposalEnqs>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            // cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from shgexternalloansplit where cdeid='" + cdeid + "' and approvedstate='1' and status='2'", conn);
            cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,shgaccnum,proposaldate,isSancRejected from [vikasardo].[vikasa].shgexternalloansplit where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and cdeid='" + cdeid + "' and status in(7,11,12,13)", conn);

            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqs { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), shgaccnum = reader["shgaccnum"].ToString(), proposaldate = reader["proposaldate"].ToString(), isSancRejected = reader["isSancRejected"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }


    //-----------------------------------------------------Add New Sanctioned----------------------------------------------------------------//

    [WebMethod(Description = "This is to Add New Proposal")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void addnew_sactioned(string loanrefid, string shgid, string shgname, string cdeid, string loan_number, string loansactioned_amount, string emi, string bankbranch, string terms, string split_type, string membername, string memberid, string aadharnum, string amount, string count, string status, string rateofintrest, string shgloanamount, string loanenqid, string shgaccnum, string sanctionedDate)
    {
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        List<string> membidlist = new List<string>();
        List<string> namelist = new List<string>();
        List<string> aadharlist = new List<string>();
        List<string> amountlist = new List<string>();

        string[] Amountarr = amount.Trim('"').Replace('[', ' ').Replace(']', ' ').Trim().TrimStart().Split(',');
        string[] Namearr = membername.Trim('"').Replace('[', ' ').Replace(']', ' ').Trim().TrimStart().Split(',');
        string[] Aadhararr = aadharnum.Trim('"').Replace('[', ' ').Replace(']', ' ').Trim().TrimStart().Split(',');
        string[] IDSarr = memberid.Trim('"').Replace('[', ' ').Replace(']', ' ').Trim().TrimStart().Split(',');
        string totmembers = IDSarr.Length.ToString();

        string rep = "";
        string splitmasterid = "";
        string extloanrepid = "";
        string extloanrepayamount = "";
        string checkstatus = "";
        string savingsid = "";
        //string extrlid = "";
        //string dateid = "";
        //Random r = new Random();
        //int tmp = r.Next(1000, 9999);
        //dateid = System.DateTime.Now.AddHours(12).AddMinutes(30).Day.ToString() + "" + System.DateTime.Now.AddHours(12).AddMinutes(30).Month.ToString();
        //extrlid = "EXT" + dateid + "" + tmp;

        string cdoid = Alib.idGetAFieldByQuery("select cdoid from [vikasardo].[vikasa].[cde] where cdeid='" + cdeid + "'");
        string cdmid = Alib.idGetAFieldByQuery("select cdmid from [vikasardo].[vikasa].[cde] where cdeid='" + cdeid + "'");
        string accType = Alib.idGetAFieldByQuery("select top 1 loantype from [vikasardo].[vikasa].[shgexternalloansplit] where cdeid='" + cdeid + "' and shgid='" + shgid + "'");

        if (IDSarr != null)
        {
            foreach (string s in Amountarr)
                amountlist.Add(s);
            foreach (string s in Namearr)
                namelist.Add(s);
            foreach (string s in Aadhararr)
                aadharlist.Add(s);
            foreach (string s in IDSarr)
                membidlist.Add(s);
        }
        try
        {
            string reason = Alib.idGetAFieldByQuery("select reason from [vikasardo].[vikasa].shgexternalloansplit where referenceID='" + loanrefid + "' and loan_enq_num='" + loanenqid + "'");
            Alib.idExecute("Update [vikasardo].[vikasa].shgexternalloansplit set status='11',loan_number='" + loan_number + "',approvedloanamount='" + loansactioned_amount + "',emi='" + emi + "', sanctionedDate='" + sanctionedDate + "', termsnconditions='" + terms + "' where referenceID='" + loanrefid + "' and loan_enq_num='" + loanenqid + "' ");
            Alib.idExecute("Update [vikasardo].[vikasa].[shg] set loanstatus='1' where shgid='" + shgid + "'");

            // insert to spc table
            Alib.idExecute("INSERT INTO [vikasardo].[spc] (cdeid,shgid,shgrefno,shg,village,members,sbaccno,atlaccno,typeofac,loanaccdate,term,emi,loanamt,totrepay,lastrepay,repaydate,challan,balance,status,spc,branch) Values ('" + cdeid + "','" + shgid + "','" + loanrefid + "','" + shgname + "','" + bankbranch + "','" + totmembers + "','" + shgaccnum + "','" + shgaccnum + "','" + accType + "','" + System.DateTime.Now.AddHours(5).AddMonths(30).ToString("dd/MM/yyyy") + "','" + terms + "','" + emi + "','" + shgloanamount + "','0','0','" + System.DateTime.Now.AddHours(5).AddMonths(30).ToString("dd/MM/yyyy") + "','0','" + shgloanamount + "','0','0','" + bankbranch + "')");

            for (int j = 0; j < IDSarr.Length; j++)
            {
                splitmasterid = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] (referenceID, memberid, name, momdate, shgid, cdeid, amount, emi, bankbrach, split_type, shgloanamount, aadharnum, shgname, loan_number, approvedloanamount, termsnconditions, rateofintrest, loan_enq_num, approvedstate, status, reason,shgaccnum,cdmid,cdoid,sanctionedDate, loanstatus, remainingloan) Output Inserted.id values ('" + loanrefid + "','" + IDSarr[j] + "', '" + Namearr[j] + "', '0', '" + shgid + "', '" + cdeid + "', '" + Amountarr[j] + "', '" + emi + "', '" + bankbranch + "','" + split_type + "','" + shgloanamount + "', '" + Aadhararr[j] + "','" + shgname + "', '" + loan_number + "', '" + loansactioned_amount + "','" + terms + "','" + rateofintrest + "','" + loanenqid + "','1', '" + status + "', '" + reason + "','" + shgaccnum + "','" + cdmid + "','" + cdoid + "','" + sanctionedDate + "', '0', '" + loansactioned_amount + "')");

                // updating to meeting table
                int mid = 1 + Convert.ToInt32(Alib.idGetAFieldByQuery("select top 1 meetings from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' order by id desc"));
                int checkmid = mid - 1;
                //   Alib.idExecute("update [vikasardo].[vikasa].[meeting] set bankloan=bankloan+'" + Amountarr[j] + "', bankloanpending=bankloanpending+'" + Amountarr[j] + "' where  memberid='" + IDSarr[j] + "' and shgid='" + shgid + "' and cdeid='" + cdeid + "' and meetings='" + checkmid + "'");

                SqlConnection conn = new SqlConnection(Alib.conStr);
                SqlCommand cmd = null;
                SqlDataReader reader = null;
                cmd = new SqlCommand("select top 1 * from [vikasardo].[vikasa].[shgexternalloanrepayincome] where shgid='" + shgid + "' and memberid='" + IDSarr[j].ToString() + "' and cdeid='" + cdeid + "' order by id desc", conn);
                conn.Open();
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        extloanrepayamount = reader["amount"].ToString();
                        checkstatus = reader["status"].ToString();

                        if (extloanrepayamount == "0" && checkstatus == "0")
                        {
                            Alib.idExecute("Update [vikasardo].[vikasa].[shgexternalloanrepayincome] set extloanid='" + loanrefid + "', momdate='" + System.DateTime.Now.AddHours(5).AddMonths(30).ToString("dd-MM-yyyy") + "', date='" + System.DateTime.Now.AddHours(5).AddMonths(30).ToString("dd-MM-yyyy") + "', amount='" + Amountarr[j].ToString() + "', amountbal='" + Amountarr[j].ToString() + "', splitmasterid='" + splitmasterid + "' where memberid='" + IDSarr[j].ToString() + "' and shgid='" + shgid + "' and cdeid='" + cdeid + "'");
                        }
                        else if (checkstatus == "1")
                        {
                            Alib.idExecute("Insert into [vikasardo].[vikasa].[shgexternalloanrepayincome] (extloanid, masterid, name, momdate, shgid, cdeid, date, amount, amountbal, lastpay, mode, incomesavingsid, splitmasterid, status) values('" + loanrefid + "','" + IDSarr[j].ToString() + "','" + Namearr[j].ToString() + "', '" + System.DateTime.Now.AddHours(5).AddMonths(30).ToString("dd-MM-yyyy") + "','" + shgid + "','" + cdeid + "','" + System.DateTime.Now.AddHours(5).AddMonths(30).ToString("dd-MM-yyyy") + "','" + Amountarr[j].ToString() + "','" + Amountarr[j].ToString() + "','0','0','" + savingsid + "','" + splitmasterid + "','0'");
                        }
                    }
                }
            }
            rep = "{\"status\":\"Success\"}";
        }
        catch
        {
            rep = "{\"status\":\"fail\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(rep);
    }

    //-----------------------------------------------------Get Finalized List----------------------------------------------------------------//
    [WebMethod(Description = "This is to Get Finalized List(Finally Approved By HO)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getfinalized_proposallist(string cdeid)
    {
        string s = "";
        List<NewProposalEnqdetails> np = new List<NewProposalEnqdetails>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            // cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from shgexternalloansplit where cdeid='" + cdeid + "' and approvedstate='1' and status='2'", conn);
            cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,loan_number,approvedloanamount,emi,shgaccnum, proposaldate, sanctionedDate, termsnconditions from [vikasardo].[vikasa].shgexternalloansplit where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and cdeid='" + cdeid + "'  and status in(14)", conn);

            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqdetails { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), loan_number = reader["loan_number"].ToString(), emi = reader["emi"].ToString(), approvedloanamount = reader["approvedloanamount"].ToString(), shgaccnum = reader["shgaccnum"].ToString(), proposaldate = reader["proposaldate"].ToString(), sanctionedDate = reader["sanctionedDate"].ToString(), termsnconditions = reader["termsnconditions"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-----------------------------------------------------Get Rejected List----------------------------------------------------------------//
    [WebMethod(Description = "This is to Get Rejected List(Finally Rejected By HO)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getrejected_proposallist(string cdeid)
    {
        string s = "";
        List<NewProposalEnqdetails> np = new List<NewProposalEnqdetails>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            // cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from shgexternalloansplit where cdeid='" + cdeid + "' and approvedstate='1' and status='2'", conn);
            cmd = new SqlCommand("select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status,loan_number,approvedloanamount,emi,shgaccnum,rejreason,rejbyrole, proposaldate from [vikasardo].[vikasa].shgexternalloansplit where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by  loan_enq_num) and cdeid='" + cdeid + "' and status in(4,6,8,20)", conn);

            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new NewProposalEnqdetails { loanrefID = reader["referenceID"].ToString(), shgid = reader["shgid"].ToString(), cdeid = reader["cdeid"].ToString(), reason = reader["reason"].ToString(), split_type = reader["split_type"].ToString(), shgloanamount = reader["shgloanamount"].ToString(), shgname = reader["shgname"].ToString(), loaenqid = reader["loan_enq_num"].ToString(), bankbranch = reader["bankbrach"].ToString(), loantype = reader["loantype"].ToString(), status = reader["status"].ToString(), loan_number = reader["loan_number"].ToString(), emi = reader["emi"].ToString(), approvedloanamount = reader["approvedloanamount"].ToString(), shgaccnum = reader["shgaccnum"].ToString(), rejreason = reader["rejreason"].ToString(), rejby = reader["rejbyrole"].ToString(), proposaldate = reader["proposaldate"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }


    //-----------------------------------------------------Add Case Study----------------------------------------------------------------//
    [WebMethod(Description = "This is to Add Case Study")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void addnew_case(string membid, string membname, string activityname, string date, string image, string cdeid, string shgid)
    {
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        string rep = "";
        string formdate = Alib.idGetAFieldByQuery("select formationDate from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
        DateTime dt1 = DateTime.ParseExact(formdate, "d/M/yyyy",
                                                  CultureInfo.InvariantCulture);
        DateTime dt2 = DateTime.ParseExact(date, "d/M/yyyy",
                                                          CultureInfo.InvariantCulture);
        TimeSpan difference = dt2 - dt1;
        int days = Convert.ToInt32(difference.TotalDays);

        if (days <= 0)
        {
            rep = "{\"status\":\"date_error\"}";
        }
        else
        {
            //
            try
            {
                byte[] imageBytes = Convert.FromBase64String(image);
                MemoryStream ms = new MemoryStream(imageBytes);
                string imgname = RandomNo.Random.GenerateIdentifier(28) + ".png";
                FileStream fs = new FileStream(Server.MapPath("~/CaseStudyImages/" + imgname), FileMode.Create);
                ms.WriteTo(fs);
                ms.Close();
                fs.Close();
                fs.Dispose();
                Alib.idInsertInto("[vikasardo].[vikasa].casestudy", "membid", membid, "membname", membname, "activityname", activityname, "date", date, "imagename", imgname, "cdeid", cdeid, "shgid", shgid);
                rep = "{\"status\":\"Success\"}";
            }
            catch
            {
                rep = "{\"status\":\"fail\"}";
            }
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(rep);
    }


    //-----------------------------------------------------Get Case Study----------------------------------------------------------------//
    [WebMethod(Description = "This is to Get Case Study)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getcase_study(string cdeid, string shgid)
    {
        string s = "";
        List<CaseStudy> np = new List<CaseStudy>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select membid,membname,activityname,date,imagename,shgid,cdeid from [vikasardo].[vikasa].casestudy where cdeid='" + cdeid + "' and shgid='" + shgid + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new CaseStudy { membid = reader["membid"].ToString(), membname = reader["membname"].ToString(), activityname = reader["activityname"].ToString(), date = reader["date"].ToString(), imagename = "http://test.vikasardo.org/CaseStudyImages/" + reader["imagename"].ToString(), cdeid = reader["cdeid"].ToString(), shgid = reader["shgid"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    //-----------------------------------------------------Get Mom Details----------------------------------------------------------------//
    [WebMethod(Description = "This is to Get Mom Details)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getmom_details(string cdeid, string shgid)
    {
        string s = "";
        MomMainDetails mommaindet = new MomMainDetails();
        List<addmomdetails> np = new List<addmomdetails>();
        Savings sav = new Savings();
        Expenses exp = new Expenses();

        MomInfo infolist = new MomInfo();

        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            //For Mom Details (Of all Members)
            int mid = 1 + Convert.ToInt32(Alib.idGetAFieldByQuery("select top 1 meetings from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' order by id desc"));
            int checkmid = mid - 1;
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            string count = Alib.idGetAFieldByQuery("select count(memberID) from [vikasardo].[vikasa].members where cdeid='" + cdeid + "' and shgid='" + shgid + "'");
            string memfee = Alib.idGetAFieldByQuery("select sum(convert(int, membershipFee)) as SHG_MembershipFee from [vikasardo].[vikasa].members where shgid='" + shgid + "' and cdeid='" + cdeid + "'");
            //cmd = new SqlCommand("select top " + count + " * from [vikasardo].[vikasa].meeting where cdeid='" + cdeid + "' and shgid='" + shgid + "' and meetings='" + checkmid + "'", conn);

            cmd = new SqlCommand("SELECT top " + count + " * FROM [vikasardo].[vikasa].meeting meeting INNER JOIN [vikasardo].[vikasa].MEMBERS members ON members.memberID = meeting.memberID WHERE meeting.shgid = '" + shgid + "' and meeting.cdeid = '" + cdeid + "'and meeting.meetings= '" + checkmid + "' order by members.memberid", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    //  np.Add(new addmomdetails { memberid = reader["memberid"].ToString(), name = reader["name"].ToString(), savings = "0", totalsavings = reader["totalsavings"].ToString(), lendbal = reader["lendbal"].ToString(), lendemi = "0", interest = reader["interest"].ToString(), lending = "0", lendpending = reader["lendpending"].ToString(), bankloan = reader["bankloan"].ToString(), bankemi = reader["bankemi"].ToString(), bankloanpending = reader["bankloanpending"].ToString(), penalty = reader["penalty"].ToString(), newmeetinglending = reader["newmeetinglending"].ToString(), cash = reader["cash"].ToString(), totalpenalty = reader["totalpenalty"].ToString(), totalcollections = reader["totalcollections"].ToString(), meetings = reader["meetings"].ToString() });
                    np.Add(new addmomdetails { memberid = reader["memberid"].ToString(), name = reader["name"].ToString(), savings = "0", totalsavings = reader["totalsavings"].ToString(), lendbal = reader["lendbal"].ToString(), lendemi = "0", interest = "0", lending = "0", lendpending = reader["lendpending"].ToString(), bankloan = reader["bankloan"].ToString(), bankemi = "0", bankloanpending = reader["bankloanpending"].ToString(), penalty = "0", newmeetinglending = reader["newmeetinglending"].ToString(), cash = reader["cash"].ToString(), totalpenalty = reader["totalpenalty"].ToString(), totalcollections = "0", meetings = reader["meetings"].ToString() });
                }
            }

            // sav = new Savings { shginbank = reader1["shginbank"].ToString(), shginhand = reader1["shginhand"].ToString(), shgsavings = reader1["shgtotlendrepay"].ToString(), shgtotsavings = reader1["shgtotlendinterest"].ToString(), shgtotlendrepay = reader1["shgtotbankemi"].ToString(), shgtotlendinterest = reader1["shgtotpenalty"].ToString(), shgtotintlending = reader1["shgotherssavings"].ToString(), shgtotbankemi = reader1["shgtotcollection"].ToString(), shgtotpenalty = reader1["shgtotgrants"].ToString(), shgtotbankinterest = reader1["shgtotbankinterest"].ToString(), shgtotgrants = reader1["shgtotgrants"].ToString(), shgotherssavings = reader1["shgotherssavings"].ToString(), shgtotcollection = reader1["shgtotcollection"].ToString(), shgincomerefundfromrid = "0" };
            sav = new Savings { shginbank = "0", shginhand = "0", shgsavings = "0", shgtotsavings = "0", shgtotlendrepay = "0", shgtotlendinterest = "0", shgtotintlending = "0", shgtotbankemi = "0", shgtotpenalty = "0", shgtotbankinterest = "0", shgtotgrants = "0", shgotherssavings = "0", shgtotcollection = "0", shgincomerefundfromrid = "0", membershipFee = "0" };

            //  exp = new Expenses { shgexpcash = reader2["shgexpcash"].ToString(), shgexpcheque = reader2["shgexpcheque"].ToString(), shgexpdepositetosavingsacc = reader2["shgexpdepositetosavingsacc"].ToString(), shgexpdepositetobankacc = reader2["shgexpdepositetobankacc"].ToString(), shgexpcashinhand = reader2["shgexpcashinhand"].ToString(), shgexpamountinbanksavings = reader2["shgexpamountinbanksavings"].ToString(), shgexpmembersavingsrepayment = reader2["shgexpmembersavingsrepayment"].ToString(), shgexpothers = reader2["shgexpothers"].ToString(), shgexptotal = reader2["shgexptotal"].ToString(), shgexprid = "0" };
            exp = new Expenses { shgexpcash = "0", shgexpcheque = "0", shgexpdepositetosavingsacc = "0", shgexpdepositetobankacc = "0", shgexpcashinhand = "0", shgexpamountinbanksavings = "0", shgexpmembersavingsrepayment = "0", shgexpothers = "0", shgexptotal = "0", shgexprid = "0", shgexp = "0" };

            //For MomMainDetails(Date,Place etc)

            SqlConnection conn3 = new SqlConnection(Alib.conStr);
            SqlCommand cmd3 = null;
            SqlDataReader reader3 = null;
            cmd3 = new SqlCommand("select date,place,persons,shgid,cdeid from [vikasardo].[vikasa].meeting where cdeid='" + cdeid + "' and shgid='" + shgid + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc)", conn3);
            conn3.Open();
            reader3 = cmd3.ExecuteReader();
            if (reader3.HasRows)
            {
                while (reader3.Read())
                {
                    mommaindet = new MomMainDetails { date = reader3["date"].ToString(), place = reader3["place"].ToString(), persons = reader3["persons"].ToString(), shgid = reader3["shgid"].ToString(), cdeid = reader3["cdeid"].ToString() };
                }
            }

            infolist = new MomInfo() { mommaindetails = mommaindet, momdetails = np, saving = sav, expense = exp };

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(infolist);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }
    ////////////////////////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------Add New Mom Details---------------------------------------------------------------//

    [WebMethod(Description = "This is to Add New Mom Details")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void addnew_momdetails(string jsondata, string shgid, string cdeid, string shgminimage, string loanchallenimage)
    {
        string rep = "";
        string remaining = "";
        //  Session["cdeid"] = cdeid.ToString();
        string json = "[{\"seq\":1,\"firstName\":\"Chris\",\"lastName\":\"Westbrook\"},{\"seq\":2,\"firstName\":\"sayyl\",\"lastName\":\"westbrook\"}]";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        string imgname1 = shgminimage;
        string imgname3 = loanchallenimage;
        if (imgname1 != "")
        {
            byte[] imageBytes = Convert.FromBase64String(imgname1);
            MemoryStream ms = new MemoryStream(imageBytes);
            imgname1 = RandomNo.Random.GenerateIdentifier(28) + ".png";
            FileStream fs = new FileStream(Server.MapPath("~/MinImages/" + imgname1), FileMode.Create);
            ms.WriteTo(fs);
            ms.Close();
            fs.Close();
            fs.Dispose();
        }
        //if (imgname2 != "")
        //{
        //    byte[] imageBytes = Convert.FromBase64String(imgname2);
        //    MemoryStream ms = new MemoryStream(imageBytes);
        //    imgname2 = RandomNo.Random.GenerateIdentifier(28) + ".png";
        //    FileStream fs = new FileStream(Server.MapPath("~/SavingAccImages/" + imgname2), FileMode.Create);
        //    ms.WriteTo(fs);
        //    ms.Close();
        //    fs.Close();
        //    fs.Dispose();
        //}
        if (imgname3 != "")
        {
            byte[] imageBytes = Convert.FromBase64String(imgname3);
            MemoryStream ms = new MemoryStream(imageBytes);
            imgname3 = RandomNo.Random.GenerateIdentifier(28) + ".png";
            FileStream fs = new FileStream(Server.MapPath("~/challan/" + imgname3), FileMode.Create);
            ms.WriteTo(fs);
            ms.Close();
            fs.Close();
            fs.Dispose();
        }

        try
        {
            MomInfo momdet = new JavaScriptSerializer().Deserialize<MomInfo>(jsondata);
            MomMainDetails momMd = momdet.mommaindetails;
            Expenses expences = momdet.expense;
            Savings savings = momdet.saving;
            List<addmomdetails> momdetails = momdet.momdetails;

            int mid = 1 + Convert.ToInt32(Alib.idGetAFieldByQuery("select top 1 meetings from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' order by id desc"));
            int checkmid = mid - 1;

            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            conn.Open();
            int totlastpay = 0;
            int lastpay;
            for (int j = 0; j < momdetails.Count; j++)
            {
                cmd = new SqlCommand("select top 1 * from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and cdeid='" + cdeid + "' and meetings='" + checkmid + "' and memberid='" + momdetails[j].memberid + "' order by id desc", conn);
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        double totalindsavings = Convert.ToDouble(reader["totalsavings"].ToString()) + Convert.ToDouble(momdetails[j].savings);
                        double totalindpenalty = Convert.ToDouble(reader["totalpenalty"].ToString()) + Convert.ToDouble(momdetails[j].penalty);
                        double intlendingbal_intpendingbal = Convert.ToDouble(momdetails[j].lendbal) - Convert.ToDouble(momdetails[j].lendemi) + Convert.ToDouble(momdetails[j].lending);
                        double bankloanbal = Convert.ToDouble(reader["bankloanpending"].ToString()) - Convert.ToDouble(momdetails[j].bankemi);
                        double bankloan = Convert.ToDouble(reader["bankloan"].ToString());
                        double totalpenalty = Convert.ToDouble(reader["totalpenalty"].ToString()) + Convert.ToDouble(momdetails[j].penalty);
                        double newbankloan = bankloan - Convert.ToDouble(momdetails[j].bankemi);
                        Alib.idExecute("Insert into [vikasardo].[vikasa].[meeting] (memberid, name, date, place, persons, shgid, cdeid, shgminimage, loanchallenimage, savings, totalsavings, lendbal, lendemi, interest, lending, lendpending, bankloan, bankemi, bankloanpending, penalty, newmeetinglending, cash, totalpenalty, totalcollections, meetings, shginbank, shginhand, shgsavings, shgtotsavings, shgtotlendrepay, shgtotlendinterest, shgtotintlending, shgtotbankemi, shgtotpenalty, shgtotbankinterest,shgtotgrants,shgotherssavings,shgtotcollection,shgexpcash,shgexpcheque,shgexpdepositetosavingsacc,shgexpdepositetobankacc,shgexpcashinhand,shgexpamountinbanksavings,shgexpmembersavingsrepayment,shgexpothers,shgexptotal,shgincomerefundfromrid,shgexprid,shgexp,membershipfee) values('" + momdetails[j].memberid + "','" + momdetails[j].name + "','" + momMd.date + "', '" + momMd.place + "','" + momMd.persons + "','" + momMd.shgid + "','" + momMd.cdeid + "','" + imgname1 + "','" + imgname3 + "','" + momdetails[j].savings + "','" + totalindsavings + "','" + intlendingbal_intpendingbal + "','" + momdetails[j].lendemi + "','" + momdetails[j].interest + "','" + momdetails[j].lending + "','" + intlendingbal_intpendingbal + "','" + newbankloan + "','" + momdetails[j].bankemi + "','" + newbankloan + "','" + momdetails[j].penalty + "','0','0','" + totalindpenalty + "','" + momdetails[j].totalcollections + "','" + mid + "','" + savings.shginbank + "','" + savings.shginhand + "','" + savings.shgsavings + "','" + savings.shgtotsavings + "','" + savings.shgtotlendrepay + "','" + savings.shgtotlendinterest + "','" + savings.shgtotintlending + "','" + savings.shgtotbankemi + "','" + savings.shgtotpenalty + "','" + savings.shgtotbankinterest + "','" + savings.shgtotgrants + "','" + savings.shgotherssavings + "','" + savings.shgtotcollection + "','" + expences.shgexpcash + "','" + expences.shgexpcheque + "','" + expences.shgexpdepositetosavingsacc + "','" + expences.shgexpdepositetobankacc + "','" + expences.shgexpcashinhand + "','" + expences.shgexpamountinbanksavings + "','" + expences.shgexpmembersavingsrepayment + "','" + expences.shgexpothers + "','" + expences.shgexptotal + "','" + savings.shgincomerefundfromrid + "','" + expences.shgexprid + "', '" + expences.shgexp + "','" + savings.membershipFee + "')");

                        if (Alib.idHasRows("SELECT shgid FROM [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] WHERE shgid='" + shgid + "'"))
                        {
                            Alib.idExecute("Update [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] set remainingloan = remainingloan- " + momdetails[j].bankemi + " where shgid='" + shgid + "' and cdeid='" + cdeid + "' and loanstatus='0'");
                            remaining = Alib.getSingleValue("Select top 1 remainingloan from [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc");
                            if (Convert.ToInt32(remaining) <= 0)
                            {
                                Alib.idExecute("Update [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] set loanstatus='1' where shgid='" + shgid + "' and cdeid='" + cdeid + "'");
                                Alib.idExecute("Update [vikasardo].[vikasa].[shg] set loanstatus='0' where shgid='" + shgid + "' and cdeid='" + cdeid + "'");
                            }
                        }
                        try
                        {
                            if (Alib.idHasRows("SELECT shgid FROM [vikasardo].[vikasardo].[spc] WHERE shgid='" + shgid + "'"))
                            {
                                string momdt = "";
                                string[] momdate = momMd.date.Split('/');


                                //--------date----------------//
                                int first = Convert.ToInt32(momdate[0].Count());
                                string first1 = momdate[0].ToString();
                                if (first == 1)
                                {
                                    first1 = "0" + first1;
                                }

                                //-----------month---------------//
                                int second = Convert.ToInt32(momdate[1].Count());
                                string second1 = momdate[1].ToString();
                                if (second == 1)
                                {
                                    second1 = "0" + second1;
                                }

                                //------------yesr--------------//
                                int third = Convert.ToInt32(momdate[2].Count());
                                string third1 = momdate[2].ToString();
                                if (third == 2)
                                {
                                    third1 = "20" + third1;
                                }

                                momdt = first1 + "/" + second1 + "/" + third1;

                                totlastpay += Convert.ToInt32(momdetails[j].bankemi);
                                lastpay = Convert.ToInt32(momdetails[j].bankemi);
                                Alib.idExecute("Update [vikasardo].[vikasardo].[spc] set term = term-1, totrepay= totrepay+ " + momdetails[j].bankemi + ", lastrepay='" + totlastpay.ToString() + "', repaydate='" + momdt + "', challan='" + imgname3 + "', balance= balance-" + lastpay + " where cdeid='" + momMd.cdeid + "' and shgid='" + momMd.shgid + "' and id=(select max(r.id) from [vikasardo].[vikasardo].[spc] r where  r.cdeid='" + momMd.cdeid + "' and r.shgid='" + momMd.shgid + "')");
                                string balance = Alib.idGetAFieldByQuery("Select top 1 balance from [vikasardo].[vikasardo].[spc] where cdeid='" + momMd.cdeid + "' and shgid='" + momMd.shgid + "' order by id desc");
                                if (balance == "0")
                                {
                                    Alib.idExecute("Update [vikasardo].[vikasardo].[spc] set term = '0', status = '1' where cdeid='" + momMd.cdeid + "' and shgid='" + momMd.shgid + "'");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                reader.Close();
            }
            Alib.idExecute("Update [vikasardo].[vikasa].[shg] set rid = rid+ " + expences.shgexprid + "-" + savings.shgincomerefundfromrid + "  where shgid='" + shgid + "' and cdeid='" + cdeid + "'");

            //-------------------------------------SPC TABLE UPDATE STARTS---------------------------------------------//

            //try
            //{
            //    if (Alib.idHasRows("SELECT shgid FROM [vikasardo].[vikasardo].[spc] WHERE shgid='" + shgid + "'"))
            //    {
            //        Alib.idExecute("Update [vikasardo].[vikasardo].[spc] set term = term-1, totrepay= totrepay+ " + expences.shgexpmembersavingsrepayment + ", lastrepay='" + expences.shgexpmembersavingsrepayment + "', repaydate='" + momMd.date + "', challan='" + imgname3 + "', balance= balance- " + expences.shgexpmembersavingsrepayment + " where cdeid='" + momMd.cdeid + "' and shgid='" + momMd.shgid + "' and id=(select max(r.id) from [vikasardo].[vikasardo].[spc] r where  r.cdeid='" + momMd.cdeid + "' and r.shgid='" + momMd.shgid + "')");
            //        string balance = Alib.idGetAFieldByQuery("Select top 1 balance from [vikasardo].[vikasardo].[spc] where cdeid='" + momMd.cdeid + "' and shgid='" + momMd.shgid + "' order by id desc");
            //        if (balance == "0")
            //        {
            //            Alib.idExecute("Update [vikasardo].[vikasardo].[spc] set term = '0', status = '1' where cdeid='" + momMd.cdeid + "' and shgid='" + momMd.shgid + "'");
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //}
            //--------------------------------------SPC TABLE UPDATE ENDS---------------------------------------------//


            //-----------------FCM Notification Starts---------------------//
            //---CDO----//
            //string cdoid = Alib.idGetAFieldByQuery("Select o.fcmid from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[cde] e on e.cdoid = o.cdoid where cdeid ='" + cdeid + "'");
            //string BrowserAPIKey = "AIzaSyA8CJ6sEkCmQoHxiHAIKn7D6z1WpRSOxfg";

            //string subject = "MOM_CDO";
            //string message = "Notification From CDE for New Month of Meeting";

            //if (cdoid != "")
            //{
            //    string postData = "{ \"registration_ids\": [\"" + cdoid + "\"], \"data\": {\"subject\":\"" + subject + "\",\"message\":\"" + message + "\"}}";
            //  CDOSendGCMNotification(BrowserAPIKey, postData);

            // }

            //---CDM----//
            //string cdmid = Alib.idGetAFieldByQuery("Select m.fcmid from [vikasardo].[vikasa].[cdm] m inner join [vikasardo].[vikasa].[cde] e on e.cdmid = m.cdmid where cdeid ='" + cdeid + "'");
            //string BrowserAPIKey1 = "AIzaSyByrnlMNpCI6gaOlqr7KIj0C5El2vVttBE";

            //string subject1 = "MOM_CDM";
            //string message1 = "Notification From CDE for New Month of Meeting";
            //if (cdmid != "")
            //{
            //    string postData = "{ \"registration_ids\": [\"" + cdmid + "\"], \"data\": {\"subject\":\"" + subject1 + "\",\"message\":\"" + message1 + "\"}}";
            //    //   CDMSendGCMNotification(BrowserAPIKey1, postData);
            //}
            //-----------------FCM Notification Ends---------------------//

            //-----------------Message Send Starts-----------------------//

            //string cdonum = Alib.idGetAFieldByQuery("Select o.cdophone from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[cde] e on e.cdoid=o.cdoid where cdeid ='" + cdeid + "'");
            //string cdmnum = Alib.idGetAFieldByQuery("Select m.cdmphone from [vikasardo].[vikasa].[cdm] m inner join [vikasardo].[vikasa].[cde] e on e.cdmid=m.cdmid where cdeid ='" + cdeid + "'");
            //string num = cdonum + "," + cdmnum;
            //string wish = "New SHG approval from CDE";
            //SqlConnection con = null;
            //SqlDataReader rdr = null;
            //try
            //{
            //    WebClient Client = new WebClient();
            //    Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + num);
            //    Stream data = Client.OpenRead(uri);
            //    StreamReader reader1 = new StreamReader(data);
            //    string s = reader1.ReadToEnd();
            //    data.Close();
            //    reader1.Close();
            //}
            //catch (SqlException ex)
            //{
            //}
            //finally
            //{
            //    if (con != null)
            //    {
            //        con.Close();
            //        con.Dispose();
            //    }
            //    if (rdr != null)
            //        rdr.Dispose();
            //}

            //-----------------Message Send Ends-------------------------//

            rep = "{\"status\":\"Success\"}";
        }
        catch (Exception ex)
        {
            rep = ex.Message;
            rep = "{\"status\":\"Fail\"},{\"Message\":" + ex.Message.ToString() + "}";
        }
        MomInfo mi = new MomInfo();
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(rep);
    }

    //-----------------------------------------------------Get Member Profile----------------------------------------------------------------//
    [WebMethod(Description = "This is to Get Member Profile)")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getmember_Profile(string cdeid, string shgid, string membid)
    {
        string s = "";
        List<MemberProfile> np = new List<MemberProfile>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select top 1 m1.memberid,m1.image,m2.totalsavings,m2.memberid,m2.name,m2.lending,m2.lendpending,m2.bankloan,m2.bankloanpending,m2.memberid from [vikasardo].[vikasa].members m1 inner join [vikasardo].[vikasa].meeting m2 on m1.memberid=m2.memberid where m2.shgid ='" + shgid + "' and m2.cdeid='" + cdeid + "' and m2.memberid='" + membid + "' order by id desc", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    np.Add(new MemberProfile { memberid = reader["memberid"].ToString(), name = reader["name"].ToString(), totalsavings = reader["totalsavings"].ToString(), lending = reader["lending"].ToString(), image = "http://test.vikasardo.org/images/" + reader["image"].ToString(), lendpending = reader["lendpending"].ToString(), bankloan = reader["bankloan"].ToString(), bankloanpending = reader["bankloanpending"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(np);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-----------------------------------------------------------------Edit Member Details------------------------------------------------------------------//
    [WebMethod(Description = "This is to Edit Member Details")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void edit_member_details(string memberID, string name, string fatherORhusband, string dob, string gender, string adhaarNum, string bankname, string savingAccNum, string nominee, string maritalStatus, string education, string caste, string occupation, string typeOfHome, string propertyType, string govtCard, string mobile, string pan, string sizeofFamily, string eldermale, string elderfemale, string childmale, string childfemale, string image, string signature, string cdeid, string shgid, string annualincome, string address)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            string imgname = "";
            string imgname1 = "";
            if (image != "")
            {
                byte[] imageBytes = Convert.FromBase64String(image);
                MemoryStream ms = new MemoryStream(imageBytes);
                imgname = RandomNo.Random.GenerateIdentifier(28) + ".png";
                FileStream fs = new FileStream(Server.MapPath("~/images/" + imgname), FileMode.Create);
                ms.WriteTo(fs);
                ms.Close();
                fs.Close();
                fs.Dispose();
            }
            if (signature != "")
            {
                byte[] imageBytes1 = Convert.FromBase64String(signature);
                MemoryStream ms1 = new MemoryStream(imageBytes1);
                imgname1 = RandomNo.Random.GenerateIdentifier(28) + ".png";
                FileStream fs1 = new FileStream(Server.MapPath("~/images/" + imgname1), FileMode.Create);
                ms1.WriteTo(fs1);
                ms1.Close();
                fs1.Close();
                fs1.Dispose();
            }
            Alib.idExecute("Update [vikasardo].[vikasa].[members] set name='" + name + "',fatherORhusband='" + fatherORhusband + "',dob='" + dob + "', gender='" + gender + "',adhaarNum='" + adhaarNum + "', bankname='" + bankname + "',savingAccNum='" + savingAccNum + "',nominee='" + nominee + "', maritalStatus='" + maritalStatus + "',education='" + education + "',caste='" + caste + "',occupation='" + occupation + "',typeOfHome='" + typeOfHome + "',propertyType='" + propertyType + "',govtCard='" + govtCard + "',mobile='" + mobile + "',pan='" + pan + "',sizeofFamily='" + sizeofFamily + "',eldermale='" + eldermale + "',elderfemale='" + elderfemale + "',childmale='" + childmale + "',childfemale='" + childfemale + "', image='" + imgname + "', signature='" + imgname1 + "', cdeid='" + cdeid + "',shgid='" + shgid + "', status='0', annualincome='" + annualincome + "', address='" + address + "' where memberID='" + memberID + "'");
            Alib.idExecute("Update [vikasardo].[vikasa].[meeting] set name='" + name + "' where memberid ='" + memberID + "'");
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    ////////////////////////////////////////////////////----Summary Service----////////////////////////////////////////////////

    [WebMethod(Description = "This is to get Summary List")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_summary(string cdeid, string shgid, string from, string to)
    {
        string[] fromdate = from.Split('/');
        string[] todate = to.Split('/');

        //--------date----------------//
        int first = Convert.ToInt32(fromdate[0].Count());
        string first1 = fromdate[0].ToString();
        if (first == 1)
        {
            first1 = "0" + first1;
        }
        int tofirst = Convert.ToInt32(todate[0].Count());
        string tofirst1 = todate[0].ToString();
        if (tofirst == 1)
        {
            tofirst1 = "0" + tofirst1;
        }

        //-----------month---------------//
        int second = Convert.ToInt32(fromdate[1].Count());
        string second1 = fromdate[1].ToString();
        if (second == 1)
        {
            second1 = "0" + second1;
        }
        int tosecond = Convert.ToInt32(todate[1].Count());
        string tosecond1 = todate[1].ToString();
        if (tosecond == 1)
        {
            tosecond1 = "0" + tosecond1;
        }

        //------------yesr--------------//
        int third = Convert.ToInt32(fromdate[2].Count());
        string third1 = fromdate[2].ToString();
        if (third == 2)
        {
            third1 = "20" + third1;
        }
        int tothird = Convert.ToInt32(todate[2].Count());
        string tothird1 = todate[2].ToString();
        if (tothird == 2)
        {
            tothird1 = "20" + tothird1;
        }

        from = first1 + "/" + second1 + "/" + third1;
        to = tofirst1 + "/" + tosecond1 + "/" + tothird1;

        string rep = "";

        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        string liabilitymembershipFee = "";
        string liabilitytotalsavings = "";
        string liabilitytotalinterest = "";
        string liabilitytotalpenalty = "";
        string liabilitytotalbankinterest = "";
        string liabilitytotalfundreceived = "";
        string liabilitytotalothers = "";
        string liabilitytotal = "";

        string assetstotalintlending = "";
        string assetscashinhand = "";
        string assetscashinbank = "";
        string assetsrid = "";
        string assetsexpothers = "";
        string assetsshgothers = "";
        string assetstotothers = "";
        string assetstotal = "";


        try
        {
            // liabilitymembershipFee = Alib.idGetAFieldByQuery("select sum(convert(int, membershipFee)) as SHG_MembershipFee from [vikasardo].[vikasa].members where shgid='" + shgid + "' and cdeid='" + cdeid + "'");
            liabilitymembershipFee = Alib.idGetAFieldByQuery("select sum(convert(int, s.membershipfee)) from (select distinct(date), membershipfee from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and cdeid='" + cdeid + "' and date<>'0' and convert(datetime, date, 103) between convert(datetime, '" + from + "', 103) and convert(datetime, '" + to + "', 103)) s");

            liabilitytotalsavings = Alib.idGetAFieldByQuery("select sum(convert(int, savings)) as SHGSavings from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' and convert(datetime, date, 103) between convert(datetime, '" + from + "', 103) and convert(datetime, '" + to + "', 103) and date<>'0'");
            liabilitytotalinterest = Alib.idGetAFieldByQuery("select sum(convert(int, interest)) as SHGInterest from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' and convert(datetime, date, 103) between convert(datetime, '" + from + "', 103) and convert(datetime, '" + to + "', 103) and date<>'0'");
            liabilitytotalpenalty = Alib.idGetAFieldByQuery("select sum(convert(int, penalty)) as SHGpENALTY from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' and convert(datetime, date, 103) between convert(datetime, '" + from + "', 103) and convert(datetime, '" + to + "', 103) and date<>'0'");
            liabilitytotalbankinterest = Alib.idGetAFieldByQuery("SELECT sum(convert(int, shgtotbankinterest)) as total from [vikasardo].[vikasa].meeting where id in (SELECT max(m.id) from [vikasardo].[vikasa].meeting m where shgid='" + shgid + "' and cdeid='" + cdeid + "' and  convert(datetime, m.date, 103) between convert(datetime, '" + from + "', 103) And convert(datetime, '" + to + "', 103) and date<>'0' group by m.date)");
            liabilitytotalfundreceived = Alib.idGetAFieldByQuery("select sum(convert(int, shgtotgrants)) as SHGSavings from [vikasardo].[vikasa].meeting where id in (SELECT max(m.id) from [vikasardo].[vikasa].meeting m where shgid='" + shgid + "' and cdeid='" + cdeid + "' and  convert(datetime, m.date, 103) between convert(datetime, '" + from + "', 103) And convert(datetime, '" + to + "', 103) and date<>'0' group by  m.date)");

            liabilitytotalothers = Alib.idGetAFieldByQuery("select sum(convert(int, s.shgotherssavings)) from (select distinct(date), shgotherssavings from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and cdeid='" + cdeid + "' and date<>'0' and convert(datetime, date, 103) between convert(datetime, '" + from + "', 103) and convert(datetime, '" + to + "', 103)) s");

            //  liabilitytotalothers = Alib.idGetAFieldByQuery("select sum(distinct(convert(int, shgotherssavings))) from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' and convert(datetime, date, 103) between convert(datetime, '" + from + "', 103) and convert(datetime, '" + to + "', 103) and date<>'0'");
            liabilitytotal = (Convert.ToDouble(liabilitymembershipFee) + Convert.ToDouble(liabilitytotalsavings) + Convert.ToDouble(liabilitytotalinterest) + Convert.ToDouble(liabilitytotalpenalty) + Convert.ToDouble(liabilitytotalbankinterest) + Convert.ToDouble(liabilitytotalfundreceived) + Convert.ToDouble(liabilitytotalothers)).ToString();

            assetstotalintlending = Alib.idGetAFieldByQuery("select sum(convert(int, lendbal)) as SHG_InternalLending from [vikasardo].[vikasa].meeting where cdeid='" + cdeid + "' and shgid='" + shgid + "' and meetings=(select top 1 meetings from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc)");
            assetscashinhand = Alib.idGetAFieldByQuery("select sum(convert(int, shgexpcashinhand)) as SHGCashinHand from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc)");
            assetscashinbank = Alib.idGetAFieldByQuery("select sum(convert(int, shgexpamountinbanksavings)) as SHGCashinBank from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc)");
            assetsrid = Alib.idGetAFieldByQuery("select rid from [vikasardo].[vikasa].shg where shgid='" + shgid + "' and cdeid='" + cdeid + "'");
            //  assetsexpothers = Alib.idGetAFieldByQuery("select sum(convert(int, shgexpothers)) as SHG_expOthers from [vikasardo].[vikasa].meeting where cdeid='" + cdeid + "' and shgid='" + shgid + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc)");
            assetsexpothers = Alib.idGetAFieldByQuery("select sum(convert(int, s.shgexpothers)) from (select distinct(date), shgexpothers from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and cdeid='" + cdeid + "' and date<>'0' and convert(datetime, date, 103) between convert(datetime, '" + from + "', 103) and convert(datetime, '" + to + "', 103)) s");
            assetsshgothers = Alib.idGetAFieldByQuery("select sum(convert(int, s.shgexp)) from (select distinct(date), shgexp from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and cdeid='" + cdeid + "' and date<>'0' and convert(datetime, date, 103) between convert(datetime, '" + from + "', 103) and convert(datetime, '" + to + "', 103)) s");
            // assetsshgothers = Alib.idGetAFieldByQuery("select expenses from [vikasardo].[vikasa].shg where shgid='" + shgid + "' and cdeid='" + cdeid + "'");
            assetstotothers = (Convert.ToDouble(assetsexpothers) + Convert.ToDouble(assetsshgothers)).ToString();


            assetstotal = (Convert.ToDouble(assetstotalintlending) + Convert.ToDouble(assetscashinhand) + Convert.ToDouble(assetscashinbank) + Convert.ToDouble(assetsrid) + Convert.ToDouble(assetstotothers)).ToString();

            rep = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":{\"entrancefee\":" + liabilitymembershipFee + ",\"totalsavings\":" + liabilitytotalsavings + ",\"totalinterest\":" + liabilitytotalinterest + ",\"totalpenalty\":" + liabilitytotalpenalty + ",\"totalbankinterest\":" + liabilitytotalbankinterest + ",\"totalfundreceived\":" + liabilitytotalfundreceived + ",\"totalothers\":" + liabilitytotalothers + ",\"totalLiability\":" + liabilitytotal + ",\"totalinternallending\":" + assetstotalintlending + ",\"totalcashinhand\":" + assetscashinhand + ",\"totalcashincheque\":" + assetscashinbank + ",\"totalRID\":" + assetsrid + ",\"totalexpenses\":" + assetsshgothers + ",\"totalOthers\":" + assetsexpothers + ",\"totalAssets\":" + assetstotal + "}}";

        }
        catch
        {
            rep = "{\"message\":\"Data not received successfully\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(rep);
    }

    public bool ValidateServerCertificate(
                                          object sender,
                                          X509Certificate certificate,
                                          X509Chain chain,
                                          SslPolicyErrors sslPolicyErrors)
    {
        return true;
    }

    private string CDESendGCMNotification(string apiKey, string postData, string postDataContentType = "application/json")
    {
        ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateServerCertificate);
        apiKey = "AIzaSyB2ssGd1Id4T2AlANre3bM-2lAk5Yy0CkA";

        //  MESSAGE CONTENT
        byte[] byteArray = Encoding.UTF8.GetBytes(postData);


        //  CREATE REQUEST
        HttpWebRequest Request = (HttpWebRequest)WebRequest.Create("https://fcm.googleapis.com/fcm/send");
        Request.Method = "POST";
        Request.KeepAlive = false;
        Request.ContentType = postDataContentType;
        Request.Headers.Add(string.Format("Authorization: key={0}", apiKey));
        Request.ContentLength = byteArray.Length;

        Stream dataStream = Request.GetRequestStream();
        dataStream.Write(byteArray, 0, byteArray.Length);
        dataStream.Close();


        //  SEND MESSAGE
        try
        {
            WebResponse Response = Request.GetResponse();
            HttpStatusCode ResponseCode = ((HttpWebResponse)Response).StatusCode;
            if (ResponseCode.Equals(HttpStatusCode.Unauthorized) || ResponseCode.Equals(HttpStatusCode.Forbidden))
            {
                var text = "Unauthorized - need new token";
            }
            else if (!ResponseCode.Equals(HttpStatusCode.OK))
            {
                var text = "Response from web service isn't OK";
            }

            StreamReader Reader = new StreamReader(Response.GetResponseStream());
            string responseLine = Reader.ReadToEnd();
            Reader.Close();

            return responseLine;
        }
        catch (Exception e)
        {

        }
        return "error";
    }

    private string CDOSendGCMNotification(string apiKey, string postData, string postDataContentType = "application/json")
    {
        ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateServerCertificate);
        apiKey = "AIzaSyA8CJ6sEkCmQoHxiHAIKn7D6z1WpRSOxfg";

        //  MESSAGE CONTENT
        byte[] byteArray = Encoding.UTF8.GetBytes(postData);


        //  CREATE REQUEST
        HttpWebRequest Request = (HttpWebRequest)WebRequest.Create("https://fcm.googleapis.com/fcm/send");
        Request.Method = "POST";
        Request.KeepAlive = false;
        Request.ContentType = postDataContentType;
        Request.Headers.Add(string.Format("Authorization: key={0}", apiKey));
        Request.ContentLength = byteArray.Length;

        Stream dataStream = Request.GetRequestStream();
        dataStream.Write(byteArray, 0, byteArray.Length);
        dataStream.Close();


        //  SEND MESSAGE
        try
        {
            WebResponse Response = Request.GetResponse();
            HttpStatusCode ResponseCode = ((HttpWebResponse)Response).StatusCode;
            if (ResponseCode.Equals(HttpStatusCode.Unauthorized) || ResponseCode.Equals(HttpStatusCode.Forbidden))
            {
                var text = "Unauthorized - need new token";
            }
            else if (!ResponseCode.Equals(HttpStatusCode.OK))
            {
                var text = "Response from web service isn't OK";
            }

            StreamReader Reader = new StreamReader(Response.GetResponseStream());
            string responseLine = Reader.ReadToEnd();
            Reader.Close();

            return responseLine;
        }
        catch (Exception e)
        {

        }
        return "error";
    }

    private string CDMSendGCMNotification(string apiKey, string postData, string postDataContentType = "application/json")
    {
        ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateServerCertificate);
        apiKey = "AIzaSyByrnlMNpCI6gaOlqr7KIj0C5El2vVttBE";

        //  MESSAGE CONTENT
        byte[] byteArray = Encoding.UTF8.GetBytes(postData);


        //  CREATE REQUEST
        HttpWebRequest Request = (HttpWebRequest)WebRequest.Create("https://fcm.googleapis.com/fcm/send");
        Request.Method = "POST";
        Request.KeepAlive = false;
        Request.ContentType = postDataContentType;
        Request.Headers.Add(string.Format("Authorization: key={0}", apiKey));
        Request.ContentLength = byteArray.Length;

        Stream dataStream = Request.GetRequestStream();
        dataStream.Write(byteArray, 0, byteArray.Length);
        dataStream.Close();


        //  SEND MESSAGE
        try
        {
            WebResponse Response = Request.GetResponse();
            HttpStatusCode ResponseCode = ((HttpWebResponse)Response).StatusCode;
            if (ResponseCode.Equals(HttpStatusCode.Unauthorized) || ResponseCode.Equals(HttpStatusCode.Forbidden))
            {
                var text = "Unauthorized - need new token";
            }
            else if (!ResponseCode.Equals(HttpStatusCode.OK))
            {
                var text = "Response from web service isn't OK";
            }

            StreamReader Reader = new StreamReader(Response.GetResponseStream());
            string responseLine = Reader.ReadToEnd();
            Reader.Close();

            return responseLine;
        }
        catch (Exception e)
        {

        }
        return "error";
    }

    //------------------------------------------------------------Get All CDE tasks List-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All CDE Tasks")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_cdetaskslist()
    {
        string s = "";
        List<getcdetasks> rg = new List<getcdetasks>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasardo].[mastercdetasks]", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                rg.Add(new getcdetasks { tasks = "Select CDE Tasks" });
                while (reader.Read())
                {
                    rg.Add(new getcdetasks { id = reader["id"].ToString(), tasks = reader["tasks"].ToString() });
                }
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);

            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" + jsonString2 + "}";

            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";

            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }
    //------------------------------------------------------------Get All CDO Tasks List-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All CDO Tasks")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_cdotaskslist()
    {
        string s = "";
        List<getcdotasks> rg = new List<getcdotasks>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasardo].[mastercdotasks]", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                rg.Add(new getcdotasks { tasks = "Select CDO Tasks" });
                while (reader.Read())
                {
                    rg.Add(new getcdotasks { id = reader["id"].ToString(), tasks = reader["tasks"].ToString() });
                }
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);

            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" + jsonString2 + "}";

            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";

            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }
    //------------------------------------------------------------Get All CDM Tasks List-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All CDM Tasks ")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_cdmtaskslist()
    {
        string s = "";
        List<getcdmtasks> rg = new List<getcdmtasks>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasardo].[mastercdmtasks]", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                rg.Add(new getcdmtasks { tasks = "Select CDM Tasks" });
                while (reader.Read())
                {
                    rg.Add(new getcdmtasks { id = reader["id"].ToString(), tasks = reader["tasks"].ToString() });
                }
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);

            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" + jsonString2 + "}";

            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";

            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    ////-----------------------------------------------------------------Live Tracking------------------------------------------------------------------//
    [WebMethod(Description = "This is to Live tracking")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void livetrack(string uid, string type, string lat, string lon)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            if (type == "1")
            {
                Alib.idExecute("Update [vikasardo].[vikasa].[cde] set lat='" + lat + "', lon='" + lon + "', date='" + System.DateTime.Now.AddHours(5).AddMonths(30).ToString("dd/MM/yyyy hh:mm:ss tt") + "' where cdeid='" + uid + "'");
                status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
            }
            if (type == "2")
            {
                Alib.idExecute("Update [vikasardo].[vikasa].[cdo] set lat='" + lat + "', lon='" + lon + "', date='" + System.DateTime.Now.AddHours(5).AddMonths(30).ToString("dd/MM/yyyy hh:mm:ss tt") + "' where cdoid='" + uid + "'");
                status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
            }
            if (type == "3")
            {
                Alib.idExecute("Update [vikasardo].[vikasa].[cdm] set lat='" + lat + "', lon='" + lon + "', date='" + System.DateTime.Now.AddHours(5).AddMonths(30).ToString("dd/MM/yyyy hh:mm:ss tt") + "' where cdmid='" + uid + "'");
                status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
            }
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //-----------------------------------------------------------------SHG rejected to inactive------------------------------------------------------------------//
    [WebMethod(Description = "This is to Rejected to Inactive")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void RejectedToInactive(string cdeid, string shgid)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("Update [vikasardo].[vikasa].[shg] set status='0' where shgid='" + shgid + "' and cdeid='" + cdeid + "'");
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //-----------------------------------------------------------------Branch to Region------------------------------------------------------------------//
    [WebMethod(Description = "This is to Branch to Region")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void BrachToRegion(string regionID)
    {
        string s = "";
        List<getBankBranch> rg = new List<getBankBranch>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[bankBranch] where regionID='" + regionID + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                rg.Add(new getBankBranch { name = "Select Branch" });
                while (reader.Read())
                {
                    rg.Add(new getBankBranch { id = reader["bankbranchID"].ToString(), name = reader["name"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    /////---------------------------------------------------------OLD SHGS----------------------------------------------------------/////

    //------------------------------------------------------------Get All Old SHGs List-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All Old SHGs list")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_oldshgs_list(string cdeid)
    {
        string s = "";
        List<getoldSHgsList> rg = new List<getoldSHgsList>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasardo].[oldshg] where cdeid='" + cdeid + "' and state='0'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                rg.Add(new getoldSHgsList { name = "Select Old SHG" });
                while (reader.Read())
                {
                    rg.Add(new getoldSHgsList { id = reader["oldshgid"].ToString(), name = reader["Name_of_the_SHG"].ToString(), savingAcc = reader["S_B_Acc_No"].ToString(), branchName = reader["Branch_Name"].ToString(), gender = reader["GENDER"].ToString(), members = reader["Members"].ToString(), village = reader["Name_of_the_village"].ToString(), loanaccnum = reader["Loan_Acc_No"].ToString() });
                }
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);

            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" + jsonString2 + "}";

            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";

            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }


    //------------------------------------------------------------Add Old SHG Loan Details-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Add All Old Loan Details")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void add_oldloan_details(string oldshgid, string shgname, string sbacc, string branch, string gender, string member, string loanDate, string emi, string term, string loanAmt, string targetRecovery, string Prev_LoanBal, string presentRecovery, string totalRecovery, string balRecovery, string date, string challanimg, string cdeid)
    {
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        string status = "";
        try
        {
            if (challanimg != "")
            {
                byte[] imageBytes = Convert.FromBase64String(challanimg);
                MemoryStream ms = new MemoryStream(imageBytes);
                challanimg = RandomNo.Random.GenerateIdentifier(28) + ".png";
                FileStream fs = new FileStream(Server.MapPath("~/challan/" + challanimg), FileMode.Create);
                ms.WriteTo(fs);
                ms.Close();
                fs.Close();
                fs.Dispose();
            }

            string momdt = "";
            string[] momdate = date.Split('/');
            //--------date----------------//
            int first = Convert.ToInt32(momdate[0].Count());
            string first1 = momdate[0].ToString();
            if (first == 1)
            {
                first1 = "0" + first1;
            }

            //-----------month---------------//
            int second = Convert.ToInt32(momdate[1].Count());
            string second1 = momdate[1].ToString();
            if (second == 1)
            {
                second1 = "0" + second1;
            }

            //------------yesr--------------//
            int third = Convert.ToInt32(momdate[2].Count());
            string third1 = momdate[2].ToString();
            if (third == 2)
            {
                third1 = "20" + third1;
            }

            momdt = first1 + "/" + second1 + "/" + third1;
            Alib.idExecute("Update [vikasardo].[vikasardo].[oldshg] set Rec_Amt= Rec_Amt+" + presentRecovery + ", Last_Repayment='" + presentRecovery + "' where oldshgid='" + oldshgid + "' and state='0'");
            Alib.idInsertInto("[vikasardo].[vikasardo].[oldshg_loandetails]", "oldshgid", oldshgid, "Name_of_the_SHG", shgname, "S_B_Acc_No", sbacc, "Branch_Name", branch, "GENDER", gender, "Members", member, "Loan_Date", loanDate, "EMI", emi, "Term", term, "LOAN_AMT", loanAmt, "Target_Recovery", targetRecovery, "Prev_LoanBal", Prev_LoanBal, "Present_Recovery", presentRecovery, "Total_Recovery", totalRecovery, "Balance_Recovery", balRecovery, "status", "0", "meetingDate", momdt);
            Alib.idInsertInto("[vikasardo].[vikasardo].[oldspc]", "oldshgid", oldshgid, "cdeid", cdeid, "shg", shgname, "sbaccno", sbacc, "branch", branch, "members", member, "loandate", loanDate, "emi", emi, "term", term, "loanamt", loanAmt, "targetRecovery", targetRecovery, "prevBal", Prev_LoanBal, "presentRecovery", presentRecovery, "totalRecovery", totalRecovery, "balRecovery", balRecovery, "repaydate", momdt, "challan", challanimg, "status", "0", "spc", "0");
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //------------------------------------------------------------Get All Old Loan List-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All Old Loan list")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_oldloan_details(string shgid)
    {
        string rep = "";
        string loanDate = "";
        string emi = "";
        string term = "";
        string target = "";
        string loanbal = "";
        string loanAmt = "";
        string totalreceived = "";
        string month = "";
        string actualrecovery = "";

        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select [oldshgid], [Region_Name], [Branch_Name], [Name_of_the_SHG],[Name_of_the_village],[Members],[GENDER],[S_B_Acc_No],[Loan_Acc_No],[Loan_Date],[Term],[EMI],[LOAN_AMT],[Rec_Amt],[Last_Repayment],[state],[cdeid],(DATEDIFF(d, convert(datetime, Loan_Date, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30) as DiffMonths FROM [vikasardo].[vikasardo].[oldshg] where oldshgid='" + shgid + "' and state='0'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    month = reader["DiffMonths"].ToString();
                    loanDate = reader["Loan_Date"].ToString();
                    term = reader["Term"].ToString();
                    emi = reader["EMI"].ToString();
                    loanAmt = reader["LOAN_AMT"].ToString();

                    totalreceived = reader["Rec_Amt"].ToString();

                    if (Convert.ToDouble(month) >= Convert.ToDouble(term))
                    {
                        target = (Convert.ToDouble(term) * Convert.ToDouble(emi)).ToString("0.00");
                    }
                    else
                    {
                        target = (Convert.ToDouble(month) * Convert.ToDouble(emi)).ToString("0.00");
                    }
                    loanbal = (Convert.ToDouble(target) - Convert.ToDouble(reader["Rec_Amt"])).ToString("0.00");
                    rep = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":{\"loanDate\":\"" + loanDate + "\",\"emi\":\"" + emi + "\",\"term\":\"" + term + "\",\"termover\":\"" + month + "\",\"targetRecover\":\"" + target + "\",\"loanAmt\":\"" + loanAmt + "\",\"receivedAmount\":\"" + totalreceived + "\",\"loanBal\":\"" + loanbal + "\"}}";
                }
            }
            else
            {
                rep = "{\"message\":\"Data not received successfully\",\"code\":\"200\"}";
            }
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(rep);
        }
        catch (Exception ex)
        {
            rep = "{\"status\":\"fail\"}" + ex;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(rep);
        }
        finally
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
            if (rdr != null)
            {
                rdr.Dispose();
            }
            GC.Collect();
        }
        HttpContext.Current.Response.End();
    }

    //------------------------------------------------------------Get All Added Old Loan List-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All Added Old Loan list")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_addedoldloan_list(string shgid, string cdeid)
    {
        string status = "";

        List<getAddedOldSHgsLoanlist> rg = new List<getAddedOldSHgsLoanlist>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;


            cmd = new SqlCommand("select [id], [oldshgid] ,[Name_of_the_SHG] ,[S_B_Acc_No] ,[Branch_Name] ,[GENDER] ,[Members] ,[Loan_Date] ,[EMI] ,[Term] ,[LOAN_AMT] ,[Target_Recovery] ,[Prev_LoanBal] ,[Present_Recovery] ,[Total_Recovery] ,[Balance_Recovery] ,[status] ,[meetingDate],(DATEDIFF(d, convert(datetime, Loan_Date, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30) as DiffMonths, village FROM [vikasardo].[vikasardo].[oldshg_loandetails] where oldshgid='" + shgid + "'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getAddedOldSHgsLoanlist
                    {
                        shgname = reader["Name_of_the_SHG"].ToString(),
                        sbacc = reader["S_B_Acc_No"].ToString(),
                        branch = reader["Branch_Name"].ToString(),
                        gender = reader["GENDER"].ToString(),
                        members = reader["Members"].ToString(),
                        loandate = reader["Loan_Date"].ToString(),
                        emi = reader["EMI"].ToString(),
                        term = reader["Term"].ToString(),
                        loanamt = reader["LOAN_AMT"].ToString(),
                        targetrecovery = reader["Target_Recovery"].ToString(),
                        prevloanbalance = reader["Prev_LoanBal"].ToString(),
                        presentrecovery = reader["Present_Recovery"].ToString(),
                        totalrecovery = reader["Total_Recovery"].ToString(),
                        balancerecovery = reader["Balance_Recovery"].ToString(),
                        meetingDate = reader["meetingDate"].ToString(),
                        termOver = reader["DiffMonths"].ToString(),
                        village = reader["village"].ToString()
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" + jsonString2 + "}";

            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            status = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(status);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //---------------------------------------------------------CDE LEAVES SERVICES-------------------------------------------------------------------//

    [WebMethod(Description = "This is for CDE leave apply")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cde_applyleave(string cdeid, string leaveType, string No, string from, string to, string name)
    {
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        string status = "";
        try
        {
            string[] leavefrom = from.Split('/');
            //--------date----------------//
            int first = Convert.ToInt32(leavefrom[0].Count());
            string first1 = leavefrom[0].ToString();
            if (first == 1)
            {
                first1 = "0" + first1;
            }

            //-----------month---------------//
            int second = Convert.ToInt32(leavefrom[1].Count());
            string second1 = leavefrom[1].ToString();
            if (second == 1)
            {
                second1 = "0" + second1;
            }

            //------------yesr--------------//
            int third = Convert.ToInt32(leavefrom[2].Count());
            string third1 = leavefrom[2].ToString();
            if (third == 2)
            {
                third1 = "20" + third1;
            }

            string[] leaveto = to.Split('/');
            //--------date----------------//
            int first111 = Convert.ToInt32(leaveto[0].Count());
            string first1111 = leaveto[0].ToString();
            if (first111 == 1)
            {
                first1111 = "0" + first1111;
            }

            //-----------month---------------//
            int second111 = Convert.ToInt32(leaveto[1].Count());
            string second1111 = leavefrom[1].ToString();
            if (second111 == 1)
            {
                second1111 = "0" + second1111;
            }

            //------------yesr--------------//
            int third111 = Convert.ToInt32(leaveto[2].Count());
            string third1111 = leavefrom[2].ToString();
            if (third111 == 2)
            {
                third1111 = "20" + third1111;
            }

            from = first1 + "/" + second1 + "/" + third1;
            to = first1111 + "/" + second1111 + "/" + third1111;

            if (leaveType == "cl")
            {
                string masterclNos = Alib.idGetAFieldByQuery("select days from [vikasardo].[vikasardo].[leaves] where leave='" + leaveType + "'");
                string clNos = Alib.idGetAFieldByQuery("select sum(days) from [vikasardo].[vikasardo].[cdeleave] where ltype='" + leaveType + "' and cdeid='" + cdeid + "' and state='1'");
                if (clNos == "")
                {
                    clNos = "0";
                }
                if (Convert.ToDouble(masterclNos) >= (Convert.ToDouble(clNos) + Convert.ToDouble(No)))
                {
                    Alib.idExecute("insert into [vikasardo].[vikasardo].[cdeleave] (cdeid, ltype, lfrom, lto, days, state,name) values('" + cdeid + "','" + leaveType + "','" + from + "','" + to + "','" + No + "', '0','" + name + "')");
                }
                else
                {
                    status = "{\"message\":\"leaves not available\",\"code\":\"200\"}";
                    return;
                }
            }
            else if (leaveType == "sl")
            {
                string masterslNos = Alib.idGetAFieldByQuery("select days from [vikasardo].[vikasardo].[leaves] where leave='" + leaveType + "'");
                string slNos = Alib.idGetAFieldByQuery("select sum(days) from [vikasardo].[vikasardo].[cdeleave] where ltype='" + leaveType + "' and cdeid='" + cdeid + "' and state='1'");
                if (slNos == "")
                {
                    slNos = "0";
                }
                if (Convert.ToDouble(masterslNos) >= (Convert.ToDouble(slNos) + Convert.ToDouble(No)))
                {
                    Alib.idExecute("insert into [vikasardo].[vikasardo].[cdeleave] (cdeid, ltype, lfrom, lto, days, state, name) values('" + cdeid + "','" + leaveType + "','" + from + "','" + to + "','" + No + "', '0','" + name + "')");
                }
                else
                {
                    status = "{\"message\":\"leaves not available\",\"code\":\"200\"}";
                    return;
                }
            }
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //---------------------------------------------------------CDO LEAVES SERVICES-------------------------------------------------------------------//

    [WebMethod(Description = "This is for CDO leave apply")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdo_applyleave(string cdoid, string leaveType, string No, string from, string to, string name)
    {
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        string status = "";
        try
        {
            string[] leavefrom = from.Split('/');
            //--------date----------------//
            int first = Convert.ToInt32(leavefrom[0].Count());
            string first1 = leavefrom[0].ToString();
            if (first == 1)
            {
                first1 = "0" + first1;
            }

            //-----------month---------------//
            int second = Convert.ToInt32(leavefrom[1].Count());
            string second1 = leavefrom[1].ToString();
            if (second == 1)
            {
                second1 = "0" + second1;
            }

            //------------yesr--------------//
            int third = Convert.ToInt32(leavefrom[2].Count());
            string third1 = leavefrom[2].ToString();
            if (third == 2)
            {
                third1 = "20" + third1;
            }

            string[] leaveto = to.Split('/');
            //--------date----------------//
            int first111 = Convert.ToInt32(leaveto[0].Count());
            string first1111 = leaveto[0].ToString();
            if (first111 == 1)
            {
                first1111 = "0" + first1111;
            }

            //-----------month---------------//
            int second111 = Convert.ToInt32(leaveto[1].Count());
            string second1111 = leavefrom[1].ToString();
            if (second111 == 1)
            {
                second1111 = "0" + second1111;
            }

            //------------yesr--------------//
            int third111 = Convert.ToInt32(leaveto[2].Count());
            string third1111 = leavefrom[2].ToString();
            if (third111 == 2)
            {
                third1111 = "20" + third1111;
            }

            from = first1 + "/" + second1 + "/" + third1;
            to = first1111 + "/" + second1111 + "/" + third1111;

            if (leaveType == "cl")
            {
                string masterclNos = Alib.idGetAFieldByQuery("select days from [Vikasardo].[vikasardo].[leaves] where leave='" + leaveType + "'");
                string clNos = Alib.idGetAFieldByQuery("select sum(days) from [vikasardo].[vikasardo].[cdoleave] where ltype='" + leaveType + "' and cdoid='" + cdoid + "' and state='1'");
                if (clNos == "")
                {
                    clNos = "0";
                }
                if (Convert.ToDouble(masterclNos) >= (Convert.ToDouble(clNos) + Convert.ToDouble(No)))
                {
                    Alib.idExecute("insert into [vikasardo].[vikasardo].[cdoleave] (cdoid, ltype, lfrom, lto, days, state,name) values('" + cdoid + "','" + leaveType + "','" + from + "','" + to + "','" + No + "', '0','" + name + "')");
                }
                else
                {
                    status = "{\"message\":\"leaves not available\",\"code\":\"200\"}";
                    return;
                }
            }
            else if (leaveType == "sl")
            {
                string masterslNos = Alib.idGetAFieldByQuery("select days from [Vikasardo].[vikasardo].[leaves] where leave='" + leaveType + "'");
                string slNos = Alib.idGetAFieldByQuery("select sum(days) from [vikasardo].[vikasardo].[cdoleave] where ltype='" + leaveType + "' and cdoid='" + cdoid + "' and state='1'");
                if (slNos == "")
                {
                    slNos = "0";
                }
                if (Convert.ToDouble(masterslNos) >= (Convert.ToDouble(slNos) + Convert.ToDouble(No)))
                {
                    Alib.idExecute("insert into [vikasardo].[vikasardo].[cdoleave] (cdoid, ltype, lfrom, lto, days, state, name) values('" + cdoid + "','" + leaveType + "','" + from + "','" + to + "','" + No + "', '0','" + name + "')");
                }
                else
                {
                    status = "{\"message\":\"leaves not available\",\"code\":\"200\"}";
                    return;
                }
            }
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //---------------------------------------------------------CDM LEAVES SERVICES-------------------------------------------------------------------//


    [WebMethod(Description = "This is for CDM leave apply")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdm_applyleave(string cdmid, string leaveType, string No, string from, string to, string name)
    {
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        string status = "";
        try
        {
            string[] leavefrom = from.Split('/');
            //--------date----------------//
            int first = Convert.ToInt32(leavefrom[0].Count());
            string first1 = leavefrom[0].ToString();
            if (first == 1)
            {
                first1 = "0" + first1;
            }

            //-----------month---------------//
            int second = Convert.ToInt32(leavefrom[1].Count());
            string second1 = leavefrom[1].ToString();
            if (second == 1)
            {
                second1 = "0" + second1;
            }

            //------------yesr--------------//
            int third = Convert.ToInt32(leavefrom[2].Count());
            string third1 = leavefrom[2].ToString();
            if (third == 2)
            {
                third1 = "20" + third1;
            }

            string[] leaveto = to.Split('/');
            //--------date----------------//
            int first111 = Convert.ToInt32(leaveto[0].Count());
            string first1111 = leaveto[0].ToString();
            if (first111 == 1)
            {
                first1111 = "0" + first1111;
            }

            //-----------month---------------//
            int second111 = Convert.ToInt32(leaveto[1].Count());
            string second1111 = leavefrom[1].ToString();
            if (second111 == 1)
            {
                second1111 = "0" + second1111;
            }

            //------------yesr--------------//
            int third111 = Convert.ToInt32(leaveto[2].Count());
            string third1111 = leavefrom[2].ToString();
            if (third111 == 2)
            {
                third1111 = "20" + third1111;
            }

            from = first1 + "/" + second1 + "/" + third1;
            to = first1111 + "/" + second1111 + "/" + third1111;

            if (leaveType == "cl")
            {
                string masterclNos = Alib.idGetAFieldByQuery("select days from [Vikasardo].[vikasardo].[leaves] where leave='" + leaveType + "'");
                string clNos = Alib.idGetAFieldByQuery("select sum(days) from [vikasardo].[vikasardo].[cdmleave] where ltype='" + leaveType + "' and cdmid='" + cdmid + "' and state='1'");
                if (clNos == "")
                {
                    clNos = "0";
                }
                if (Convert.ToDouble(masterclNos) >= (Convert.ToDouble(clNos) + Convert.ToDouble(No)))
                {
                    Alib.idExecute("insert into [vikasardo].[vikasardo].[cdmleave] (cdmid, ltype, lfrom, lto, days, state, name) values('" + cdmid + "','" + leaveType + "','" + from + "','" + to + "','" + No + "', '0','" + name + "')");
                }
                else
                {
                    status = "{\"message\":\"leaves not available\",\"code\":\"200\"}";
                    return;
                }
            }
            else if (leaveType == "sl")
            {
                string masterslNos = Alib.idGetAFieldByQuery("select days from [Vikasardo].[vikasardo].[leaves] where leave='" + leaveType + "'");
                string slNos = Alib.idGetAFieldByQuery("select sum(days) from [vikasardo].[vikasardo].[cdmleave] where ltype='" + leaveType + "' and cdmid='" + cdmid + "' and state='1'");
                if (slNos == "")
                {
                    slNos = "0";
                }
                if (Convert.ToDouble(masterslNos) >= (Convert.ToDouble(slNos) + Convert.ToDouble(No)))
                {
                    Alib.idExecute("insert into [vikasardo].[vikasardo].[cdmleave] (cdmid, ltype, lfrom, lto, days, state, name) values('" + cdmid + "','" + leaveType + "','" + from + "','" + to + "','" + No + "', '0','" + name + "')");
                }
                else
                {
                    status = "{\"message\":\"leaves not available\",\"code\":\"200\"}";
                    return;
                }
            }
            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //------------------------------------------------------------Get All CDE Leaves from CDO-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All CDE leaves from CDO")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getcde_leaves_CDO(string cdoid)
    {
        string rep = "";
        List<getCDELeave> rg = new List<getCDELeave>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select l.id, l.cdeid, l.ltype, l.lfrom, l.lto, l.days, l.name from [vikasardo].[vikasardo].[cdeleave] l inner join [vikasardo].[vikasa].[cde] c on c.cdeid = l.cdeid where c.cdoid='" + cdoid + "' and l.state='0'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getCDELeave
                    {
                        leaveid = reader["id"].ToString(),
                        cdeid = reader["cdeid"].ToString(),
                        type = reader["ltype"].ToString(),
                        from = reader["lfrom"].ToString(),
                        to = reader["lto"].ToString(),
                        days = reader["days"].ToString(),
                        name = reader["name"].ToString(),
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" + jsonString2 + "}";

            rep = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(rep);
        }
        catch (Exception ex)
        {
            rep = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(rep);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Get All CDE Leaves from CDM-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All CDE leaves from CDM")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getcde_leaves_CDM(string cdmid)
    {
        string rep = "";
        List<getCDELeave> rg = new List<getCDELeave>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select l.id, l.cdeid, l.ltype, l.lfrom, l.lto, l.days, l.name from [vikasardo].[vikasardo].[cdeleave] l inner join [vikasardo].[vikasa].[cde] c on c.cdeid = l.cdeid where c.cdmid='" + cdmid + "' and l.state='0'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getCDELeave
                    {
                        leaveid = reader["id"].ToString(),
                        cdeid = reader["cdeid"].ToString(),
                        type = reader["ltype"].ToString(),
                        from = reader["lfrom"].ToString(),
                        to = reader["lto"].ToString(),
                        days = reader["days"].ToString(),
                        name = reader["name"].ToString(),
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" + jsonString2 + "}";

            rep = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(rep);
        }
        catch (Exception ex)
        {
            rep = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(rep);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }


    //------------------------------------------------------------Get All CDO Leaves from CDM-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All CDO leaves from CDM")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getcdo_leaves_CDM(string cdmid)
    {
        string rep = "";
        List<getCDELeave> rg = new List<getCDELeave>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select l.id, l.cdoid, l.ltype, l.lfrom, l.lto, l.days, l.name from [vikasardo].[vikasardo].[cdoleave] l inner join [Vikasardo].[vikasa].[cdo] c on c.cdoid = l.cdoid where c.cdmid='" + cdmid + "' and l.state='0'", conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new getCDELeave
                    {
                        leaveid = reader["id"].ToString(),
                        cdeid = reader["cdoid"].ToString(),
                        type = reader["ltype"].ToString(),
                        from = reader["lfrom"].ToString(),
                        to = reader["lto"].ToString(),
                        days = reader["days"].ToString(),
                        name = reader["name"].ToString(),
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" + jsonString2 + "}";

            rep = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(rep);
        }
        catch (Exception ex)
        {
            rep = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(rep);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-----------------------------------------------------------------CDE LEAVE APPROVE------------------------------------------------------------------//
    [WebMethod(Description = "This is to CDE LEAVE APPROVE")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdeleave_approve(string cdeid, string leaveid)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("update [vikasardo].[vikasardo].[cdeleave] set state='1' where id='" + leaveid + "'");

            string num = Alib.idGetAFieldByQuery("select cdephone from [vikasardo].[vikasa].[cde] where cdeid='" + cdeid + "'");
            string wish = "Your leave has been approved";
            WebClient Client = new WebClient();
            Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + num);
            Stream data = Client.OpenRead(uri);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            data.Close();
            reader.Close();

            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //-----------------------------------------------------------------CDE LEAVE REJECT------------------------------------------------------------------//
    [WebMethod(Description = "This is to CDE LEAVE REJECT")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdeleave_reject(string cdeid, string leaveid)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("delete from [vikasardo].[vikasardo].[cdeleave] where id='" + leaveid + "'");

            string num = Alib.idGetAFieldByQuery("select cdephone from [vikasardo].[vikasa].[cde] where cdeid='" + cdeid + "'");
            string wish = "Your leave has been rejected";
            WebClient Client = new WebClient();
            Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + num);
            Stream data = Client.OpenRead(uri);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            data.Close();
            reader.Close();

            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //-----------------------------------------------------------------CDO LEAVE APPROVE------------------------------------------------------------------//
    [WebMethod(Description = "This is to CDO LEAVE APPROVE")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdoleave_approve(string cdoid, string leaveid)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("update [vikasardo].[vikasardo].[cdoleave] set state='1' where id='" + leaveid + "'");

            string num = Alib.idGetAFieldByQuery("select cdophone from [vikasardo].[vikasa].[cdo] where cdoid='" + cdoid + "'");
            string wish = "Your leave has been approved";
            WebClient Client = new WebClient();
            Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + num);
            Stream data = Client.OpenRead(uri);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            data.Close();
            reader.Close();

            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //-----------------------------------------------------------------CDO LEAVE REJECT------------------------------------------------------------------//
    [WebMethod(Description = "This is to CDO LEAVE REJECT")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void cdoleave_reject(string cdoid, string leaveid)
    {
        string status = "";
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        try
        {
            Alib.idExecute("delete from [vikasardo].[vikasardo].[cdoleave] where id='" + leaveid + "'");

            string num = Alib.idGetAFieldByQuery("select cdophone from [vikasardo].[vikasa].[cdo] where cdoid='" + cdoid + "'");
            string wish = "Your leave has been rejected";
            WebClient Client = new WebClient();
            Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + num);
            Stream data = Client.OpenRead(uri);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            data.Close();
            reader.Close();

            status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":\"success\"}";
        }
        catch
        {
            status = "{\"message\":\"fail\",\"code\":\"200\"}";
        }
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Write(status);
    }

    //------------------------------------------------------------suresh work---------------------------------------------------------------------------------

    [WebMethod(Description = "This is for Get Due List from New SHGs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getDue_SHG(string cdeid)
    {
        string s = "";
        List<SHGs_Due_Irregular_NPA> rg = new List<SHGs_Due_Irregular_NPA>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("Select id as Sno, shgid as [SHG_ID], shg as SHG, village as Village, term as Terms, emi as EMI, loanamt as Loan, totrepay as Repayment, lastrepay as [Last Pay], repaydate as [Repay Date], balance as Balance, branch as Branch, ((((DATEDIFF(d, convert(datetime, loanaccdate, 103), CONVERT(datetime, '" + System.DateTime.Now.AddHours(5).AddMinutes(30).ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totrepay) as Due from [vikasardo].[vikasardo].[spc] where cdeid='" + cdeid + "' and ((((DATEDIFF(d, convert(datetime, loanaccdate, 103), CONVERT(datetime, '" + System.DateTime.Now.AddHours(5).AddMinutes(30).ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totrepay) >= emi and ((((DATEDIFF(d, convert(datetime, loanaccdate, 103), CONVERT(datetime, '" + System.DateTime.Now.AddHours(5).AddMinutes(30).ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totrepay)<=(emi*2)-1", conn);
            conn.Open();

            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new SHGs_Due_Irregular_NPA { shgid = reader["SHG_ID"].ToString(), name = reader["shg"].ToString(), branchName = reader["branch"].ToString(), receivedAmount = reader["Repayment"].ToString(), balance = reader["Balance"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-----------------------------------------------------SHG IRREGULAR-----------------------------------------------------------//
    [WebMethod(Description = "This is for Get Irregular List from New SHGs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getIrregular_SHG(string cdeid)
    {
        string s = "";
        List<SHGs_Due_Irregular_NPA> rg = new List<SHGs_Due_Irregular_NPA>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("Select id as Sno, shgid as [SHG_ID], shg as SHG, village as Village, term as Terms, emi as EMI, loanamt as Loan, totrepay as Repayment, lastrepay as [Last Pay], repaydate as [Repay Date], balance as Balance, branch, ((((DATEDIFF(d, convert(datetime, loanaccdate, 103), CONVERT(datetime, '" + System.DateTime.Now.AddHours(5).AddMinutes(30).ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totrepay) as Irregular from [vikasardo].[vikasardo].[spc] where cdeid='" + cdeid + "' and ((((DATEDIFF(d, convert(datetime, loanaccdate, 103), CONVERT(datetime, '" + System.DateTime.Now.AddHours(5).AddMinutes(30).ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totrepay) >= (emi*2) and ((((DATEDIFF(d, convert(datetime, loanaccdate, 103), CONVERT(datetime, '" + System.DateTime.Now.AddHours(5).AddMinutes(30).ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totrepay)<=(emi*3)-1", conn);
            conn.Open();

            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new SHGs_Due_Irregular_NPA { shgid = reader["SHG_ID"].ToString(), name = reader["shg"].ToString(), branchName = reader["branch"].ToString(), receivedAmount = reader["Repayment"].ToString(), balance = reader["Balance"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-----------------------------------------------------SHG NPA-----------------------------------------------------------//
    [WebMethod(Description = "This is for Get NPA List from New SHGs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getNPA_SHG(string cdeid)
    {
        string s = "";
        List<SHGs_Due_Irregular_NPA> rg = new List<SHGs_Due_Irregular_NPA>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("Select id as Sno, shgid as [SHG_ID], shg as SHG, village as Village, branch, term as Terms, emi as EMI, loanamt as Loan, totrepay as Repayment, lastrepay as [Last Pay], repaydate as [Repay Date], balance as Balance, ((((DATEDIFF(d, convert(datetime, loanaccdate, 103), CONVERT(datetime, '" + System.DateTime.Now.AddHours(5).AddMinutes(30).ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totrepay) as NPA from [vikasardo].[vikasardo].[spc] where cdeid='" + cdeid + "' and ((((DATEDIFF(d, convert(datetime, loanaccdate, 103), CONVERT(datetime, '" + System.DateTime.Now.AddHours(5).AddMinutes(30).ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totrepay) >= (emi*3)", conn);
            conn.Open();

            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new SHGs_Due_Irregular_NPA { shgid = reader["SHG_ID"].ToString(), name = reader["shg"].ToString(), branchName = reader["branch"].ToString(), receivedAmount = reader["Repayment"].ToString(), balance = reader["Balance"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-------------------------------------------------------------------OLD SHG DUE-----------------------------------------------------//

    [WebMethod(Description = "This is for Get Due List from New SHGs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getDue_OldSHG(string cdeid)
    {
        string s = "";
        List<SHGs_Due_Irregular_NPA> rg = new List<SHGs_Due_Irregular_NPA>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("Select  id as Sno, oldshgid as [SHG_ID], shg as SHG, term as Terms, emi as EMI, loanamt as Loan, totalRecovery as Repayment, presentRecovery as [Last Pay], branch, repaydate as [Repay Date], balRecovery as Balance, ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) as Due from [vikasardo].[vikasardo].[oldspc] where cdeid='" + cdeid + "' and ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) >= emi and ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery)<=(emi*2)-1", conn);
            conn.Open();

            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new SHGs_Due_Irregular_NPA { shgid = reader["SHG_ID"].ToString(), name = reader["shg"].ToString(), branchName = reader["branch"].ToString(), receivedAmount = reader["Repayment"].ToString(), balance = reader["Balance"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-----------------------------------------------------SHG IRREGULAR-----------------------------------------------------------//
    [WebMethod(Description = "This is for Get Irregular List from New SHGs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getIrregular_OldSHG(string cdeid)
    {
        string s = "";
        List<SHGs_Due_Irregular_NPA> rg = new List<SHGs_Due_Irregular_NPA>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("Select id as Sno, oldshgid as [SHG_ID], shg as SHG, term as Terms, emi as EMI, loanamt as Loan, branch, totalRecovery as Repayment, presentRecovery as [Last Pay], repaydate as [Repay Date], balRecovery as Balance, ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) as Irregular from [vikasardo].[vikasardo].[oldspc] where cdeid='" + cdeid + "' and ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) >= (emi*2) and ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery)<=(emi*3)-1", conn);
            conn.Open();

            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new SHGs_Due_Irregular_NPA { shgid = reader["SHG_ID"].ToString(), name = reader["shg"].ToString(), branchName = reader["branch"].ToString(), receivedAmount = reader["Repayment"].ToString(), balance = reader["Balance"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Closed SHGS Status-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All Old SHGs list")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void oldshgs_status_change(string oldshgid, string state)
    {
        string s = "";
        //string oldshg1 = oldshgid;
        //string status1 = state;
        List<getoldSHgsList> rg = new List<getoldSHgsList>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {

            Alib.idExecute("update [vikasardo].[vikasardo].[oldshg] set status= '" + state + "' where oldshgid='" + oldshgid + "'");


            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" +jsonString2 + "}";

            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\"}";

            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //------------------------------------------------------------Closed SHGS Status-----------------------------------------------------------------------//
    [WebMethod(Description = "This is for Get All Old SHGs list")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void oldshgs_closed_status(string cdeid, string query)
    {
        string s = "";
        //string cdeid1 = cdeid;
        //string query1 = query;
        List<getoldSHgsList> rg = new List<getoldSHgsList>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from  [vikasardo].[vikasardo].[oldshg] where cdeid ='" + cdeid + "' and status in " + query, conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                // rg.Add(new getoldSHgsList { name = "Select Old SHG" });
                while (reader.Read())
                {
                    rg.Add(new getoldSHgsList { id = reader["oldshgid"].ToString(), name = reader["Name_of_the_SHG"].ToString(), savingAcc = reader["S_B_Acc_No"].ToString(), branchName = reader["Branch_Name"].ToString(), gender = reader["GENDER"].ToString(), members = reader["Members"].ToString(), village = reader["Name_of_the_village"].ToString(), loanaccnum = reader["Loan_Acc_No"].ToString(), status = reader["status"].ToString() });
                }
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);

            HttpContext.Current.Response.ClearContent();
            //string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + ",\"data1\":" + jsonString1 + ",\"data2\":" +jsonString2 + "}";

            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";

            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //----------------------
    [WebMethod(Description = "This is for Inactive SHGs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void get_inactivedemo_shg(string cdeid)
    {
        string s = "";
        List<getInactiveSHG> rg = new List<getInactiveSHG>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("select * from [vikasardo].[vikasa].[shg] where cdeid='" + cdeid + "' and status in (0,1,2,3)", conn);
            conn.Open();
            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    string shgid = reader["shgid"].ToString();
                    rg.Add(new getInactiveSHG
                    {
                        shgid = shgid,
                        name = reader["name"].ToString(),
                        type = reader["type"].ToString(),
                        village = reader["village"].ToString(),
                        taluka = reader["taluka"].ToString(),
                        region = reader["region"].ToString(),
                        district = reader["district"].ToString(),
                        savingAccount = reader["savingAccount"].ToString(),
                        meetingDate = reader["meetingDate"].ToString(),
                        annualmeetDate = reader["annualmeetDate"].ToString(),
                        annualIncome = reader["annualIncome"].ToString(),
                        expenses = reader["expenses"].ToString(),
                        rid = reader["rid"].ToString(),
                        meetingPlace = reader["meetingPlace"].ToString(),
                        meetingTime = reader["meetingTime"].ToString(),
                        formationDate = reader["formationDate"].ToString(),
                        savingAccOpenDate = reader["savingAccOpenDate"].ToString(),
                        cdeid = reader["cdeid"].ToString(),
                        totalmembshg = reader["totalMember"].ToString(),
                        status = reader["status"].ToString(),
                        reason = reader["reason"].ToString(),
                        branch = reader["branch"].ToString(),
                        totalMember = Alib.idGetAFieldByQuery("select count(memberID) from [vikasardo].[vikasa].members where shgid='" + shgid + "' and cdeid='" + cdeid + "'")
                    });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }

    //-----------------------------------------------------SHG NPA-----------------------------------------------------------//
    [WebMethod(Description = "This is for Get NPA List from New SHGs")]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void getNPA_OldSHG(string cdeid)
    {
        string s = "";
        List<SHGs_Due_Irregular_NPA> rg = new List<SHGs_Due_Irregular_NPA>();
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "true");
        try
        {
            SqlConnection conn = new SqlConnection(Alib.conStr);
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            cmd = new SqlCommand("Select id as Sno, oldshgid as [SHG_ID], shg as SHG, term as Terms, emi as EMI, branch, loanamt as Loan, totalRecovery as Repayment, presentRecovery as [Last Pay], repaydate as [Repay Date], balRecovery as Balance, ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) as NPA from [vikasardo].[vikasardo].[oldspc] where cdeid='" + cdeid + "' and ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) >= (emi*3)", conn);
            conn.Open();

            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rg.Add(new SHGs_Due_Irregular_NPA { shgid = reader["SHG_ID"].ToString(), name = reader["shg"].ToString(), branchName = reader["branch"].ToString(), receivedAmount = reader["Repayment"].ToString(), balance = reader["Balance"].ToString() });
                }
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize(rg);
            HttpContext.Current.Response.ClearContent();
            string status = "{\"message\":\"Data received successfully\",\"code\":\"200\",\"data\":" + jsonString + "}";
            HttpContext.Current.Response.Write(status);
        }
        catch (Exception ex)
        {
            s = "{\"message\":\"fail\"}";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Write(s);
        }
        HttpContext.Current.Response.End();
        GC.Collect();
    }
}



