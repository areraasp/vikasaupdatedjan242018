﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClosedOldSHG.aspx.cs" Inherits="ClosedOldSHG" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="assets/js/jquery.min.js"></script>
    <link href="assets/css/sweetalert.css" rel="stylesheet" />
    <script src="assets/js/sweetalert.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

   
    <script>
        $(function () {
            $('#<%=txtsrch.ClientID%>').autocomplete({
                 source: function (request, response) {
                     $.ajax({
                         url: "ClosedOldSHG.aspx/GetInvName",
                         data: "{ 'pre':'" + request.term + "'}",
                         dataType: "json",
                         type: "POST",
                         contentType: "application/json; charset=utf-8",
                         success: function (d) {
                             var data = JSON.parse(d.d);
                             console.log(data);
                             response($.map(data, function (item) {
                                 return { value: item.tmp }
                             }))
                         },
                         error: function (XMLHttpRequest, textStatus, errorThrown) {
                             alert(textStatus);
                         }
                     });
                 }
             });
         });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     
      <div>
            <div class="form-inline">
                <h3>Old SHGs</h3>
                <asp:TextBox ID="txtsrch" runat="server" class="form-control" placeholder="Enter SHG name / Branch" Width="25%"></asp:TextBox>
                <asp:Button ID="btnsrch" runat="server" CssClass="form-control" Text="Search" Width="10%" OnClick="btnsrch_Click" />
            </div><br />
            <div class="col-lg-12 card-box">
                <div class="table-responsive">
                    <asp:GridView ID="gvsprovider" runat="server" EmptyDataText="No Data Available"  EmptyDataRowStyle-CssClass="text-danger" EmptyDataRowStyle-Font-Size="Medium" CssClass="table table-bordered table-hover table-striped" OnPageIndexChanging="gvsprovider_PageIndexChanging"
                        AllowPaging="true" PageSize="50" AutoGenerateColumns="false" >
                        <Columns>
                              <asp:BoundField DataField="Name_of_the_SHG" HeaderText="OldSHG Name"></asp:BoundField>
                            <asp:BoundField DataField="Region_Name" HeaderText="Region Name"></asp:BoundField>
                            <asp:BoundField DataField="Branch_Name" HeaderText="Branch Name"></asp:BoundField>                       
                            <asp:BoundField DataField="Name_of_the_village" HeaderText="Village"></asp:BoundField>
                            <asp:BoundField DataField="Members" HeaderText="Members"></asp:BoundField>
                            <asp:BoundField DataField="GENDER" HeaderText="GENDER"></asp:BoundField>
                            <asp:BoundField DataField="S_B_Acc_No" HeaderText="SB Account No."></asp:BoundField>
                            <asp:BoundField DataField="Loan_Acc_No" HeaderText="Loan Account No."></asp:BoundField>
                            <asp:BoundField DataField="Loan_Date" HeaderText="Loan_Date"></asp:BoundField>
                            <asp:BoundField DataField="Term" HeaderText="Term"></asp:BoundField>
                            <asp:BoundField DataField="EMI" HeaderText="EMI"></asp:BoundField>
                            <asp:BoundField DataField="LOAN_AMT" HeaderText="Balance Recovery"></asp:BoundField>
                            <asp:BoundField DataField="Rec_Amt" HeaderText="Actual Recovery"></asp:BoundField>
                            <asp:BoundField DataField="Last_Repayment" HeaderText="Last_Repayment"></asp:BoundField>
                            <asp:BoundField DataField="cdeid" HeaderText="cdeid"></asp:BoundField>
                        </Columns>
                        <PagerStyle CssClass="gridview"></PagerStyle>
                    </asp:GridView>
                </div>
            </div>
        </div>
</asp:Content>

