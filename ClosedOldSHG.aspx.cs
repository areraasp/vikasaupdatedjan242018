﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ClosedOldSHG : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        oldshg();
    }
    protected void btnsrch_Click(object sender, EventArgs e)

    {
        if (txtsrch.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter SHG Name or Branch',text: '',timer: 1000,showConfirmButton: false})", true);
            return;
        }
        else
        {
            Glib.LoadRequest(" Select [Region_Name],[Branch_Name],[Name_of_the_SHG],[Name_of_the_village],[Members],[GENDER],[S_B_Acc_No],[Loan_Acc_No],[Loan_Date],[Term],[EMI],[LOAN_AMT],[Rec_Amt],[Last_Repayment],[state] ,[cdeid] from [vikasardo].[vikasardo].[oldshg]  where Name_of_the_SHG  like '" + txtsrch.Text.Trim() + "%' or Branch_Name like '" + txtsrch.Text.Trim() + "%' and status = '4' order by [Name_of_the_SHG] desc", gvsprovider);
        }
    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest(" Select [Region_Name],[Branch_Name],[Name_of_the_SHG],[Name_of_the_village],[Members],[GENDER],[S_B_Acc_No],[Loan_Acc_No],[Loan_Date],[Term],[EMI],[LOAN_AMT],[Rec_Amt],[Last_Repayment],[state],[cdeid] from [vikasardo].[vikasardo].[oldshg]  where [Name_of_the_SHG]  like '" + txtsrch.Text.Trim() + "%' or Branch_Name like '" + txtsrch.Text.Trim() + "%' and status = '4'  order by [Name_of_the_SHG] desc", gvsprovider);

    }

    public void oldshg()
    {
        string qur = " Select [Region_Name],[Branch_Name],[Name_of_the_SHG],[Name_of_the_village],[Members],[GENDER],[S_B_Acc_No],[Loan_Acc_No],[Loan_Date],[Term],[EMI],[LOAN_AMT],[Rec_Amt],[Last_Repayment],[state] ,[cdeid] from [vikasardo].[vikasardo].[oldshg] WHERE status = '4' order by [Name_of_the_village] desc";
        try
        {
            gvsprovider.DataSource = Alib.getData(qur);
            gvsprovider.DataBind();
        }
        catch (Exception ex)
        {
        }
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        string qur = "Select distinct top 10 [Region_Name],[Branch_Name],[Name_of_the_SHG],[Name_of_the_village],[Members],[GENDER],[S_B_Acc_No],[Loan_Acc_No],[Loan_Date],[Term],[EMI],[LOAN_AMT],[Rec_Amt],[Last_Repayment],[state] ,[cdeid] from [vikasardo].[vikasardo].[oldshg]  where Name_of_the_SHG  like '" + pre + "%' or Branch_Name like '" + pre + "%' and status = '4'  order by [Name_of_the_SHG] desc";
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }
    }
}