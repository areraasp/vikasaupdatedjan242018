﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false&libraries=geometry,places&key=AIzaSyDJAKRsK0Tb1gS0MTJu6ns65icUqhwTkmc">
    </script>
    <style>
        a:hover {
            cursor: pointer;
        }
    </style>
    <script>
        var lt = 0, ln = 0, geocoder, ltln, circle, markers, mark1 = [], mark = [], mark2 = [], infowindow, idval, valids, StringMark = [], map, autocomplete, veh = false;

        $(function () {

            $.getJSON("http://ipinfo.io", function (rs) {
                lt = rs.loc.split(',')[0];
                ln = rs.loc.split(',')[1];
                $("#txtadd").val(rs.city + "," + rs.region);
                lmap(lt, ln);
            });
            //----------------------Start----------------------//
            $('a[href="#cde"]').click(function () {
                clearmarkers();
                getmarkers(1);
            });
            $('a[href="#cdo"]').click(function () {
                clearmarkers();
                getmarkers(2);
            });
            $('a[href="#cdm"]').click(function () {
                clearmarkers();
                getmarkers(3);
            });
            $('a[href="#refresh"]').click(function () {
                clearmarkers();
                getmarkers(4);
            });
            //----------------------END----------------------//
        });
        function lmap(lat, lon) {
            geocoder = new google.maps.Geocoder();
            getmarkers(4);
            console.log("asdasdasd");
            autocomplete = new google.maps.places.Autocomplete($('#txtadd').get(0), options);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                map.setCenter(autocomplete.getPlace().geometry.location);
                // circle.setCenter(autocomplete.getPlace().geometry.location);
                lt = autocomplete.getPlace().geometry.location.lat();
                ln = autocomplete.getPlace().geometry.location.lng();
                getmarkers(4);

            });
            var options = {
                componentRestrictions: { country: "IN" }
            };
            var mapOptions = {
                center: new google.maps.LatLng(lt, ln),
                // center: results[0].geometry.location,
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: false
                //draggableCursor: 'url(icon/icon.png),crosshair'
                //  marker:true
            };
            map = new google.maps.Map(document.getElementById("map"), mapOptions);
            geocoder.geocode({ 'address': $("#txtadd").val() }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                }
            });
        }
        function setsizemarker() {
            var bounds = circle.getBounds();
            var lng = bounds.getNorthEast().lng();
            sizemarker.setPosition(new google.maps.LatLng(circle.get('center').lat(), lng));
        }

        var getmkricon = function (status) {
            if (status == "cdm") {
                $("#actids").html(parseInt($("#actids").html()) + 1);
                return 'icon/cdm.png';
            }
            if (status == "cde") {
                $("#runid").html(parseInt($("#runid").html()) + 1);
                return 'icon/cde.png';
            }
            if (status == "cdo") {
                $("#actid").html(parseInt($("#actid").html()) + 1);
                return 'icon/cdo.png';
            }
        }

        function clearmarkers() {
            var i;
            for (i = 0; i < mark.length; i++) {
                mark[i].setMap(null);
            }
            mark = [];
        }
        function getmarkers(type) {

            if (type == 1) {
                url = "cdo.asmx/search?type=1";
            }
            if (type == 2)
                url = "cdo.asmx/search?type=2";

            if (type == 3)
                url = "cdo.asmx/search?type=3";

            if (type == 4)
                url = "cdo.asmx/search?type=4";
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    console.log(response);
                    $("#runid").html("0");
                    $("#actid").html("0");
                    $("#actids").html("0");

                    $('#ttl').html(response.length);

                    infowindow = new google.maps.InfoWindow({});
                    clearmarkers();
                    $.each(response, function (i, val) {
                        var latlon = new google.maps.LatLng(response[i].lat, response[i].lon);
                        var x;
                        x = getmkricon(response[i].state);
                        if (type == 1) {

                            map.setCenter(latlon);
                            //circle.setCenter(latlon);

                            markers = new google.maps.Marker({
                                position: new google.maps.LatLng(response[i].lat, response[i].lon),
                                map: map,
                                animation: google.maps.Animation.DROP,
                                title: response[i].name,
                                icon: x
                            });

                            google.maps.event.addListener(markers, 'mouseover', function () {
                                infowindow.close();
                                infowindow.setContent("<div><br />Name:&nbsp;&nbsp;<b>" + response[i].name + "</b><br />Contact:&nbsp;&nbsp;<b>" + response[i].contact + "</b><br />Last Update:&nbsp;&nbsp;<b>" + response[i].date + "</b></div>");
                                infowindow.open(map, this);
                            });
                            mark.push(markers);
                        }
                        if (type == 2) {

                            map.setCenter(latlon);
                            //circle.setCenter(latlon);

                            markers = new google.maps.Marker({
                                position: new google.maps.LatLng(response[i].lat, response[i].lon),
                                map: map,
                                animation: google.maps.Animation.DROP,
                                title: response[i].name,
                                icon: x
                            });

                            google.maps.event.addListener(markers, 'mouseover', function () {
                                infowindow.close();
                                infowindow.setContent("<div><br />Name:&nbsp;&nbsp;<b>" + response[i].name + "</b><br />Contact:&nbsp;&nbsp;<b>" + response[i].contact + "</b><br />Last Update:&nbsp;&nbsp;<b>" + response[i].date + "</b></div>");
                                infowindow.open(map, this);
                            });
                            mark.push(markers);
                        }
                        if (type == 3) {

                            map.setCenter(latlon);
                            //circle.setCenter(latlon);
                          //  marker.setAnimation(google.maps.Animation.BOUNCE);

                            markers = new google.maps.Marker({
                                position: new google.maps.LatLng(response[i].lat, response[i].lon),
                                map: map,
                                animation: google.maps.Animation.BOUNCE,
                                title: response[i].name,
                                icon: x
                            });

                            google.maps.event.addListener(markers, 'mouseover', function () {
                                infowindow.close();
                                infowindow.setContent("<div><br />Name:&nbsp;&nbsp;<b>" + response[i].name + "</b><br />Contact:&nbsp;&nbsp;<b>" + response[i].contact + "</b><br />Last Update:&nbsp;&nbsp;<b>" + response[i].date + "</b></div>");
                                infowindow.open(map, this);
                            });
                            mark.push(markers);
                        }
                    });

                },
                failure: function (response) {
                    console.log(response.status);
                },
                error: function (response) {
                    console.log(response.status);
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li>
                    <a href="#cde"><span>
                        <img src="icon/cde.png" />&nbsp;&nbsp;CDE(<label id="runid">0</label>)</span></a>
                </li>
                <li>
                    <a href="#cdo"><span>
                        <img src="icon/cdo.png" />&nbsp;&nbsp;CDO(<label id="actid">0</label>)</span></a>
                </li>
                <li>
                    <a href="#cdm"><span>
                        <img src="icon/cdm.png" />&nbsp;&nbsp;CDM(<label id="actids">0</label>)</span></a>
                </li>
                <li style="display: none;">
                    <a href="#innactive"><span>
                        <img src="icon/offline.png" />&nbsp;&nbsp;Innactive(<label id="parid">0</label>)</span></a>
                </li>
                <li style="display: none;">
                    <a href="#sp"><span>
                        <img src="icon/sp.png" />&nbsp;&nbsp;Serive Providers(<label id="sp">0</label>)</span></a>
                </li>
                <li style="display: none;">
                    <a href="#pa"><span>
                        <img src="icon/parking.png" />&nbsp;&nbsp;Parking (<label id="pa">0</label>)</span></a>
                </li>
                <li style="display: none;">
                    <a href="#hs"><span>
                        <img src="icon/hospital.png" />&nbsp;&nbsp;Hospital (<label id="hs">0</label>)</span></a>
                </li>
                <li style="display: none;">
                    <a href="#refresh"><i class="fa fa-refresh "></i>&nbsp;&nbsp;All</a>
                </li>
                <li style="display: none;">
                    <a href="#total"><i class="fa fa-cubes"></i>&nbsp;&nbsp;Total(<label id="ttl">0</label>)</a>
                </li>
            </ol>
            <div class="row">
                <div class="col-lg-6">
                    <input type="text" class="form-control" id="txtadd" placeholder="Enter Address" /><input type="hidden" id="txtltn" />
                </div>
                <div class="form-inline col-lg-6">
                    <asp:DropDownList ID="ddlspdetails" runat="server" class="form-control" Visible="false"></asp:DropDownList>
                    <%--   <input type="button" class="btn btn-default" value="Search" name="search" onclick="getvehicle(1);" />--%>
                </div>
            </div>
        </div>
    </div>
    <br />
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="m-t-0 m-b-20 header-title"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;&nbsp; <a href="#refresh"><i class="fa fa-refresh "></i>&nbsp;&nbsp;Refresh</a></h4>
                <div id="map" style="width: 100%; height: 500px"></div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</asp:Content>




