﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class PendingOldSHG : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        oldshg();
    }
    protected void btnsrch_Click(object sender, EventArgs e)

    {
        if (txtsrch.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter SHG Name or Branch',text: '',timer: 1000,showConfirmButton: false})", true);
            return;
        }
        else
        {
            Glib.LoadRequest(" Select [Region_Name],[Branch_Name],[Name_of_the_SHG],[Name_of_the_village],[Members],[GENDER],[S_B_Acc_No],[Loan_Acc_No],[Loan_Date],[Term],[EMI],[LOAN_AMT],[Rec_Amt],[Last_Repayment],[state] ,[cdeid] from [vikasardo].[vikasardo].[oldshg]  where Name_of_the_SHG  like '" + txtsrch.Text.Trim() + "%' or Branch_Name like '" + txtsrch.Text.Trim() + "%' and status = '3' order by [Name_of_the_SHG] desc", gvsprovider);
        }
    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest(" Select [Region_Name],[Branch_Name],[Name_of_the_SHG],[Name_of_the_village],[Members],[GENDER],[S_B_Acc_No],[Loan_Acc_No],[Loan_Date],[Term],[EMI],[LOAN_AMT],[Rec_Amt],[Last_Repayment],[state],[cdeid] from [vikasardo].[vikasardo].[oldshg]  where [Name_of_the_SHG]  like '" + txtsrch.Text.Trim() + "%' or Branch_Name like '" + txtsrch.Text.Trim() + "%' and status = '3'  order by [Name_of_the_SHG] desc", gvsprovider);

    }

    public void oldshg()
    {
        string qur = @"SELECT [oldshgid]
                    , [Name_of_the_SHG]
                    , [Region_Name] 
                    , [Branch_Name] 
                    , [Name_of_the_village] 
                    , [Members] 
                    , [GENDER] 
                    , [S_B_Acc_No] 
                    , [Loan_Acc_No] 
                    , [Loan_Date] 
                    , [Term] 
                    , [EMI] 
                    , [LOAN_AMT] 
                    , [Rec_Amt] 
                    , [LASt_Repayment] 
                    , [cdeid]  FROM[vikasardo].[oldshg] where status ='3'
                      order by[Name_of_the_village] desc";
        try
        {
            gvsprovider.DataSource = Alib.getData(qur);
            gvsprovider.DataBind();
        }
        catch (Exception ex)
        {
        }
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        string qur = "Select distinct top 10 Name_of_the_SHG, [Region_Name],[Branch_Name],[Name_of_the_village],[Members],[GENDER],[S_B_Acc_No],[Loan_Acc_No],[Loan_Date],[Term],[EMI],[LOAN_AMT],[Rec_Amt],[Last_Repayment],[state] ,[cdeid] from [vikasardo].[vikasardo].[oldshg]  where Name_of_the_SHG  like '" + pre + "%' or Branch_Name like '" + pre + "%' and status = '3' order by [Name_of_the_SHG] desc";
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                Dictionary<string, object> row;
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }

    }

    protected void lnkapprove_Click(object sender, EventArgs e)
    {

        LinkButton link = sender as LinkButton;
        if (link.Text == "")
        {

        }
        else
        {
            //Looping through each Gridview row to find exact Row 
            //of the Grid from where the SelectedIndex change event is fired.
            foreach (GridViewRow row in gvsprovider.Rows)
            {
                //Finding Dropdown control  
                Control ctrl = row.FindControl("lnkapprove") as LinkButton;
                if (ctrl != null)
                {
                    LinkButton link1 = (LinkButton)ctrl;

                    //Comparing ClientID of the LinkButton with sender
                    //  if(ddl1.SelectedIndex==0
                    if (link.ClientID == link1.ClientID)
                    {
                        if (link.Text != "")
                        {
                            string oldShgId = row.Cells[2].Text;

                            //To accept old SHG and updating status = 4
                            string confirmValue = Request.Form["confirm_value"];
                            if (confirmValue == "Yes")
                            {
                                Alib.idExecute("UPDATE [vikasardo].[oldshg] SET status = '4' WHERE [oldshgid] = '"+oldShgId+"'");
                                Glib.LoadRequest("SELECT [oldshgid], [Name_of_the_SHG], [Region_Name], [Branch_Name], [Name_of_the_village], [Members], [GENDER], [S_B_Acc_No], [Loan_Acc_No], [Loan_Date], [Term], [EMI], [LOAN_AMT], [Rec_Amt], [LASt_Repayment], [cdeid]  FROM[vikasardo].[oldshg] where status = '3'                                                     order by[Name_of_the_village] desc", gvsprovider);
                                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Approved Successfully..!!')", true);
                            }
                        }
                        else
                        {
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked No..!!')", true);
                        }
                    }
                }
            }
        }
    }


    //To reject old SHG and updating status = 0
    protected void linkrej_Click(object sender, EventArgs e)
    {
        LinkButton link = sender as LinkButton;
        if (link.Text == "")
        {

        }
        else
        {
            //Looping through each Gridview row to find exact Row 
            //of the Grid from where the SelectedIndex change event is fired.
            foreach (GridViewRow row in gvsprovider.Rows)
            {
                //Finding Dropdown control  
                Control ctrl = row.FindControl("linkrej") as LinkButton;
                if (ctrl != null)
                {
                    LinkButton link1 = (LinkButton)ctrl;

                    //Comparing ClientID of the LinkButton with sender
                    //  if(ddl1.SelectedIndex==0
                    if (link.ClientID == link1.ClientID)
                    {
                        if (link.Text != "")
                        {
                            string oldShgId = row.Cells[2].Text;

                            //To reject old SHG and updating status = 0
                            string confirmValue = Request.Form["confirm_value"];
                            if (confirmValue == "Yes")
                            {
                                Alib.idExecute("UPDATE [vikasardo].[oldshg] SET status = '0' WHERE [oldshgid] = '" + oldShgId + "'");
                                Glib.LoadRequest("SELECT [oldshgid], [Name_of_the_SHG], [Region_Name], [Branch_Name], [Name_of_the_village], [Members], [GENDER], [S_B_Acc_No], [Loan_Acc_No], [Loan_Date], [Term], [EMI], [LOAN_AMT], [Rec_Amt], [LASt_Repayment], [cdeid]  FROM[vikasardo].[oldshg] where status = '3'                                                     order by[Name_of_the_village] desc", gvsprovider);

                                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Rejected Successfully..!!')", true);
                            }
                        }
                        else
                        {
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked No..!!')", true);
                        }
                    }
                }
            }
        }
    }
}
