﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="approved.aspx.cs" Inherits="approved" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery-ui.js"></script>
    <link href="assets/js/jquery-ui.min.css" rel="stylesheet" />
    <link href="assets/css/sweetalert.css" rel="stylesheet" />
    <script src="assets/js/sweetalert.min.js"></script>

      <script type="text/javascript">

          function Confirm() {
              var confirm_value = document.createElement("INPUT");
              confirm_value.type = "hidden";
              confirm_value.name = "confirm_value";
              if (confirm("Are You Sure..?")) {
                  confirm_value.value = "Yes";
              } else {
                  confirm_value.value = "No";
              }
              document.forms[0].appendChild(confirm_value);
          }
          function Confirm1() {
              var confirm_value = document.createElement("INPUT");
              confirm_value.type = "hidden";
              confirm_value.name = "confirm_value";
              if (confirm("Are You Sure you want to generate PDF..?")) {
                  confirm_value.value = "Yes";
              } else {
                  confirm_value.value = "No";
              }
              document.forms[0].appendChild(confirm_value);
          }

</script>
     <script>
         $(function () {
             $('#<%=txtsrch.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "approved.aspx/GetInvName",
                        data: "{ 'pre':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (d) {
                            var data = JSON.parse(d.d);
                            console.log(data);
                            response($.map(data, function (item) {
                                return { value: item.tmp }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                }
            });
        });
    </script>
    <style>
          .ui-widget-content {
            z-index: 2000;
        }

        .ui-autocomplete {
            display: none;
            width: 201px;
            top: 289.717px;
            left: 390.5px;
            height: 300px;
            overflow: scroll;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:HiddenField ID="lbllatlon" runat="server"></asp:HiddenField>
    <asp:Label ID="lblid" runat="server" Visible="false"></asp:Label>
    <asp:HiddenField ID="hf1" runat="server" Visible="false" />
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Approved Proposals</h4>
        </div>
    </div>
    <!--/ .page title -->

    <div class="row">
        <br />
        <div class="form-inline">
            <asp:TextBox ID="txtsrch" runat="server" class="form-control" placeholder="Enter SHG name / Branch" Width="25%"></asp:TextBox>
            <asp:Button ID="btnsrch" runat="server" CssClass="form-control" Text="Search" Width="10%" OnClick="btnsrch_Click" />
        </div>
    </div>
    <br />
    <br />
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="row">
                <div class="col-lg-5 card-box">
                    <div class="form-group">
                        <label>Enter Service Provider Name</label>
                        <asp:TextBox ID="txtspname" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Enter Primary contact Number</label>
                        <asp:TextBox ID="txtphone" runat="server" class="form-control"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" SetFocusOnError="True"
                            ControlToValidate="txtphone" ErrorMessage="Must be Valid 10 digit" ForeColor="Red"
                            ValidationExpression="^[7-9][0-9]{9}$" Font-Size="Small"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-group">
                        <label>Enter Email ID</label>
                        <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                            runat="server" ErrorMessage="Please Enter Valid Email ID"
                            ValidationGroup="vgSubmit" ControlToValidate="txtEmail"
                            CssClass="requiredFieldValidateStyle"
                            ForeColor="Red"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                        </asp:RegularExpressionValidator>
                    </div>

                    <div class="form-group">
                        <label>Enter Address</label>
                        <asp:TextBox ID="txtadd" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-lg btn-default btn-custom" Text="Submit" />
                        <asp:Button ID="btncancel" runat="server" CssClass="btn btn-lg btn-danger btn-custom" Text="Cancel" />
                    </div>
                </div>
                <div class="col-lg-7">
                </div>
            </div>
            <!-- /.row -->
        </asp:View>
    </asp:MultiView>

    <%--<div class="col-lg-12 card-box">
        <div class="table-responsive">
            <asp:GridView ID="gvsprovider" runat="server" EmptyDataText="No New Proposals" EmptyDataRowStyle-CssClass="text-danger" EmptyDataRowStyle-Font-Size="Medium" CssClass="table table-bordered table-hover table-striped"  OnPageIndexChanging="gvsprovider_PageIndexChanging" OnRowCommand="gvsprovider_RowCommand"
                  AllowPaging="true" PageSize="10">
                <Columns>
                   <%-- <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkapprove" runat="server"  ControlStyle-CssClass="btn btn-default btn-custom"
                                CommandArgument='<%#Eval("ID") %>'
                                Text="Approve"
                                CommandName="approve"
                                OnClientClick="Confirm()" OnClick="lnkapprove_Click">Approve</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkrej" runat="server"  ControlStyle-CssClass="btn btn-danger"
                                CommandArgument='<%#Eval("ID") %>'
                                Text="Reject"
                                CommandName="approve"
                                OnClientClick="Confirm()" OnClick="lnkrej_Click">Reject</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Training Report Image">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" data-fancybox-group="gallery" class="fancybox"
                                NavigateUrl='<%# Eval("reportimage","Reportimages/{0}") %>'>
                                <asp:Image ID="picture" CssClass="img-zoom" runat="server" Width="50px" Height="50px" ImageUrl='<%# Eval("reportimage","Reportimages/{0}") %>' />
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Training Photo Image">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink2" runat="server" data-fancybox-group="gallery" class="fancybox"
                                NavigateUrl='<%# Eval("photoimage","Photoimages/{0}") %>'>
                                <asp:Image ID="picture2" CssClass="img-zoom" runat="server" Width="50px" Height="50px" ImageUrl='<%# Eval("photoimage","Photoimages/{0}") %>' />
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <PagerStyle CssClass="gridview"></PagerStyle>
            </asp:GridView>
        </div>
    </div>--%>

    
    <div class="col-lg-12 card-box">
        <div class="table-responsive">
            <asp:GridView ID="gvsprovider" runat="server" EmptyDataText="No New Proposals" EmptyDataRowStyle-CssClass="text-danger" EmptyDataRowStyle-Font-Size="Medium" CssClass="table table-bordered table-hover table-striped" OnPageIndexChanging="gvsprovider_PageIndexChanging"
                OnRowCommand="gvsprovider_RowCommand" AutoGenerateColumns="false" AllowPaging="true" PageSize="10">
                <Columns>
                    <%--  <asp:ButtonField CommandName="edi" ControlStyle-CssClass="btn btn-default btn-custom" ButtonType="Button"
                        Text="Edit" HeaderText="Edit "></asp:ButtonField>--%>
                    <%-- <asp:ButtonField CommandName="Del" ControlStyle-CssClass="btn btn-danger" ButtonType="Button"
                        Text="Delete" HeaderText="Delete "></asp:ButtonField>--%>
                    
                    <asp:TemplateField HeaderText="Download">
                        <ItemTemplate>
                            <asp:LinkButton ID="linkpdf" runat="server" ControlStyle-CssClass="btn btn-default btn-custom"
                                CommandArgument='<%#Eval("ShgID") %>'
                                Text="PDF"
                                CommandName="download"
                                OnClientClick="Confirm1()" OnClick="linkpdf_Click">PDF</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ShgID" HeaderText="ShgID"></asp:BoundField>
                    <asp:BoundField DataField="ShgName" HeaderText="ShgName"></asp:BoundField>
                    <asp:BoundField DataField="CDE_ID" HeaderText="CDE_ID"></asp:BoundField>
                    <asp:BoundField DataField="ShgLoanAmount" HeaderText="ShgLoanAmount"></asp:BoundField>
                    <asp:BoundField DataField="shgaccnum" HeaderText="SB Account"></asp:BoundField>
                    <asp:BoundField DataField="Reason" HeaderText="Reason"></asp:BoundField>
                    <asp:BoundField DataField="BankBranch" HeaderText="BankBranch"></asp:BoundField>

                    <asp:BoundField DataField="Split_Type" HeaderText="Split_Type"></asp:BoundField>
                    <asp:BoundField DataField="LoanEnqNo" HeaderText="LoanEnqNo"></asp:BoundField>
                    <asp:BoundField DataField="proposaldate" HeaderText="Proposal Date"></asp:BoundField>
                    <asp:TemplateField HeaderText="Training Report Image">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" data-fancybox-group="gallery" class="fancybox"
                                NavigateUrl='<%# Eval("reportimage","Reportimages/{0}") %>'>
                                <asp:Image ID="picture" CssClass="img-zoom" runat="server" Width="50px" Height="50px" ImageUrl='<%# Eval("reportimage","Reportimages/{0}") %>' />
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Training Photo Image">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink2" runat="server" data-fancybox-group="gallery" class="fancybox"
                                NavigateUrl='<%# Eval("photoimage","Photoimages/{0}") %>'>
                                <asp:Image ID="picture2" CssClass="img-zoom" runat="server" Width="50px" Height="50px" ImageUrl='<%# Eval("photoimage","Photoimages/{0}") %>' />
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="gridview"></PagerStyle>
            </asp:GridView>
        </div>
    </div>


</asp:Content>

