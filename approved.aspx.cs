﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class approved : System.Web.UI.Page
{
    int newnum;
    string loanrefid = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //    select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from shgexternalloansplit where id in (SELECT max(id) from shgexternalloansplit group by  loan_enq_num) and cdeid='" + cdeid + "' and approvedstate='1' and status='2'
            //Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, momdate as MomDate, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status='7' and approvedstate='7'", gvsprovider);
            Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, shgaccnum as shgaccnum, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status='7' and approvedstate='7'", gvsprovider);
        }

    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (txtsrch.Text == "")
        {
            gvsprovider.PageIndex = e.NewPageIndex;
            //Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, momdate as MomDate, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status='7' and approvedstate='7'", gvsprovider);
            Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, shgaccnum as shgaccnum, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status='7' and approvedstate='7'", gvsprovider);
        }
        else
        {
            gvsprovider.PageIndex = e.NewPageIndex;
            //Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, momdate as MomDate, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status='7' and approvedstate='7' and (shgname='" + txtsrch.Text + "' or bankbrach='" + txtsrch.Text + "')", gvsprovider);
            Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, shgaccnum as shgaccnum, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status='7' and approvedstate='7' and (shgname='" + txtsrch.Text + "' or bankbrach='" + txtsrch.Text + "')", gvsprovider);
        }

    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        //string qur = "Select name as tmp from [vikasardo].[vikasa].[shg] where name like '" + pre + "%' ";
        string qur = "select distinct shgname as tmp from[vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from[vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status = '7' and approvedstate = '7' and shgname like '" + pre + "%' ";

        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void btnsrch_Click(object sender, EventArgs e)
    {
        if (txtsrch.Text == "")
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter SHG Name or Branch',text: '',timer: 1000,showConfirmButton: false})", true);
            //Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, momdate as MomDate, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status='7' and approvedstate='7'", gvsprovider);
            Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, shgaccnum as shgaccnum, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status='7' and approvedstate='7'", gvsprovider);
            return;
        }
        else
        {
           //Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, momdate as MomDate, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status='7' and approvedstate='7' and (shgname='" + txtsrch.Text + "' or bankbrach='" + txtsrch.Text + "')", gvsprovider);
             Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, shgaccnum as shgaccnum, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status='7' and approvedstate='7' and (shgname='" + txtsrch.Text + "' or bankbrach='" + txtsrch.Text + "')", gvsprovider);
        }
    }

    private string generatenum()
    {

        int oldnum = Convert.ToInt32(Alib.idGetAFieldByQuery("select max(number) from [vikasardo].[vikasa].sponsernum"));
        newnum = oldnum + 1;
        Alib.idInsertInto("sponsernum", "number", newnum.ToString());
        String numb = "";
        if (newnum.ToString().Length == 1)
        {
            numb = "000" + newnum.ToString();
        }
        else if (newnum.ToString().Length == 2)
        {
            numb = "00" + newnum.ToString();
        }
        else if (newnum.ToString().Length == 3)
        {
            numb = "0" + newnum.ToString();
        }
        else if (newnum.ToString().Length == 4)
        {
            numb = newnum.ToString();
        }
        string pattern = "VRDO/SBIPROP/17-18/" + numb;
        hf1.Value = pattern;

        return pattern;
    }

    protected void linkpdf_Click(object sender, EventArgs e)
    {
        string dateid = "";
        Random r = new Random();
        int tmp = r.Next(1000, 9999);
        dateid = System.DateTime.Now.Day.ToString() + "" + System.DateTime.Now.Month.ToString();
        loanrefid = generatenum();
        Session["approved"] = "";
        LinkButton link = sender as LinkButton;
        if (link.Text == "")
        {

        }
        else
        {
            //Looping through each Gridview row to find exact Row 
            //of the Grid from where the SelectedIndex change event is fired.
            foreach (GridViewRow row in gvsprovider.Rows)
            {
                //Finding Dropdown control  
                Control ctrl = row.FindControl("linkpdf") as LinkButton;
                if (ctrl != null)
                {
                    LinkButton link1 = (LinkButton)ctrl;

                    //Comparing ClientID of the LinkButton with sender
                    //  if(ddl1.SelectedIndex==0
                    if (link.ClientID == link1.ClientID)
                    {
                        if (link.Text != "")
                        {
                            string confirmValue = Request.Form["confirm_value"];
                            Session["shgname"] = row.Cells[2].Text;
                            Session["shgid"] = row.Cells[1].Text;
                            Session["cdeid"] = row.Cells[3].Text;
                            Session["shgloanamt"] = row.Cells[4].Text;
                            Session["bankbranch"] = row.Cells[7].Text;
                            Session["loanenqid"] = row.Cells[9].Text;

                            if (confirmValue == "Yes")
                            {
                                Session["sponsnum"] = loanrefid;
                                Session["approved"] = "approved";
                                Response.Redirect("pdf.aspx");
                            }
                        }
                        else
                        {
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked No..!!')", true);
                        }
                    }
                }
            }
        }
    }
}