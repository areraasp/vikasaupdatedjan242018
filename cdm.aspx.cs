﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cdm : System.Web.UI.Page
{
    int index;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Glib.LoadRequest("Select m.cdmid, m.employment, m.cdmname, m.cdmphone, m.cdmmail, m.cdmphoto, count(*) as CDOs from [vikasardo].[vikasa].cdm m inner join [vikasardo].[vikasa].cdo o on cast(m.cdmid as varchar(50)) = cast(o.cdmid as varchar(50)) group by m.cdmid, m.employment, m.cdmname, m.cdmphone, m.cdmmail, m.cdmphoto ", gvsprovider);
        }
    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select m.cdmid, m.employment, m.cdmname, m.cdmphone, m.cdmmail, m.cdmphoto, count(*) as CDOs from [vikasardo].[vikasa].cdm m inner join [vikasardo].[vikasa].cdo o on cast(m.cdmid as varchar(50)) = cast(o.cdmid as varchar(50)) group by m.cdmid, m.employment, m.cdmname, m.cdmphone, m.cdmmail, m.cdmphoto ", gvsprovider);
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Del"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            lblid.Text = id;
            Alib.idExecute("delete from [vikasardo].[vikasa].cdm where cdmid=" + id);
            Glib.LoadRequest("Select m.cdmid, m.employment, m.cdmname, m.cdmphone, m.cdmmail, m.cdmphoto, count(*) as CDOs from [vikasardo].[vikasa].cdm m inner join [vikasardo].[vikasa].cdo o on cast(m.cdmid as varchar(50)) = cast(o.cdmid as varchar(50)) group by m.cdmid, m.employment, m.cdmname, m.cdmphone, m.cdmmail, m.cdmphoto ", gvsprovider);
        }
        if (e.CommandName.Equals("edi"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            lblid.Text = id;
            MultiView1.ActiveViewIndex = 0;
            btnsubmit.Text = "Update";
            string radiobuttonText = HttpUtility.HtmlDecode(rows.Cells[3].Text).ToString();
            if (radiobuttonText.ToString() == "Contract")
            {
                rad1.Checked = true;
            }
            else
            {
                rad2.Checked = true;
            }
            txtname.Text = HttpUtility.HtmlDecode(rows.Cells[4].Text).ToString();
            txtcontact.Text = HttpUtility.HtmlDecode(rows.Cells[5].Text).ToString();
            txtemail.Text = HttpUtility.HtmlDecode(rows.Cells[6].Text).ToString();
        }
        if (e.CommandName.Equals("view"))
        {
            index = Convert.ToInt32(e.CommandArgument);

            string qur = "Select o.cdoid, o.employment, o.cdoname, o.cdophone, o.cdomail, o.cdmid, o.cdophoto, count(*) as CDEs from [vikasardo].[vikasa].cdo o inner join [vikasardo].[vikasa].cde e on cast(e.cdoid as varchar(50)) = cast(o.cdoid as varchar(50)) where o.cdmid='" + index + "' group by o.cdoid, o.employment, o.cdoname, o.cdophone, o.cdomail, o.cdmid, o.cdophoto";
            try
            {
                GridView1.DataSource = Alib.getData(qur);
                GridView1.DataBind();
            }
            catch (Exception ex)
            {
            }
        }
    }
    protected void Button2_ServerClick(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
        btnsubmit.Text = "Submit";
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        string rad = "";
        string fname = "";
        if (rad1.Checked != true)
        {
            if (rad2.Checked != true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Select Any Type Permanent/ Freelancer',text: '',timer: 2000,showConfirmButton: false})", true);
                return;
            }
        }
        if (txtname.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Name',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtcontact.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Contact Number',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtemail.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Email Address',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (fpphoto.HasFile)
        {
            fname = fpphoto.FileName;
            fpphoto.SaveAs(Server.MapPath("~/images/") + fname);
        }
        if (rad1.Checked == true)
        {
            rad = rad1.Text.ToString();
        }
        else
        {
            rad = rad1.Text.ToString();
        }
        if (btnsubmit.Text == "Submit")
        {
            if (Alib.idHasRows("SELECT cdmphone FROM [vikasardo].[vikasa].[cdm] WHERE cdmphone='" + txtcontact.Text + "'"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'CDM Exist With  '+'" + txtcontact.Text + "',text: '',timer: 3000,showConfirmButton: false})", true);
                return;
            }
            else
            {
                Alib.idInsertInto("[vikasardo].[vikasa].[cdm]", "employment", rad.ToString(), "cdmname", txtname.Text, "cdmphone", txtcontact.Text, "cdmmail", txtemail.Text, "cdmphoto", fname);
            }
        }
        else
        {
            if (!fpphoto.HasFile)
            {
                Alib.idExecute("Update [vikasardo].[vikasa].[cdm] set employment= '" + rad.ToString() + "', cdmname= '" + txtname.Text + "', cdmphone= '" + txtcontact.Text + "', cdmmail= '" + txtemail.Text + "' where cdeid='" + lblid.Text + "'");
            }
            else
            {
                Alib.idExecute("Update [vikasardo].[vikasa].[cdm] set employment= '" + rad.ToString() + "', cdmname= '" + txtname.Text + "', cdmphone= '" + txtcontact.Text + "', cdmmail= '" + txtemail.Text + "', cdmphoto= '" + fname + "' where cdeid='" + lblid.Text + "'");
            }
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Successfully Updated',text: '',timer: 2000,showConfirmButton: false})", true);
        Glib.LoadRequest("Select m.cdmid, m.employment, m.cdmname, m.cdmphone, m.cdmmail, m.cdmphoto, count(*) as CDOs from [vikasardo].[vikasa].cdm m inner join [vikasardo].[vikasa].cdo o on cast(m.cdmid as varchar(50)) = cast(o.cdmid as varchar(50)) group by m.cdmid, m.employment, m.cdmname, m.cdmphone, m.cdmmail, m.cdmphoto ", gvsprovider);
        clean();
        return;
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = -1;
        clean();
    }
    public void clean()
    {
        btnsubmit.Text = "Submit";
        txtname.Text = txtemail.Text = txtcontact.Text = "";
        rad1.Checked = false;
        rad2.Checked = false;
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select o.cdoid, o.employment, o.cdoname, o.cdophone, o.cdomail, o.cdmid, o.cdophoto, count(*) as CDEs from [vikasardo].[vikasa].cdo o inner join [vikasardo].[vikasa].cde e on cast(e.cdoid as varchar(50)) = cast(o.cdoid as varchar(50 )) where o.cdmid='" + index + "' group by o.cdoid, o.employment, o.cdoname, o.cdophone, o.cdomail, o.cdmid, o.cdophoto", GridView1);
    }
    protected void btnsrch_Click(object sender, EventArgs e)
    {
        if (txtsrch.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter SHG Name or Branch',text: '',timer: 1000,showConfirmButton: false})", true);
            return;
        }
        else
        {
            Glib.LoadRequest("Select m.cdmid, m.employment, m.cdmname, m.cdmphone, m.cdmmail, m.cdmphoto, count(*) as CDOs from [vikasardo].[vikasa].cdm m inner join [vikasardo].[vikasa].cdo o on cast(m.cdmid as varchar(50)) = cast(o.cdmid as varchar(50)) where m.cdmname like '" + txtsrch.Text + "%' or m.cdmphone like '" + txtsrch.Text + "%' group by m.cdmid, m.employment, m.cdmname, m.cdmphone, m.cdmmail, m.cdmphoto ", gvsprovider);

        }
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        string qur = "Select top 10 cdmname as tmp from [vikasardo].[vikasa].[cdm] where cdmname like '" + pre + "%' union Select top 10 cdmphone as tmp from [vikasardo].[vikasa].cdm where cdmphone like '" + pre + "%'";
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }
    }
}