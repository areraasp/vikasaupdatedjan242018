﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class spc : System.Web.UI.Page
{

    // status 0 for loan pending tab
    // status 1 for loan completed when loan amount is Zero
    // status 2 for finalised loan tab
    // status 3 for completed loan tab

    int index = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            branchBind();
            //Glib.LoadRequest("Select * from [vikasardo].[vikasardo].[spc] where status='0' order by id desc", gvsprovider);
        }
    }
    public void branchBind()
    {
        try
        {
            string qur = "select * from [vikasardo].[vikasa].[bankBranch]";
            if (Alib.idHasRows(qur))
            {
                SqlConnection con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddlbranch.DataSource = ds;
                ddlbranch.DataTextField = "name";
                ddlbranch.DataValueField = "bankbranchID";
                ddlbranch.DataBind();
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnsrch_Click(object sender, EventArgs e)
    {

    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        Alib.idExecute("Update [vikasardo].[vikasardo].[spc] set shgrefno='" + txtref.Text + "', sbaccno='" + txtsb.Text + "', atlaccno='" + txtatl.Text + "', term='" + txtterms.Text + "', emi='" + txtemi.Text + "', loanamt='" + txtloanamount.Text + "', totrepay='" + txttotrepayment.Text + "', lastrepay='" + txtlastrepayment.Text + "', repaydate='" + txtrepaydate.Text + "', balance='" + txtbal.Text + "' where id='" + lblid.Text + "'");

        string balance = Alib.idGetAFieldByQuery("Select balance from [vikasardo].[vikasardo].[spc] where id='" + lblid.Text + "'");
        if (balance != "0")
        {
            Alib.idExecute("Update [vikasardo].[vikasardo].[spc] set term = '1', status = '0' where id='" + lblid.Text + "'");
        }
        Glib.LoadRequest("Select spc.id as Sno, spc.cdeid as CDE, spc.shgid as SHG, spc.shgrefno as Ref, spc.shg as SHGName, spc.village as Village, spc.members as Members, spc.sbaccno as SBAcc, spc.atlaccno as AtlAcc, spc.typeofac as Type, spc.loanaccdate as LoanAccDate, spc.term as Term, spc.emi as EMI, spc.loanamt as LoanAmt, spc.totrepay as TotalRepay, spc.lastrepay as LastRepay, spc.repaydate as RepayDate, spc.challan as Challan, spc.balance as Bal, spc.status as Status from [vikasardo].[vikasardo].[spc] spc inner join [vikasardo].[vikasa].[shg] shg on shg.shgid= spc.shgid where spc.status='0' and shg.branch ='" + ddlbranch.SelectedItem.Text + "' and convert(datetime, RepayDate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by id desc", gvsprovider);
    }
    protected void btncan_Click(object sender, EventArgs e)
    {

    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select * from [vikasardo].[vikasardo].[spc] where status='0' order by id desc", gvsprovider);
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Del"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[3].Text).ToString();
            lblid.Text = id;
            Alib.idExecute("delete from [vikasardo].[vikasardo].spc where id=" + id);
            Glib.LoadRequest("Select spc.id as Sno, spc.cdeid as CDE, spc.shgid as SHG, spc.shgrefno as Ref, spc.shg as SHGName, spc.village as Village, spc.members as Members, spc.sbaccno as SBAcc, spc.atlaccno as AtlAcc, spc.typeofac as Type, spc.loanaccdate as LoanAccDate, spc.term as Term, spc.emi as EMI, spc.loanamt as LoanAmt, spc.totrepay as TotalRepay, spc.lastrepay as LastRepay, spc.repaydate as RepayDate, spc.challan as Challan, spc.balance as Bal, spc.status as Status from [vikasardo].[vikasardo].[spc] spc inner join [vikasardo].[vikasa].[shg] shg on shg.shgid= spc.shgid where spc.status='0' and shg.branch ='" + ddlbranch.SelectedItem.Text + "' and convert(datetime, RepayDate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by id desc", gvsprovider);
        }
        if (e.CommandName.Equals("edi"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[3].Text).ToString();
            lblid.Text = id;
            MultiView1.ActiveViewIndex = 0;


            txtref.Text = HttpUtility.HtmlDecode(rows.Cells[6].Text).ToString();
            txtsb.Text = HttpUtility.HtmlDecode(rows.Cells[10].Text).ToString();
            txtatl.Text = HttpUtility.HtmlDecode(rows.Cells[11].Text).ToString();
            txtterms.Text = HttpUtility.HtmlDecode(rows.Cells[14].Text).ToString();
            txtemi.Text = HttpUtility.HtmlDecode(rows.Cells[15].Text).ToString();

            txtloanamount.Text = HttpUtility.HtmlDecode(rows.Cells[16].Text).ToString();
            txttotrepayment.Text = HttpUtility.HtmlDecode(rows.Cells[17].Text).ToString();
            hiddenlastrepay.Value = txtlastrepayment.Text = HttpUtility.HtmlDecode(rows.Cells[18].Text).ToString();
            txtrepaydate.Text = HttpUtility.HtmlDecode(rows.Cells[19].Text).ToString();
            txtbal.Text = HttpUtility.HtmlDecode(rows.Cells[21].Text).ToString();

        }
        if (e.CommandName.Equals("approve"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            string lastpay = Alib.idGetAFieldByQuery("select lastrepay from [vikasardo].[vikasardo].[spc] where id='" + index + "' ");

            string spc = (Convert.ToDouble(lastpay) * 3 / 100).ToString();

            Alib.idExecute("Update [vikasardo].[vikasardo].[spc] set status = '2', spc='" + spc + "' where id='" + index + "'");
            Glib.LoadRequest("Select spc.id as Sno, spc.cdeid as CDE, spc.shgid as SHG, spc.shgrefno as Ref, spc.shg as SHGName, spc.village as Village, spc.members as Members, spc.sbaccno as SBAcc, spc.atlaccno as AtlAcc, spc.typeofac as Type, spc.loanaccdate as LoanAccDate, spc.term as Term, spc.emi as EMI, spc.loanamt as LoanAmt, spc.totrepay as TotalRepay, spc.lastrepay as LastRepay, spc.repaydate as RepayDate, spc.challan as Challan, spc.balance as Bal, spc.status as Status from [vikasardo].[vikasardo].[spc] spc inner join [vikasardo].[vikasa].[shg] shg on shg.shgid= spc.shgid where spc.status='0' and shg.branch ='" + ddlbranch.SelectedItem.Text + "' and convert(datetime, RepayDate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by id desc", gvsprovider);
        }
    }
    protected void btnaddnew_ServerClick(object sender, EventArgs e)
    {

    }
    protected void txtlastrepayment_TextChanged(object sender, EventArgs e)
    {
        txttotrepayment.Text = (Convert.ToDouble(txttotrepayment.Text) - Convert.ToDouble(txtlastrepayment.Text)).ToString();
        txtbal.Text = (Convert.ToDouble(txtbal.Text) + Convert.ToDouble(hiddenlastrepay.Value) - Convert.ToDouble(txtlastrepayment.Text)).ToString();
    }
    protected void lnkapprove_Click(object sender, EventArgs e)
    {

    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Glib.LoadRequest("Select spc.id as Sno, spc.cdeid as CDE, spc.shgid as SHG, spc.shgrefno as Ref, spc.shg as SHGName, spc.village as Village, spc.members as Members, spc.sbaccno as SBAcc, spc.atlaccno as AtlAcc, spc.typeofac as Type, spc.loanaccdate as LoanAccDate, spc.term as Term, spc.emi as EMI, spc.loanamt as LoanAmt, spc.totrepay as TotalRepay, spc.lastrepay as LastRepay, spc.repaydate as RepayDate, spc.challan as Challan, spc.balance as Bal, spc.status as Status from [vikasardo].[vikasardo].[spc] spc inner join [vikasardo].[vikasa].[shg] shg on shg.shgid= spc.shgid where spc.status='0' and shg.branch ='" + ddlbranch.SelectedItem.Text + "' and convert(datetime, repaydate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by id desc", gvsprovider);
        }
        catch (Exception ex)
        {
        }
    }

    public DataTable GetDataTableValue(string qur)
    {
        DataTable dt = null;
        try
        {
            dt = Alib.getData(qur).Tables[0];
        }
        catch
        {
        }
        return dt;
    }
    protected void export_Click(object sender, EventArgs e)
    {
        DataTable dt = null;
        try
        {
            string qur = "Select spc.id as Sno, spc.cdeid as CDE, spc.shgid as SHG, spc.shgrefno as Ref, spc.shg as SHGName, spc.village as Village, spc.members as Members, spc.sbaccno as SBAcc, spc.atlaccno as AtlAcc, spc.typeofac as Type, spc.loanaccdate as LoanAccDate, spc.term as Term, spc.emi as EMI, spc.loanamt as LoanAmt, spc.totrepay as TotalRepay, spc.lastrepay as LastRepay, spc.repaydate as RepayDate, spc.challan as Challan, spc.balance as Bal, spc.status as Status from [vikasardo].[vikasardo].[spc] spc inner join [vikasardo].[vikasa].[shg] shg on shg.shgid= spc.shgid where spc.status='0' and shg.branch ='" + ddlbranch.SelectedItem.Text + "' and convert(datetime, repaydate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by id desc";
            dt = GetDataTableValue(qur);
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                GridView GridView1 = new GridView();
                GridView1.DataSource = dt;
                GridView1.DataBind();

                GridView1.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in GridView1.HeaderRow.Cells)
                {
                    cell.BackColor = GridView1.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in GridView1.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = GridView1.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = GridView1.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                GridView1.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }
        catch (Exception ex)
        {
        }


    }
}