﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class finalised : System.Web.UI.Page
{
    // status 0 for loan pending tab
    // status 1 for loan completed when loan amount is Zero
    // status 2 for finalised loan tab
    // status 3 for completed loan tab

    int index = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        { 
            Glib.LoadRequest("Select spc.id as Sno, spc.cdeid as CDE, spc.shgid as SHG, spc.shgrefno as Ref, spc.shg as SHGName, spc.village as Village, spc.members as Members, spc.sbaccno as SBAcc, spc.atlaccno as AtlAcc, spc.typeofac as Type, spc.loanaccdate as LoanAccDate, spc.term as Term, spc.emi as EMI, spc.loanamt as LoanAmt, spc.totrepay as TotalRepay, spc.lastrepay as LastRepay, spc.repaydate as RepayDate, spc.challan as Challan, spc.balance as Bal, spc.status as Status, spc.spc as spc from [vikasardo].[vikasardo].[spc] spc inner join [vikasardo].[vikasa].[shg] shg on shg.shgid= spc.shgid where spc.status='2' order by id desc", gvsprovider);
            branchBind();
        }
    }
    public DataTable GetDataTableValue(string qur)
    {
        DataTable dt = null;
        try
        {
            dt = Alib.getData(qur).Tables[0];
        }
        catch
        {
        }
        return dt;
    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {

    }
    protected void btncan_Click(object sender, EventArgs e)
    {

    }
    protected void btnsrch_Click(object sender, EventArgs e)
    {

    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select spc.id as Sno, spc.cdeid as CDE, spc.shgid as SHG, spc.shgrefno as Ref, spc.shg as SHGName, spc.village as Village, spc.members as Members, spc.sbaccno as SBAcc, spc.atlaccno as AtlAcc, spc.typeofac as Type, spc.loanaccdate as LoanAccDate, spc.term as Term, spc.emi as EMI, spc.loanamt as LoanAmt, spc.totrepay as TotalRepay, spc.lastrepay as LastRepay, spc.repaydate as RepayDate, spc.challan as Challan, spc.balance as Bal, spc.status as Status, spc.spc as spc from [vikasardo].[vikasardo].[spc] spc inner join [vikasardo].[vikasa].[shg] shg on shg.shgid= spc.shgid where spc.status='2' order by id desc", gvsprovider);
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Received"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            Alib.idExecute("Update [vikasardo].[vikasardo].[spc] set status = '0' where id='" + index + "'");
            Glib.LoadRequest("Select spc.id as Sno, spc.cdeid as CDE, spc.shgid as SHG, spc.shgrefno as Ref, spc.shg as SHGName, spc.village as Village, spc.members as Members, spc.sbaccno as SBAcc, spc.atlaccno as AtlAcc, spc.typeofac as Type, spc.loanaccdate as LoanAccDate, spc.term as Term, spc.emi as EMI, spc.loanamt as LoanAmt, spc.totrepay as TotalRepay, spc.lastrepay as LastRepay, spc.repaydate as RepayDate, spc.challan as Challan, spc.balance as Bal, spc.status as Status, spc.spc as spc from [vikasardo].[vikasardo].[spc] spc inner join [vikasardo].[vikasa].[shg] shg on shg.shgid= spc.shgid where spc.status='2' order by id desc", gvsprovider);
        }
    }
    protected void btnaddnew_ServerClick(object sender, EventArgs e)
    {

    }
    protected void export_Click(object sender, EventArgs e)
    {
        DataTable dt = null;
        try
        {
            string bal = Alib.idGetAFieldByQuery("Select top 1 totalbalance from [vikasardo].[vikasardo].[branchSPC] where branch='" + ddlbranch.SelectedItem.Text + "' order by sno desc");
            if (bal == "")
            {
                bal = "0";
            }

            float prvBal = float.Parse(bal);
            float totBal = prvBal + float.Parse(lbltotspc.Text);

            Alib.idExecute("Insert into [vikasardo].[vikasardo].[branchSPC] (branch,balance,claimed,received,totalbalance,claimedDate,status) values ('" + ddlbranch.SelectedItem.Text + "','" + prvBal + "','" + lbltotspc.Text + "','0','" + totBal + "','" + System.DateTime.Now.ToString("dd/MM/yyyy") + "','0')");

            string qur = "Select DISTINCT  spc.shg as SHGName,shg.[branch] as Branch, spc.village as Village,  spc.loanaccdate as LoanAccDate, spc.loanamt as LoanAmt, loan.loan_number,spc.lastrepay as LastRepay, spc.repaydate as RepayDate,  spc.spc as spc from[vikasardo].[vikasardo].[spc] spc inner  JOIN[vikasardo].[vikasa].[shgexternalloansplit] loan on spc.shg = loan.shgname inner join[vikasardo].[vikasa].[shg] shg on shg.shgid= spc.shgid where spc.status='2' and shg.branch ='KAGGALIPURA' and convert(datetime, RepayDate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103)";
      
            //now string qur = "Select DISTINCT  spc.id as Sno, spc.cdeid as CDE, spc.shgid as SHG, spc.shgrefno as Ref, spc.shg as SHGName, spc.village as Village, spc.members as Members, spc.sbaccno as SBAcc, spc.atlaccno as AtlAcc, spc.typeofac as Type, spc.loanaccdate as LoanAccDate,loan.loan_number, spc.term as Term, spc.emi as EMI, spc.loanamt as LoanAmt, spc.totrepay as TotalRepay, spc.lastrepay as LastRepay, spc.repaydate as RepayDate, spc.challan as Challan, spc.balance as Bal, spc.status as Status, spc.spc as spc from[vikasardo].[vikasardo].[spc] spc inner  JOIN[vikasardo].[vikasa].[shgexternalloansplit] loan on spc.shg = loan.shgname inner join[vikasardo].[vikasa].[shg] shg on shg.shgid= spc.shgid where spc.status='2' and shg.branch ='KAGGALIPURA' and convert(datetime, RepayDate, 103) between convert(datetime, '02-01-2017', 103) and convert(datetime, '01-11-2017', 103)";
            //string qur = " Select distinct  [shg] as SHG,[village] as Village,shge.loan_number,[loanaccdate] as [Loan Account Date],[loanamt] as [Loan Amount],repaydate as [Repayment Date],[lastrepay] as [Last Repayment], spc as SPC from [vikasardo].[vikasardo].[spc] as spc  inner join [vikasardo].[vikasa].[shgexternalloansplit] as shge on spc.shgid=shge.shgid ";
         //  20nov string qur = "Select [shg] as SHG,[village] as Village,[sbaccno] as [SB Account],[atlaccno] as [ATL Account],[typeofac] as [Account Type],[loanaccdate] as [Loan Account Date],[loanamt] as [Loan Amount],repaydate as [Repayment Date],[lastrepay] as [Last Repayment], spc as SPC from [vikasardo].[vikasardo].[spc] where status='2' order by id desc";
            dt = GetDataTableValue(qur);
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                GridView GridView1 = new GridView();
                GridView1.DataSource = dt;
                GridView1.DataBind();

                GridView1.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in GridView1.HeaderRow.Cells)
                {
                    cell.BackColor = GridView1.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in GridView1.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = GridView1.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = GridView1.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                GridView1.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void lnkapprove_Click(object sender, EventArgs e)
    {

    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Glib.LoadRequest("Select spc.id as Sno, spc.cdeid as CDE, spc.shgid as SHG, spc.shgrefno as Ref, spc.shg as SHGName, spc.village as Village, spc.members as Members, spc.sbaccno as SBAcc, spc.atlaccno as AtlAcc, spc.typeofac as Type, spc.loanaccdate as LoanAccDate, spc.term as Term, spc.emi as EMI, spc.loanamt as LoanAmt, spc.totrepay as TotalRepay, spc.lastrepay as LastRepay, spc.repaydate as RepayDate, spc.challan as Challan, spc.balance as Bal, spc.status as Status, spc.spc as spc from [vikasardo].[vikasardo].[spc] spc inner join [vikasardo].[vikasa].[shg] shg on shg.shgid= spc.shgid where spc.status='2' and shg.branch ='" + ddlbranch.SelectedItem.Text + "' and convert(datetime, RepayDate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by id desc", gvsprovider);

            //-----------------------Total SPC--------------------------//

            double spc = 0.0;
            for (int i = 0; i <= gvsprovider.Rows.Count - 1; i++)
            {
                spc = spc + Convert.ToDouble(gvsprovider.Rows[i].Cells[20].Text);
            }
            lbltotspc.Text = spc.ToString();
            //---------------------End Total SPC-------------------------//

        }
        catch (Exception ex)
        {
        }
    }
    public void branchBind()
    {
        try
        {
            string qur = "select * from [vikasardo].[vikasa].[bankBranch]";
            if (Alib.idHasRows(qur))
            {
                SqlConnection con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddlbranch.DataSource = ds;
                ddlbranch.DataTextField = "name";
                ddlbranch.DataValueField = "bankbranchID";
                ddlbranch.DataBind();
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }
}