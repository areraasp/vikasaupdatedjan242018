﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<!DOCTYPE html>
<html lang="en">

<head runat="server">
    <meta charset="utf-8">
    <title>VIKASA | Sign In</title>

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="css/animate.css">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="css/style.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
</head>

<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="top" style="margin-top: -10%;">
                <h1 id="title" class="hidden"><span id="logo">VIKASA</span></h1>
            </div>
            <div class="login-box animated fadeInUp">
                <div class="box-header">
                    <h2>Log In</h2>
                </div>
                <label for="username">Username</label>
                <br />
                <asp:TextBox ID="username" runat="server" placeholder="Username" />
                <br />
                <label for="password">Password</label>
                <br />
                <asp:TextBox ID="password" runat="server" placeholder="Password" TextMode="Password"/>
                <br />
                <button id="submit" type="submit" runat="server" onserverclick="submit_ServerClick">Sign In</button>
                <br />
                <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                <a href="#">
                    <p class="small">Forgot your password?</p>
                </a>
            </div>
        </div>
    </form>
</body>

<script>
    $(document).ready(function () {
        $('#logo').addClass('animated fadeInDown');
        $("input:text:visible:first").focus();
    });
    $('#username').focus(function () {
        $('label[for="username"]').addClass('selected');
    });
    $('#username').blur(function () {
        $('label[for="username"]').removeClass('selected');
    });
    $('#password').focus(function () {
        $('label[for="password"]').addClass('selected');
    });
    $('#password').blur(function () {
        $('label[for="password"]').removeClass('selected');
    });
</script>

</html>
