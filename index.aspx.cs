using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void submit_ServerClick(object sender, EventArgs e)
    {
        SqlConnection conn = null;
        if (username.Text == "")
        {
            lblError.Visible = true;
            lblError.Text = "Enter Username";
            username.Focus();
            return;
        }
        if (password.Text == "")
        {
            lblError.Visible = true;
            lblError.Text = "Enter Password";
            password.Focus();
            return;
        }
        string qur = "select * from [vikasardo].[vikasardo].[users] where username=@username and password=@password";
        try
        {
            using (conn = new SqlConnection(Alib.conStr))
            {
                SqlCommand cmd = new SqlCommand(qur, conn);
                cmd.Parameters.Add(new SqlParameter("@username", username.Text));
                cmd.Parameters.Add(new SqlParameter("@password", password.Text));
                conn.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        lblError.Visible = false;
                        Context.ApplicationInstance.CompleteRequest();
                        Session["aauserid"] = username.Text;
                        if (Session["aauserid"] != null)
                        {
                            Response.Redirect("members.aspx");
                        }
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = "Invalid UserName or Password!";
                        return;
                    }
                }
            }
        }
        catch (System.Threading.ThreadAbortException lException)
        {
            // do nothing
        }
        catch (Exception ex)
        {
            string ss = ex.Message;
        }
        finally
        {
            if (conn != null)
                conn.Dispose();
        }
    }
}