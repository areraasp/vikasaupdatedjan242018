﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class leaves : System.Web.UI.Page
{
    int index = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Glib.LoadRequest("Select * from [vikasardo].[vikasardo].[leaves]", gvsprovider);
            Glib.LoadRequest("Select l.id, l.cdmid, l.ltype, l.lfrom, l.lto, l.days, l.state from [vikasardo].[vikasardo].[cdmleave] l where l.state='0' order by l.id desc", GridView1);
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (txtname.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Task Name',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (btnsubmit.Text == "Submit")
        {
            Alib.idInsertInto("[vikasardo].[vikasardo].[leaves]", "leave", txtname.Text, "days", txtdays.Text);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Successfully Updated',text: '',timer: 2000,showConfirmButton: false})", true);
            Glib.LoadRequest("Select * from [vikasardo].[vikasardo].[leaves]", gvsprovider);
        }
        else
        {
            Alib.idExecute("Update [vikasardo].[vikasardo].[leaves] set leave='" + txtname.Text + "', days='" + txtdays.Text + "' where id='" + lblid.Text + "'");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Successfully Updated',text: '',timer: 2000,showConfirmButton: false})", true);
            Glib.LoadRequest("Select * from [vikasardo].[vikasardo].[leaves]", gvsprovider);
        }
        clean();
        return;
    }
    public void clean()
    {
        MultiView1.ActiveViewIndex = -1;
        txtname.Text = " ";
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = -1;
        txtname.Text = " ";
    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select * from [vikasardo].[vikasardo].[leaves]", gvsprovider);
    }
    protected void Button2_ServerClick(object sender, EventArgs e)
    {
        clean();
        MultiView1.ActiveViewIndex = 0;
        btnsubmit.Text = "Submit";
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Del"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            lblid.Text = id;
            Alib.idExecute("delete from [vikasardo].[vikasardo].[leaves] where id=" + id);
            Glib.LoadRequest("Select * from [vikasardo].[vikasardo].[leaves]", gvsprovider);
        }
        if (e.CommandName.Equals("edi"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            lblid.Text = id;
            MultiView1.ActiveViewIndex = 0;
            btnsubmit.Text = "Update";
            txtname.Text = HttpUtility.HtmlDecode(rows.Cells[3].Text).ToString();
            txtdays.Text = HttpUtility.HtmlDecode(rows.Cells[4].Text).ToString();
        }
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Rej"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = GridView1.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            string cdmid = HttpUtility.HtmlDecode(rows.Cells[3].Text).ToString();
            string num = Alib.idGetAFieldByQuery("select cdmphone from [vikasardo].[vikasa].[cdm] where cdmid='" + cdmid + "'");
            lblid.Text = id;
            string wish = "Your leave has been rejected";
            WebClient Client = new WebClient();
            Uri uri = new Uri("http://teleduce.in/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + num);
            Stream data = Client.OpenRead(uri);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            data.Close();
            reader.Close();

            Alib.idExecute("delete from [vikasardo].[vikasardo].[cdmleave] where id=" + id);
            Glib.LoadRequest("Select * from [vikasardo].[vikasardo].[cdmleave] where state='0' order by id desc", GridView1);
        }
        if (e.CommandName.Equals("App"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = GridView1.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            string cdmid = HttpUtility.HtmlDecode(rows.Cells[3].Text).ToString();
            string num = Alib.idGetAFieldByQuery("select cdmphone from [vikasardo].[vikasa].[cdm] where cdmid='" + cdmid + "'");
            lblid.Text = id;
            string wish = "Your leave has been approved";
            WebClient Client = new WebClient();
            Uri uri = new Uri("http://teleduce.in/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + num);
            Stream data = Client.OpenRead(uri);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            data.Close();
            reader.Close();

            Alib.idExecute("update [vikasardo].[vikasardo].[cdmleave] set state='1' where id=" + id);
            Glib.LoadRequest("Select * from [vikasardo].[vikasardo].[cdmleave] where state='0' order by id desc", GridView1);
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select * from [vikasardo].[vikasardo].[cdmleave] order by id desc", GridView1);
    }
}