﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mbranch : System.Web.UI.Page
{
    int index = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].[bankBranch]", gvsprovider);
            regionBind();
        }
    }
    public void regionBind()
    {
        try
        {
            string qur = "select * from [vikasardo].[vikasa].[regions]";
            if (Alib.idHasRows(qur))
            {
                SqlConnection con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddlregion.DataSource = ds;
                ddlregion.DataTextField = "name";
                ddlregion.DataValueField = "regionID";
                ddlregion.DataBind();
                ddlregion.Items.Insert(0, new ListItem("Choose", "0"));
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void Button2_ServerClick(object sender, EventArgs e)
    {
        clean();
        MultiView1.ActiveViewIndex = 0;
        btnsubmit.Text = "Submit";
      
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (txtname.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Task Name',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (btnsubmit.Text == "Submit")
        {
            Alib.idInsertInto("[vikasardo].[vikasa].[bankBranch]", "name", txtname.Text, "regionID", ddlregion.SelectedValue);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Successfully Updated',text: '',timer: 2000,showConfirmButton: false})", true);
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].[bankBranch]", gvsprovider);
        }
        else
        {
            Alib.idExecute("Update [vikasardo].[vikasa].[bankBranch] set name='" + txtname.Text + "', regionID='" + ddlregion.SelectedValue + "' where bankbranchID='" + lblid.Text + "'");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Successfully Updated',text: '',timer: 2000,showConfirmButton: false})", true);
            Glib.LoadRequest("Select * from  [vikasardo].[vikasa].[bankBranch]", gvsprovider);
        }
        clean();
        return;
    }
    public void clean()
    {
        MultiView1.ActiveViewIndex = -1;
        txtname.Text = "";
        regionBind();
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = -1;
        txtname.Text = "";
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Del"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            lblid.Text = id;
            Alib.idExecute("delete from [vikasardo].[vikasa].[bankBranch] where bankbranchID=" + id);
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].[bankBranch] ", gvsprovider);
        }
        if (e.CommandName.Equals("edi"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            lblid.Text = id;
            MultiView1.ActiveViewIndex = 0;
            btnsubmit.Text = "Update";
            txtname.Text = HttpUtility.HtmlDecode(rows.Cells[3].Text).ToString();
            ddlregion.SelectedValue = HttpUtility.HtmlDecode(rows.Cells[4].Text).ToString();
        }
    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select * from  [vikasardo].[vikasa].[bankBranch]", gvsprovider);
    }
}