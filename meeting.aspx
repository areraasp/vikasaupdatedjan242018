﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="meeting.aspx.cs" Inherits="meeting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery-1.11.0.js"></script>
    <script src="js/jquery-ui.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="js/jquery.textchange.min.js"></script>
    <link href="js/jquery-ui.min.css" rel="stylesheet" />


    <link href="assets/css/sweetalert.css" rel="stylesheet" />
    <script src="assets/js/sweetalert.min.js"></script>
    <style type="text/css">
        .img-zoom {
            width: 30px;
            -webkit-transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            -ms-transition: all .2s ease-in-out;
        }

        .transition {
            -webkit-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
        }
    </style>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.1.5"
        media="screen" />
    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript">
        $(function () {
            $('#<%=txtsbacc.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "meeting.aspx/GetInvName",
                        data: "{ 'pre':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (d) {
                            var data = JSON.parse(d.d);
                            console.log(data);
                            response($.map(data, function (item) {
                                return { value: item.tmp }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#bfs5').addClass('active');
            $('#id3').addClass('active');
            $('.img-zoom').hover(function () {
                $(this).addClass('transition');
            }, function () {
                $(this).removeClass('transition');
            });
            $('.fancybox').fancybox();
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    overlay: {
                        speedOut: 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS: 'fancybox-custom',
                closeClick: true,

                openEffect: 'none',

                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        css: {
                            'background': 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });
            $(function () {
                $('#bfs2').addClass('active');
                $('#<%=from.ClientID %>').datepicker({ dateFormat: "dd-mm-yy" });
                $('#<%=to.ClientID %>').datepicker({ dateFormat: "dd-mm-yy" });
            });
        });
    </script>

    <style type="text/css">
        .fancybox-custom .fancybox-skin {
            box-shadow: 0 0 50px #222;
        }

        .auto-style2 {
            width: 290px;
        }

        .auto-style3 {
            width: 244px;
        }

        .auto-style4 {
            height: 55px;
        }

        .auto-style5 {
            width: 290px;
            height: 55px;
        }

        .auto-style6 {
            width: 244px;
            height: 55px;
        }

        .hiddencol {
            display: none;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblid" runat="server" Visible="false"></asp:Label>
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Meeting Minutes</h4>
        </div>
    </div>
    <!--/ .page title -->
    <br />
    <div class="row">
        <div class="col-sm-3">
            <asp:TextBox ID="txtsbacc" runat="server" CssClass="form-control" placeholder="Enter SB Account Number" OnTextChanged="txtsbacc_TextChanged" AutoPostBack="true"></asp:TextBox>
        </div>
        <div class="col-sm-3">
            <asp:DropDownList ID="ddshg" runat="server" CssClass="form-control"></asp:DropDownList>
        </div>
        <div class="col-sm-2">
            <asp:TextBox ID="from" runat="server" CssClass="form-control" placeholder="Enter From Date"></asp:TextBox>
        </div>
        <div class="col-sm-2">
            <asp:TextBox ID="to" runat="server" CssClass="form-control" placeholder="Enter To Date"></asp:TextBox>
        </div>
        <div class="col-sm-2">
            <asp:Button ID="btnsub" runat="server" Text="Get" OnClick="btnsub_Click" CssClass="btn btn-lg btn-default btn-custom" />
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-6 card-box">
            <div class="form-control">
                <strong>Meeting Date :</strong>&nbsp;&nbsp;<asp:Label runat="server" ID="lblmeetingdate"></asp:Label>
            </div>
            <asp:Image ID="picture1" CssClass="img-zoom" runat="server" ImageUrl='<%# Eval("shgminimage","MinImages/{0}") %>' Height="70"
                Width="70" />
        </div>
        <div class="col-md-6 card-box">
            <div class="form-control">
                <strong>Meeting Place :</strong>&nbsp;&nbsp;<asp:Label runat="server" ID="lblmeetingplace"></asp:Label>
            </div>
            <asp:Image ID="picture2" CssClass="img-zoom" runat="server" ImageUrl='<%# Eval("loanchallenimage","challan/{0}") %>' Height="70"
                Width="70" />

        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-6 card-box">
            <table style="border: thin solid #000000; width: 100%; height: 286px;">
                <tr>
                    <td style="border: thin solid #000000; background-color: #dfdfdf; font-weight: 900" class="auto-style5">&nbsp;LIABILITY</td>
                    <td style="border: thin solid #000000; background-color: #dfdfdf; font-weight: 900; text-align: center;" class="auto-style6">&nbsp;VALUES</td>

                </tr>
                <tr>
                    <td class="auto-style2" style="border: thin solid #000000">&nbsp;Membership Fee</td>
                    <td class="auto-style3" style="border: thin solid #000000; text-align: right">&nbsp;<asp:Label runat="server" ID="liabilitymembershipFee"></asp:Label>&nbsp;&nbsp;&nbsp;</td>

                </tr>
                <tr>
                    <td class="auto-style2" style="border: thin solid #000000">&nbsp;Total Savings</td>
                    <td class="auto-style3" style="border: thin solid #000000; text-align: right">&nbsp;<asp:Label runat="server" ID="liabilitytotalsavings"></asp:Label>&nbsp;&nbsp;&nbsp;</td>

                </tr>
                <tr>
                    <td class="auto-style2" style="border: thin solid #000000">&nbsp;Total Interest</td>
                    <td class="auto-style3" style="border: thin solid #000000; text-align: right">&nbsp;<asp:Label runat="server" ID="liabilitytotalinterest"></asp:Label>&nbsp;&nbsp;&nbsp;</td>

                </tr>
                <tr>
                    <td class="auto-style2" style="border: thin solid #000000">&nbsp;Total Penalty</td>
                    <td class="auto-style3" style="border: thin solid #000000; text-align: right">&nbsp;<asp:Label runat="server" ID="liabilitytotalpenalty"></asp:Label>&nbsp;&nbsp;&nbsp;</td>

                </tr>
                <tr>
                    <td class="auto-style2" style="border: thin solid #000000">&nbsp;Total Bank Interest</td>
                    <td class="auto-style3" style="border: thin solid #000000; text-align: right">&nbsp;<asp:Label runat="server" ID="liabilitytotalbankinterest"></asp:Label>&nbsp;&nbsp;&nbsp;</td>

                </tr>
                <tr>
                    <td class="auto-style2" style="border: thin solid #000000">&nbsp;Fund Received From NGO</td>
                    <td class="auto-style3" style="border: thin solid #000000; text-align: right">&nbsp;<asp:Label runat="server" ID="liabilitytotalfundreceived"></asp:Label>&nbsp;&nbsp;&nbsp;</td>

                </tr>
                <tr>
                    <td class="auto-style2" style="border: thin solid #000000">&nbsp;Others</td>
                    <td class="auto-style3" style="border: thin solid #000000; text-align: right">&nbsp;<asp:Label runat="server" ID="liabilitytotalothers"></asp:Label>&nbsp;&nbsp;&nbsp;</td>

                </tr>
                <tr>
                    <td class="auto-style2" style="border: thin solid #000000; background-color: #dfdfdf; font-weight: 900">&nbsp;Total</td>
                    <td class="auto-style3" style="border: thin solid #000000; background-color: #dfdfdf; font-weight: 900; text-align: right">&nbsp;<asp:Label runat="server" ID="liabilitytotal"></asp:Label>&nbsp;&nbsp;&nbsp;</td>

                </tr>
            </table>
        </div>
        <div class="col-md-6 card-box">
            <table style="border: thin solid #000000; width: 100%; height: 286px;">
                <tr>

                    <td style="border: thin solid #000000; background-color: #dfdfdf; font-weight: 900" class="auto-style4">&nbsp;ASSETS</td>
                    <td style="border: thin solid #000000; background-color: #dfdfdf; font-weight: 900; text-align: center;" class="auto-style4">&nbsp;VALUES</td>
                </tr>
                <tr>

                    <td style="border: thin solid #000000">&nbsp;Total Internal Lending</td>
                    <td style="border: thin solid #000000; text-align: right">&nbsp;<asp:Label runat="server" ID="assetstotalintlending"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>

                    <td style="border: thin solid #000000">&nbsp;Cash in Hand</td>
                    <td style="border: thin solid #000000; text-align: right">&nbsp;<asp:Label runat="server" ID="assetscashinhand"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>

                    <td style="border: thin solid #000000">&nbsp;Cash in Bank</td>
                    <td style="border: thin solid #000000; text-align: right">&nbsp;<asp:Label runat="server" ID="assetscashinbank"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>

                    <td style="border: thin solid #000000">&nbsp;Total Expenses</td>
                    <td style="border: thin solid #000000; text-align: right">&nbsp;<asp:Label runat="server" ID="assetstotothers"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>

                    <td style="border: thin solid #000000">&nbsp;FD /RID</td>
                    <td style="border: thin solid #000000; text-align: right">&nbsp;<asp:Label runat="server" ID="assetsrid"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>

                    <td style="border: thin solid #000000">&nbsp;Others</td>
                    <td style="border: thin solid #000000; text-align: right">&nbsp;<asp:Label runat="server" ID="assetsexpothers"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>

                    <td style="border: thin solid #000000">&nbsp;</td>
                    <td style="border: thin solid #000000; text-align: right">&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>

                    <td style="border: thin solid #000000; background-color: #dfdfdf; font-weight: 900">&nbsp;Total</td>
                    <td style="border: thin solid #000000; background-color: #dfdfdf; font-weight: 900; text-align: right">&nbsp;<asp:Label runat="server" ID="assetstotal"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </table>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <asp:GridView ID="gvsprovider" runat="server" Caption="Loan Enquiry List" EmptyDataText="No Data Available" EmptyDataRowStyle-CssClass="text-danger" EmptyDataRowStyle-Font-Size="Medium" CssClass="table table-bordered table-hover table-striped"
                    AutoGenerateColumns="false" OnRowCommand="gvsprovider_RowCommand">
                    <Columns>
                        <asp:ButtonField CommandName="Del" ControlStyle-CssClass="btn btn-danger btn-custom" ButtonType="Button"
                            Text="Del" HeaderText="Del"></asp:ButtonField>
                        <asp:BoundField DataField="id" HeaderText="Sno"></asp:BoundField>
                        <asp:BoundField DataField="memberid" HeaderText="Member"></asp:BoundField>
                        <asp:BoundField DataField="name" HeaderText="Name"></asp:BoundField>
                        <asp:BoundField DataField="date" HeaderText="Date" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="place" HeaderText="Place" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="persons" HeaderText="Person" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgid" HeaderText="SHG" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="cdeid" HeaderText="CDE" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="savings" HeaderText="Savings"></asp:BoundField>
                        <asp:BoundField DataField="totalsavings" HeaderText="Total Savings"></asp:BoundField>
                        <asp:BoundField DataField="lendbal" HeaderText="Lending Balance"></asp:BoundField>
                        <asp:BoundField DataField="lendemi" HeaderText="Lending EMI"></asp:BoundField>
                        <asp:BoundField DataField="interest" HeaderText="Interest"></asp:BoundField>
                        <asp:BoundField DataField="lending" HeaderText="Lending"></asp:BoundField>
                        <asp:BoundField DataField="lendpending" HeaderText="Lending Pending" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="bankloan" HeaderText="Bank Loan"></asp:BoundField>
                        <asp:BoundField DataField="bankemi" HeaderText="Bank EMI"></asp:BoundField>
                        <asp:BoundField DataField="bankloanpending" HeaderText="Bank Loan"></asp:BoundField>
                        <asp:BoundField DataField="penalty" HeaderText="Penalty"></asp:BoundField>
                        <asp:BoundField DataField="cash" HeaderText="Cash" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="totalpenalty" HeaderText="Total Penalty"></asp:BoundField>
                        <asp:BoundField DataField="totalcollections" HeaderText="Total Collections" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="meetings" HeaderText="Meeting" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shginbank" HeaderText="in Bank" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shginhand" HeaderText="in Hand" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgsavings" HeaderText="Savings" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgtotsavings" HeaderText="Total Savings" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgtotlendrepay" HeaderText="Total Lending Repay" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgtotlendinterest" HeaderText="Total Lending Interest" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgtotintlending" HeaderText="Total Lending" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgtotbankemi" HeaderText="Total Bank EMI" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgtotpenalty" HeaderText="Total Penalty" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgtotbankinterest" HeaderText="Total Bank Interest"></asp:BoundField>
                        <asp:BoundField DataField="shgtotgrants" HeaderText="Total Grants" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgotherssavings" HeaderText="Other Savings" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgtotcollection" HeaderText="Total Collection" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgexpcash" HeaderText="Cash" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgexpcheque" HeaderText="Cheque" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgexpdepositetosavingsacc" HeaderText="Deposit to SavingAcc" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgexpdepositetobankacc" HeaderText="Deposit to BankAcc" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgexpcashinhand" HeaderText="Cash in Hand" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgexpamountinbanksavings" HeaderText="Amount in Bank" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgexpmembersavingsrepayment" HeaderText="Savings Repay" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgexpothers" HeaderText="Exp Others" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgexptotal" HeaderText="Exp Total" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgincomerefundfromrid" HeaderText="Fund from RID" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgexprid" HeaderText="Exp RID" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="shgexp" HeaderText="Expenses" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                        <asp:BoundField DataField="membershipfee" HeaderText="Membership" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="gridview"></PagerStyle>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>

