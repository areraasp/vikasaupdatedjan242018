﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class meeting : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }
    public void pic1Bind()
    {
        int mid = 1 + Convert.ToInt32(Alib.idGetAFieldByQuery("select top 1 meetings from [vikasardo].[vikasa].[meeting] where shgid='" + ddshg.SelectedValue + "' order by id desc"));
        int checkmid = mid - 1;
        string cmdstr = Alib.idGetAFieldByQuery("Select shgminimage from [vikasardo].[vikasa].[meeting] where shgid='" + ddshg.SelectedValue + "' and meetings='" + checkmid.ToString() + "'");
        picture1.ImageUrl = "http://test.vikasardo.org/MinImages/" + cmdstr;
    }
    public void pic2Bind()
    {
        int mid = 1 + Convert.ToInt32(Alib.idGetAFieldByQuery("select top 1 meetings from [vikasardo].[vikasa].[meeting] where shgid='" + ddshg.SelectedValue + "' order by id desc"));
        int checkmid = mid - 1;
        string cmdstr = Alib.idGetAFieldByQuery("Select loanchallenimage from [vikasardo].[vikasa].[meeting] where shgid='" + ddshg.SelectedValue + "' and meetings='" + checkmid.ToString() + "'");
        picture2.ImageUrl = "http://test.vikasardo.org/challan/" + cmdstr;
    }
    public void databind()
    {
        //string liabilitymembershipFee = "";
        //string liabilitytotalsavings = "";
        //string liabilitytotalinterest = "";
        //string liabilitytotalpenalty = "";
        //string liabilitytotalbankinterest = "";
        //string liabilitytotalfundreceived = "";
        //string liabilitytotalothers = "";
        //string liabilitytotal = "";

        //string assetstotalintlending = "";
        //string assetscashinhand = "";
        //string assetscashinbank = "";
        //string assetsrid = "";
        //string assetsexpothers = "";
        string assetsshgothers = "";
        //string assetstotothers = "";
        //string assetstotal = "";


        try
        {
            int mid = 1 + Convert.ToInt32(Alib.idGetAFieldByQuery("select top 1 meetings from [vikasardo].[vikasa].[meeting] where shgid='" + ddshg.SelectedValue + "' order by id desc"));
            int checkmid = mid - 1;
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].[meeting] where shgid='" + ddshg.SelectedValue + "' and meetings='" + checkmid.ToString() + "' ", gvsprovider);

            // liabilitymembershipFee = Alib.idGetAFieldByQuery("select sum(convert(int, membershipFee)) as SHG_MembershipFee from [vikasardo].[vikasa].members where shgid='" + ddshg.SelectedValue + "' and cdeid='" + cdeid + "'");
            lblmeetingdate.Text = Alib.idGetAFieldByQuery("select top 1 date from [vikasardo].[vikasa].[meeting] where shgid='" + ddshg.SelectedValue + "' order by id desc");
            lblmeetingplace.Text = Alib.idGetAFieldByQuery("select top 1 place from [vikasardo].[vikasa].[meeting] where shgid='" + ddshg.SelectedValue + "' order by id desc");


            liabilitymembershipFee.Text = Alib.idGetAFieldByQuery("select sum(convert(int, s.membershipfee)) from (select distinct(date), membershipfee from [vikasardo].[vikasa].[meeting] where shgid='" + ddshg.SelectedValue + "' and date<>'0' and convert(datetime, date, 103) between convert(datetime, '" + from.Text + "', 103) and convert(datetime, '" + to.Text + "', 103)) s");
            if (liabilitymembershipFee.Text == "")
            {
                liabilitymembershipFee.Text = "0";
            }

            liabilitytotalsavings.Text = Alib.idGetAFieldByQuery("select sum(convert(int, savings)) as SHGSavings from [vikasardo].[vikasa].meeting where shgid='" + ddshg.SelectedValue + "' and convert(datetime, date, 103) between convert(datetime, '" + from.Text + "', 103) and convert(datetime, '" + to.Text + "', 103) and date<>'0'");
            if (liabilitytotalsavings.Text == "")
            {
                liabilitytotalsavings.Text = "0";
            }

            liabilitytotalinterest.Text = Alib.idGetAFieldByQuery("select sum(convert(int, interest)) as SHGInterest from [vikasardo].[vikasa].meeting where shgid='" + ddshg.SelectedValue + "' and convert(datetime, date, 103) between convert(datetime, '" + from.Text + "', 103) and convert(datetime, '" + to.Text + "', 103) and date<>'0'");
            if (liabilitytotalinterest.Text == "")
            {
                liabilitytotalinterest.Text = "0";
            }

            liabilitytotalpenalty.Text = Alib.idGetAFieldByQuery("select sum(convert(int, penalty)) as SHGpENALTY from [vikasardo].[vikasa].meeting where shgid='" + ddshg.SelectedValue + "' and convert(datetime, date, 103) between convert(datetime, '" + from.Text + "', 103) and convert(datetime, '" + to.Text + "', 103) and date<>'0'");
            if (liabilitytotalpenalty.Text == "")
            {
                liabilitytotalpenalty.Text = "0";
            }

            liabilitytotalbankinterest.Text = Alib.idGetAFieldByQuery("SELECT sum(convert(int, shgtotbankinterest)) as total from [vikasardo].[vikasa].meeting where id in (SELECT max(m.id) from [vikasardo].[vikasa].meeting m where shgid='" + ddshg.SelectedValue + "' and convert(datetime, m.date, 103) between convert(datetime, '" + from.Text + "', 103) And convert(datetime, '" + to.Text + "', 103) and date<>'0' group by m.date)");
            if (liabilitytotalbankinterest.Text == "")
            {
                liabilitytotalbankinterest.Text = "0";
            }

            liabilitytotalfundreceived.Text = Alib.idGetAFieldByQuery("select sum(convert(int, shgtotgrants)) as SHGSavings from [vikasardo].[vikasa].meeting where id in (SELECT max(m.id) from [vikasardo].[vikasa].meeting m where shgid='" + ddshg.SelectedValue + "' and convert(datetime, m.date, 103) between convert(datetime, '" + from.Text + "', 103) And convert(datetime, '" + to.Text + "', 103) and date<>'0' group by  m.date)");
            if (liabilitytotalfundreceived.Text == "")
            {
                liabilitytotalfundreceived.Text = "0";
            }

            liabilitytotalothers.Text = Alib.idGetAFieldByQuery("select sum(convert(int, s.shgotherssavings)) from (select distinct(date), shgotherssavings from [vikasardo].[vikasa].[meeting] where shgid='" + ddshg.SelectedValue + "' and date<>'0' and convert(datetime, date, 103) between convert(datetime, '" + from.Text + "', 103) and convert(datetime, '" + to.Text + "', 103)) s");
            if (liabilitytotalothers.Text == "")
            {
                liabilitytotalothers.Text = "0";
            }

            //  liabilitytotalothers = Alib.idGetAFieldByQuery("select sum(distinct(convert(int, shgotherssavings))) from [vikasardo].[vikasa].meeting where shgid='" + ddshg.SelectedValue + "' and cdeid='" + cdeid + "' and convert(datetime, date, 103) between convert(datetime, '" + from + "', 103) and convert(datetime, '" + to + "', 103) and date<>'0'");
            liabilitytotal.Text = (Convert.ToDouble(liabilitymembershipFee.Text) + Convert.ToDouble(liabilitytotalsavings.Text) + Convert.ToDouble(liabilitytotalinterest.Text) + Convert.ToDouble(liabilitytotalpenalty.Text) + Convert.ToDouble(liabilitytotalbankinterest.Text) + Convert.ToDouble(liabilitytotalfundreceived.Text) + Convert.ToDouble(liabilitytotalothers.Text)).ToString();

            assetstotalintlending.Text = Alib.idGetAFieldByQuery("select sum(convert(int, lendbal)) as SHG_InternalLending from [vikasardo].[vikasa].meeting where shgid='" + ddshg.SelectedValue + "' and meetings=(select top 1 meetings from [vikasardo].[vikasa].meeting where shgid='" + ddshg.SelectedValue + "' order by id desc)");
            if (assetstotalintlending.Text == "")
            {
                assetstotalintlending.Text = "0";
            }

            assetscashinhand.Text = Alib.idGetAFieldByQuery("select sum(convert(int, shgexpcashinhand)) as SHGCashinHand from [vikasardo].[vikasa].meeting where shgid='" + ddshg.SelectedValue + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + ddshg.SelectedValue + "' order by id desc)");
            if (assetscashinhand.Text == "")
            {
                assetscashinhand.Text = "0";
            }

            assetscashinbank.Text = Alib.idGetAFieldByQuery("select sum(convert(int, shgexpamountinbanksavings)) as SHGCashinBank from [vikasardo].[vikasa].meeting where shgid='" + ddshg.SelectedValue + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + ddshg.SelectedValue + "' order by id desc)");
            if (assetscashinbank.Text == "")
            {
                assetscashinbank.Text = "0";
            }

            assetsrid.Text = Alib.idGetAFieldByQuery("select rid from [vikasardo].[vikasa].shg where shgid='" + ddshg.SelectedValue + "'");
            //  assetsexpothers = Alib.idGetAFieldByQuery("select sum(convert(int, shgexpothers)) as SHG_expOthers from [vikasardo].[vikasa].meeting where cdeid='" + cdeid + "' and ddshg.SelectedValue='" + ddshg.SelectedValue + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + ddshg.SelectedValue + "' and cdeid='" + cdeid + "' order by id desc)");
            if (assetsrid.Text == "")
            {
                assetsrid.Text = "0";
            }

            assetsexpothers.Text = Alib.idGetAFieldByQuery("select sum(convert(int, s.shgexpothers)) from (select distinct(date), shgexpothers from [vikasardo].[vikasa].[meeting] where shgid='" + ddshg.SelectedValue + "' and date<>'0' and convert(datetime, date, 103) between convert(datetime, '" + from.Text + "', 103) and convert(datetime, '" + to.Text + "', 103)) s");
            assetsshgothers = Alib.idGetAFieldByQuery("select sum(convert(int, s.shgexp)) from (select distinct(date), shgexp from [vikasardo].[vikasa].[meeting] where shgid='" + ddshg.SelectedValue + "' and date<>'0' and convert(datetime, date, 103) between convert(datetime, '" + from.Text + "', 103) and convert(datetime, '" + to.Text + "', 103)) s");
            // NA // assetsshgothers = Alib.idGetAFieldByQuery("select expenses from [vikasardo].[vikasa].shg where shgid='" + ddshg.SelectedValue + "' and cdeid='" + cdeid + "'");
            if (assetsexpothers.Text == "")
            {
                assetsexpothers.Text = "0";
            }

            assetstotothers.Text = (Convert.ToDouble(assetsshgothers)).ToString();
            if (assetstotothers.Text == "")
            {
                assetstotothers.Text = "0";
            }
            assetstotal.Text = (Convert.ToDouble(assetstotalintlending.Text) + Convert.ToDouble(assetscashinhand.Text) + Convert.ToDouble(assetscashinbank.Text) + Convert.ToDouble(assetsrid.Text) + Convert.ToDouble(assetstotothers.Text) + Convert.ToDouble(assetsexpothers.Text)).ToString();

            pic1Bind();
            pic2Bind();
        }
        catch
        {

        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        string qur = "Select savingAccount as tmp from [vikasardo].[vikasa].[shg] where savingAccount like '" + pre + "%' ";
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string remaining = "";
        if (e.CommandName.Equals("Del"))
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            string bankemi = HttpUtility.HtmlDecode(rows.Cells[11].Text).ToString();
            lblid.Text = id;
            Alib.idExecute("delete from [vikasardo].[vikasa].[meeting] where memberid='" + lblid.Text + "' and meetings in (select max(meetings) from [vikasardo].[vikasa].[meeting] where memberid='" + lblid.Text + "')");

            if (Alib.idHasRows("SELECT memberid FROM [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] WHERE memberid='" + lblid.Text + "'"))
            {
                Alib.idExecute("Update [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] set remainingloan = remainingloan+ " + bankemi + " where memberid='" + lblid.Text + "' and loanstatus='0'");
                remaining = Alib.getSingleValue("Select top 1 remainingloan from [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] where memberid='" + lblid.Text + "' order by id desc");
                if (Convert.ToInt32(remaining) <= 0)
                {
                    Alib.idExecute("Update [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] set loanstatus='1' where memberid='" + lblid.Text + "'");
                    Alib.idExecute("Update [vikasardo].[vikasa].[shg] set loanstatus='0' where shgid='" + ddshg.SelectedValue + "'");
                }
                else
                {
                    Alib.idExecute("Update [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] set loanstatus='0' where memberid='" + lblid.Text + "'");
                    Alib.idExecute("Update [vikasardo].[vikasa].[shg] set loanstatus='1' where shgid='" + ddshg.SelectedValue + "'");
                }
            }



            if (Alib.idHasRows("SELECT shgid FROM [vikasardo].[vikasardo].[spc] WHERE shgid='" + ddshg.SelectedValue + "'"))
            {
                Alib.idExecute("Update [vikasardo].[vikasardo].[spc] set term = term+1, totrepay= totrepay- " + bankemi + ", lastrepay= lastrepay-" + bankemi + ", balance= balance+" + bankemi + " where shgid='" + ddshg.SelectedValue + "' and id=(select max(r.id) from [vikasardo].[vikasardo].[spc] r where r.shgid='" + ddshg.SelectedValue + "')");
                string balance = Alib.idGetAFieldByQuery("Select top 1 balance from [vikasardo].[vikasardo].[spc] where shgid='" + ddshg.SelectedValue + "' order by id desc");
                if (balance == "0")
                {
                    Alib.idExecute("Update [vikasardo].[vikasardo].[spc] set term = '0', status = '1' where shgid='" + ddshg.SelectedValue + "'");
                }
                if (balance != "0")
                {
                    Alib.idExecute("Update [vikasardo].[vikasardo].[spc] set term = '1', status = '0' where shgid='" + ddshg.SelectedValue + "'");
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Deleted successfully..!',text: '',timer: 2000,showConfirmButton: false},function(){location.href='shg.aspx'})", true);
            databind();
        }
    }
    protected void txtsbacc_TextChanged(object sender, EventArgs e)
    {
        try
        {
            string qur = "select * from [vikasardo].[vikasa].[shg] where savingAccount='" + txtsbacc.Text + "'";
            if (Alib.idHasRows(qur))
            {
                SqlConnection con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddshg.DataSource = ds;
                ddshg.DataTextField = "name";
                ddshg.DataValueField = "shgid";
                ddshg.DataBind();
                ddshg.Items.Insert(0, new ListItem("Select SHG", "0"));
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddshg_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
    protected void lnkapprove_Click(object sender, EventArgs e)
    {

    }
    protected void btnsub_Click(object sender, EventArgs e)
    {
        databind();
        pic1Bind();
        pic2Bind();
    }
}