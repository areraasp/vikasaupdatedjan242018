﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="members.aspx.cs" Inherits="members" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="assets/js/jquery.min.js"></script>
    <link href="assets/css/sweetalert.css" rel="stylesheet" />
    <script src="assets/js/sweetalert.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

    <link href="assets/js/jquery.timepicker.css" rel="stylesheet" />
    <script src="assets/js/jquery.timepicker.js"></script>
    <script src="assets/js/jquery.timepicker.min.js"></script>
    <style type="text/css">
        .img-zoom {
            width: 30px;
            -webkit-transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            -ms-transition: all .2s ease-in-out;
        }

        .transition {
            -webkit-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
        }
    </style>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.1.5"
        media="screen" />
    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#bfs5').addClass('active');
            $('#id3').addClass('active');
            $('.img-zoom').hover(function () {
                $(this).addClass('transition');
            }, function () {
                $(this).removeClass('transition');
            });
            $('.fancybox').fancybox();
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    overlay: {
                        speedOut: 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS: 'fancybox-custom',
                closeClick: true,
                openEffect: 'none',
                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        css: {
                            'background': 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });
        });
    </script>

    <style type="text/css">
        .fancybox-custom .fancybox-skin {
            box-shadow: 0 0 50px #222;
        }
    </style>
    <script>
        $(function () {
            $('#<%=txtsrch.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "members.aspx/GetInvName",
                        data: "{ 'pre':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (d) {
                            var data = JSON.parse(d.d);
                            console.log(data);
                            response($.map(data, function (item) {
                                return { value: item.tmp }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblid" runat="server" Visible="false"></asp:Label>
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Members</h4>
        </div>
    </div>
    <!--/ .page title -->

    <div class="row">
        <div class="col-sm-12 ">
            <p class="text-muted page-title-alt">
                <button type="button" id="Button2" style="float: right;" runat="server" class="btn btn-default waves-effect waves-light" onserverclick="Button2_ServerClick">
                    <span class="btn-label"><i class="fa fa-plus"></i>
                    </span>Create New</button>
            </p>
        </div>
        <div class="form-inline">
            <asp:TextBox ID="txtsrch" runat="server" class="form-control" placeholder="SHG-ID/Member Name/Adhaar Num" Width="25%"></asp:TextBox>
            <asp:Button ID="btnsrch" runat="server" CssClass="form-control" Text="Search" Width="10%" OnClick="btnsrch_Click" />
        </div>
    </div>
    <br />
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="row">
                <div class="col-lg-3 card-box">
                    <div class="form-group">
                        <label>Name</label>
                        <asp:TextBox ID="txtname" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Father/Husband</label>
                        <asp:TextBox ID="txtfname" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Age</label>
                        <asp:TextBox ID="txtdob" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Gender</label>
                        <asp:DropDownList ID="dgender" runat="server" class="form-control">
                            <asp:ListItem>Choose</asp:ListItem>
                            <asp:ListItem>MALE</asp:ListItem>
                            <asp:ListItem>FEMALE</asp:ListItem>
                            <asp:ListItem>MIXED</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>Aadhar Number</label>
                        <asp:TextBox ID="txtadhar" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Savings Acc</label>
                        <asp:TextBox ID="txtsavings" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-lg btn-default btn-custom" Text="Submit" OnClick="btnsubmit_Click" />
                        <asp:Button ID="btncancel" runat="server" CssClass="btn btn-lg btn-danger btn-custom" Text="Cancel" OnClick="btncancel_Click" />
                    </div>
                </div>
                <div class="col-lg-3 card-box">
                    <div class="form-group">
                        <label>Nominee</label>
                        <asp:TextBox ID="txtnominee" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Marital Status</label>
                        <asp:DropDownList ID="dmstatus" runat="server" class="form-control">
                            <asp:ListItem>Choose</asp:ListItem>
                            <asp:ListItem>SINGLE</asp:ListItem>
                            <asp:ListItem>MARRIED</asp:ListItem>
                            <asp:ListItem>WIDOWED</asp:ListItem>
                            <asp:ListItem>DIVORCED</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>Education</label>
                        <asp:TextBox ID="txtedu" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Caste</label>
                        <asp:TextBox ID="txtcaste" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Occupation</label>
                        <asp:TextBox ID="txtocc" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Type of Home</label>
                        <asp:DropDownList ID="dtoh" runat="server" class="form-control">
                            <asp:ListItem>Choose</asp:ListItem>
                            <asp:ListItem>PERMANENT</asp:ListItem>
                            <asp:ListItem>RENT</asp:ListItem>
                            <asp:ListItem>LEASE</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-3 card-box">
                    <div class="form-group">
                        <label>Property Type</label>
                        <asp:DropDownList ID="dpt" runat="server" class="form-control">
                            <asp:ListItem>Choose</asp:ListItem>
                            <asp:ListItem>IRRIGATION LAND</asp:ListItem>
                            <asp:ListItem>DRY LAND</asp:ListItem>
                            <asp:ListItem>NO LAND</asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    <div class="form-group">
                        <label>Govt Card</label>
                        <asp:DropDownList ID="dcd" runat="server" class="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>Contact Number</label>
                        <asp:TextBox ID="txtcontact" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>PAN</label>
                        <asp:TextBox ID="txtpan" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group" style="display: none;">
                        <label>Membership Fee</label>
                        <asp:TextBox ID="txtmf" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Size of Family</label>
                        <asp:TextBox ID="txtsof" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Elder Male</label>
                        <asp:TextBox ID="txtem" runat="server" class="form-control"></asp:TextBox>
                    </div>

                </div>
                <div class="col-lg-3 card-box">
                    <div class="form-group">
                        <label>Elder Female</label>
                        <asp:TextBox ID="txtefm" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Child Male</label>
                        <asp:TextBox ID="txtcm" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Child Female</label>
                        <asp:TextBox ID="txtcfm" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Photo</label>
                        <asp:FileUpload ID="fpimage" runat="server" Style="padding: 0px 0px;"></asp:FileUpload>
                    </div>
                    <div class="form-group">
                        <label>Signature</label>
                        <asp:FileUpload ID="fpsignature" runat="server" Style="padding: 0px 0px;"></asp:FileUpload>
                    </div>
                    <div class="form-group">
                        <label>CDE</label>
                        <asp:DropDownList ID="dcde" runat="server" class="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>SHG</label>
                        <asp:DropDownList ID="dshg" runat="server" class="form-control"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    
                </div>
            </div>
        </asp:View>
    </asp:MultiView>

    <div class="col-lg-12 card-box">
        <div class="table-responsive">
            <asp:GridView ID="gvsprovider" runat="server" EmptyDataText="No Data Available" EmptyDataRowStyle-CssClass="text-danger" EmptyDataRowStyle-Font-Size="Medium" CssClass="table table-bordered table-hover table-striped" OnPageIndexChanging="gvsprovider_PageIndexChanging"
                OnRowCommand="gvsprovider_RowCommand" AllowPaging="true" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:ButtonField CommandName="edi" ControlStyle-CssClass="btn btn-default btn-custom" ButtonType="Button"
                        Text="Edit" HeaderText="Edit"></asp:ButtonField>
                    <asp:ButtonField CommandName="Del" ControlStyle-CssClass="btn btn-danger btn-custom" ButtonType="Button"
                        Text="Delete" HeaderText="Delete"></asp:ButtonField>
                    <asp:BoundField DataField="memberID" HeaderText="Identity"></asp:BoundField>
                    <asp:BoundField DataField="name" HeaderText="SHG Member Name"></asp:BoundField>
                    <asp:BoundField DataField="fatherORhusband" HeaderText="Father/Husband"></asp:BoundField>
                    <asp:BoundField DataField="dob" HeaderText="DOB"></asp:BoundField>
                    <asp:BoundField DataField="gender" HeaderText="Gender"></asp:BoundField>
                    <asp:BoundField DataField="adhaarNum" HeaderText="Aadhar"></asp:BoundField>
                    <asp:BoundField DataField="savingAccNum" HeaderText="Savings Acc"></asp:BoundField>
                    <asp:BoundField DataField="mobile" HeaderText="Mobile"></asp:BoundField>
                    <asp:BoundField DataField="pan" HeaderText="PAN"></asp:BoundField>
                     <asp:BoundField DataField="cdeid" HeaderText="CDE"></asp:BoundField>
                    <asp:BoundField DataField="shgid" HeaderText="SHG"></asp:BoundField>
                    <asp:TemplateField HeaderText="Photo">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" data-fancybox-group="gallery" class="fancybox"
                                NavigateUrl='<%# Eval("image","images/{0}") %>'>
                                <asp:Image ID="picture" CssClass="img-zoom" runat="server" Width="50px" Height="50px" ImageUrl='<%# Eval("image","images/{0}") %>' />
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>                   
                    <asp:TemplateField HeaderText="Sign/Thumb">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" data-fancybox-group="gallery" class="fancybox"
                                NavigateUrl='<%# Eval("signature","images/{0}") %>'>
                                <asp:Image ID="picture1" CssClass="img-zoom" runat="server" Width="50px" Height="50px" ImageUrl='<%# Eval("signature","images/{0}") %>' />
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="gridview"></PagerStyle>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

