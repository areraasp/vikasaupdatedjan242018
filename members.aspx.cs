using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class members : System.Web.UI.Page
{
    int index = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
		if (Session["aauserid"] == null)
        {
            Response.Redirect("index.aspx");            
        }

        if (!IsPostBack)
        {
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].[members]", gvsprovider);
            cdeBind();
            shgBind();
            govtBind();
        }
    }
    public void cdeBind()
    {
        try
        {
            string qur = "select * from [vikasardo].[vikasa].[cde]";
            if (Alib.idHasRows(qur))
            {
                SqlConnection con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dcde.DataSource = ds;
                dcde.DataTextField = "cdename";
                dcde.DataValueField = "cdeid";
                dcde.DataBind();
                dcde.Items.Insert(0, new ListItem("Choose", "0"));
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void shgBind()
    {
        try
        {
            string qur = "select * from [vikasardo].[vikasa].[shg]";
            if (Alib.idHasRows(qur))
            {
                SqlConnection con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dshg.DataSource = ds;
                dshg.DataTextField = "name";
                dshg.DataValueField = "shgid";
                dshg.DataBind();
                dshg.Items.Insert(0, new ListItem("Choose", "0"));
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void govtBind()
    {
        try
        {
            string qur = "select * from [vikasardo].[vikasa].[govtCard]";
            if (Alib.idHasRows(qur))
            {
                SqlConnection con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dcd.DataSource = ds;
                dcd.DataTextField = "govtCard";
                dcd.DataValueField = "id";
                dcd.DataBind();
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }

   
    protected void Button2_ServerClick(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
        clean();
        cdeBind();
        shgBind();
        govtBind();
        btnsubmit.Text = "Submit";
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (txtname.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Name',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtfname.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Father/Husband Name',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtdob.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter DOB',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (dgender.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Select Gender',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtcontact.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Contact Number',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtadhar.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Aadhar Number',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtsavings.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Savings',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtnominee.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Nominee',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (dmstatus.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Select Marital Status',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtedu.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Education',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtcaste.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Caste',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtocc.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Occupation',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (dtoh.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Select Type of Home',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (dpt.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Select Property Type',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (dcd.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Select Government Card Type',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtpan.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter PAN Number',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtsof.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Size of family',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtem.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Male Elders',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtefm.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Female Elders',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtcm.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Male Child',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtcfm.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Female Child',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (dcde.SelectedItem.Text == "Choose")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Select CDE',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (dshg.SelectedItem.Text == "Choose")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Select SHG',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
      
        if (btnsubmit.Text == "Submit")
        {
            string fname = fpimage.FileName;
            fpimage.SaveAs(Server.MapPath("~/images/") + fname);

            string fsign = fpsignature.FileName;
            fpsignature.SaveAs(Server.MapPath("~/images/") + fname);

            string memberid = "";
            memberid = Alib.getSingleValue("INSERT INTO [vikasardo].[vikasa].[members] (name, fatherORhusband, dob, gender, adhaarNum, savingAccNum, nominee, maritalStatus, education, caste, occupation, typeOfHome, propertyType, govtCard, mobile, pan, sizeofFamily, eldermale, elderfemale, childmale, childfemale, image, signature, cdeid, shgid, status) Output Inserted.memberID values('" + txtname.Text + "','" + txtfname.Text + "','" + txtdob.Text + "','" + dgender.SelectedItem.Text + "','" + txtadhar.Text + "','" + txtsavings.Text + "','" + txtnominee.Text + "','" + dmstatus.SelectedItem.Text + "','" + txtedu.Text + "','" + txtcaste.Text + "','" + txtocc.Text + "','" + dtoh.SelectedItem.Text + "','" + dpt.SelectedItem.Text + "', '" + dcd.SelectedItem.Text + "', '" + txtcontact.Text + "','" + txtpan.Text + "','" + txtsof.Text + "','" + txtem.Text + "', '" + txtefm.Text + "','" + txtcm.Text + "','" + txtcfm.Text + "','" + fpimage.FileName + "','" + fpsignature.FileName + "','" + dcde.SelectedItem.Text + "', '" + dshg.SelectedItem.Text + "', '0')");
            Alib.idExecute("Insert into [vikasardo].[vikasa].[meeting] (memberid,name,date,place,persons,shgid,cdeid,savings,totalsavings,lendbal,lendemi,interest,lending,lendpending,bankloan,bankemi,bankloanpending,penalty,totalpenalty,totalcollections,meetings,shginbank,shginhand,shgsavings,shgtotsavings,shgtotlendrepay,shgtotlendinterest,shgtotintlending,shgtotbankemi,shgtotpenalty,shgtotbankinterest,shgtotgrants,shgtotcollection, shgexpcash,shgexpcheque,shgexpdepositetosavingsacc,shgexpdepositetobankacc,shgexpcashinhand,shgexpamountinbanksavings,shgexpmembersavingsrepayment,shgexpothers,shgexptotal)VALUES('" + memberid + "','" + txtname.Text + "','0','0','0','" + dshg.SelectedValue + "','" + Session["cdeid"].ToString() + "','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Successfully Updated',text: '',timer: 2000,showConfirmButton: false})", true);
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].[members]", gvsprovider);
            clean();
            return;
        }
        else
        {
            Alib.idUpdateTable("[vikasardo].[vikasa].[members]", "memberID=" + lblid.Text, "name", txtname.Text.Trim(), "fatherORhusband", txtfname.Text, "dob", txtdob.Text, "gender", dgender.SelectedItem.Text, "adhaarNum", txtadhar.Text.Trim(), "savingAccNum", txtsavings.Text.Trim(), "mobile", txtcontact.Text.Trim(), "pan", txtpan.Text.Trim(), "cdeid", dcde.SelectedItem.Text, "shgid", dshg.SelectedItem.Text);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Successfully Updated',text: '',timer: 2000,showConfirmButton: false})", true);
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].[members]", gvsprovider);
            clean();
            return;
        }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = -1;
        clean();
    }
    public void clean()
    {
        btnsubmit.Text = "Submit";
        txtname.Text  = txtcontact.Text = "";
        txtfname.Text = txtdob.Text = txtadhar.Text = txtsavings.Text = txtpan.Text = "";
        cdeBind();
        shgBind();
    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        if (txtsrch.Text == "")
        {
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].[members]", gvsprovider);
        }
        else
        {
            //Glib.LoadRequest("Select * from [vikasardo].[vikasa].[members] where name like '%" + txtsrch.Text.Trim() + "%' or adhaarNum like '%" + txtsrch.Text.Trim() + "%' order by memberID desc", gvsprovider);
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].[members] where (name='" + txtsrch.Text.Trim() + "' or adhaarNum='" + txtsrch.Text.Trim() + "' or shgid='" + txtsrch.Text.Trim() + "') order by memberID desc", gvsprovider);
        }
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("Del"))
            {
                index = Convert.ToInt32(e.CommandArgument);
                GridViewRow rows = gvsprovider.Rows[index];
                string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
                lblid.Text = id;
                Alib.idExecute("delete from [vikasardo].[vikasa].[members] where memberID='" + lblid.Text + "'");
                Glib.LoadRequest("Select * from [vikasardo].[vikasa].[members]", gvsprovider);
            }
            if (e.CommandName.Equals("edi"))
            {
                index = Convert.ToInt32(e.CommandArgument);
                GridViewRow rows = gvsprovider.Rows[index];
                string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
                lblid.Text = id;
                MultiView1.ActiveViewIndex = 0;
                btnsubmit.Text = "Update";

                SqlConnection conn = new SqlConnection(Alib.conStr);
                SqlCommand cmd = null;
                SqlDataReader reader = null;
                cmd = new SqlCommand("Select * from [vikasardo].[vikasa].[members] where memberID='" + lblid.Text + "'", conn);
                conn.Open();
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        txtname.Text = reader["name"].ToString();
                        txtfname.Text = reader["fatherORhusband"].ToString();
                        txtdob.Text = reader["dob"].ToString();
                        dgender.SelectedValue = reader["gender"].ToString();
                        txtadhar.Text = reader["adhaarNum"].ToString();
                        txtsavings.Text = reader["savingAccNum"].ToString();
                        txtnominee.Text = reader["nominee"].ToString();
                        dmstatus.SelectedItem.Text = reader["maritalStatus"].ToString();
                        txtedu.Text = reader["education"].ToString();
                        txtcaste.Text = reader["caste"].ToString();
                        txtocc.Text = reader["occupation"].ToString();
                        dtoh.SelectedItem.Text = reader["typeOfHome"].ToString();
                        dpt.SelectedItem.Text = reader["propertyType"].ToString();
                        dcd.SelectedItem.Text = reader["govtCard"].ToString();

                        txtcontact.Text = reader["mobile"].ToString();
                        txtpan.Text = reader["pan"].ToString();
                        txtsof.Text = reader["sizeofFamily"].ToString();
                        txtem.Text = reader["eldermale"].ToString();
                        txtefm.Text = reader["elderfemale"].ToString();
                        txtcm.Text = reader["childmale"].ToString();
                        txtcfm.Text = reader["childfemale"].ToString();

                        //
                        //txtcontact.Text = reader[""].ToString();
                        //txtpan.Text = reader[""].ToString();
                        
                        shgBind();
                        cdeBind();
                        dshg.SelectedItem.Text = reader["shgid"].ToString();
                        dcde.SelectedItem.Text = reader["cdeid"].ToString();
                    }
                }
                
            }
        }
        catch (Exception ex)
        {
        }
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        string qur = "Select top 10 name as tmp from [vikasardo].[vikasa].[members] where name like '" + pre + "%' union Select top 10 adhaarNum as tmp from [vikasardo].[vikasa].[members] where adhaarNum like '" + pre + "%'";
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }
    }
    protected void btnsrch_Click(object sender, EventArgs e)
    {
        if (txtsrch.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Member Name / Adhaar Number',text: '',timer: 1000,showConfirmButton: false})", true);
            return;
        }
        else
        {
            //Glib.LoadRequest("Select * from [vikasardo].[vikasa].[members] where name like '%" + txtsrch.Text.Trim() + "%' or adhaarNum like '%" + txtsrch.Text.Trim() + "%' order by memberID desc", gvsprovider);
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].[members] where (name='" + txtsrch.Text.Trim() + "' or adhaarNum='" + txtsrch.Text.Trim() + "' or shgid='"+txtsrch.Text.Trim()+"') order by memberID desc", gvsprovider);

        }
    }
}