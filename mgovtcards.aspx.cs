﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mgovtcards : System.Web.UI.Page
{
    int index = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Glib.LoadRequest("Select * from  [vikasardo].[vikasa].[govtCard]", gvsprovider);
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (txtname.Text.Trim().Equals(""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Task Name',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (btnsubmit.Text == "Submit")
        {
            Alib.idInsertInto("[vikasardo].[vikasa].[govtCard]", "govtCard", txtname.Text);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Successfully Updated',text: '',timer: 2000,showConfirmButton: false})", true);
            Glib.LoadRequest("Select * from  [vikasardo].[vikasa].[govtCard]", gvsprovider);
        }
        else
        {
            Alib.idExecute("Update  [vikasardo].[vikasa].[govtCard] set govtCard='" + txtname.Text + "' where id='" + lblid.Text + "'");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Successfully Updated',text: '',timer: 2000,showConfirmButton: false})", true);
            Glib.LoadRequest("Select * from  [vikasardo].[vikasa].[govtCard]", gvsprovider);
        }
        clean();
        return;
    }
    public void clean()
    {
        MultiView1.ActiveViewIndex = -1;
        txtname.Text = " ";
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = -1;
        txtname.Text = " ";
        
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Del"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            lblid.Text = id;
            Alib.idExecute("delete from  [vikasardo].[vikasa].[govtCard] where id=" + id);
            Glib.LoadRequest("Select * from  [vikasardo].[vikasa].[govtCard]", gvsprovider);
        }
        if (e.CommandName.Equals("edi"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            lblid.Text = id;
            MultiView1.ActiveViewIndex = 0;
            btnsubmit.Text = "Update";
            txtname.Text = HttpUtility.HtmlDecode(rows.Cells[3].Text).ToString();
        }
    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select * from  [vikasardo].[vikasa].[govtCard]", gvsprovider);
    }
    protected void Button2_ServerClick(object sender, EventArgs e)
    {
        clean();
        MultiView1.ActiveViewIndex = 0;
        btnsubmit.Text = "Submit";
    }
}