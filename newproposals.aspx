﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="newproposals.aspx.cs" Inherits="newproposals" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery-ui.js"></script>
    <link href="assets/js/jquery-ui.min.css" rel="stylesheet" />
    <link href="assets/css/sweetalert.css" rel="stylesheet" />
    <script src="assets/js/sweetalert.min.js"></script>
      <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Are You Sure..?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }

        function Confirm1() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Are You Sure you want to generate PDF..?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
        function Confirm2() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Are You Sure..?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
    <script>
        $(function () {
            $('#<%=txtsrch.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "newproposals.aspx/GetInvName",
                        data: "{ 'pre':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (d) {
                            var data = JSON.parse(d.d);
                            console.log(data);
                            response($.map(data, function (item) {
                                return { value: item.tmp }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                }
            });
        });
    </script>
      <link href="assets/js/jquery.timepicker.css" rel="stylesheet" />
    <script src="assets/js/jquery.timepicker.js"></script>
    <script src="assets/js/jquery.timepicker.min.js"></script>
    <style type="text/css">
        .img-zoom {
            width: 30px;
            -webkit-transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            -ms-transition: all .2s ease-in-out;
        }

        .transition {
            -webkit-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
        }
    </style>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.1.5"
        media="screen" />
    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#bfs5').addClass('active');
            $('#id3').addClass('active');
            $('.img-zoom').hover(function () {
                $(this).addClass('transition');
            }, function () {
                $(this).removeClass('transition');
            });
            $('.fancybox').fancybox();
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    overlay: {
                        speedOut: 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS: 'fancybox-custom',
                closeClick: true,
                openEffect: 'none',
                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        css: {
                            'background': 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });
        });
    </script>

    <style type="text/css">
        .fancybox-custom .fancybox-skin {
            box-shadow: 0 0 50px #222;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="lbllatlon" runat="server"></asp:HiddenField>
    <asp:Label ID="lblid" runat="server" Visible="false"></asp:Label>
    <asp:HiddenField ID="hf1" runat="server" Visible="false" />

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">New Propsals</h4>
        </div>
    </div>
    <!--/ .page title -->

    <div class="row">
        <br />
        <div class="form-inline">
            <asp:TextBox ID="txtsrch" runat="server" class="form-control" placeholder="Enter SHG name / Branch" Width="25%"></asp:TextBox>
            <asp:Button ID="btnsrch" runat="server" CssClass="form-control" Text="Search" Width="10%" OnClick="btnsrch_Click" />
        </div>
    </div>
    <br />
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="row">
                <div class="col-lg-5 card-box">
                    <div class="form-group">
                        <label>Enter Service Provider Name</label>
                        <asp:TextBox ID="txtspname" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Enter Primary contact Number</label>
                        <asp:TextBox ID="txtphone" runat="server" class="form-control"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" SetFocusOnError="True"
                            ControlToValidate="txtphone" ErrorMessage="Must be Valid 10 digit" ForeColor="Red"
                            ValidationExpression="^[7-9][0-9]{9}$" Font-Size="Small"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-group">
                        <label>Enter Email ID</label>
                        <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                            runat="server" ErrorMessage="Please Enter Valid Email ID"
                            ValidationGroup="vgSubmit" ControlToValidate="txtEmail"
                            CssClass="requiredFieldValidateStyle"
                            ForeColor="Red"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                        </asp:RegularExpressionValidator>
                    </div>

                    <div class="form-group">
                        <label>Enter Address</label>
                        <asp:TextBox ID="txtadd" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-lg btn-default btn-custom" Text="Submit" />
                        <asp:Button ID="btncancel" runat="server" CssClass="btn btn-lg btn-danger btn-custom" Text="Cancel" />
                    </div>
                </div>
                <div class="col-lg-7">
                </div>
            </div>
            <!-- /.row -->
        </asp:View>
    </asp:MultiView>

    <div class="col-lg-12 card-box">
        <div class="table-responsive">
            <asp:GridView ID="gvsprovider" runat="server" EmptyDataText="No New Proposals" EmptyDataRowStyle-CssClass="text-danger" EmptyDataRowStyle-Font-Size="Medium" CssClass="table table-bordered table-hover table-striped" OnPageIndexChanging="gvsprovider_PageIndexChanging"
                OnRowCommand="gvsprovider_RowCommand" AutoGenerateColumns="false" AllowPaging="true" PageSize="10">
                <Columns>
                    <%--  <asp:ButtonField CommandName="edi" ControlStyle-CssClass="btn btn-default btn-custom" ButtonType="Button"
                        Text="Edit" HeaderText="Edit "></asp:ButtonField>--%>
                    <%-- <asp:ButtonField CommandName="Del" ControlStyle-CssClass="btn btn-danger" ButtonType="Button"
                        Text="Delete" HeaderText="Delete "></asp:ButtonField>--%>
                    <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkapprove" runat="server" ControlStyle-CssClass="btn btn-default btn-custom"
                                CommandArgument='<%#Eval("shgid") %>'
                                Text="Approve"
                                CommandName="approve"
                                OnClientClick="Confirm()" OnClick="lnkapprove_Click">Approve</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Download">
                        <ItemTemplate>
                            <asp:LinkButton ID="linkpdf" runat="server" ControlStyle-CssClass="btn btn-default btn-custom"
                                CommandArgument='<%#Eval("shgid") %>'
                                Text="PDF"
                                CommandName="download"
                                OnClientClick="Confirm1()" OnClick="linkpdf_Click">PDF</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reject">
                        <ItemTemplate>
                            <asp:LinkButton ID="linkrej" runat="server" ControlStyle-CssClass="btn btn-default btn-danger"
                                CommandArgument='<%#Eval("shgid") %>'
                                Text="Reject"
                                CommandName="reject"
                                OnClientClick="Confirm2()" OnClick="linkrej_Click">Reject</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="ShgName" HeaderText="ShgName"></asp:BoundField>
                    <asp:BoundField DataField="ShgID" HeaderText="ShgID"></asp:BoundField>
                    <asp:BoundField DataField="CDE_ID" HeaderText="CDE_ID"></asp:BoundField>
                    <asp:BoundField DataField="ShgLoanAmount" HeaderText="ShgLoanAmount"></asp:BoundField>
                    <asp:BoundField DataField="shgaccnum" HeaderText="SB Account"></asp:BoundField>
                    <asp:BoundField DataField="Reason" HeaderText="Reason"></asp:BoundField>
                    <asp:BoundField DataField="BankBranch" HeaderText="BankBranch"></asp:BoundField>

                    <asp:BoundField DataField="Split_Type" HeaderText="Split_Type"></asp:BoundField>
                    <asp:BoundField DataField="LoanEnqNo" HeaderText="LoanEnqNo"></asp:BoundField>
                    <asp:BoundField DataField="proposaldate" HeaderText="Proposal Date"></asp:BoundField>
                    <asp:TemplateField HeaderText="Training Report Image">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" data-fancybox-group="gallery" class="fancybox"
                                NavigateUrl='<%# Eval("reportimage","Reportimages/{0}") %>'>
                                <asp:Image ID="picture" CssClass="img-zoom" runat="server" Width="50px" Height="50px" ImageUrl='<%# Eval("reportimage","Reportimages/{0}") %>' />
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Training Photo Image">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink2" runat="server" data-fancybox-group="gallery" class="fancybox"
                                NavigateUrl='<%# Eval("photoimage","Photoimages/{0}") %>'>
                                <asp:Image ID="picture2" CssClass="img-zoom" runat="server" Width="50px" Height="50px" ImageUrl='<%# Eval("photoimage","Photoimages/{0}") %>' />
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="gridview"></PagerStyle>
            </asp:GridView>
        </div>
    </div>
</asp:Content>



