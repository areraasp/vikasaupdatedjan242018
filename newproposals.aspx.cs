﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class newproposals : System.Web.UI.Page
{
    int newnum;
    string loanrefid = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, momdate as MomDate, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where approvedstate='5' and status='5' group by shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type,loan_enq_num,proposaldate,reportimage,photoimage", gvsprovider);
            Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, shgaccnum as shgaccnum, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where approvedstate='5' and status='5' group by shgname,shgid,cdeid,shgloanamount,shgaccnum,reason,bankbrach,split_type,loan_enq_num,proposaldate,reportimage,photoimage", gvsprovider);
        }
    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (txtsrch.Text == "")
        {
            gvsprovider.PageIndex = e.NewPageIndex;
           // Glib.LoadRequest("select shgname as ShgName,shgid as ShgID,cdeid as CDE_ID,shgloanamount as ShgLoanAmount,momdate as MomDate,reason as Reason,bankbrach as BankBranch,split_type as Split_Type,loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where approvedstate='5' and status='5' group by shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type,loan_enq_num,proposaldate,reportimage,photoimage", gvsprovider);
            Glib.LoadRequest("select shgname as ShgName,shgid as ShgID,cdeid as CDE_ID,shgloanamount as ShgLoanAmount,shgaccnum as shgaccnum,reason as Reason,bankbrach as BankBranch,split_type as Split_Type,loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where approvedstate='5' and status='5' group by shgname,shgid,cdeid,shgloanamount,shgaccnum,reason,bankbrach,split_type,loan_enq_num,proposaldate,reportimage,photoimage", gvsprovider);
        }
        else
        {
            gvsprovider.PageIndex = e.NewPageIndex;
            //Glib.LoadRequest("Select shgname as ShgName,shgid as ShgID,cdeid as CDE_ID,shgloanamount as ShgLoanAmount,momdate as MomDate,reason as Reason,bankbrach as BankBranch,split_type as Split_Type,loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where shgname='" + txtsrch.Text.Trim() + "' and approvedstate='5' or bankbrach='" + txtsrch.Text.Trim() + "' and approvedstate='5' group by shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type,loan_enq_num, proposaldate,reportimage,photoimage order by shgname desc", gvsprovider);
            Glib.LoadRequest("Select shgname as ShgName,shgid as ShgID,cdeid as CDE_ID,shgloanamount as ShgLoanAmount,shgaccnum as shgaccnum,reason as Reason,bankbrach as BankBranch,split_type as Split_Type,loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where shgname='" + txtsrch.Text.Trim() + "' and approvedstate='5' or bankbrach='" + txtsrch.Text.Trim() + "' and approvedstate='5' group by shgname,shgid,cdeid,shgloanamount,shgaccnum,reason,bankbrach,split_type,loan_enq_num, proposaldate,reportimage,photoimage order by shgname desc", gvsprovider);
        }
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void lnkapprove_Click(object sender, EventArgs e)
    {
        string dateid = "";
        Random r = new Random();
        int tmp = r.Next(1000, 9999);
        dateid = System.DateTime.Now.Day.ToString() + "" + System.DateTime.Now.Month.ToString();
        loanrefid = generatenum();

        LinkButton link = sender as LinkButton;
        if (link.Text == "")
        {

        }
        else
        {
            //Looping through each Gridview row to find exact Row 
            //of the Grid from where the SelectedIndex change event is fired.
            foreach (GridViewRow row in gvsprovider.Rows)
            {
                //Finding Dropdown control  
                Control ctrl = row.FindControl("lnkapprove") as LinkButton;
                if (ctrl != null)
                {
                    LinkButton link1 = (LinkButton)ctrl;

                    //Comparing ClientID of the LinkButton with sender
                    //  if(ddl1.SelectedIndex==0
                    if (link.ClientID == link1.ClientID)
                    {
                        if (link.Text != "")
                        {
                            string shgname = row.Cells[3].Text;
                            lblid.Text = row.Cells[4].Text;
                            string cdeid = row.Cells[5].Text;
                            string shgloan = row.Cells[6].Text;
                            string loanenqno = row.Cells[11].Text;
                            string confirmValue = Request.Form["confirm_value"];
                            if (confirmValue == "Yes")
                            {
                                Alib.idExecute("Update [vikasardo].[vikasa].[shgexternalloansplit] set approvedstate='7', status='7', referenceID='" + loanrefid + "' where shgid='" + lblid.Text + "' and shgname='" + shgname + "' and shgloanamount='" + shgloan + "' and cdeid='" + cdeid + "' and loan_enq_num='" + loanenqno + "'");
                                //Glib.LoadRequest("select shgname as ShgName,shgid as ShgID,cdeid as CDE_ID,shgloanamount as ShgLoanAmount,momdate as MomDate,reason as Reason,bankbrach as BankBranch,split_type as Split_Type,loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].shgexternalloansplit where approvedstate='5' and status='5' group by shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type,loan_enq_num,proposaldate,reportimage,photoimage", gvsprovider);
                                Glib.LoadRequest("select shgname as ShgName,shgid as ShgID,cdeid as CDE_ID,shgloanamount as ShgLoanAmount,shgaccnum as shgaccnum,reason as Reason,bankbrach as BankBranch,split_type as Split_Type,loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].shgexternalloansplit where approvedstate='5' and status='5' group by shgname,shgid,cdeid,shgloanamount,shgaccnum,reason,bankbrach,split_type,loan_enq_num,proposaldate,reportimage,photoimage", gvsprovider);
                                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Approved Successfully..!!')", true);


                                //-----------------Message Send Starts-----------------------//

                                string cdmnum = Alib.idGetAFieldByQuery("Select m.cdmphone from [vikasardo].[vikasa].[cdm] m inner join [vikasardo].[vikasa].[cde] e on e.cdmid = m.cdmid where e.cdeid ='" + cdeid + "'");
                                string cdmname = Alib.idGetAFieldByQuery("Select m.cdmname from [vikasardo].[vikasa].[cdm] m inner join [vikasardo].[vikasa].[cde] e on e.cdmid = m.cdmid where e.cdeid ='" + cdeid + "'");
                                string wish = "Dear " + cdmname + ",\n HO Panel has approved your requested proposal of SHG-" + shgname + ", Inform related CDE to make disbursment loan amount equally and make recovery monthly ,\n Regards,\n HO Panel";

                                Session["sponsnum"] = loanrefid;
                                Session["approved"] = "";
                                SqlConnection con = null;
                                SqlDataReader rdr = null;
                                try
                                {
                                    WebClient Client = new WebClient();
                                    Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + cdmnum);
                                    Stream data = Client.OpenRead(uri);
                                    StreamReader reader = new StreamReader(data);
                                    string s = reader.ReadToEnd();
                                    data.Close();
                                    reader.Close();
                                }
                                catch (SqlException ex)
                                {
                                }
                                finally
                                {
                                    if (con != null)
                                    {
                                        con.Close();
                                        con.Dispose();
                                    }
                                    if (rdr != null)
                                        rdr.Dispose();
                                }

                                //-----------------Message Send Ends-------------------------//
                            }
                        }
                        else
                        {
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked No..!!')", true);
                        }
                    }
                }
            }
        }
    }

    //Used to generate ID in Sponser Page
    private string generatenum()
    {

        int oldnum = Convert.ToInt32(Alib.idGetAFieldByQuery("select max(number) from [vikasardo].[vikasa].sponsernum"));
        newnum = oldnum + 1;
        Alib.idInsertInto("sponsernum", "number", newnum.ToString());
        String numb = "";
        if (newnum.ToString().Length == 1)
        {
            numb = "000" + newnum.ToString();
        }
        else if (newnum.ToString().Length == 2)
        {
            numb = "00" + newnum.ToString();
        }
        else if (newnum.ToString().Length == 3)
        {
            numb = "0" + newnum.ToString();
        }
        else if (newnum.ToString().Length == 4)
        {
            numb = newnum.ToString();
        }
        string pattern = "VRDO/SBIPROP/17-18/" + numb;
        hf1.Value = pattern;

        return pattern;
    }

    protected void linkpdf_Click(object sender, EventArgs e)
    {
        string dateid = "";
        Random r = new Random();
        int tmp = r.Next(1000, 9999);
        dateid = System.DateTime.Now.Day.ToString() + "" + System.DateTime.Now.Month.ToString();
        //loanrefid = generatenum();
        Session["approved"] = "";
        LinkButton link = sender as LinkButton;
        if (link.Text == "")
        {

        }
        else
        {
            //Looping through each Gridview row to find exact Row 
            //of the Grid from where the SelectedIndex change event is fired.
            foreach (GridViewRow row in gvsprovider.Rows)
            {
                //Finding Dropdown control  
                Control ctrl = row.FindControl("linkpdf") as LinkButton;
                if (ctrl != null)
                {
                    LinkButton link1 = (LinkButton)ctrl;

                    //Comparing ClientID of the LinkButton with sender
                    //  if(ddl1.SelectedIndex==0
                    if (link.ClientID == link1.ClientID)
                    {
                        if (link.Text != "")
                        {
                            //string shgname = row.Cells[1].Text;
                            //lblid.Text = row.Cells[2].Text;
                            //string cdeid = row.Cells[3].Text;
                            //string shgloan = row.Cells[4].Text;
                            //string loanenqno = row.Cells[9].Text;
                            string confirmValue = Request.Form["confirm_value"];
                            Session["shgname"] = row.Cells[3].Text;
                            Session["shgid"] = row.Cells[4].Text;
                            Session["cdeid"] = row.Cells[5].Text;
                            Session["shgloanamt"] = row.Cells[6].Text;
                            Session["bankbranch"] = row.Cells[9].Text;
                            Session["loanenqid"] = row.Cells[11].Text;

                            if (confirmValue == "Yes")
                            {                               
                                //Session["sponsnum"] = loanrefid;
                                Session["sponsnum"] = "";
                                Response.Redirect("pdf.aspx");
                            }
                        }
                        else
                        {
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked No..!!')", true);
                        }
                    }
                }
            }
        }
    }
    protected void btnsrch_Click(object sender, EventArgs e)
    {
        if (txtsrch.Text == "")
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter SHG Name or Branch',text: '',timer: 1000,showConfirmButton: false})", true);
           // Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, momdate as MomDate, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where approvedstate='5' and status='5' group by shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type,loan_enq_num,proposaldate,reportimage,photoimage", gvsprovider);
            Glib.LoadRequest("select shgname as ShgName, shgid as ShgID, cdeid as CDE_ID, shgloanamount as ShgLoanAmount, shgaccnum as shgaccnum, reason as Reason, bankbrach as BankBranch, split_type as Split_Type, loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where approvedstate='5' and status='5' group by shgname,shgid,cdeid,shgloanamount,shgaccnum,reason,bankbrach,split_type,loan_enq_num,proposaldate,reportimage,photoimage", gvsprovider);
            return;
        }
        else
        {
            //Glib.LoadRequest("Select shgname as ShgName,shgid as ShgID,cdeid as CDE_ID,shgloanamount as ShgLoanAmount,momdate as MomDate,reason as Reason,bankbrach as BankBranch,split_type as Split_Type,loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where shgname='" + txtsrch.Text.Trim() + "' and approvedstate='5' or bankbrach='" + txtsrch.Text.Trim() + "' and approvedstate='5' group by shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type,loan_enq_num, proposaldate,reportimage,photoimage order by shgname desc", gvsprovider);
            Glib.LoadRequest("Select shgname as ShgName,shgid as ShgID,cdeid as CDE_ID,shgloanamount as ShgLoanAmount,shgaccnum as shgaccnum,reason as Reason,bankbrach as BankBranch,split_type as Split_Type,loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where shgname='" + txtsrch.Text.Trim() + "' and approvedstate='5' or bankbrach='" + txtsrch.Text.Trim() + "' and approvedstate='5' group by shgname,shgid,cdeid,shgloanamount,shgaccnum,reason,bankbrach,split_type,loan_enq_num, proposaldate,reportimage,photoimage order by shgname desc", gvsprovider);
        }
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        //string qur = "Select top 10 shgname as tmp from [vikasardo].[vikasa].[shgexternalloansplit] where shgname like '" + pre + "%' and approvedstate='0' union Select top 10 bankbrach as tmp from [vikasardo].[vikasa].[shgexternalloansplit] where bankbrach like '" + pre + "%' and approvedstate='0'";
        string qur = "Select distinct shgname as tmp from [vikasardo].[vikasa].[shgexternalloansplit] where shgname like '" + pre + "%' and approvedstate='5' union Select top 10 bankbrach as tmp from [vikasardo].[vikasa].[shgexternalloansplit] where bankbrach like '" + pre + "%' and approvedstate='5'";
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }
    }
    protected void linkrej_Click(object sender, EventArgs e)
    {
        LinkButton link = sender as LinkButton;
        if (link.Text == "")
        {

        }
        else
        {
            //Looping through each Gridview row to find exact Row 
            //of the Grid from where the SelectedIndex change event is fired.
            foreach (GridViewRow row in gvsprovider.Rows)
            {
                //Finding Dropdown control  
                Control ctrl = row.FindControl("linkrej") as LinkButton;
                if (ctrl != null)
                {
                    LinkButton link1 = (LinkButton)ctrl;

                    //Comparing ClientID of the LinkButton with sender
                    //  if(ddl1.SelectedIndex==0
                    if (link.ClientID == link1.ClientID)
                    {
                        if (link.Text != "")
                        {
                            string shgname = row.Cells[3].Text;
                            lblid.Text = row.Cells[4].Text;
                            string cdeid = row.Cells[5].Text;
                            string shgloan = row.Cells[6].Text;
                            string loanenqno = row.Cells[11].Text;
                            string confirmValue = Request.Form["confirm_value"];
                            if (confirmValue == "Yes")
                            {
                                Alib.idExecute("update [vikasardo].[vikasa].[shgexternalloansplit] set approvedstate='8',status='8',rejreason='Reason:RejectedbyHO',rejbyid='HO',rejbyrole='HO' where shgid='" + lblid.Text + "' and shgname='" + shgname + "' and shgloanamount='" + shgloan + "' and cdeid='" + cdeid + "' and loan_enq_num='" + loanenqno + "'");
                                //Glib.LoadRequest("select shgname as ShgName,shgid as ShgID,cdeid as CDE_ID,shgloanamount as ShgLoanAmount,momdate as MomDate,reason as Reason,bankbrach as BankBranch,split_type as Split_Type,loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where approvedstate='5' and status='5' group by shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type,loan_enq_num,proposaldate,reportimage,photoimage", gvsprovider);
                                Glib.LoadRequest("select shgname as ShgName,shgid as ShgID,cdeid as CDE_ID,shgloanamount as ShgLoanAmount,shgaccnum as shgaccnum,reason as Reason,bankbrach as BankBranch,split_type as Split_Type,loan_enq_num as LoanEnqNo, proposaldate,reportimage,photoimage from [vikasardo].[vikasa].[shgexternalloansplit] where approvedstate='5' and status='5' group by shgname,shgid,cdeid,shgloanamount,shgaccnum,reason,bankbrach,split_type,loan_enq_num,proposaldate,reportimage,photoimage", gvsprovider);
                                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Rejected Successfully..!!')", true);
                            }
                        }
                        else
                        {
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked No..!!')", true);
                        }
                    }
                }
            }
        }
    }
}