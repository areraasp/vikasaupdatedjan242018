﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class npa : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            calc();
        }
    }
     public void calc()
    {
        try
        {
            //SqlConnection conn = new SqlConnection(Alib.conStr);
            //SqlCommand cmd = null;
            //SqlDataReader reader = null;
            //cmd = new SqlCommand("select top 1 m1.memberid,m1.image,m2.totalsavings,m2.memberid,m2.name,m2.lending,m2.lendpending,m2.bankloan,m2.bankloanpending,m2.memberid from [vikasardo].[vikasa].members m1 inner join [vikasardo].[vikasa].meeting m2 on m1.memberid=m2.memberid where m2.shgid ='" + shgid + "' and m2.cdeid='" + cdeid + "' and m2.memberid='" + membid + "' order by id desc", conn);
            //conn.Open();
            //reader = cmd.ExecuteReader();
            //if (reader.HasRows)
            //{
            //    while (reader.Read())
            //    {
            //        np.Add(new MemberProfile { memberid = reader["memberid"].ToString(), name = reader["name"].ToString(), totalsavings = reader["totalsavings"].ToString(), lending = reader["lending"].ToString(), image = "http://test.vikasardo.org/images/" + reader["image"].ToString(), lendpending = reader["lendpending"].ToString(), bankloan = reader["bankloan"].ToString(), bankloanpending = reader["bankloanpending"].ToString() });
            //    }
            //}

            //----------------------------------------------------------------------//
            string qur4 = "Select id as Sno, shgid as [SHG ID], shg as SHG, village as Village, term as Terms, emi as EMI, loanamt as Loan, totrepay as Repayment, lastrepay as [Last Pay], repaydate as [Repay Date], balance as Balance, ((((DATEDIFF(d, convert(datetime, loanaccdate, 103), CONVERT(datetime, '" + System.DateTime.Now.AddHours(5).AddMinutes(30).ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totrepay) as NPA from [vikasardo].[vikasardo].[spc] where ((((DATEDIFF(d, convert(datetime, loanaccdate, 103), CONVERT(datetime, '" + System.DateTime.Now.AddHours(5).AddMinutes(30).ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totrepay) >= (emi*3)";
            try
            {
                gvsprovider.DataSource = Alib.getData(qur4);
                gvsprovider.DataBind();
            }
            catch (Exception ex)
            {
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
}