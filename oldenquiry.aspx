﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="oldenquiry.aspx.cs" Inherits="oldenquiry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="assets/js/jquery.min.js"></script>
    <link href="assets/css/sweetalert.css" rel="stylesheet" />
    <script src="assets/js/sweetalert.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

    <link href="assets/js/jquery.timepicker.css" rel="stylesheet" />
    <script src="assets/js/jquery.timepicker.js"></script>
    <script src="assets/js/jquery.timepicker.min.js"></script>
    <style type="text/css">
        .img-zoom {
            width: 30px;
            -webkit-transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            -ms-transition: all .2s ease-in-out;
        }

        .hiddencol {
            display: none;
        }

        .transition {
            -webkit-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
        }
    </style>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.1.5"
        media="screen" />
    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#bfs5').addClass('active');
            $('#id3').addClass('active');
            $('.img-zoom').hover(function () {
                $(this).addClass('transition');
            }, function () {
                $(this).removeClass('transition');
            });
            $('.fancybox').fancybox();
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    overlay: {
                        speedOut: 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS: 'fancybox-custom',
                closeClick: true,
                openEffect: 'none',
                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        css: {
                            'background': 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });

            $(function () {
                $('#bfs2').addClass('active');
                $('#<%=txtfrom.ClientID %>').datepicker({ dateFormat: "dd-mm-yy" });
                $('#<%=txtto.ClientID %>').datepicker({ dateFormat: "dd-mm-yy" });
            });
        });
    </script>
    <style type="text/css">
        .fancybox-custom .fancybox-skin {
            box-shadow: 0 0 50px #222;
        }

        .navbar-inverse {
            background-color: #797979;
            border-color: #797979;
            width: 65%;
        }

        .hiddencol {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblid" runat="server" Visible="false"></asp:Label>
    <asp:HiddenField ID="hiddenlastrepay" runat="server" />
    <div class="row">
        <div class="col-sm-10 ">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav" runat="server" id="listdata1">
                            <li class="active"><a href="oldenquiry.aspx">Enquiry</a></li>
                            <li><a href="oldfinalised.aspx">Finalize</a></li>
                            <%--  <li><a href="completed.aspx">Completed</a></li>--%>
                            <li><a href="olddue.aspx">Due</a></li>
                            <li><a href="oldirregular.aspx">Irregular</a></li>
                            <li><a href="oldnpa.aspx">NPA</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
            <!--/ .page title -->
        </div>
    </div>
    <br />
    <div class="col-md-3">
        <asp:DropDownList ID="ddlbranch" runat="server" CssClass="form-control"></asp:DropDownList>
    </div>
    <div class="col-md-3">
        <asp:TextBox ID="txtfrom" runat="server" CssClass="form-control" placeholder="Enter From Date"></asp:TextBox>
    </div>
    <div class="col-md-3">
        <asp:TextBox ID="txtto" runat="server" CssClass="form-control" placeholder="Enter To Date"></asp:TextBox>
    </div>
    <div class="col-md-3">
        <asp:Button ID="btnsubmit" runat="server" Text="Search" CssClass="btn btn-lg btn-default btn-custom" OnClick="btnsubmit_Click"></asp:Button>
    </div>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="row">
                <h3>
                    <asp:Label ID="lblheader" runat="server" Text="Loan Details"></asp:Label></h3>
                <div class="col-lg-6 card-box">
                    <div class="form-group">
                        <label>SB Account</label>
                        <asp:TextBox ID="txtsb" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Terms</label>
                        <asp:TextBox ID="txtterms" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>EMI</label>
                        <asp:TextBox ID="txtemi" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Loan Amount</label>
                        <asp:TextBox ID="txtloanamount" runat="server" class="form-control"></asp:TextBox>
                    </div>
                   
                </div>
                <div class="col-lg-6 card-box">
                     <div class="form-group">
                        <label>Total Repayment</label>
                        <asp:TextBox ID="txttotrepayment" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Last Repayment</label>
                        <asp:TextBox ID="txtlastrepayment" runat="server" class="form-control" AutoPostBack="true" OnTextChanged="txtlastrepayment_TextChanged"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Repay Date</label>
                        <asp:TextBox ID="txtrepaydate" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Balance</label>
                        <asp:TextBox ID="txtbal" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <br />
                    <div class="form-group">
                        <asp:Button ID="btnupdate" runat="server" Text="Update" CssClass="btn btn-default btn-custom" Height="32" Width="80" Style="padding: 2px;" OnClick="btnupdate_Click" />
                        <asp:Button ID="btncan" runat="server" Text="Cancel" CssClass="btn btn-danger btn-custom" Height="32" Width="80" Style="padding: 2px;" OnClick="btncan_Click" />
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
    <div class="col-lg-12 card-box" style="margin-top: 2%;">
        <div class="table-responsive">
            <asp:GridView ID="gvsprovider" runat="server" Caption="Loan Enquiry List" EmptyDataText="No Data Available" EmptyDataRowStyle-CssClass="text-danger" EmptyDataRowStyle-Font-Size="Medium" CssClass="table table-bordered table-hover table-striped"
                AllowPaging="true" PageSize="10" AutoGenerateColumns="false" OnPageIndexChanging="gvsprovider_PageIndexChanging" OnRowCommand="gvsprovider_RowCommand">
                <Columns>
                    <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkapprove" runat="server" ControlStyle-CssClass="btn btn-default btn-custom"
                                CommandArgument='<%#Eval("Sno") %>'
                                Text="Move"
                                CommandName="approve"
                                OnClientClick="Confirm()" OnClick="lnkapprove_Click">Move</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField CommandName="edi" ControlStyle-CssClass="btn btn-default btn-custom" ButtonType="Button"
                        Text="Edit" HeaderText="Edit"></asp:ButtonField>
                    <asp:ButtonField CommandName="Del" ControlStyle-CssClass="btn btn-danger btn-custom" ButtonType="Button"
                        Text="Del" HeaderText="Del"></asp:ButtonField>
                    <asp:BoundField DataField="Sno" HeaderText="Sno"></asp:BoundField>
                    <asp:BoundField DataField="CDE" HeaderText="CDE" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                    <asp:BoundField DataField="SHG" HeaderText="SHG"></asp:BoundField>
                     <asp:BoundField DataField="branch" HeaderText="Branch"></asp:BoundField>
                    <asp:BoundField DataField="SHGName" HeaderText="SHG Name"></asp:BoundField>
                    <asp:BoundField DataField="Members" HeaderText="Members"></asp:BoundField>
                    <asp:BoundField DataField="SBAcc" HeaderText="SB Account"></asp:BoundField>
                    <asp:BoundField DataField="LoanDate" HeaderText="Laon Date"></asp:BoundField>
                    <asp:BoundField DataField="Term" HeaderText="Terms"></asp:BoundField>
                    <asp:BoundField DataField="EMI" HeaderText="EMI"></asp:BoundField>
                    <asp:BoundField DataField="LoanAmt" HeaderText="Loan Amount"></asp:BoundField>
                    <asp:BoundField DataField="TotalRepay" HeaderText="Total Repay"></asp:BoundField>
                    <asp:BoundField DataField="LastRepay" HeaderText="Last Repay"></asp:BoundField>
                    <asp:BoundField DataField="RepayDate" HeaderText="Repay Date"></asp:BoundField>
                    <asp:TemplateField HeaderText="Challan">
                        <ItemTemplate>
                            <asp:Image ID="Image2" runat="server" CssClass="img-zoom" Width="30px" Height="30px" ImageUrl='<%# Eval("Challan","challan/{0}") %>'></asp:Image>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Bal" HeaderText="Balance"></asp:BoundField>
                    <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                </Columns>
                <PagerStyle CssClass="gridview"></PagerStyle>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

