﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class oldenquiry : System.Web.UI.Page
{
    int index = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            branchBind();
            //Glib.LoadRequest("Select * from [vikasardo].[vikasardo].[spc] where status='0' order by id desc", gvsprovider);
        }
    }
    public void branchBind()
    {
        try
        {
            string qur = "select * from [vikasardo].[vikasa].[bankBranch]";
            if (Alib.idHasRows(qur))
            {
                SqlConnection con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddlbranch.DataSource = ds;
                ddlbranch.DataTextField = "name";
                ddlbranch.DataValueField = "bankbranchID";
                ddlbranch.DataBind();
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }

   

    protected void btnsrch_Click(object sender, EventArgs e)
    {

    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        Alib.idExecute("Update [vikasardo].[vikasardo].[oldspc] set sbaccno='" + txtsb.Text + "', term='" + txtterms.Text + "', emi='" + txtemi.Text + "', loanamt='" + txtloanamount.Text + "', totalRecovery='" + txttotrepayment.Text + "', presentRecovery='" + txtlastrepayment.Text + "', repaydate='" + txtrepaydate.Text + "', balRecovery='" + txtbal.Text + "' where id='" + lblid.Text + "'");
        string balance = Alib.idGetAFieldByQuery("Select balRecovery from [vikasardo].[vikasardo].[oldspc] where id='" + lblid.Text + "'");
        if (balance != "0")
        {
            Alib.idExecute("Update [vikasardo].[vikasardo].[oldspc] set status = '0' where id='" + lblid.Text + "'");
        }
        Glib.LoadRequest("Select oldspc.id as Sno, oldspc.oldshgid as SHG,oldspc.branch, oldspc.cdeid as CDE, oldspc.shg as SHGName, oldspc.sbaccno as SBAcc, oldspc.members as Members, oldspc.loandate as LoanDate, oldspc.emi as EMI, oldspc.term as Term, oldspc.loanamt as LoanAmt, oldspc.totalRecovery as TotalRepay, oldspc.presentRecovery as LastRepay, oldspc.repaydate as RepayDate, oldspc.challan as Challan, oldspc.balRecovery as Bal, oldspc.status as Status from [vikasardo].[vikasardo].[oldspc] oldspc inner join [vikasardo].[vikasardo].[oldshg] oldshg on oldshg.oldshgid= oldspc.oldshgid where oldspc.status='0' and oldshg.Branch_Name ='" + ddlbranch.SelectedItem.Text + "' and convert(datetime, RepayDate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by id desc", gvsprovider);
  
 ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Updated Successfully',text: '',timer: 2000,showConfirmButton:   false})", true);
        return;
}
    protected void btncan_Click(object sender, EventArgs e)
    {

    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select oldspc.id as Sno, oldspc.oldshgid as SHG, oldspc.branch, oldspc.cdeid as CDE, oldspc.shg as SHGName, oldspc.sbaccno as SBAcc, oldspc.members as Members, oldspc.loandate as LoanDate, oldspc.emi as EMI, oldspc.term as Term, oldspc.loanamt as LoanAmt, oldspc.totalRecovery as TotalRepay, oldspc.presentRecovery as LastRepay, oldspc.repaydate as RepayDate, oldspc.challan as Challan, oldspc.balRecovery as Bal, oldspc.status as Status from [vikasardo].[vikasardo].[oldspc] oldspc inner join [vikasardo].[vikasardo].[oldshg] oldshg on oldshg.oldshgid= oldspc.oldshgid where oldspc.status='0' and oldshg.Branch_Name ='" + ddlbranch.SelectedItem.Text + "' order by id desc", gvsprovider);
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Del"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[3].Text).ToString();
            lblid.Text = id;
            Alib.idExecute("delete from [vikasardo].[vikasardo].[oldspc] where id=" + id);
            Glib.LoadRequest("Select oldspc.id as Sno, oldspc.oldshgid as SHG, oldspc.branch, oldspc.cdeid as CDE, oldspc.shg as SHGName, oldspc.sbaccno as SBAcc, oldspc.members as Members, oldspc.loandate as LoanDate, oldspc.emi as EMI, oldspc.term as Term, oldspc.loanamt as LoanAmt, oldspc.totalRecovery as TotalRepay, oldspc.presentRecovery as LastRepay, oldspc.repaydate as RepayDate, oldspc.challan as Challan, oldspc.balRecovery as Bal, oldspc.status as Status from [vikasardo].[vikasardo].[oldspc] oldspc inner join [vikasardo].[vikasardo].[oldshg] oldshg on oldshg.oldshgid= oldspc.oldshgid where oldspc.status='0' and oldshg.Branch_Name ='" + ddlbranch.SelectedItem.Text + "' and convert(datetime, RepayDate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by id desc", gvsprovider);
        }
        if (e.CommandName.Equals("edi"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[3].Text).ToString();
            lblid.Text = id;
            MultiView1.ActiveViewIndex = 0;
            txtsb.Text = HttpUtility.HtmlDecode(rows.Cells[9].Text).ToString();
            txtterms.Text = HttpUtility.HtmlDecode(rows.Cells[11].Text).ToString();
            txtemi.Text = HttpUtility.HtmlDecode(rows.Cells[12].Text).ToString();
            txtloanamount.Text = HttpUtility.HtmlDecode(rows.Cells[13].Text).ToString();
            txttotrepayment.Text = HttpUtility.HtmlDecode(rows.Cells[14].Text).ToString();
            hiddenlastrepay.Value = txtlastrepayment.Text = HttpUtility.HtmlDecode(rows.Cells[15].Text).ToString();
            txtrepaydate.Text = HttpUtility.HtmlDecode(rows.Cells[16].Text).ToString();
            txtbal.Text = HttpUtility.HtmlDecode(rows.Cells[18].Text).ToString();
        }
        if (e.CommandName.Equals("approve"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            string lastpay = Alib.idGetAFieldByQuery("select presentRecovery from [vikasardo].[vikasardo].[oldspc] where id='" + index + "' ");
            string spc = (Convert.ToDouble(lastpay) * 3 / 100).ToString();
            Alib.idExecute("Update [vikasardo].[vikasardo].[oldspc] set status = '2', spc='" + spc + "' where id='" + index + "'");
            Glib.LoadRequest("Select oldspc.id as Sno, oldspc.oldshgid as SHG,oldspc.branch, oldspc.cdeid as CDE, oldspc.shg as SHGName, oldspc.sbaccno as SBAcc, oldspc.members as Members, oldspc.loandate as LoanDate, oldspc.emi as EMI, oldspc.term as Term, oldspc.loanamt as LoanAmt, oldspc.totalRecovery as TotalRepay, oldspc.presentRecovery as LastRepay, oldspc.repaydate as RepayDate, oldspc.challan as Challan, oldspc.balRecovery as Bal, oldspc.status as Status from [vikasardo].[vikasardo].[oldspc] oldspc inner join [vikasardo].[vikasardo].[oldshg] oldshg on oldshg.oldshgid= oldspc.oldshgid where oldspc.status='0' and oldshg.Branch_Name ='" + ddlbranch.SelectedItem.Text + "' and convert(datetime, RepayDate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by id desc", gvsprovider);
        }
    }
    protected void btnaddnew_ServerClick(object sender, EventArgs e)
    {

    }
    protected void txtlastrepayment_TextChanged(object sender, EventArgs e)
    {
        txttotrepayment.Text = (Convert.ToDouble(txttotrepayment.Text) - Convert.ToDouble(txtlastrepayment.Text)).ToString();
        txtbal.Text = (Convert.ToDouble(txtbal.Text) + Convert.ToDouble(hiddenlastrepay.Value) - Convert.ToDouble(txtlastrepayment.Text)).ToString();
    }
    protected void lnkapprove_Click(object sender, EventArgs e)
    {

    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Glib.LoadRequest("Select oldspc.id as Sno, oldspc.oldshgid as SHG, oldspc.branch, oldspc.cdeid as CDE, oldspc.shg as SHGName, oldspc.sbaccno as SBAcc, oldspc.members as Members, oldspc.loandate as LoanDate, oldspc.emi as EMI, oldspc.term as Term, oldspc.loanamt as LoanAmt, oldspc.totalRecovery as TotalRepay, oldspc.presentRecovery as LastRepay, oldspc.repaydate as RepayDate, oldspc.challan as Challan, oldspc.balRecovery as Bal, oldspc.status as Status from [vikasardo].[vikasardo].[oldspc] oldspc inner join [vikasardo].[vikasardo].[oldshg] oldshg on oldshg.oldshgid= oldspc.oldshgid where oldspc.status='0' and oldshg.Branch_Name ='" + ddlbranch.SelectedItem.Text + "' and convert(datetime, RepayDate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by id desc", gvsprovider);
        }
        catch (Exception ex)
        {
        }
    }
}