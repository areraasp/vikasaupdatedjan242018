using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class oldfinalised : System.Web.UI.Page
{
    int index = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           // Glib.LoadRequest("Select oldspc.id as Sno, oldspc.cdeid as CDE, oldspc.oldshgid as SHG, oldspc.shg as SHGName, oldspc.members as Members, oldspc.sbaccno as SBAcc, oldspc.loandate as LoanDate, oldspc.term as Term, oldspc.emi as EMI, oldspc.loanamt as LoanAmt, oldspc.totalRecovery as TotalRepay, oldspc.presentRecovery as LastRepay, oldspc.repaydate as RepayDate, oldspc.challan as Challan, oldspc.balRecovery as Bal, oldspc.status as Status, oldspc.spc as spc from [vikasardo].[vikasardo].[oldspc] oldspc inner join [vikasardo].[vikasardo].[oldshg] oldshg on oldshg.oldshgid= oldspc.oldshgid where oldspc.status='2' and oldshg.Branch_Name ='" + ddlbranch.SelectedItem.Text + "' and convert(datetime, RepayDate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by id desc", gvsprovider);
            // earlier Glib.LoadRequest("Select oldspc.id as Sno, oldspc.cdeid as CDE, oldspc.oldshgid as SHG, oldspc.shg as SHGName, oldspc.members as Members, oldspc.sbaccno as SBAcc, oldspc.loandate as LoanDate, oldspc.term as Term, oldspc.emi as EMI, oldspc.loanamt as LoanAmt, oldspc.totalRecovery as TotalRepay, oldspc.presentRecovery as LastRepay, oldspc.repaydate as RepayDate, oldspc.challan as Challan, oldspc.balRecovery as Bal, oldspc.status as Status, oldspc.spc as spc from [vikasardo].[vikasardo].[oldspc] oldspc inner join [vikasardo].[vikasardo].[oldshg] oldshg on oldshg.oldshgid = oldspc.oldshgid where oldspc.status='2' order by id desc", gvsprovider);
            //branchBind();
            Glib.LoadRequest("Select oldspc.id as Sno, oldspc.cdeid as CDE, oldspc.oldshgid as SHG, oldspc.shg as SHGName, oldspc.members as Members, oldspc.sbaccno as SBAcc, oldspc.loandate as LoanDate, oldspc.term as Term, oldspc.emi as EMI, oldspc.loanamt as LoanAmt, oldspc.totalRecovery as TotalRepay, oldspc.presentRecovery as LastRepay, oldspc.repaydate as RepayDate, oldspc.challan as Challan, oldspc.balRecovery as Bal, oldspc.status as Status, oldspc.spc as spc from [vikasardo].[vikasardo].[oldspc] oldspc inner join [vikasardo].[vikasardo].[oldshg] oldshg on oldshg.oldshgid = oldspc.oldshgid where oldspc.status='2' order by id desc", gvsprovider);
            branchBind();
        }
    }
    public DataTable GetDataTableValue(string qur)
    {
        DataTable dt = null;
        try
        {
            dt = Alib.getData(qur).Tables[0];
        }
        catch
        {
        }
        return dt;
    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {

    }
    protected void btncan_Click(object sender, EventArgs e)
    {

    }
    protected void btnsrch_Click(object sender, EventArgs e)
    {

    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
      //  Glib.LoadRequest("Select oldspc.id as Sno, oldspc.cdeid as CDE, oldspc.oldshgid as SHG, oldspc.shg as SHGName, oldspc.members as Members, oldspc.sbaccno as SBAcc, oldspc.loandate as LoanDate, oldspc.term as Term, oldspc.emi as EMI, oldspc.loanamt as LoanAmt, oldspc.totalRecovery as TotalRepay, oldspc.presentRecovery as LastRepay, oldspc.repaydate as RepayDate, oldspc.challan as Challan, oldspc.balRecovery as Bal, oldspc.status as Status, oldspc.spc as spc from [vikasardo].[vikasardo].[oldspc] oldspc inner join [vikasardo].[vikasardo].[oldshg] oldshg on oldshg.oldshgid = oldspc.oldshgid where oldspc.status='2' order by id desc", gvsprovider);

 Glib.LoadRequest("Select oldspc.id as Sno, oldspc.cdeid as CDE, oldspc.oldshgid as SHG, oldspc.shg as SHGName, oldspc.members as Members, oldspc.sbaccno as SBAcc, oldspc.loandate as LoanDate, oldspc.term as Term, oldspc.emi as EMI, oldspc.loanamt as LoanAmt, oldspc.totalRecovery as TotalRepay, oldspc.presentRecovery as LastRepay, oldspc.repaydate as RepayDate, oldspc.challan as Challan, oldspc.balRecovery as Bal, oldspc.status as Status, oldspc.spc as spc from [vikasardo].[vikasardo].[oldspc] oldspc inner join [vikasardo].[vikasardo].[oldshg] oldshg on oldshg.oldshgid= oldspc.oldshgid where oldspc.status='2' and oldspc.Branch ='" + ddlbranch.SelectedItem.Text + "' and convert(datetime, RepayDate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by id desc", gvsprovider);


    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Received"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            Alib.idExecute("Update [vikasardo].[vikasardo].[oldspc] set status = '0' where id='" + index + "'");
            Glib.LoadRequest("Select oldspc.id as Sno, oldspc.cdeid as CDE, oldspc.oldshgid as SHG, oldspc.shg as SHGName, oldspc.members as Members, oldspc.sbaccno as SBAcc, oldspc.loandate as LoanDate, oldspc.term as Term, oldspc.emi as EMI, oldspc.loanamt as LoanAmt, oldspc.totalRecovery as TotalRepay, oldspc.presentRecovery as LastRepay, oldspc.repaydate as RepayDate, oldspc.challan as Challan, oldspc.balRecovery as Bal, oldspc.status as Status, oldspc.spc as spc from [vikasardo].[vikasardo].[oldspc] oldspc inner join [vikasardo].[vikasardo].[oldshg] oldshg on oldshg.oldshgid = oldspc.oldshgid where oldspc.status='2' order by id desc", gvsprovider);
        }
    }
    protected void btnaddnew_ServerClick(object sender, EventArgs e)
    {

    }
    protected void export_Click(object sender, EventArgs e)
    {
        DataTable dt = null;
        try
        {
            string bal = Alib.idGetAFieldByQuery("Select top 1 totalbalance from [vikasardo].[vikasardo].[oldbranchSPC] where branch='" + ddlbranch.SelectedItem.Text + "' order by sno desc");
            if (bal == "")
            {
                bal = "0";
            }

            float prvBal = float.Parse(bal);
            float totBal = prvBal + float.Parse(lbltotspc.Text);

            Alib.idExecute("Insert into [vikasardo].[vikasardo].[oldbranchSPC] (branch,balance,claimed,received,totalbalance,claimedDate,status) values ('" + ddlbranch.SelectedItem.Text + "','" + prvBal + "','" + lbltotspc.Text + "','0','" + totBal + "','" + System.DateTime.Now.ToString("dd/MM/yyyy") + "','0')");
			string qur = @"Select oldspc.shg as [SHG Name]
                        , oldshg.name_of_the_village as Village
                        , oldshg.loan_acc_no as [Loan Account]
                        , oldspc.loandate as [Loan Date]
                        , oldspc.loanamt as [Loan Amount]
                        , oldspc.repaydate as [Repayment Date]
                        , oldspc.presentRecovery as [Last Repayment]
                        , oldspc.spc as SPC
                          from[vikasardo].[vikasardo].[oldspc] oldspc inner join[vikasardo].[vikasardo].[oldshg] oldshg on oldshg.oldshgid = oldspc.oldshgid where branch = '" + ddlbranch.SelectedItem.Text + "'and oldspc.status = '2'  order by id desc";            


dt = GetDataTableValue(qur);
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                GridView GridView1 = new GridView();
                GridView1.DataSource = dt;
                GridView1.DataBind();

                GridView1.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in GridView1.HeaderRow.Cells)
                {
                    cell.BackColor = GridView1.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in GridView1.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = GridView1.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = GridView1.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                GridView1.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void lnkapprove_Click(object sender, EventArgs e)
    {

    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Glib.LoadRequest("Select oldspc.id as Sno, oldspc.cdeid as CDE, oldspc.oldshgid as SHG, oldspc.shg as SHGName, oldspc.members as Members, oldspc.sbaccno as SBAcc, oldspc.loandate as LoanDate, oldspc.term as Term, oldspc.emi as EMI, oldspc.loanamt as LoanAmt, oldspc.totalRecovery as TotalRepay, oldspc.presentRecovery as LastRepay, oldspc.repaydate as RepayDate, oldspc.challan as Challan, oldspc.balRecovery as Bal, oldspc.status as Status, oldspc.spc as spc from [vikasardo].[vikasardo].[oldspc] oldspc inner join [vikasardo].[vikasardo].[oldshg] oldshg on oldshg.oldshgid= oldspc.oldshgid where oldspc.status='2' and oldspc.Branch ='" + ddlbranch.SelectedItem.Text + "' and convert(datetime, RepayDate, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by id desc", gvsprovider);

            //-----------------------Total SPC--------------------------//

            double spc = 0.0;
            for (int i = 0; i <= gvsprovider.Rows.Count - 1; i++)
            {
                spc = spc + Convert.ToDouble(gvsprovider.Rows[i].Cells[16].Text);
            }
            lbltotspc.Text = spc.ToString();
            //---------------------End Total SPC-------------------------//

        }
        catch (Exception ex)
        {
        }
    }
    public void branchBind()
    {
        try
        {
            string qur = "select * from [vikasardo].[vikasa].[bankBranch]";
            if (Alib.idHasRows(qur))
            {
                SqlConnection con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddlbranch.DataSource = ds;
                ddlbranch.DataTextField = "name";
                ddlbranch.DataValueField = "bankbranchID";
                ddlbranch.DataBind();
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }
}