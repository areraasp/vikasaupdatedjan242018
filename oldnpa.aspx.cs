﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class oldnpa : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            calc();
        }
    }
    public void calc()
    {
        try
        {
            //SqlConnection conn = new SqlConnection(Alib.conStr);
            //SqlCommand cmd = null;
            //SqlDataReader reader = null;
            //cmd = new SqlCommand("select top 1 m1.memberid,m1.image,m2.totalsavings,m2.memberid,m2.name,m2.lending,m2.lendpending,m2.bankloan,m2.bankloanpending,m2.memberid from [vikasardo].[vikasa].members m1 inner join [vikasardo].[vikasa].meeting m2 on m1.memberid=m2.memberid where m2.shgid ='" + shgid + "' and m2.cdeid='" + cdeid + "' and m2.memberid='" + membid + "' order by id desc", conn);
            //conn.Open();
            //reader = cmd.ExecuteReader();
            //if (reader.HasRows)
            //{
            //    while (reader.Read())
            //    {
            //        np.Add(new MemberProfile { memberid = reader["memberid"].ToString(), name = reader["name"].ToString(), totalsavings = reader["totalsavings"].ToString(), lending = reader["lending"].ToString(), image = "http://test.vikasardo.org/images/" + reader["image"].ToString(), lendpending = reader["lendpending"].ToString(), bankloan = reader["bankloan"].ToString(), bankloanpending = reader["bankloanpending"].ToString() });
            //    }
            //}

            //----------------------------------------------------------------------//
            string qur4 = "Select id as Sno, oldshgid as [SHG ID], shg as SHG,branch as Branch, term as Terms, emi as EMI, loanamt as Loan, totalRecovery as Repayment, presentRecovery as [Last Pay], repaydate as [Repay Date], balRecovery as Balance, ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) as NPA from [vikasardo].[vikasardo].[oldspc] where ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) >= (emi*3)";
            try
            {
                gvsprovider.DataSource = Alib.getData(qur4);
                gvsprovider.DataBind();
            }
            catch (Exception ex)
            {
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (txtsrch.Text == "")
        {
            gvsprovider.PageIndex = e.NewPageIndex;
            Glib.LoadRequest("Select id as Sno, oldshgid as [SHG ID], shg as SHG,branch as Branch, term as Terms, emi as EMI, loanamt as Loan, totalRecovery as Repayment, presentRecovery as [Last Pay], repaydate as [Repay Date], balRecovery as Balance, ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) as NPA from [vikasardo].[vikasardo].[oldspc] where ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) >= (emi*3)", gvsprovider);
        }
        else
        {
            gvsprovider.PageIndex = e.NewPageIndex;
            Glib.LoadRequest("Select id as Sno, oldshgid as [SHG ID], shg as SHG,branch as Branch, term as Terms, emi as EMI, loanamt as Loan, totalRecovery as Repayment, presentRecovery as [Last Pay], repaydate as [Repay Date], balRecovery as Balance, ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) as NPA from [vikasardo].[vikasardo].[oldspc] where ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) >= (emi*3) and shg='" + txtsrch.Text + "' OR branch='" + txtsrch.Text + "'", gvsprovider);      
        }
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void btnsrch_Click(object sender, EventArgs e)
    {
        if (txtsrch.Text == "")
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter SHG Name or Branch',text: '',timer: 1000,showConfirmButton: false})", true);
            Glib.LoadRequest("Select id as Sno, oldshgid as [SHG ID], shg as SHG,branch as Branch, term as Terms, emi as EMI, loanamt as Loan, totalRecovery as Repayment, presentRecovery as [Last Pay], repaydate as [Repay Date], balRecovery as Balance, ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) as NPA from [vikasardo].[vikasardo].[oldspc] where ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) >= (emi*3)", gvsprovider);
            return;
        }
        else
        {
            Glib.LoadRequest("Select id as Sno, oldshgid as [SHG ID], shg as SHG,branch as Branch, term as Terms, emi as EMI, loanamt as Loan, totalRecovery as Repayment, presentRecovery as [Last Pay], repaydate as [Repay Date], balRecovery as Balance, ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) as NPA from [vikasardo].[vikasardo].[oldspc] where ((((DATEDIFF(d, convert(datetime, loandate, 103), CONVERT(datetime, '" + System.DateTime.Now.ToString("dd/MM/yyyy") + "', 103))/ 30)*EMI)) - totalRecovery) >= (emi*3) and shg='" + txtsrch.Text + "' OR branch='" + txtsrch.Text + "'", gvsprovider);
        }
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        string qur = " Select shg from [vikasardo].[vikasardo].[oldspc] where shg like '" + pre + "%' ";

        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }
    }
}