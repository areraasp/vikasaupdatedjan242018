﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="oldspcstatus.aspx.cs" Inherits="oldspcstatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="assets/js/jquery.min.js"></script>
    <link href="assets/css/sweetalert.css" rel="stylesheet" />
    <script src="assets/js/sweetalert.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

    <link href="assets/js/jquery.timepicker.css" rel="stylesheet" />
    <script src="assets/js/jquery.timepicker.js"></script>
    <script src="assets/js/jquery.timepicker.min.js"></script>
    <style type="text/css">
        .img-zoom {
            width: 30px;
            -webkit-transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            -ms-transition: all .2s ease-in-out;
        }

        .hiddencol {
            display: none;
        }

        .transition {
            -webkit-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
        }
    </style>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.1.5"
        media="screen" />
    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#bfs5').addClass('active');
            $('#id3').addClass('active');
            $('.img-zoom').hover(function () {
                $(this).addClass('transition');
            }, function () {
                $(this).removeClass('transition');
            });
            $('.fancybox').fancybox();
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    overlay: {
                        speedOut: 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS: 'fancybox-custom',
                closeClick: true,
                openEffect: 'none',
                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        css: {
                            'background': 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });

            $(function () {
                $('#bfs2').addClass('active');
                $('#<%=txtfrom.ClientID %>').datepicker({ dateFormat: "dd-mm-yy" });
                $('#<%=txtto.ClientID %>').datepicker({ dateFormat: "dd-mm-yy" });
            });
        });
    </script>
    <style type="text/css">
        .fancybox-custom .fancybox-skin {
            box-shadow: 0 0 50px #222;
        }

        .navbar-inverse {
            background-color: #797979;
            border-color: #797979;
            width: 65%;
        }

        .hiddencol {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblid" runat="server" Visible="false"></asp:Label>
    <div class="col-md-3">
        <asp:DropDownList ID="ddlbranch" runat="server" CssClass="form-control"></asp:DropDownList>
    </div>
    <div class="col-md-3">
        <asp:TextBox ID="txtfrom" runat="server" CssClass="form-control" placeholder="Enter From Date"></asp:TextBox>
    </div>
    <div class="col-md-3">
        <asp:TextBox ID="txtto" runat="server" CssClass="form-control" placeholder="Enter To Date"></asp:TextBox>
    </div>
    <div class="col-md-3">
        <asp:Button ID="Button1" runat="server" Text="Search" CssClass="btn btn-lg btn-default btn-custom" OnClick="Button1_Click"></asp:Button>
    </div>
    <br />
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <asp:ScriptManager ID="scr1" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="up1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-lg-4 card-box">
                            <div class="form-group">
                                <label>Branch</label>
                                <asp:TextBox ID="txtbranch" runat="server" placeholder="Enter Branch Name" class="form-control"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Prev Balance</label>
                                <asp:TextBox ID="txtprevbal" runat="server" placeholder="Enter Prev Balance" OnTextChanged="txtprevbal_TextChanged" AutoPostBack="true" class="form-control"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Claimed</label>
                                <asp:TextBox ID="txtclaimed" runat="server" placeholder="Enter Claimed Amount" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-4 card-box">
                            <div class="form-group">
                                <label>Received</label>
                                <asp:TextBox ID="txtreceived" runat="server" OnTextChanged="txtreceived_TextChanged" placeholder="Enter Received Amount" AutoPostBack="true" class="form-control"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Total Balance</label>
                                <asp:TextBox ID="txttotbal" runat="server" class="form-control" placeholder="Enter Total Balance"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Claimed Date</label>
                                <asp:TextBox ID="txtclaimeddate" runat="server" class="form-control" placeholder="Enter Claimed Date"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-4">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="col-md-12">
                <div class="form-group">
                    <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-lg btn-default btn-custom" Text="Submit" OnClick="btnsubmit_Click" />
                    <asp:Button ID="btncancel" runat="server" CssClass="btn btn-lg btn-danger btn-custom" Text="Cancel" OnClick="btncancel_Click" />
                </div>
            </div>
            <!-- /.row -->
        </asp:View>
    </asp:MultiView>
    <div class="col-md-12"> 
        <div class="table-responsive">
            <asp:GridView ID="gvsprovider" runat="server" EmptyDataText="No Data Available" Caption="Service Provider Charges" EmptyDataRowStyle-CssClass="text-danger" EmptyDataRowStyle-Font-Size="Medium" CssClass="table table-bordered table-hover table-striped"
                OnRowCommand="gvsprovider_RowCommand" AutoGenerateColumns="true">
                <Columns>
                    <asp:ButtonField CommandName="edi" ControlStyle-CssClass="btn btn-default btn-custom" ButtonType="Button"
                        Text="Edit" HeaderText="Edit"></asp:ButtonField>
                    <asp:ButtonField CommandName="Del" ControlStyle-CssClass="btn btn-danger btn-custom" ButtonType="Button"
                        Text="Del" HeaderText="Del"></asp:ButtonField>
                </Columns>
                <PagerStyle CssClass="gridview"></PagerStyle>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

