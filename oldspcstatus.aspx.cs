﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class oldspcstatus : System.Web.UI.Page
{
    int index = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            branchBind();
            Glib.LoadRequest("Select sno as Sno, branch as Branch, balance as [Prev Balance], claimed as Claimed, received as Received, totalbalance as [Total Balance], claimedDate as [Claimed Date] from [vikasardo].[vikasardo].[oldbranchSPC]", gvsprovider);
        }
    }
    public void branchBind()
    {
        try
        {
            string qur = "select * from [vikasardo].[vikasa].[bankBranch]";
            if (Alib.idHasRows(qur))
            {
                SqlConnection con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddlbranch.DataSource = ds;
                ddlbranch.DataTextField = "name";
                ddlbranch.DataValueField = "bankbranchID";
                ddlbranch.DataBind();
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Del"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            lblid.Text = id;
            Alib.idExecute("delete from [vikasardo].[vikasardo].[oldbranchSPC] where sno=" + id);
            Glib.LoadRequest("Select sno as Sno, branch as Branch, balance as [Prev Balance], claimed as Claimed, received as Received, totalbalance as [Total Balance], claimedDate as [Claimed Date] from [vikasardo].[vikasardo].[oldbranchSPC]", gvsprovider);
        }
        if (e.CommandName.Equals("edi"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            lblid.Text = id;
            MultiView1.ActiveViewIndex = 0;
            btnsubmit.Text = "Update";
            txtbranch.Text = HttpUtility.HtmlDecode(rows.Cells[3].Text).ToString();
            txtprevbal.Text = HttpUtility.HtmlDecode(rows.Cells[4].Text).ToString();
            txtclaimed.Text = HttpUtility.HtmlDecode(rows.Cells[5].Text).ToString();
            txtreceived.Text = HttpUtility.HtmlDecode(rows.Cells[6].Text).ToString();
            txttotbal.Text = HttpUtility.HtmlDecode(rows.Cells[7].Text).ToString();
            txtclaimeddate.Text = HttpUtility.HtmlDecode(rows.Cells[8].Text).ToString();
        }
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        Alib.idExecute("Update [vikasardo].[vikasardo].[oldbranchSPC] set balance='" + txtprevbal.Text + "', claimed='" + txtclaimed.Text + "', received='" + txtreceived.Text + "', totalbalance='" + txttotbal.Text + "', claimedDate='" + txtclaimeddate.Text + "' where sno='" + lblid.Text + "'");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Successfully Updated',text: '',timer: 2000,showConfirmButton: false})", true);
        Glib.LoadRequest("Select sno as Sno, branch as Branch, balance as [Prev Balance], claimed as Claimed, received as Received, totalbalance as [Total Balance], claimedDate as [Claimed Date] from [vikasardo].[vikasardo].[oldbranchSPC]", gvsprovider);
        clean();
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        clean();
    }
    public void clean()
    {
        txtbranch.Text = txtclaimed.Text = txtclaimeddate.Text = txtprevbal.Text = txtreceived.Text = txttotbal.Text = "";
    }
    protected void txtreceived_TextChanged(object sender, EventArgs e)
    {
        txttotbal.Text = (Convert.ToDouble(txtprevbal.Text) + Convert.ToDouble(txtclaimed.Text) - Convert.ToDouble(txtreceived.Text)).ToString("0.00");
    }
    protected void txtprevbal_TextChanged(object sender, EventArgs e)
    {
        txttotbal.Text = (Convert.ToDouble(txtprevbal.Text) + Convert.ToDouble(txtclaimed.Text) - Convert.ToDouble(txtreceived.Text)).ToString("0.00");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (txtfrom.Text != "" && txtto.Text != "")
        {
            try
            {
                Glib.LoadRequest("Select sno as Sno, branch as Branch, balance as [Prev Balance], claimed as Claimed, received as Received, totalbalance as [Total Balance], claimedDate as [Claimed Date] from [vikasardo].[vikasardo].[oldbranchSPC] where branch='" + ddlbranch.SelectedItem.Text + "' and convert(datetime,claimedDate,103) between convert(datetime,'" + txtfrom.Text + "', 103) and convert(datetime,'" + txtto.Text + "',103)", gvsprovider);
            }
            catch (Exception ex)
            {
            }
        }
        else
        {
            try
            {
                Glib.LoadRequest("Select sno as Sno, branch as Branch, balance as [Prev Balance], claimed as Claimed, received as Received, totalbalance as [Total Balance], claimedDate as [Claimed Date] from [vikasardo].[vikasardo].[oldbranchSPC] where branch='" + ddlbranch.SelectedItem.Text + "'", gvsprovider);
            }
            catch (Exception ex)
            {
            }
        }
    }
    
}