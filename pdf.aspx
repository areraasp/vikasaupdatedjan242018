﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="pdf.aspx.cs" Inherits="pdf" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery-1.11.0.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script type="text/javascript">
        function PrintPanel() {
            var panel = document.getElementById("pnlContents1");
            var printWindow = window.open('', '', 'height=400, width=1200');
            printWindow.document.write("<html><head></head><body>");
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>

    <style type="text/css">
        /*.auto-style1 {
            height: 23px;
        }

        .auto-style2 {
            text-decoration: underline;
        }

        .auto-style3 {
            height: 112px;
        }*/

        .auto-style4 {
            height: 90px;
            width: 385px;
        }

        /*.auto-style5 {
            font-size: large;
        }

        .auto-style7 {
            width: 53px;
        }

        .auto-style8 {
            height: 20px;
        }

        .auto-style9 {
            width: 238px;
            height: 20px;
        }*/

        /*.auto-style10 {
            width: 74px;
        }*/

        /*.auto-style11 {
            height: 20px;
            width: 74px;
        }

        .auto-style12 {
            width: 54px;
        }

        .auto-style13 {
            height: 20px;
            width: 54px;
        }

        .auto-style14 {
            width: 52px;
        }

        .auto-style15 {
            height: 20px;
            width: 52px;
        }

        .auto-style16 {
            width: 236px;
            height: 20px;
        }

        .auto-style17 {
            width: 53px;
            height: 20px;
        }

        .auto-style18 {
            height: 22px;
        }

        .auto-style19 {
            width: 53px;
            height: 22px;
        }

        .auto-style20 {
            width: 52px;
            height: 22px;
        }*/
        .auto-style5 {
            height: 109px;
        }

        .auto-style6 {
            height: 90px;
        }

        .auto-style8 {
            width: 501px;
        }

        .auto-style9 {
            width: 503px;
        }

        .auto-style10 {
            width: 504px;
        }

        .auto-style11 {
            width: 505px;
        }

        .auto-style12 {
            width: 507px;
        }

        .auto-style13 {
            width: 508px;
        }

        .auto-style17 {
            width: 103px;
        }

        .auto-style18 {
            width: 514px;
        }

        .auto-style21 {
            width: 252px;
        }

        .auto-style22 {
            width: 89px;
        }

        .auto-style23 {
            width: 180px;
        }

        .auto-style24 {
            width: 80px;
        }

        .auto-style25 {
            width: 82px;
        }

        .auto-style26 {
            width: 90px;
        }

        .auto-style27 {
            width: 157px;
        }

        .auto-style28 {
            width: 1332px;
        }

        .auto-style29 {
            width: 304px;
        }

        .auto-style30 {
            width: 150px;
        }

        .auto-style31 {
            width: 975px;
        }

        .auto-style32 {
            width: 153px;
        }

        .auto-style33 {
            width: 792px;
        }

        .auto-style34 {
            width: 153px;
            height: 23px;
        }

        .auto-style35 {
            width: 505px;
            height: 23px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="pnlContents1">
        <table style="width: 100%;">
            <tr>
                <td class="auto-style21"><strong>
                    <img id="Img1" src="images/sbi.png" runat="server" width="230" height="110" style="text-align: right;" /></strong></td>

                <td style="font-size: 16px;">
                    <asp:Label ID="lbl1" runat="server" Text="STATE BANK OF INDIA"></asp:Label>
                    <br class="auto-style13" />
                    <asp:Label ID="lblbankbranch" runat="server"></asp:Label><span>&nbsp;
                    <span>BRANCH</span></span></td>
            </tr>
            <tr>
                <td class="auto-style21">&nbsp;</td>

                <td style="font-size: 16px;">&nbsp;</td>
            </tr>
        </table>
        <table style="width: 100%; height: 400px;">
            <tr>
                <td style="border: thin solid #000000; text-align: center;">
                    <asp:Label ID="Label1" runat="server" Text="GROUP PHOTO OF THE SHG" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
        </table>
        <p>&nbsp;</p>
        <table style="border: thin solid #000000; width: 100%; font-size: 15px; height: 357px;">
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000">&nbsp;<asp:Label ID="Label2" runat="server" Text="NAME AND ADDRESS OF THE SHG" Style="font-size: 15px;"></asp:Label>
                </td>
                <td class="text-left" style="border: thin solid #000000">&nbsp;<asp:Label ID="lblnamenaddressofshg" runat="server" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000">&nbsp;<asp:Label ID="Label3" runat="server" Text="NAME OF THE REPRESENTATIVE 1" Style="font-size: 15px;"></asp:Label>
                </td>
                <td class="text-left" style="border: thin solid #000000">&nbsp;<asp:Label ID="lblnameofrep1" runat="server" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000">&nbsp;<asp:Label ID="Label4" runat="server" Text="NAME OF THE REPRESENTATIVE 2" Style="font-size: 15px;"></asp:Label>
                </td>
                <td class="text-left" style="border: thin solid #000000">&nbsp;<asp:Label ID="lblnameofrep2" runat="server" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000">&nbsp;<asp:Label ID="Label5" runat="server" Text="SAVINGS A/C NO" Style="font-size: 15px;"></asp:Label>
                </td>
                <td class="text-left" style="border: thin solid #000000">&nbsp;<asp:Label ID="lblsavingsaccnum" runat="server" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000">&nbsp;<asp:Label ID="Label6" runat="server" Text="LOAN A/C NO" Style="font-size: 15px;"></asp:Label>
                </td>
                <td class="text-left" style="border: thin solid #000000">&nbsp;<asp:Label ID="lblloanaccnum" runat="server" Visible="false" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000">&nbsp;<asp:Label ID="Label7" runat="server" Text="LOAN DATE" Style="font-size: 15px;"></asp:Label>
                </td>
                <td class="text-left" style="border: thin solid #000000">&nbsp;<asp:Label ID="lblloandate" runat="server" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000">&nbsp;<asp:Label ID="Label8" runat="server" Text="LOAN AMOUNT" Style="font-size: 15px;"></asp:Label>
                </td>
                <td class="text-left" style="border: thin solid #000000">&nbsp;<asp:Label ID="lblloanamount" runat="server" Text="LOAN AMOUNT" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000">&nbsp;<asp:Label ID="Label9" runat="server" Text="PURPOSE OF LOAN" Style="font-size: 15px;"></asp:Label>
                </td>
                <td class="text-left" style="border: thin solid #000000">&nbsp;<asp:Label ID="lblpurposeofloan" runat="server" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <table style="border: thin solid #000000; width: 100%; height: 300px; font-size: 15px;">
            <tr>
                <td colspan="2" style="text-align: center;">&nbsp;
                <asp:Label ID="Label18" runat="server" Text="SELF HELP GROUP LOAN APPLICATION FORM TO BE SUBMITTED BY SHG TO LINK BRANCH WHILE APPLYING FOR LOAN ASSISTANCE" Font-Underline="true" Style="text-align: center; font-weight: 700; font-size: 19px;"></asp:Label>
                </td>
            </tr>
            <tr style="font-size: 15px;">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<asp:Label ID="Label19" runat="server" Text="Name and Address of the Self Help Group" Style="font-size: 15px;"></asp:Label>
                </td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<asp:Label ID="lblshgname" runat="server" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<asp:Label ID="Label20" runat="server" Text="Formed/Established on" Style="font-size: 15px;"></asp:Label>
                </td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<asp:Label ID="lblshgformdate" runat="server" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<asp:Label ID="Label33" runat="server" Text="Registered: Yes / No" Style="font-size: 15px;"></asp:Label>
                    <br />
                    &nbsp;<asp:Label ID="Label34" runat="server" Text="If registered, give number and date and furnish True Copy of the Certificate Date of Registration" Style="font-size: 15px; text-align: left;"></asp:Label>
                </td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<asp:Label ID="lblregyesorno" runat="server" Text="Registered: Yes / No" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<asp:Label ID="Label22" runat="server" Text="Number of members in the Group" Style="font-size: 15px;"></asp:Label>
                </td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<asp:Label ID="lbltotalmembinshg" runat="server" Text="Number of members in the Group" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<asp:Label ID="Label23" runat="server" Text="SB Account No./Date of opening" Style="font-size: 15px;"></asp:Label>
                </td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<asp:Label ID="lblsbaccnumnopendate" runat="server" Text="SB Account No./Date of opening" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<asp:Label ID="Label24" runat="server" Text="Name of SHPI / NGO /VA assisting the Group, if any" Style="font-size: 15px;"></asp:Label>
                </td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<asp:Label ID="lblnameofshpi" runat="server" Style="font-size: 15px;"></asp:Label>
                </td>
            </tr>
        </table>
        <p><strong>&nbsp;</strong></p>
        <p>--------------------------------------------------------------------------------------------------------</p>
        <p style="font-size: 15px; text-align: justify;">
            <strong><span>To</span>,</strong>
            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Place</span> :<asp:Label ID="lblplacesecond" runat="server"></asp:Label>
        </p>
        <asp:Label ID="lblplace" runat="server" Style="font-size: 15px; text-align: justify;"></asp:Label>
        <p style="font-size: 15px; text-align: justify;"><span class="auto-style12">The Branch/Chief Manager</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="auto-style12">&nbsp; Date :</span><asp:Label ID="lbldatesecond" runat="server"></asp:Label></p>
        <asp:Label ID="lbldate" runat="server" Style="font-size: 15px; text-align: justify;"></asp:Label>
        <p style="font-size: 15px; text-align: justify;">State Bank of India</p>
        <p style="font-size: 15px; text-align: justify;">
            <asp:Label ID="lblbranch" runat="server" Text="Branch" Style="font-size: 15px; text-align: justify;"></asp:Label>
            <span style="font-size: 15px; text-align: justify;">Branch</span>
        </p>
        <p>&nbsp;</p>
        <p style="font-size: 15px; text-align: justify;">Dear Sir,</p>
        <p style="font-size: 15px; text-align: justify;"><strong style="font-size: 15px; text-align: justify;">APPLICATION FOR LOAN:</strong></p>
        <p style="font-size: 15px; text-align: justify;">
            <span style="font-size: 15px; text-align: justify; text-align: justify;">We, the duly authorized representatives of the above SHG hereby apply for loan aggregating Rs.<b>
                <asp:Label ID="lblamountrequired" runat="server" Text="amount" Font-Underline="true" Style="font-size: 15px; text-align: justify;"></asp:Label></b>
                (
      <b>
          <asp:Label ID="lblamountwords" runat="server" Text="Amountin Words" Font-Underline="true" Style="font-size: 15px; text-align: justify;"></asp:Label></b>
                ) for on-lending to our members. The financial particulars of the group as on
            </span>(
            <asp:Label ID="lbldateempty" runat="server" Font-Underline="true" Style="font-size: 15px; text-align: justify;"></asp:Label>)
            <span style="font-size: 15px; text-align: justify;">are given in the enclosed sheet.</span>
        </p>
        <p style="font-size: 15px; text-align: justify;"><strong>REPAYMENT SCHEDULE:</strong></p>
        <ol style="font-size: 15px; text-align: justify;">
            <li>We agree to repay the loan amount as per the repayment schedule which may be fixed by the bank.</li>
            <br />
            <li>A copy of the Inter-se Agreement executed by all the members of the group authorizing us inter alia to borrow on behalf of the SHG is enclosed.</li>
            <br />
            <li>We hereby declare that the particulars given above are true and correct to the best of our knowledge and belief.</li>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <li>We hereby authorize the Bank to disclose all or any particulars or details or information relating to our loan accounts with the Bank to any other financial institution including NABARD, Government or any agency as may be considered necessary or desirable by the bank. It will be in order for the Bank to disqualify the SHG from receiving any credit facilities from the bank and/or recall the entire loan amount or any part thereof granted on this application, if any of the information pertaining to the Group, furnished herewith is found incorrect and/or containing misrepresentation of facts.</li>
        </ol>
        <p>&nbsp;</p>
        <p style="font-size: 15px; text-align: justify;">Yours faithfully</p>
        <p style="font-size: 15px; text-align: justify;">1.</p>
        <p style="font-size: 15px; text-align: justify;">2.</p>
        <p>&nbsp;</p>
        <p style="font-size: 15px; text-align: justify;">(Authorized Representatives)</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <p style="font-size: 15px;">
            NAME OF THE SELF HELP GROUP:
        <b>
            <asp:Label ID="lblshgname2" runat="server" Text="SHG Name" Style="font-size: 15px;"></asp:Label></b>
        </p>
        <p style="font-size: 19px;"><strong>FINANCIAL PARTICULARS AS ON:</strong></p>
        <table style="border: thin solid #000000; width: 100%; height: 403px;">
            <tr style="border: thin solid #000000; font-size: 15px; text-align: justify;">
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;<strong>Slno</strong></td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp; <strong>Particulars</strong></td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp; <strong>Amount (in Rupees)</strong></td>
            </tr>
            <tr style="border: thin solid #000000; font-size: 15px; text-align: justify;">
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp; 1</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;"><span lang="EN-US">&nbsp;Savings from members</span></td>
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp;
                    <asp:Label ID="lblshgtotalsavings" runat="server" Text="amount" Style="font-size: 15px; text-align: justify;"></asp:Label>
                </td>
            </tr>
            <tr style="border: thin solid #000000; font-size: 15px; text-align: justify;">
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp; 2</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;"><span lang="EN-US">&nbsp;Seed money from SHPI/NGO/VA, if any</span></td>
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp;
                    <asp:Label ID="lbltotalgrants" runat="server" Style="font-size: 15px; text-align: justify;"></asp:Label>
                </td>
            </tr>
            <tr style="border: thin solid #000000; font-size: 15px; text-align: justify;">
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp; 3</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;"><span lang="EN-US">&nbsp;Borrowings out standings (please specify source)</span></td>
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp;
                    <asp:Label ID="lbltotalborrowings" runat="server" Text="NO" Style="font-size: 15px; text-align: justify;"></asp:Label>
                </td>
            </tr>
            <tr style="border: thin solid #000000; font-size: 15px; text-align: justify;">
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp; 4</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;"><span lang="EN-US">&nbsp;Loans outstanding against members</span></td>
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp;
                    <asp:Label ID="lbltotintlendbal" runat="server" Text="amount" Style="font-size: 15px; text-align: justify;"></asp:Label>
                </td>
            </tr>
            <tr style="border: thin solid #000000; font-size: 15px; text-align: justify;">
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;"><span lang="EN-US">&nbsp;Amount in default, if any, against members</span></td>
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp;
                    <asp:Label ID="lbltotexpense" runat="server" Text="NO" Style="font-size: 15px; text-align: justify;"></asp:Label>
                </td>
            </tr>
            <tr style="border: thin solid #000000; font-size: 15px; text-align: justify;">
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp; 6</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;"><span lang="EN-US">&nbsp;Recovery percentage</span></td>
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp;
                    <asp:Label ID="lblrecper" runat="server" Text="100%" Style="font-size: 15px; text-align: justify;"></asp:Label>
                </td>
            </tr>
            <tr style="border: thin solid #000000; font-size: 15px; text-align: justify;">
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp; 7</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;"><span lang="EN-US">&nbsp;Cash <span>&nbsp;</span>balance</span></td>
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp;
                    <asp:Label ID="lblcashinhand" runat="server" Text="amount" Style="font-size: 15px; text-align: justify;"></asp:Label>
                </td>
            </tr>
            <tr style="border: thin solid #000000; font-size: 15px; text-align: justify;">
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp; 8</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;"><span lang="EN-US">&nbsp;Bank balance</span></td>
                <td style="border: thin solid #000000; text-align: center; font-size: 15px; text-align: justify;">&nbsp;
                    <asp:Label ID="lblcashinbank" runat="server" Text="amount" Style="font-size: 15px; text-align: justify;"></asp:Label>
                </td>
            </tr>
        </table>
        <p>&nbsp;</p>
        <br />
        <br />
        <p style="font-size: 15px; text-align: justify;">&nbsp;<span>(Signature of Authorized Representatives of SHG)</span></p>
        <p>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
        <p style="font-size: 19px;"><strong><u>Assessment of SHG</u></strong></p>
        <p>&nbsp;</p>
        <ol style="font-size: 15px; text-align: justify;">
            <li>Whether the SHG has completed 6 months from the date of formation / 12 months from the date of last sanction: Yes / No (*Strike out which is not applicable)</li>
            <br />
            <li>Whether the SHG has been Graded /Evaluated: Yes / No (*Strike out which is not applicable)</li>
            <br />
            <li>Whether the SHG has passed the Grading / Evaluation Exercise: Yes / No (*Strike out which is not applicable</li>
            <br />

            <li>% of Marks obtained in the Grading / Evaluation Exercise:&nbsp; ___________&nbsp;
            <asp:Label ID="lblpercent" runat="server" Text="Percentage"></asp:Label></li>
            <br />
            <li>Financial position of the SHG( as on ____________
                <asp:Label ID="lblpercent0" runat="server" Text="Percentage"></asp:Label>):</li>
        </ol>
        <p>&nbsp;</p>
        <br />
        <br />
        <br />
        <p style="font-size: 19px;"><strong>SCHEDULE OF CHARGES</strong></p>
        <ol style="font-size: 15px; text-align: justify;">
            <li><strong><span>Interest Rate: </span></strong><span>Presently rate of interest on SHG loans are  ______________ % above/below to one year MCLR with minimum  ______________% p.a. w.e.f.  ______________to be reset after one year.</li>
            <br />
            <li><strong>Processing Charges: </strong>No processing charges are to be levied as the benefit of financing SHGs goes mainly to weaker sections of the society.</li>
            <br />
            <li><strong>Inspection Charges: </strong>As the advance is being made to weaker section, 50% of the normal rate should be charged.</span></li>
        </ol>
        <p style="font-size: 15px; text-align: justify;">1.</p>
        <p style="font-size: 15px; text-align: justify;">2.</p>
        <p>&nbsp;</p>
        <p style="font-size: 15px; text-align: justify;">&nbsp;<span>(Signature of Authorized Representatives of SHG)</span></p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <p style="font-size: 19px;"><strong>ARTICLES OF LOAN AGREEMENT FOR FINANCING SELF-HELP GROUPS</strong></p>
        <p style="font-size: 15px;">(To be stamped as Agreement per the respective State Stamp act.)</p>
        <p>&nbsp;</p>
        <p style="font-size: 15px; text-align: justify;">
            <span>The Articles of Agreement made on this&nbsp;_________________
            &nbsp;day of      
      _________________
            &nbsp;at _________________________ by and Between M/s 
            <u>
                <b>
                    <asp:Label ID="lblshgname3" runat="server"></asp:Label></b></u>&nbsp;&nbsp;&nbsp; [name of the SHG] an unregistered association of persons/individuals having its office at
            <u>
                <b>
                    <asp:Label ID="lblshgloc" runat="server"></asp:Label></b></u> Represented by its authorized representatives.
        </p>
        <p style="font-size: 15px; text-align: justify;">
            Shri/Smt.&nbsp;<u><b><asp:Label ID="lblrep1" runat="server"></asp:Label></b></u>&nbsp;(Name)&nbsp;<u><b>Representative 1</b></u>&nbsp;(Designation);
        </p>
        <p style="font-size: 15px; text-align: justify;">
            Shri/Smt.&nbsp;<u><b><asp:Label ID="lblrep2" runat="server"></asp:Label></b></u>&nbsp;(Name)&nbsp;<u><b>Representative 2</b></u>&nbsp;(Designation);
        </p>
        <p style="font-size: 15px; text-align: justify;">
            Who are fully authorized by all the members of the SHG, (a copy of such Authorization is annexed hereto and forms part of this agreement), hereinafter referred to as the "Borrower" which expression shall unless repugnant to the subject or context thereof, mean and include members of the unregistered association for the time being, their respective successors, legal heirs, administrators and assigns of the one part and STATE BANK OF INDIA, a body corporate constituted under the State Bank of India Act 1955, having its Central Office at Mumbai and one of the Local Head Offices at&nbsp; Bangalore and the Branches, inter-alia, one at <u>
                <b>
                    <asp:Label ID="lblbankbranch1" runat="server"></asp:Label></b> </u>
            here in after called "the Bank" which expression shall unless repugnant to the subject or context thereof mean and include its successors and assignees of the second part.
        </p>
        <p style="font-size: 15px; text-align: justify;">Whereas the borrower an unregistered association of persons who have inter-se agreed to help each other as self-help group. With a view to developing and ameliorating the socioeconomic conditions of their members. Whereas having formed the association as a Self Help Group, the Borrower as per application</p>
        <p style="font-size: 15px; text-align: justify;">
            Dated&nbsp;&nbsp;<asp:Label ID="lblproposaldatesixthpage" runat="server" Font-Bold="true" Font-Underline="true"></asp:Label>&nbsp;&nbsp;made by the said
        </p>
        <p style="font-size: 15px; text-align: justify;">
            Shri/Smt.&nbsp;<u><b>
                <asp:Label ID="lblrep11" runat="server"></asp:Label></b></u>&nbsp;(Name)&nbsp;<u><b>Representative 1</b></u>&nbsp;(Designation);
        </p>
        <p style="font-size: 15px; text-align: justify;">
            Shri/Smt.&nbsp;<u><b><asp:Label ID="lblrep22" runat="server"></asp:Label></b></u>&nbsp;(Name)&nbsp;<u><b>Representative 2</b></u>&nbsp;(Designation);
        </p>
        <p style="font-size: 15px; text-align: justify;">
            Duly authorized to borrow in terms of its resolution dated&nbsp;&nbsp;<asp:Label ID="Label27" runat="server" Font-Bold="true" Font-Underline="true"></asp:Label>&nbsp;&nbsp;(copy enclosed) requested the Bank to *grant a Term Loan/extend Cash Credit facility* of Rs.&nbsp;<u><b><asp:Label ID="lblshgloanamt" runat="server"></asp:Label></b></u> &nbsp; up to the limit of Rs.<b><asp:Label ID="lblshgloanamount12" runat="server"></asp:Label>(<asp:Label ID="lblrsinwords" runat="server"></asp:Label>)</b>&nbsp; for on-lending to its members. And whereas the Bank has agreed to grant the * Term Loan /extend Cash Credit facility* to the borrower on certain terms and conditions. (* deleted whichever is not applicable) And whereas the Bank and the borrowers are desirous of reducing the agreed terms into writing.
        </span>
        </p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <br />
        <br />
        <br />
        <p style="font-size: 19px;"><strong><u>Now, therefore, this agreement witnesses as follows:</u></strong></p>
        <ol style="font-size: 15px; text-align: justify;">
            <li>The Bank has agreed to grant&nbsp;and&nbsp;the borrower has agreed to borrow by way of term Loan Cash / Credit (clean) up to the limit of Rs. _______________/- (Rupees_______________________________________________________________________________________________________only) and the Bank has opened (SPECIFY THE KIND OF LOAN ACCOUNT) ________________________ A/C No. _________________________of date _______________
                ________- in the name of the Borrower in its book of accounts.</li>
            <br />
            <li>In case the facility availed is Cash Credit the Borrowers will operate the Cash Credit account satisfactorily and within the limit and the Borrower shall repay the outstanding liability in the account inclusive of interest and other charges debited from time to time on demand without demur.</li>
            <br />
            <li>In case loan availed is Demand Loan, without prejudice to the right of the Bank to recall the loan on demand the Borrower undertakes to repay the loan with interest and other charges within the period stipulated in terms of sanction.</li>
            <br />
            <li>In case the credit facility availed of by the borrower is a Term Loan the same shall be repayable in installments in the manner specified herein-below in the repayment schedule. (to be specified) Besides the borrower will pay interest at the rates that may be prescribed for such lending by RBI/NABARD from time to time.</li>
            <br />
            <li>It is clearly understood by and between the parties hereto that in the event of the borrower's failure to utilize the proceeds of the credit facility for the purpose for which the same has been made available by the bank to the borrower, the borrower shall repay immediately on demand without demur together with interest without prejudice to Bank's right to initiate other legal action.</li>
            <br />
            <li>The borrower shall pay interest on the loans to be calculated on the daily balances in the loan account and be debited thereto at quarterly rests or as the Bank may decide.</li>
            <br />
            <li>The borrower should utilize the proceeds of the credit facility for the purpose of lending to its members to improve the socio-economic conditions of their members and their families</li>
            <br />
            <li>The borrower shall repay the credit facility availed of together with interest payable as per the interest rates that may be fixed by RBI/NABARD from time to time for such lending</li>
            <br />
            <li>The borrower shall be liable to repay the facility on demand together with the interest and other charges payable by the borrower to the bank in accordance with the rules of the Bank.</li>
        </ol>
        <p>&nbsp;</p>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <p style="font-size: 19px;"><strong>REPAYMENT SCHEDULE</strong></p>
        <p style="font-size: 15px; text-align: justify;"><strong>(Please Specify)</strong></p>
        <p style="font-size: 15px; text-align: justify;">
            Principal loan is to be repaid in  ______________ equal monthly / quarterly / half-yearly installments. Interest debited to the A/C is to be repaid / deposited promptly.
        </p>
        <p>&nbsp;</p>
        <p style="font-size: 15px; text-align: justify;">
            In witness where of the parties hereto have affixed their signature on the _______date and the ________ month and ____________  year first herein above written.
        </p>
        <p><strong>&nbsp;</strong></p>
        <p style="font-size: 15px; text-align: justify;"><strong>&nbsp;FOR STATE BANK OF INDIA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
        <%-- <br /> <br /><br />
          <p><strong>&nbsp;(Branch Manager) with office seal</strong></p>
         <p style="margin-left:65%;"><strong>&nbsp;(1) Reprsentative 1:</strong></p>
        <p style="margin-left:65%;"><strong>&nbsp;(2) Reprsentative 2:</strong></p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>--%>
        <br />
        <br />
        <table style="width: 100%;">
            <tr>
                <td style="font-size: 15px; text-align: justify;">
                    <b>(Branch Manager) with office seal</b>
                </td>
                <td style="font-size: 15px; text-align: justify;">
                    <b>(1) Representative 1:</b>
                </td>
            </tr>

            <tr style="line-height: 45px;">
                <td></td>
                <td style="font-size: 15px; text-align: justify;">
                    <b>(2) Representative 2:</b> </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <p style="font-size: 15px; text-align: justify;"><strong>(To be stamped as Power of Attorney as per the respective State Stamp act.)</strong></p>
        <p style="font-size: 19px;"><strong><u>INTER-SE AGREEMENT TO BE EXECUTED BY THE MEMBERS OF THE SELF HELP GROUP</u></strong></p>
        <p style="font-size: 15px; text-align: justify;">
            <strong>This agreement made on this ________day of __________20___________between</strong>
        </p>
        <br />
        <br />
        <div>
            <table class="lnt" style="width: 100%;">
                <tr>
                    <td>
                        <ol style="border: 1px solid #000000;">
                            <asp:ListView ID="listcart" runat="server" RepeatColumns="3" ItemPlaceholderID="ItemPlaceholderID1" Style="width: 100%;">
                                <ItemTemplate>
                                    <li style="font-size: 15px; height: 78px; border: 1px solid #000000;">&nbsp;Smt/Shri&nbsp;<u><b><asp:Label ID="lblname" Text='<%#Eval("name") %>' runat="server" Style="font-size: 15px;"></asp:Label></b></u>&nbsp;(&nbsp;<b><asp:Label ID="lblparentname" Text='<%#Eval("fatherorhusband") %>' runat="server" Style="font-size: 15px;"></asp:Label></b>)&nbsp;Aged&nbsp;<b><asp:Label ID="lblaged" Text='<%#Eval("dob") %>' runat="server" Style="font-size: 15px;"></asp:Label></b>&nbsp;Residing At&nbsp;<b><asp:Label ID="lbladdr" Text='<%#Eval("address") %>' runat="server" Style="font-size: 15px;"></asp:Label></b>
                                        <asp:PlaceHolder ID="ItemPlaceholderID1" runat="server"></asp:PlaceHolder>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                        </ol>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <br />
        <p style="font-size: 15px; text-align: justify;">
            Who are members of the&nbsp;<u><b><asp:Label ID="lblshgname4" runat="server"></asp:Label></b></u>&nbsp;group, hereinafter referred to collectively as "The Self Help Group (SHG) Members" which expression shall, unless repugnant to the context or meaning, include every members of the said SHG and their respective legal heirs, executors and administrators.
        </p>
        <p style="font-size: 15px; text-align: justify;">WHEREAS the SHG members above named have joined voluntarily together and formed the SHG with intent to carry on savings and credit and other economic activities for mutual benefit subject to the terms and conditions hereinafter appearing:</p>
        <p>&nbsp;</p>
        <br />
        <br />
        <br />


        <br />
        <br />
        <p style="font-size: 19px;"><strong><u>NOW THEREFORE THIS AGREEMENT WITNESSETH THAT:-</u></strong></p>
        <ol style="font-size: 15px; text-align: justify;">
            <li>Each member of the SHG shall save a sum of Rs.<u><b><asp:Label ID="lblshgsplitamount" runat="server" Text="________________"></asp:Label></b></u>&nbsp;<u><b>(<asp:Label ID="lblamtinrs" runat="server" Text="_______________"></asp:Label></b></u>)&nbsp; or such sum as may be decided by the Group, on (weekly fortnightly/monthly) basis which shall be deposited with the authorized member of the group.</li>
            <br />
            <li>Each member shall strive for the success of the SHG and shall not act in any manner detrimental to the business interests of the SHG.</li>
            <br />
            <li>The SHG members shall be jointly and severally liable for all the debts contracted by the SHG.</li>
            <br />
            <li>All assets and goods acquired by the SHG shall be in the joint ownership of all the members of the SHG and shall ordinarily be in the constructive custody of such member as may be authorized by the Group and shall be kept at the place of business at&nbsp;<strong><asp:Label ID="lblbankbranch12" runat="server"></asp:Label></strong>&nbsp; which shall not be changed without consent of the SHG members.</li>
            <br />
            <li>The SHG members hereby duly elect and appoint Shri/Smt/Kum.<u><b><asp:Label ID="lblrep111" runat="server"></asp:Label></b></u>&nbsp;  as Representative 1, Shri/Smt/Kum&nbsp;<u><b><asp:Label ID="lblrep222" runat="server"></asp:Label></b></u>&nbsp;as Representative 2 to look after and manage the day to day affairs of the SHG's activities and also act in their name and on their behalf in all matters relating thereto. The authorized representatives, may however, be removed at any time by majority vote of the members and new representatives elected.</li>
            <br />
            <li>Each of the SHG members hereby agrees to abide by and ratify all such act, deeds and things as the authorized representatives may do in the interest of the said activities.</li>
            <br />
            <li>The authorized representatives shall take decisions in the day to day working of the SHG and each representative shall actively involve herself and co-operate in looking after the day-to-day affairs of the SHG activities in particular to attend to the following activities. Every member of the SHG hereby authorizes the representatives to apply for the loan on our behalf in the name of SHG and execute necessary agreements/documents on behalf of the SHG for the purpose. The authorized representative may collect loan amounts from the bank on behalf of SHG, deposit the same in the savings account of the SHG for on-lending to members in accordance with the decision of the SHG and also deposit recovery of loan installment from members in the loan account/s of SHG with the bank.</li>
            <br />
            <li>The SHG members hereby specifically authorize the representatives:-</li>
        </ol>
        <ul style="font-size: 15px; text-align: justify;">
            <li>To open Savings, Fixed Deposits and other accounts in <u>
                <asp:Label ID="lblbankbranch11" runat="server"></asp:Label></u>&nbsp; Branch of State Bank of India approved by the SHG and operate the same under the joint signature of any two of the following authorized representatives:</li>
        </ul>
        <p style="font-size: 15px; text-align: justify;">
            Shri/Smt/Kum. <b><u>
                <b>
                    <asp:Label ID="lblrep33" runat="server"></asp:Label></b></u></b>&nbsp;as Representative 1,
            <br />
            Shri/Smt/Kum<u>
                <b>
                    <asp:Label ID="lblrep34" runat="server"></asp:Label></b> </u>&nbsp;as Representative 2
        </p>
        <br />
        <br />
        <br />
        <br />
        <br />
        <ul style="font-size: 15px; text-align: justify;">
            <li>To keep or cause to be kept proper books of accounts of the savings, made by the SHG Members, loans granted to them and the recoveries made from them and render every year the full accounts to the SHG members for their approval and adoption;</li>
            <br />
            <li>To receive all payments due to the SHG and issue requisite receipts or acknowledgments for and on behalf of the SHG;</li>
            <br />
            <li>To institute and defend on behalf of the SHG members any legal proceedings and safeguard the interests of each member of the said SHG and for this purpose engage or disengage any lawyer or advocate or agent and incur the necessary legal expenses in connection therewith.</li>
        </ul>
        <ol start="9" style="font-size: 15px; text-align: justify;">
            <li>In the event of death of any of the members of the SHG, his/her legal heirs shall be entitled to the benefits and be liable for the obligations of the deceased member under this agreement.</li>
            <br />
            <li>It is agreed that no new person shall be inducted as a member of the SHG without consent of all the existing members. If a member intends to leave the group, he or she is liable to repay the amount received from SHG with interest and other charges if any.</li>
        </ol>
        <p style="font-size: 15px; text-align: justify;">
            IN WITNESS WHEREOF the aforesaid members of the SHG have set their respective hands here unto at the place and on _______ day of _______ month of ______________year first herein appearing. 
        </p>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <table style="width: 100%; height: auto;">
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;"><strong>&nbsp;Name of The Members</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;"><strong>&nbsp;Signature/ Thumb Impression</strong></td>
            </tr>
            <asp:ListView ID="ListView1" runat="server" RepeatColumns="3" ItemPlaceholderID="ItemPlaceholderID1">
                <ItemTemplate>
                    <tr style="font-size: 15px;">
                        <td rowspan="2" style="border: thin solid #000000; font-size: 15px;">&nbsp;
                            <asp:Label ID="lblname" Text='<%#Eval("name") %>' runat="server"></asp:Label></td>
                        <td rowspan="1" style="border: thin solid #000000; line-height: 30px; font-size: 15px;">&nbsp;Signature:</td>
                    </tr>
                    <tr style="font-size: 15px;">
                        <td rowspan="1" style="border: thin solid #000000; line-height: 55px; font-size: 15px;">&nbsp;Thumb:</td>
                    </tr>
                    <asp:PlaceHolder ID="ItemPlaceholderID1" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:ListView>
        </table>
        <br />
        <br />

        <p style="font-size: 14px;">WITNESSES:-</p>
        <p style="font-size: 13px;">1.</p>
        <p style="font-size: 13px;">2.</p>
        <p style="font-size: 12px;"><strong>(Note: The SHG shall not consist more than 20 persons).</strong></p>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <p style="font-size: 19px;"><strong>RESOLUTION FOR AVAILING LOAN</strong></p>
        <p style="font-size: 15px; text-align: justify;">
            We the below mentioned are the members of <b>
                <asp:Label ID="lblshgname5" runat="server" /></b>&nbsp;(name of the SHG) conducted a meeting on&nbsp;&nbsp;<asp:Label ID="lblprposaldate99" runat="server" Font-Bold="true" Font-Underline="true"></asp:Label>
            &nbsp;&nbsp; ( date ) at&nbsp;&nbsp;<asp:Label ID="Label29" runat="server" Font-Bold="true" Font-Underline="true"></asp:Label>
            &nbsp;&nbsp; ( Place of meeting ) and unanimously resolved to authorize the following 2 ( two ) members of our SHG.
        </p>
        <p style="font-size: 15px; text-align: justify;">
            1.  Shri/Smt.<b><asp:Label runat="server" ID="lblrep55"></asp:Label></b>
        </p>
        <p style="font-size: 15px; text-align: justify;">
            2.  Shri/Smt.<b><asp:Label runat="server" ID="lblrep56"></asp:Label></b>&nbsp;<asp:Label ID="lbldum" runat="server" Visible="false"></asp:Label>
        </p>
        <p style="font-size: 15px; text-align: justify;">
            As Authorized members to avail loan (cash credit /Term loan) of Rs. <strong>
                <asp:Label ID="lblshgloanreqamt" runat="server"></asp:Label></strong>&nbsp;from of State bank of India and also to execute the loan documents on behalf our <strong>
                    <asp:Label ID="lblshgname43" runat="server"></asp:Label></strong>&nbsp;SHG. All the members are aware that all the members of our SHG are jointly and severally liable to repay the loan availed by State Bank of India. All the members are also aware that the above bank is at liberty and it is at its discretion to initiate action against one or some or all members of the our SHG and can also proceed with sale of any assets /properties acquired out of bank finance or out of own funds of the members. The bank can also call back the advance before the due date of loan, in case of any default in repayment of loan, as per the terms of sanction prescribed by the above Bank. All the members of our SHG have consented to the resolution passed as above and also agreed to proceedings mentioned above, by signing the Resolution as below:
        </p>
        <table style="border: thin solid #000000; width: 100%;">
            <tr style="font-size: 15px; text-align: justify;">
                <%-- <td style="border: thin solid #000000;font-size:15px;">Sl No</td>--%>
                <td style="border: thin solid #000000; font-size: 15px;"><strong>&nbsp;Name of the Member of the above Group</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;"><strong>&nbsp;Signature of the Member</strong></td>
            </tr>
            <asp:ListView ID="ListView2" runat="server" RepeatColumns="3" ItemPlaceholderID="ItemPlaceholderID1">
                <ItemTemplate>

                    <tr style="font-size: 15px; text-align: justify;">

                        <td style="border: thin solid #000000; font-size: 15px;">&nbsp;
                    <asp:Label ID="lblname" Text='<%#Eval("name") %>' runat="server"></asp:Label>
                        </td>
                        <td style="border: thin solid #000000; font-size: 15px; height: 70px;">&nbsp;
                        </td>
                    </tr>

                </ItemTemplate>
            </asp:ListView>
        </table>
        <p><strong>&nbsp;</strong></p>
        <p><strong>&nbsp;</strong></p>
        <br />
        <br />

        <br />
        <br />

        <p style="font-size: 15px; text-align: justify;">
            <strong>Name &amp; Address of SHG :&nbsp;&nbsp;<asp:Label ID="lblshgname90" runat="server"></asp:Label>
            </strong>
        </p>
        <p style="font-size: 15px; text-align: justify;">
            <strong>Activity: 
            </strong>
        </p>
        <p style="font-size: 15px; text-align: justify;">
            <strong>S B A/C No:&nbsp;
                <asp:Label ID="lblsbaccno" runat="server"></asp:Label>
            </strong>
        </p>

        <p style="font-size: 15px; text-align: justify;"><strong>MEMBERS LIST</strong></p>
        <br />
        <br />
        <br />
        <br />
        <table style="border: thin solid #000000; width: 100%;" border="1">
            <tr style="font-size: 12px;">
                <td style="font-size: 12px;"><strong>&nbsp;Sno</strong></td>
                <td style="font-size: 12px;"><strong>&nbsp;Members</strong><strong>&nbsp;Info</strong></td>
                <td></td>
                <td style="font-size: 12px;"><strong>&nbsp;Age</strong></td>
                <td style="font-size: 12px;"><strong>&nbsp;Photo</strong><br />
                    &nbsp;(<strong>Passport</strong> <strong>&nbsp;size</strong>)</td>
                <td style="font-size: 12px;"><strong>&nbsp;Signature/ Thumb</strong></td>
            </tr>
            <asp:ListView ID="ListView3" runat="server" RepeatColumns="3" ItemPlaceholderID="ItemPlaceholderID1">
                <ItemTemplate>
                    <tr style="font-size: 12px;">
                        <td style="border: thin solid #000000; font-size: 15px;">&nbsp;
                           <%#Container.DataItemIndex+1 %></td>
                        <td style="font-size: 12px;">&nbsp;Name :</td>
                        <td style="font-size: 12px;">&nbsp;
                            <asp:Label Text='<%#Eval("name") %>' runat="server" ID="lblname"></asp:Label>
                        </td>
                        <td></td>
                        <td rowspan="9"></td>
                        <td rowspan="9"></td>
                    </tr>

                    <tr style="font-size: 12px;">
                        <td></td>
                        <td style="font-size: 12px;">&nbsp;Father Name :</td>
                        <td style="font-size: 12px;">&nbsp;
                            <asp:Label Text='<%#Eval("fatherORhusband") %>' runat="server" ID="lblfname"></asp:Label></td>
                        <td>&nbsp;</td>

                    </tr>
                    <tr>
                        <td></td>
                        <td style="font-size: 12px;">&nbsp;Aadhar No :</td>
                        <td style="font-size: 12px;">&nbsp;
                            <asp:Label Text='<%#Eval("adhaarNum") %>' runat="server" ID="lblaadharnum"></asp:Label></td>
                        <td>&nbsp;</td>

                    </tr>
                    <tr>
                        <td></td>
                        <td style="font-size: 12px;">&nbsp;SB A/c :</td>
                        <td style="font-size: 12px;">&nbsp;
                            <asp:Label Text='<%#Eval("savingAccNum") %>' runat="server" ID="lblsbaccnum"></asp:Label></td>
                        <td style="font-size: 12px;">
                            <strong>&nbsp;
                                <asp:Label ID="lblday187" runat="server" Text='<%#Eval("dob") %>'></asp:Label>
                            </strong>
                        </td>

                    </tr>
                    <tr>
                        <td></td>
                        <td style="font-size: 12px;">&nbsp;PAN No :</td>
                        <td style="font-size: 12px;">&nbsp;
                            <asp:Label Text='<%#Eval("pan") %>' runat="server" ID="lblpannum"></asp:Label></td>
                        <td style="font-size: 12px;">
                            <strong>&nbsp;
                                <asp:Label ID="Label30" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="font-size: 12px;">&nbsp;Contact :</td>
                        <td style="font-size: 12px;">&nbsp;
                            <asp:Label Text='<%#Eval("mobile") %>' runat="server" ID="lblcontnum"></asp:Label></td>
                        <td>&nbsp;</td>

                    </tr>
                    <tr style="border-bottom: 1px solid #000000;">
                        <td></td>
                        <td style="font-size: 12px;">&nbsp;Address :</td>
                        <td rowspan="2" style="font-size: 12px;">&nbsp;
                            <asp:Label Text='<%#Eval("address") %>' runat="server" ID="Label31"></asp:Label></td>
                        <td>&nbsp</td>

                    </tr>
                    <tr style="border-bottom: 1px solid #000000;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>&nbsp;</td>

                    </tr>
                    <tr style="border-bottom: 1px solid #000000;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>&nbsp;</td>

                    </tr>
                    <tr style="border-bottom: 1px solid #000000;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>&nbsp;</td>

                    </tr>
                    </tbody>
                </ItemTemplate>
            </asp:ListView>
        </table>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <table style="border: thin solid #000000; width: 100%;">
            <tr>
                <td style="font-size: 15px; text-align: justify;"><strong>(For Office Use)</strong></td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;"><b><span lang="EN-US">Slno Allotted SHG:</span></b></td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;"><b><span lang="EN-US">Particulars of Credit Facility Availed:</span></b></td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;"><b><span lang="EN-US">Name &amp; Designation of Members Operating Account:</span></b></td>
            </tr>
        </table>
        <br />
        <br />
        <p style="font-size: 19px;"><strong><u>SIGNATURE OF VIKASA RURAL DEVELOPMENT ORGANISATION STAFFS</u></strong></p>
        <br />
        <br />
        <br />
        <br />
        <table style="border: thin solid #000000; width: 100%;">
            <tr style="text-align: center">
                <td style="border: thin solid #000000; font-size: 15px;"><b>
                    <asp:Label ID="lblcdm" runat="server"></asp:Label>
                    <br />
                </b>&nbsp;Community Development Manager<br />
                    &nbsp;VIKASA<br />
                    &nbsp;Region:<b><asp:Label ID="lblreg" runat="server"></asp:Label></b> </td>
                <td style="border: thin solid #000000; font-size: 15px;"><b>&nbsp;<asp:Label ID="lblcdo" runat="server"></asp:Label>
                    <br />
                </b>&nbsp;Community Development Officer<br />
                    &nbsp;VIKASA<br />
                    &nbsp;Taluk:<b><asp:Label ID="lbltq" runat="server"></asp:Label></b></td>
                <td style="font-size: 15px;"><b>&nbsp;<asp:Label ID="lblcde" runat="server"></asp:Label>
                    <br />
                </b>&nbsp;Community Development Executive<br />
                    &nbsp;VIKASA<br />
                    &nbsp;Branch:<b><asp:Label ID="lblbranch90" runat="server"></asp:Label></b></td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <p style="font-size: 19px;"><strong><u>Format of Annexure to Loan Application form</u></strong></p>
        <br />
        <p style="font-size: 15px; text-align: justify;">The Branch Manager,</p>
        <p style="font-size: 15px; text-align: justify;">State Bank of India,</p>
        <p style="font-size: 15px; text-align: justify;">
            <asp:Label ID="lblbankbranch15" runat="server"></asp:Label>
            Branch
        </p>
        <p style="font-size: 15px; text-align: justify;">Dear Sir,</p>
        <p style="font-size: 15px; text-align: justify;"><strong><u>SANCTION OF LOAN</u></strong></p>
        <p style="font-size: 15px; text-align: justify;">We enclose application form for sanction of loan in the name of our SHG.</p>
        <ol style="font-size: 15px; text-align: justify;">
            <li>We confirm that from our family, only one member is associated with this SHG and no other member is part of any other SHG.</li>
        </ol>
        <ol start="2">
            <li style="font-size: 15px; text-align: justify;">We have submitted the particulars of individual members as under and their KYC compliance at the time of opening SB account in the name of our group vide Account No: <strong>
                <asp:Label ID="lblsavaccnum23" runat="server"></asp:Label></strong>&nbsp;&nbsp;,Dated
                <asp:Label ID="lbccodate" runat="server"></asp:Label>
            </li>
        </ol>
        <ol start="3">
            <li style="font-size: 15px; text-align: justify;">We confirm that we are not members of any other SHG and we have not taken any loan earlier from any other bank under:</li>
        </ol>
        <table style="border: thin solid #000000; width: 100%; height: 110%;" border="1">
            <tr>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>Name of Group Member</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>Address</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>Bank Name</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>SB A/c No</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>Amount</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>Reason for Loan</strong></td>
            </tr>
            <asp:ListView ID="ListView4" runat="server" RepeatColumns="3" ItemPlaceholderID="ItemPlaceholderID1">
                <ItemTemplate>
                    <tr>
                        <td style="border: thin solid #000000; font-size: 15px;">&nbsp;
                            <asp:Label ID="lblname" runat="server" Text='<%#Eval("name") %>'></asp:Label>
                            &nbsp;</td>
                        <td style="border: thin solid #000000; font-size: 15px; height: 70px;">&nbsp;
                            <asp:Label ID="lbladdress" runat="server" Text='<%#Eval("village") %>'></asp:Label>
                            &nbsp;</td>
                        <td>&nbsp;</td>
                        <td style="border: thin solid #000000; font-size: 15px;">&nbsp;
                            <asp:Label ID="lblsbaccnum" runat="server" Text='<%#Eval("savingAccNum") %>'></asp:Label>
                            &nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </table>
        <br />
        <p style="font-size: 15px; text-align: justify;">
            We have explained our credit needs to your branch field staff during the pre sanction survey conducted on the date&nbsp;&nbsp;<asp:Label ID="lblproposaldate999" runat="server" Font-Bold="true" Font-Underline="true"></asp:Label>
        </p>
        <br />
        <ol start="5" style="font-size: 15px; text-align: justify;">
            <li>We confirm that our group savings are deposited and lent among our members through the group SB Account no: <strong>
                <asp:Label ID="lblshgsbaccnum23" runat="server"></asp:Label></strong>  opened on:
                <asp:Label ID="lblsbacccopendate" runat="server"></asp:Label></li>
            <li>Particulars of earlier loans taken / repaid by our group are as under:</li>
        </ol>
        <table style="width: 100%;">
            <tr>
                <td class="auto-style9" style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;<strong>Date of Saction</strong></td>
                <td class="auto-style9" style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;<strong>Amount Sactioned</strong></td>
                <td class="auto-style9" style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;<strong>Date of Closure</strong></td>
                <td class="auto-style9" style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;<strong>Remarks</strong></td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>

            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>

            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>

            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>

            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>

            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;</td>

            </tr>
        </table>
        <p>&nbsp;</p>
        <ol start="7" style="font-size: 15px; text-align: justify;">
            <li>We confirm that all members of this SHG are from homogeneous group.</li>
        </ol>
        <p style="font-size: 15px; text-align: justify;"><strong>Signatures of Group Members:</strong></p>
        <table style="border: thin solid #000000; font-size: 15px; text-align: justify; width: 100%; height: 375px;">
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;1</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;11</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;2</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;12</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;3</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;13</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;4</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;14</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;5</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;15</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;6</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;16</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;7</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;17</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;8</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;18</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;9</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;19</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;10</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;20</td>
            </tr>
        </table>
        <p><strong>&nbsp;</strong></p>
        <p style="font-size: 15px; text-align: justify;">Certified that the above mentioned particulars are correct as per Bank records and out interaction during pre sanction conducted on ________________________</p>
        <p>&nbsp;</p>
        <p style="margin-left: 10%; font-size: 15px;">RMRO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Field Officer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Branch Manager</p>
        <p>&nbsp;</p>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />

        <p style="font-size: 19px;">Annexure 'c'</p>
        <p style="font-size: 15px; text-align: justify;">UNDERTAKING TO PAY SERVICE CHARGES</p>
        <p style="font-size: 15px; text-align: justify;">
            Sri/Smt <strong>
                <asp:Label ID="lblrep88" runat="server"></asp:Label></strong>&nbsp; and Sri/Smt <strong>
                    <asp:Label ID="lblrep99" runat="server"></asp:Label>
                </strong>&nbsp;Representing the SHG on behalf of the SHG undertake as follows:
        </p>
        <p style="font-size: 15px; text-align: justify;">1.	That our SHG has been sponsored by NGO M/s VIKASA Rural Development Organisation, a society registered under the provisions of Karnataka Societies Act 1960 and having its registered office at #261, ‘F’, Balaji Co Op Society Layout, Vajarahalli, Kanakapura Main Road, Bangalore -62, Karnataka State. (here in after referred to as the “Service Provider” in abbreviation as “SP”)</p>
        <p style="font-size: 15px; text-align: justify;">
            2. That in consideration of the Bank granting the SHG a loan / credit facility as per the terms and conditions letter No&nbsp;&nbsp;<asp:Label ID="lblproposedate999" runat="server" Font-Bold="true" Font-Underline="true">&nbsp;&nbsp;</asp:Label>
            dated&nbsp;&nbsp;<asp:Label ID="Label32" runat="server" Font-Bold="true" Font-Underline="true"></asp:Label>&nbsp;&nbsp;<strong></strong>we also agreeable to pay service charges at the rate of 3% <strong></strong>of the amount of loan and interest to the SP.
        </p>
        <p style="font-size: 15px; text-align: justify;">
            3.	We authorise you to deduct the service charges payable to SP from out of the proceeds available in our account no <strong>
                <asp:Label ID="lblshgsbaccnum89" runat="server" Text="_____________________"></asp:Label>
            </strong>and credit the amount to SB No 54007222040 of VIKASA RURAL DEVELOPMENT ORGANISATION with your J P Nagar Branch and may be paid to the SP.
        </p>
        <p style="font-size: 15px; text-align: justify;">4.	The said service charges are in addition to the liability of the SHG as per document by the SHG in favour of the bank.</p>
        <br />
        <p style="font-size: 15px; text-align: justify;"><strong>Signatures of Group Members:</strong></p>
        <table style="border: thin solid #000000; font-size: 15px; text-align: justify; width: 100%; height: 375px;">
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;1</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;11</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;2</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;12</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;3</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;13</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;4</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;14</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;5</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;15</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;6</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;16</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;7</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;17</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;8</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;18</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;9</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;19</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;10</td>
                <td style="border: thin solid #000000; font-size: 15px; text-align: justify;">&nbsp;20</td>
            </tr>
        </table>
        <br />
        <table style="width: 100%;">
            <tr>
                <td style="font-size: 15px; text-align: justify;">Date :&nbsp;
                    <asp:Label ID="lblproposal9999" runat="server" Font-Bold="true" Font-Underline="true"></asp:Label>
                </td>
                <td class="auto-style2">&nbsp;</td>
                <td style="font-size: 15px; text-align: justify;">1.</td>
            </tr>
            <tr>
                <td style="font-size: 15px; text-align: justify;">Place :&nbsp;
                    <asp:Label ID="Label56" runat="server" Font-Bold="true" Font-Underline="true"></asp:Label></td>
                <td class="auto-style2">&nbsp;</td>
                <td style="font-size: 15px; text-align: justify;">2.</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="auto-style2">&nbsp;</td>
                <td style="font-size: 15px; text-align: justify;">Signature</td>
            </tr>
        </table>
        <br />

        <br />
        <br />
        <p style="font-size: 13px;">
            <b style="font-size: 13px; text-align: justify;">To,</b>
        </p>
        <p style="font-size: 13px;">
            <b style="font-size: 13px; text-align: justify;">The Branch Manager</b>
        </p>
        <p style="font-size: 13px;">
            <b style="font-size: 13px; text-align: justify;">
                <asp:Label ID="lblbankbranch67" runat="server"></asp:Label>&nbsp;&nbsp;Branch</b>
        </p>
        <p style="font-size: 13px;">
            <b style="font-size: 13px; text-align: justify;">Dear Sir,</b>
        </p>
        <p style="font-size: 13px;">
            <font size="3"><B>Sub: Regarding Sanction of loan to 
</B></font><font size="3"><B><asp:Label ID="lblshgname78" runat="server"></asp:Label></B></font><font size="3"><B>&nbsp;&nbsp;SHG.</B></font>
        </p>
        <div style="border: solid 1px #333; width: 100%;">
            <p lang="en-US" class="western" style="width: 70%; margin-bottom: 0.14in; font-size: 13px;">
                <img src="http://arerainfotech.com/vikasa.png" name="Picture 0" align="LEFT" hspace="12" width="80" height="73" border="0">
                <p lang="en-US" align="CENTER" style="margin-bottom: 0in; font-size: 19px;">
                    <span class="auto-style2">
                        <font face="Trebuchet MS, serif"><FONT SIZE=3 style="font-size: 16pt"><b style="font-size: 15px; text-align:center;">VIKASA
	RURAL DEVELOPMENT ORGANISATION</b></FONT></font></span><b></b>
                </p>
                <p lang="en-US" align="CENTER" style="margin-bottom: 0.14in; font-size: 13px; text-align: center;">
                    <font color="#000000">
	                <font SIZE=3 style="font-size: 16px"><b style="font-size:13px;">HO: #261/’’F’, Balaji Co-op Society Layout, Vajarahalli, Kanakapura Main Road, Bangalore<BR>                                                       
	                                                          Email:
	vrdovikasa@gmail.com</b></font></font>
                </p>
                <p lang="en-US" class="western" style="border-bottom: 1px solid #333; margin-bottom: 0.14in; font-size: 13px; text-align: justify;">
                </p>
                <span dir="LTR" style="float: left; font-size: 13px; text-align: justify; width: 443px;">
                    <p lang="en-US" style="width: 438px; font-size: 13px; text-align: justify;">
                        <font style="font-size: 19px;"><font SIZE=3 style="font-size:15px;"><b style="font-size:15px;">Ref No:<asp:Label ID="lblnum" runat="server"></asp:Label>
	</b></font></font>
                    </p>
                </span>
                <span dir="LTR" style="float: right; font-size: 13px; text-align: justify; width: 184px;">
                    <font size="3" style="font-size: 13px;"><b style="font-size:15px;">Date:
	<asp:Label ID="lbldate23" runat="server"></asp:Label></b></font>
                </span>
                <br>
                <br>
            </p>

            <p lang="en-US" align="JUSTIFY" style="padding: 1em; margin-bottom: 0.14in; line-height: 150%; font-size: 13px; text-align: justify;">
                <font size="3"><B><asp:Label ID="lblshgname1" runat="server"></asp:Label>&nbsp;&nbsp;SHG loan
	application of Rs <asp:Label ID="lblshgloanamt90" runat="server"></asp:Label> (
	<asp:Label ID="lblrsinwords12" runat="server"></asp:Label> only) has cross verified and recommended by VIKASA for submit the proposal to the State
	Bank of India <asp:Label ID="lblbankbranch78" runat="server"></asp:Label> &nbsp;&nbsp;Branch.</B></font>
            </p>

        </div>
        <br />
        <p style="line-height: 20px; font-size: 13px; text-align: justify;">
            <font size="3">With regard to above mentioned subject, here we would
like to inform you that the </font><font size="3"><asp:Label ID="lblshgname87" runat="server"></asp:Label></font>&nbsp;&nbsp;<font size="3">SHG
of </font><font size="3">
</font><font size="3">village of </font><font size="3"><asp:Label ID="lbltaluk" runat="server"></asp:Label>
</font><font size="3">taluk, has been formed and mentioned since
</font><font size="3"><asp:Label ID="lblsince" runat="server"></asp:Label></font><font size="3">years,
under the guidance of our </font><font size="3"><B>VIKASA Rural
Development Organisation</B></font><font size="3">, As you are aware we
have signed Memorandum of Understanding with your head office for
recovery of loan sanctioned to the SHG’s sponsored by our
organisation. Now we are requesting </font><font size="3"><asp:Label ID="lblshgname45" runat="server"></asp:Label></font><font size="3">SHGs
for sanction of loan amount of Rs</font><font size="3"><asp:Label ID="lblshgamount6" runat="server"></asp:Label>
</font><font size="3">(Rupees </font><font size="3"><asp:Label ID="lblshgamtinrs" runat="server"></asp:Label></font><font size="3">only)
to enable the group members to undertake various economic activities.</font>
        </p>
        <p style="line-height: 20px; font-size: 13px; text-align: justify;">
            <font size="3">	Please sanction loan limit of Rs </font><font size="3"><asp:Label ID="lblshgloanamt56" runat="server"></asp:Label></font><font size="3">
(Rupees </font><font size="3"><asp:Label ID="lblamtinrupis" runat="server"/></font><font size="3">
only) as per the agreement reached with your head office. We
undertake recovery of the sanctioned loan amount within the
stipulated period.</font>
        </p>
        <p style="font-size: 13px; text-align: justify;">
            <font face="Arial Unicode MS, serif"><FONT SIZE=3><B>Thanking You</B></FONT></font>
        </p>
        <p style="font-size: 13px; text-align: justify;">
            <font face="Arial Unicode MS, serif"><FONT SIZE=3><B>Yours Truly,</B></FONT></font>
        </p>
        <br />
        <br />
        <br />

        <table style="border: thin solid #000000; width: 100%;" align="center">
            <tr style="text-align: center;">
                <td style="border: thin solid #000000; font-size: 13px;"><b>
                    <asp:Label ID="Label10" runat="server"></asp:Label>
                    <br />
                </b>&nbsp;Community Development Manager<br />
                    &nbsp;VIKASA<br />
                    &nbsp;Region:<b><asp:Label ID="lblreg56" runat="server"></asp:Label></b></td>
                <td style="border: thin solid #000000; font-size: 13px;"><b>
                    <asp:Label ID="Label11" runat="server"></asp:Label>
                    <br />
                </b>&nbsp;Community Development Officer<br />
                    &nbsp;VIKASA<br />
                    &nbsp;Taluk:<b><asp:Label ID="lbltaluk12" runat="server"></asp:Label></b></td>
                <td style="border: thin solid #000000; font-size: 13px;"><b>
                    <asp:Label ID="Label12" runat="server"></asp:Label>
                    <br />
                </b>&nbsp;Community Development Executive<br />
                    &nbsp;VIKASA<br />
                    &nbsp;Branch:<b><asp:Label ID="lblbranch67" runat="server"></asp:Label></b></td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:Label ID="Label15" runat="server" Style="font-weight: 700; font-size: 19PX;"></asp:Label>&nbsp;<strong><span class="auto-style5" style="font-size: 19PX;">&nbsp;SHG MEMBER WISE TOTAL FINANCIAL REPORT</span></strong>
        <br />
        <br />
        <table style="border: thin solid #000000; width: 100%;">
            <tr style="font-size: 15px;">
                <%-- <td style="border: thin solid #000000; font-size: 15px;">Sl No</td>--%>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>Sno</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style23">&nbsp;<strong>Members</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style26">&nbsp;<strong>Savings</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>Interest</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style22">&nbsp;<strong>Penalty</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style25">&nbsp;<strong>Internal Lending</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>Repayment</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style24">&nbsp;<strong>Loan Balance</strong></td>
            </tr>
            <asp:ListView ID="ListView5" runat="server" RepeatColumns="11" ItemPlaceholderID="ItemPlaceholderID1">
                <ItemTemplate>
                    <tr style="font-size: 15px;">
                        <td style="border: thin solid #000000; font-size: 15px;">&nbsp;
                           <%#Container.DataItemIndex+1 %></td>
                        <td style="border: thin solid #000000; font-size: 15px;">&nbsp;
                            <asp:Label ID="Label13" Text='<%#Eval("name") %>' runat="server"></asp:Label>
                        </td>
                        <td style="border: thin solid #000000; font-size: 15px; text-align: right">&nbsp;
                            <asp:Label ID="Label14" Text='<%#Eval("totalsavings") %>' runat="server"></asp:Label>
                        </td>
                        <td style="border: thin solid #000000; font-size: 15px; text-align: right">&nbsp;
                            <asp:Label ID="Label16" Text='<%#Eval("interest") %>' runat="server"></asp:Label>
                        </td>
                        <td style="border: thin solid #000000; font-size: 15px; text-align: right">&nbsp;
                            <asp:Label ID="Label17" Text='<%#Eval("penalty") %>' runat="server"></asp:Label>
                        </td>
                        <td style="border: thin solid #000000; font-size: 15px; text-align: right">&nbsp;
                            <asp:Label ID="Label21" Text='<%#Eval("lending") %>' runat="server"></asp:Label>
                        </td>
                        <td style="border: thin solid #000000; font-size: 15px; text-align: right">&nbsp;
                            <asp:Label ID="Label25" Text='<%#Eval("repay") %>' runat="server"></asp:Label>
                        </td>
                        <td style="border: thin solid #000000; font-size: 15px; text-align: right">&nbsp;
                            <asp:Label ID="Label26" Text='<%#Eval("lendbal") %>' runat="server"></asp:Label>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </table>
        &nbsp;<br />
        <table style="width: 100%;">
            <tr>
                <td><strong style="font-size: 15px;">
                    <asp:Label ID="lblrp1" runat="server"></asp:Label>&nbsp;&nbsp;Representative 1</strong></td>
                <td>&nbsp;</td>
                <td><strong style="font-size: 15px;">
                    <asp:Label ID="lblrp2" runat="server"></asp:Label>&nbsp;&nbsp;Representative 2</strong></td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:Label ID="lblshgname99" runat="server" Style="font-size: 19px; font-weight: 700;"></asp:Label>&nbsp;<strong style="font-size: 19px;">SHG FINANCIAL REPORT FROM</strong>&nbsp;<asp:Label ID="lblfinancialfrom" runat="server" Style="font-size: 19px; font-weight: 700;"></asp:Label>&nbsp;<strong style="font-size: 19px;">TO</strong>&nbsp;<asp:Label ID="lblfinancialto" runat="server" Style="font-size: 19px; font-weight: 700;"></asp:Label>
        <br />
        <br />
        <table style="border: thin solid #000000; width: 100%;">
            <tr>
                <td style="border: thin solid #000000; font-size: 15PX; font-weight: 700; text-align: center;" colspan="3">LIABILITY</td>
                <td style="border: thin solid #000000; font-size: 15PX; font-weight: 700; text-align: center;" colspan="3">EXPENSES</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000;">&nbsp;<strong>SL NO</strong></td>
                <td style="border: thin solid #000000;">&nbsp;<strong>PARTICULARS</strong></td>
                <td style="border: thin solid #000000;">&nbsp;<strong>IN RS</strong></td>
                <td style="border: thin solid #000000;">&nbsp;<strong>SL NO</strong></td>
                <td style="border: thin solid #000000;">&nbsp;<strong>PARTICULARS</strong></td>
                <td style="border: thin solid #000000;">&nbsp;<strong>IN RS</strong></td>
            </tr>
            <tr>
                <td style="border: thin solid #000000">&nbsp;1</td>
                <td style="border: thin solid #000000">&nbsp;ENTRANCE FEE</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="lblrp23" runat="server" Text="0"></asp:Label></strong></td>
                <td style="border: thin solid #000000">&nbsp;1</td>
                <td style="border: thin solid #000000">&nbsp;INTERENAL LOAN ISSUED TO THE MEMBERS</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label58" runat="server"></asp:Label></strong></td>
            </tr>
            <tr>
                <td style="border: thin solid #000000">&nbsp;2</td>
                <td style="border: thin solid #000000">&nbsp;MEMBERS SAVINGS</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label50" runat="server"></asp:Label></strong></td>
                <td style="border: thin solid #000000">&nbsp;2</td>
                <td style="border: thin solid #000000">&nbsp;BANK SAVINGS A/C BALANCE</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label59" runat="server"></asp:Label></strong></td>
            </tr>
            <tr>
                <td style="border: thin solid #000000">&nbsp;3</td>
                <td style="border: thin solid #000000">&nbsp;TOTAL INTEREST</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label51" runat="server"></asp:Label></strong></td>
                <td style="border: thin solid #000000">&nbsp;3</td>
                <td style="border: thin solid #000000">&nbsp;AMOUNT PAID ON FEDERATION</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label60" runat="server"></asp:Label></strong></td>
            </tr>
            <tr>
                <td style="border: thin solid #000000">&nbsp;4</td>
                <td style="border: thin solid #000000">&nbsp;TOTAL PENALTY</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label52" runat="server"></asp:Label></strong></td>
                <td style="border: thin solid #000000">&nbsp;4</td>
                <td style="border: thin solid #000000">&nbsp;BANK LOAN REPAYMENT</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label61" runat="server"></asp:Label></strong></td>
            </tr>
            <tr>
                <td style="border: thin solid #000000">&nbsp;5</td>
                <td style="border: thin solid #000000">&nbsp;BANK INTEREST ON SAVING A/C</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label53" runat="server"></asp:Label></strong></td>
                <td style="border: thin solid #000000">&nbsp;5</td>
                <td style="border: thin solid #000000">&nbsp;GROUP EXPENSES</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label62" runat="server"></asp:Label></strong></td>
            </tr>
            <tr>
                <td style="border: thin solid #000000">&nbsp;6</td>
                <td style="border: thin solid #000000">&nbsp;REVOLVING FUND</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label54" runat="server"></asp:Label></strong></td>
                <td style="border: thin solid #000000">&nbsp;6</td>
                <td style="border: thin solid #000000">&nbsp;CASH IN HAND</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label63" runat="server"></asp:Label></strong></td>
            </tr>
            <tr>
                <td style="border: thin solid #000000">&nbsp;7</td>
                <td style="border: thin solid #000000">&nbsp;OTHERS</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label55" runat="server"></asp:Label></strong></td>
                <td style="border: thin solid #000000">&nbsp;7</td>
                <td style="border: thin solid #000000">&nbsp;OTHERS</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label64" runat="server"></asp:Label></strong></td>
            </tr>
            <tr>
                <td style="border: thin solid #000000">&nbsp;</td>
                <td style="border: thin solid #000000">&nbsp;TOTAL AMOUNT</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label57" runat="server"></asp:Label></strong></td>
                <td style="border: thin solid #000000">&nbsp;</td>
                <td style="border: thin solid #000000">&nbsp;TOTAL AMOUNT</td>
                <td style="border: thin solid #000000">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label66" runat="server"></asp:Label></strong></td>
            </tr>
        </table>
        <br />
        <table style="width: 100%;">
            <tr>
                <td style="border: thin solid #000000; text-align: center;" colspan="3"><strong>FINANCIAL REPORT</strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000;" class="auto-style17">&nbsp;<strong>SL NO</strong></td>
                <td style="border: thin solid #000000;">&nbsp;<strong>PARTICULARS</strong></td>
                <td style="border: thin solid #000000;">&nbsp;<strong>IN RS</strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style17" style="border: thin solid #000000">&nbsp;1</td>
                <td style="border: thin solid #000000">&nbsp;ENTRANCE FEE</td>
                <td style="border: thin solid #000000; text-align: right">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label67" runat="server" Text="0"></asp:Label></strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style17" style="border: thin solid #000000">&nbsp;2</td>
                <td style="border: thin solid #000000">&nbsp;MEMBERS SAVINGS</td>
                <td style="border: thin solid #000000; text-align: right">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label68" runat="server"></asp:Label></strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style17" style="border: thin solid #000000">&nbsp;3</td>
                <td class="auto-style8" style="border: thin solid #000000">&nbsp;TOTAL INTEREST</td>
                <td class="auto-style8" style="border: thin solid #000000; text-align: right">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label69" runat="server"></asp:Label></strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style17" style="border: thin solid #000000">&nbsp;4</td>
                <td style="border: thin solid #000000" class="auto-style18">&nbsp;TOTAL PENALTY</td>
                <td style="border: thin solid #000000; text-align: right" class="auto-style18;">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label70" runat="server"></asp:Label></strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style17" style="border: thin solid #000000">&nbsp;5</td>
                <td style="border: thin solid #000000" class="auto-style18">&nbsp;BANK INTEREST ON SAVING A/C</td>
                <td style="border: thin solid #000000; text-align: right" class="auto-style18">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label71" runat="server"></asp:Label></strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style17" style="border: thin solid #000000">&nbsp;6</td>
                <td style="border: thin solid #000000">&nbsp;REVOLVING FUND</td>
                <td style="border: thin solid #000000; text-align: right">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label72" runat="server"></asp:Label></strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style17" style="border: thin solid #000000">&nbsp;7</td>
                <td style="border: thin solid #000000">&nbsp;OTHERS</td>
                <td style="border: thin solid #000000; text-align: right">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label73" runat="server"></asp:Label></strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style17" style="border: thin solid #000000">&nbsp;</td>
                <td style="border: thin solid #000000">&nbsp;<strong>TOTAL AMOUNT</strong></td>
                <td style="border: thin solid #000000; text-align: right">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label74" runat="server"></asp:Label></strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style17" style="border: thin solid #000000">&nbsp;</td>
                <td style="border: thin solid #000000">&nbsp;<strong>EXPENSES</strong></td>
                <td style="border: thin solid #000000; text-align: right">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label28" runat="server"></asp:Label></strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style17" style="border: thin solid #000000">&nbsp;8</td>
                <td style="border: thin solid #000000">&nbsp;SHG TOTAL LIABILITY</td>
                <td style="border: thin solid #000000; text-align: right">&nbsp;<strong style="font-size: 15px;"><asp:Label ID="Label75" runat="server"></asp:Label></strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <br />
        <table style="width: 100%;">
            <tr>
                <td><strong style="font-size: 15px;">&nbsp;&nbsp;
&nbsp;<asp:Label ID="lblrp11" runat="server"></asp:Label>&nbsp;&nbsp;Representative 1</strong></td>
                <td>&nbsp;</td>
                <td><strong style="font-size: 15px;">
                    <asp:Label ID="lblrp22" runat="server"></asp:Label>&nbsp;&nbsp;Representative 2</strong></td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <p style="font-size: 13px;">
            <b style="font-size: 13px; text-align: justify;">To,</b>
        </p>
        <p style="font-size: 13px;">
            <b style="font-size: 13px; text-align: justify;">The Director,</b>
        </p>
        <p style="font-size: 13px;">
            <b style="font-size: 13px; text-align: justify;">VIKASA Rural Development Organisation,</b>
        </p>
        <p style="font-size: 13px;">
            <b style="font-size: 13px; text-align: justify;">HO, Bangalore.</b>
        </p>
        <p style="font-size: 13px;">
            <b style="font-size: 13px; text-align: justify;">From,</b>
        </p>
        <table style="width: 100%;">
            <tr>
                <td class="auto-style4" style="border: thin solid #000000; text-align: center;">SHG seal</td>
                <td class="auto-style6"></td>
                <td class="auto-style6"></td>
            </tr>
        </table>
        <br />
        <p style="font-size: 13px; text-align: center;">
            <b style="font-size: 13px;">Sub: Details of Loan Amount Disbursed</b>
        </p>
        <br />
        <p style="line-height: 20px; font-size: 13px; text-align: justify;">
            <font size="3">We the members of&nbsp;&nbsp;<asp:Label ID="lbldisbursed" runat="server" style="text-decoration: underline"></asp:Label>&nbsp;&nbsp;SHG hereby acknowledge receipt of loan amount Rs__________________[Rupees____________________________] Lakhs sanctioned and disbursed to the credit of our SB a/c number&nbsp;&nbsp;<asp:Label ID="lbldisbursedacc" runat="server" style="text-decoration: underline"></asp:Label>&nbsp;&nbsp;by State Bank India&nbsp;&nbsp;<asp:Label ID="lbldisbursedbranch" runat="server" style="text-decoration: underline"></asp:Label>&nbsp;&nbsp;Branch on date ________________.
The details of amount received by the members as furnished below</font>
        </p>
        <table style="border: thin solid #000000; width: 100%; height: 110%;" border="1">
            <tr>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>Sno</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>Name of The Member</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>Loan Amount</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>Member Account Number/Issued Cheque No</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;<strong>Signature of Member</strong></td>
            </tr>
            <asp:ListView ID="ListView6" runat="server" RepeatColumns="3" ItemPlaceholderID="ItemPlaceholderID1">
                <ItemTemplate>
                    <tr>
                        <td style="border: thin solid #000000; font-size: 15px;">&nbsp;
                           <%#Container.DataItemIndex+1 %></td>
                        <td style="border: thin solid #000000; font-size: 15px;">&nbsp;
                            <asp:Label ID="lblname" runat="server" Text='<%#Eval("name") %>'></asp:Label>
                            &nbsp;</td>
                        <td style="border: thin solid #000000; font-size: 15px;">&nbsp;
                            &nbsp;</td>
                        <td style="border: thin solid #000000; font-size: 15px;">&nbsp;
                            <asp:Label ID="lblsbaccnum" runat="server" Text='<%#Eval("savingAccNum") %>'></asp:Label>
                            &nbsp;</td>
                        <td style="border: thin solid #000000; font-size: 15px;">&nbsp;
                            &nbsp;</td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </table>
        <br />
        <br />
        <br />
        <table style="width: 100%;">
            <tr>
                <td colspan="3">
                    <img align="LEFT" border="0" height="73" hspace="12" name="Picture 1" src="http://arerainfotech.com/vikasa.png" width="80" /><p align="center" class="MsoNormal">
                        <span>VIKASA RURAL DEVELOPMENT ORGANISATION<o:p></o:p></span>
                    </p>
                    <p align="center" class="MsoNormal">
                        <b><span>: #261 F, Balaji Co-op Society Layout, Vajarahalli, Kanakapura Main Road,<br />
                            Bangalore - 560062</span></b>
                    </p>
                </td>
            </tr>
            <tr style="text-align: center;">
                <td colspan="3"><strong>Receipt</strong></td>
            </tr>
            <tr style="text-align: right;">
                <td colspan="3"><strong>Date:</strong>&nbsp;<asp:Label ID="lbldisbursedDate" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3"><strong>SHG Name and Address:</strong>
                    <asp:Label ID="lbldisbursedSHGname" runat="server"></asp:Label>
                    <br />
                    <strong>Proposals No:</strong>
                    <asp:Label ID="lbldisbursedProposal" runat="server"></asp:Label>
                    <br />
                    <strong>SB A/C No:</strong>
                    <asp:Label ID="lbldisbursedSB" runat="server"></asp:Label>
                    <br />
                    <strong>Loan A/C No:</strong>
                    <asp:Label ID="lbldisbursedLoan" runat="server"></asp:Label>
                    <br />
                    <br />
                    <h5 style="text-align: center;">Received Rs. 100/- [Rupees one hundred only] towards training and documentation charges.</h5>
                    <p style="text-align: center;">&nbsp;</p>

                </td>
            </tr>
            <tr style="text-align: center;">
                <td style="border: thin solid #000000">
                    <asp:Label ID="lbldisbursedcdm" runat="server" Style="font-weight: 700"></asp:Label>
                    <br />
                    <br />
                    Community Development Manager<strong><br />
                    </strong>
                    <asp:Label ID="lbldisbusedcdmregion" runat="server" Style="font-weight: 700; text-decoration: underline;"></asp:Label>&nbsp;<strong>Region<br />
                    </strong>
                    <br />
                    <strong>-------------------------------------</strong></td>
                <td style="border: thin solid #000000">
                    <asp:Label ID="Label35" runat="server" Style="font-weight: 700"></asp:Label>
                    <br />
                    <br />
                    Community Development Officer<br />
                    <asp:Label ID="Label36" runat="server" Style="font-weight: 700; text-decoration: underline;"></asp:Label>&nbsp;<strong>Sector<br />
                        <br />
                        ----------------------------------</strong></td>
                <td style="border: thin solid #000000">
                    <asp:Label ID="Label37" runat="server" Style="font-weight: 700"></asp:Label>
                    <br />
                    <br />
                    Community Development Executive<br />
                    <asp:Label ID="Label38" runat="server" Style="font-weight: 700; text-decoration: underline;"></asp:Label>&nbsp;<strong>Branch<br />
                        <br />
                        -----------------------------------</strong></td>
            </tr>
        </table>
        <br />
        <p style="text-align: center;">Karnataka State society group registered act 1880 No 624/97-98 under applicable registered group</p>
        <i class="fa fa-scissors" aria-hidden="true"></i>---------------------------------------<i class="fa fa-scissors" aria-hidden="true"></i>---------------------------------------<i class="fa fa-scissors" aria-hidden="true"></i>---------------------------------------<i class="fa fa-scissors" aria-hidden="true"></i>------------
        <br />
        <br />
        <table style="width: 100%;">
            <tr>
                <td colspan="3">
                    <img align="LEFT" border="0" height="73" hspace="12" name="Picture 1" src="http://arerainfotech.com/vikasa.png" width="80" /><p align="center" class="MsoNormal">
                        <span>VIKASA RURAL DEVELOPMENT ORGANISATION<o:p></o:p></span>
                    </p>
                    <p align="center" class="MsoNormal">
                        <b><span>: #261 F, Balaji Co-op Society Layout, Vajarahalli, Kanakapura Main Road,<br />
                            Bangalore - 560062</span></b>
                    </p>
                </td>
            </tr>
            <tr style="text-align: center;">
                <td colspan="3"><strong>Receipt</strong></td>
            </tr>
            <tr style="text-align: right;">
                <td colspan="3"><strong>Date:</strong>&nbsp;<asp:Label ID="Label39" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3"><strong>SHG Name and Address:</strong>
                    <asp:Label ID="Label40" runat="server"></asp:Label>
                    <br />
                    <strong>Proposals No:</strong>
                    <asp:Label ID="Label41" runat="server"></asp:Label>
                    <br />
                    <strong>SB A/C No:</strong>
                    <asp:Label ID="Label42" runat="server"></asp:Label>
                    <br />
                    <strong>Loan A/C No:</strong>
                    <asp:Label ID="Label43" runat="server"></asp:Label>
                    <br />
                    <br />
                    <h5 style="text-align: center;">Received Rs. 100/- [Rupees one hundred only] towards training and documentation charges.</h5>
                    <p style="text-align: center;">&nbsp;</p>

                </td>
            </tr>
            <tr style="text-align: center;">
                <td style="border: thin solid #000000">
                    <asp:Label ID="Label44" runat="server" Style="font-weight: 700"></asp:Label>
                    <br />
                    <br />
                    Community Development Manager<br />
                    <asp:Label ID="Label45" runat="server" Style="font-weight: 700; text-decoration: underline;"></asp:Label>&nbsp;<strong>Region<br />
                        <br />
                        ----------------------------------</strong></td>
                <td style="border: thin solid #000000">
                    <asp:Label ID="Label46" runat="server" Style="font-weight: 700"></asp:Label>
                    <br />
                    <br />
                    Community Development Officer<br />
                    <asp:Label ID="Label47" runat="server" Style="font-weight: 700; text-decoration: underline;"></asp:Label>&nbsp;<strong>Sector<br />
                        <br />
                        ----------------------------------</strong></td>
                <td style="border: thin solid #000000">
                    <asp:Label ID="Label48" runat="server" Style="font-weight: 700"></asp:Label>
                    <br />
                    <br />
                    Community Development Executive<br />
                    <asp:Label ID="Label49" runat="server" Style="font-weight: 700; text-decoration: underline;"></asp:Label>&nbsp;<strong>Branch<br />
                        <br />
                        ------------------------------------</strong></td>
            </tr>
        </table>
        <br />
        <p style="text-align: center;">Karnataka State society group registered act 1880 No 624/97-98 under applicable registered group</p>
        <br />
        <br />
        <br />
        <p style="font-size: 15px; text-align: center;"><strong>STATE BANK OF INDIA&nbsp;<asp:Label ID="lblshgbranchlast" runat="server" Style="text-decoration: underline"></asp:Label>&nbsp;BRANCH</strong></p>
        <p style="font-size: 15px; text-align: center;"><strong>APPRAISAL NOTE FOR LOANS TO SELF HELP GROUPS (LOAN UNDER AGRI SEGMENT)</strong></p>
        <table style="border: thin solid #000000; width: 100%;">
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;1</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;Name and Address of the SHG</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<asp:Label ID="lblshgnamelast1" runat="server"></asp:Label></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;2</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;Group of Men/Women</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<asp:Label ID="Label65" runat="server"></asp:Label></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;3</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;Purpose</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<span>Agriculture and allied activities (Internal Lending to meet financial needs by taking up income generating activities being carried out by the group members) not required</span>&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;4</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;Segment</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<span>Agri</span>&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;Number of Members</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<asp:Label ID="Label76" runat="server"></asp:Label></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;6</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;Date of Formation</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<asp:Label ID="Label77" runat="server"></asp:Label></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;7</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;SB A/C</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<asp:Label ID="Label78" runat="server"></asp:Label></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;8</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;SHG Promoted By</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<span>VIKASA RURAL DEVELOPMENT ORGANISATION</span>&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;9</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Address of sponsor/promoter</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5"><span>&nbsp;VIKASA RURAL DEVELOPMENT ORGANISATION</span></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Maturity of the SHG</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<span>Matured Group</span>&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;11</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Grading of the SHG</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<span>Good</span>&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;12</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Level of Credit Linkage</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;13</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Periodicity of Meeting</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;14</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Savings Per Member Per Meeting</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;15</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Corpus of the SHG (Amt in Rs)</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;(a)</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Loan outstanding against members</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<asp:Label ID="Label79" runat="server"></asp:Label></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;(b)</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Cash on Hand</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<asp:Label ID="Label80" runat="server"></asp:Label></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;(c)</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Bank Balance</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<asp:Label ID="Label81" runat="server"></asp:Label></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;(d)</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>RID /Others</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<asp:Label ID="Label82" runat="server"></asp:Label></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<b><span>Total Corpus of the SHG [15a+15b+15c+15d]</span></b></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<asp:Label ID="Label83" runat="server"></asp:Label></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style34">&nbsp; 16</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style35">&nbsp;<span>Loan Applied</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style35" colspan="5">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;17</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Maximum Loan Eligible</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;18</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Loan Recommended</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;19</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Rate of Interest</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<span>______%above MCLR which is currently ___%, the present effective rate being _____% with monthly rests.</span></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;20</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Repayment period</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<span>__ equated monthly instalments of Rs ________/- commencing on month after the disbursement of the loan.</span></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;21</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Equated Monthly instalments</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;22</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Date of Application</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<asp:Label ID="Label84" runat="server"></asp:Label></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style34">&nbsp;23</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style35"><span>&nbsp;Date of Inspection</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style35" colspan="5"></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;24</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Security</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<span>Group Guarantee</span></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;25</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Details of loan availed earlier by the SHG</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;Sno</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;Loan A/C No</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;Limit</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp; Sanction Date</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;Closed Date</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32" rowspan="4">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" rowspan="4">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;1</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;2</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;3</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;4</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;<span>Recommendation/Remarks if</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;<p class="MsoNormal">
                    <span>&nbsp;&nbsp;<asp:Label ID="Label85" runat="server"></asp:Label>&nbsp;SHG is a proactive Self Help Group, formed on&nbsp;(<asp:Label ID="Label86" runat="server"></asp:Label>)&nbsp;with an objective of thrift and onward lending to its members for income generation and consumption. The group consists of&nbsp;<asp:Label ID="Label87" runat="server"></asp:Label>&nbsp;
                    members belonging to middle class and labour families. The group has requested the bank for financial assistance to meet the credit requirements for the group for onward lending to its group members.</span>
                </p>
                    <p class="MsoNormal">
                        <span>&nbsp;&nbsp;Pre sanction inspection has been carried out on ______________ and verified their minute books, individual savings book of each member and found properly maintained and updated. Group meeting is regular.</span>
                    </p>
                    &nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11" colspan="5">&nbsp;</td>
            </tr>
        </table>
        <br />
        <p style="font-size: 15px; text-align: justify;">&nbsp;&nbsp;Recommended for sanction of Rs. ___________________/- (Rupees ________________________________only)</p>
        <br />
        <p style="font-size: 15px; text-align: left;">&nbsp;&nbsp;Deputy Manager (Advances)</p>
        <p style="font-size: 15px; text-align: right;">&nbsp;&nbsp;Branch Manager</p>
        <p style="font-size: 15px; text-align: left;">&nbsp;&nbsp;Date</p>
        <p style="font-size: 15px; text-align: left;">&nbsp;&nbsp;Place</p>

        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <p style="font-size: 19px;"><strong><u>CHECK-LIST FOR SELECTION OF SELF HELP GROUPS FOR FINANCE</u></strong></p>
        <p style="font-size: 15px; text-align: justify;">A total of 300 marks are allotted covering various aspects of the functioning of a Self Help Group. Marks are to be allotted to each of the parameters taking into account the factual position obtaining in the SHG</p>
        <ol style="font-size: 15px; text-align: justify;">
            <li>SHG scoring, more than 200 points can be selected without any reservation</li>
            <li>SHG scoring less than 200 points but more than 150 points can be selected with CAUTION SHG scoring less than 150 points need to be considered.</li>
        </ol>
        <p style="font-size: 15px; text-align: justify;"><strong>The various aspects of the SHG as and the scoring pattern are as under:</strong></p>

        <table style="border: thin solid #000000; width: 100%;">
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27"><strong>&nbsp;Sno</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11"><b><span lang="EN-US">&nbsp;Criteria</span></b></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29"><b><span lang="EN-US">&nbsp;Allotted Marks</span></b></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><b><span lang="EN-US">&nbsp;Determining Marks</span></b></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11"><strong>&nbsp;Marks</strong></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;1</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Size of SHG</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp;10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Members between 15 &amp; 20</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td class="auto-style27" style="border: thin solid #000000; font-size: 15px;"></td>
                <td style="border: thin solid #000000; font-size: 15px;"></td>
                <td class="auto-style29" style="border: thin solid #000000; font-size: 15px;">&nbsp;8</td>
                <td class="auto-style28" style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Members between 10 &amp; 15</span></td>
                <td class="auto-style10" style="border: thin solid #000000; font-size: 15px;"></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp;5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Members less than 10</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;2</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Composition of Members</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp;10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Homogenous group</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp;5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Partially Homogenous</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp;0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Not Homogenous</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;3</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Target Group</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp;8</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Target Group only</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Non-target members 1 to 5</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Non-target members over 5</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;4</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Age of the Group</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;1 year and above</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 8</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Over 6 months &amp; less than 1 year</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 2</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Less than 6 months</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Number of Monthly Meetings</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Four Meetings</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 8</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;2 to 3 Meetings</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Only one Meeting</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp; 6</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Regularity of Meetings</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Regular</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Irregular</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp; 7</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Conduct of Meetings</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Without any persuasion of NGO</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;On persuasion/Direction of NGO</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp; 8</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Attendance</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 15</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Over 90 %</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27"></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style1"></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;70% to less than 90%</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style1"></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Less than 70 %</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Low</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp; 9</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Participation</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 15</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;High and democratic</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Medium</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Low</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Decision Making</span></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 15</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Decision taken independently</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style27">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style29">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style28"><span lang="EN-US">&nbsp;Decisions taken with NGO help</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <table style="border: thin solid #000000; width: 100%;">
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30"><strong>&nbsp;Sno</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11"><b><span lang="EN-US">&nbsp;Criteria</span></b></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11"><b><span lang="EN-US">&nbsp;Allotted Marks</span></b></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><b><span lang="EN-US">&nbsp;Determining Marks</span></b></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11"><strong>&nbsp;Marks</strong></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp; 11</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Frequency of Savings</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 15</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Monthly 4 times</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Monthly 3 times</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Monthly 2 times</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 3</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Monthly 1 time</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp; 12</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Regularity of Savings</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Regular</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 8</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Irregular</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp; 13</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Participation</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 7</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Optional rate</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Fixed Rate</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp; 14</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Interest on loans</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Depending on purpose</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 8</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;24 to 36%</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;More than 36%</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;No interest</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp; 15</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Loan Utilization by members</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 15</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Above 80 %</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Above 50% upto 80%</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Less than 50%</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;No loans</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>

            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp; 16</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Quantum of Savings</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 20</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;More than Rs. 15,000</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 15</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Rs. 10,000 to Rs. 15,000</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Rs. 5,000 to Rs. 10,000</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Less than Rs. 5,000</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp; 17</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Recovery of Loans</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Above 70%</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 15</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Above 10 % &amp; upto 90%</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Less than 70%</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;No recovery due to non-granting</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp; 18</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Record maintenance</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;good</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 8</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Satisfactory</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Bad</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp; 19</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Own Maintenance of Records</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 15</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Managed by Group</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Managed with paid employee</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Assistance from NGO</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp; 20</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Knowledge of Rules of SHG members</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Known to all members</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Known to most of members</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Not known to most of members</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp; 21</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Functional literacy of members</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;More than 20%</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Less than 50%</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;All illiterates</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp; 22</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Signing Capacity</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 10</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;More than 50%</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Less than 50%</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style30">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style31"><span lang="EN-US">&nbsp;Nobody knows signing</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <table style="border: thin solid #000000; width: 100%;">
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32"><strong>&nbsp;Sno</strong></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11"><b><span lang="EN-US">&nbsp;Criteria</span></b></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11"><b><span lang="EN-US">&nbsp;Allotted Marks</span></b></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33">&nbsp; <b><span lang="EN-US">Determining Marks</span></b></td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style11"><strong>&nbsp;Marks</strong></td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp; 23</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Schemes/Bank Schemes</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;All are aware</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 3</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Part of the Group are aware</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;None is aware</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp; 24</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Maintenance of Books</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 30</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Attendance Register</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 3</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Not maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Loan Ledger</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 3</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Not maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Savings Ledger</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Not maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Minutes Book</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Not maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Cash Book</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 5</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Not maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;General Ledger</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 3</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Not maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;"><span lang="EN-US">&nbsp;Bank Pass Books SHG</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 2</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 0</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33"><span lang="EN-US">&nbsp;Not maintained</span></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
            <tr style="font-size: 15px;">
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style32">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;"><b><span lang="EN-US">&nbsp;Total Marks</span></b></td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp; 300</td>
                <td style="border: thin solid #000000; font-size: 15px;" class="auto-style33">&nbsp;</td>
                <td style="border: thin solid #000000; font-size: 15px;">&nbsp;</td>
            </tr>
        </table>
        <br />
        <br />
    </div>
    <asp:Button ID="btnprint" runat="server" OnClick="btnprint_Click" Visible="false" Text="Save PDF" />
</asp:Content>
