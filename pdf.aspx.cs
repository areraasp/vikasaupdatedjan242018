﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class pdf : System.Web.UI.Page
{
    string shgid = string.Empty;
    string cdeid = string.Empty;
    string shgname = string.Empty;
    string bankbranch = string.Empty;
    string loanenqid = string.Empty;
    string spnum = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
			
			
			//to hide the master page vikasa image
            ((Image)Master.FindControl("picture1")).Visible = false; 
            shgid = Session["shgid"].ToString();
            cdeid = Session["cdeid"].ToString();
            Label85.Text = lblshgnamelast1.Text = shgname = Session["shgname"].ToString();
            lblshgbranchlast.Text = bankbranch = Session["bankbranch"].ToString();
            loanenqid = Session["loanenqid"].ToString();
            spnum = Session["sponsnum"].ToString();

            dataload1();
            loadnamesandsign();
            dataload3();
            dataload4();
            dataload5();
            string v = lblplacesecond.Text = Label29.Text = Label32.Text = Label56.Text = Alib.idGetAFieldByQuery("select village from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            string tq = Alib.idGetAFieldByQuery("select taluka from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblshgname99.Text = lbldisbursed.Text = lbldisbursedSHGname.Text = Label40.Text = lblnamenaddressofshg.Text = shgname + "," + v + "," + tq + "(Tq)";
            lbldisbursedbranch.Text = lblbankbranch.Text = Alib.idGetAFieldByQuery("select branch from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            Label65.Text = Alib.idGetAFieldByQuery("select type from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblrp1.Text = lblrp11.Text = lblnameofrep1.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='0'");
            lblrp2.Text = lblrp22.Text = lblnameofrep2.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='1'");
            lbldisbursedacc.Text = lblsavingsaccnum.Text = Alib.idGetAFieldByQuery("select savingAccount from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");

            Label42.Text = lbldisbursedSB.Text = lblloanaccnum.Text = Alib.idGetAFieldByQuery("select savingAccount from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblloandate.Text = "";
            lblloanamount.Text = "";
            lblpurposeofloan.Text = Alib.idGetAFieldByQuery("select reason from [vikasardo].[vikasa].shgexternalloansplit where loan_enq_num='" + loanenqid + "'");
            lblshgname.Text = shgname + "," + v + "," + tq + "(Tq)";
            Label86.Text = Label77.Text = lblfinancialfrom.Text = lblshgformdate.Text = Alib.idGetAFieldByQuery("select formationDate from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblregyesorno.Text = "NO";
            Label87.Text = Label76.Text = lbltotalmembinshg.Text = Alib.idGetAFieldByQuery("select totalMember from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblsbaccnumnopendate.Text = lblloanaccnum.Text + " " + " " + " " + " " + " " + " " + " " + "--" + Alib.idGetAFieldByQuery("select savingAccOpenDate from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblnameofshpi.Text = "VIKASA RURAL DEVELOPMENT ORGANISATION";
            lblbranch.Text = Alib.idGetAFieldByQuery("select branch from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblplace.Text = "";
            lbldate.Text = "";
            lblamountrequired.Text = Alib.idGetAFieldByQuery("select shgloanamount from [vikasardo].[vikasa].shgexternalloansplit where loan_enq_num='" + loanenqid + "'");
            lblamountwords.Text = rupees(Convert.ToInt32(lblamountrequired.Text));
            lbldateempty.Text = "";
            lblshgname99.Text = Label15.Text = lblshgname2.Text = shgname;
            lblshgtotalsavings.Text = Alib.idGetAFieldByQuery("select sum(convert(int, savings)) as SHGSavings from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "'");
            lblcashinhand.Text = Alib.idGetAFieldByQuery("select sum(convert(int, shgexpcashinhand)) as SHGCashinHand from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc)");
            lblcashinbank.Text = Alib.idGetAFieldByQuery("select sum(convert(int, shgexpamountinbanksavings)) as SHGCashinBank from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc)");

            string assetsexpothers = Alib.idGetAFieldByQuery("select sum(convert(int, shgexpothers)) as SHG_expOthers from [vikasardo].[vikasa].meeting where cdeid='" + cdeid + "' and shgid='" + shgid + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc)");
            string assetsshgothers = Alib.idGetAFieldByQuery("select expenses from [vikasardo].[vikasa].[shg] where shgid='" + shgid + "' and cdeid='" + cdeid + "'");
            // 10th august lbltotexpense.Text = (Convert.ToDouble(assetsexpothers) + Convert.ToDouble(assetsshgothers)).ToString();
            Label79.Text = lbltotintlendbal.Text = Alib.idGetAFieldByQuery("select sum(convert(int, lendbal)) as SHG_InternalLending from [vikasardo].[vikasa].meeting where cdeid='" + cdeid + "' and shgid='" + shgid + "' and meetings=(select top 1 meetings from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc)");
            if (Label79.Text == "")
            {
                Label79.Text = "0";
            }

            Label80.Text = Alib.idGetAFieldByQuery("select sum(convert(int, shgexpcashinhand)) as SHGCashinHand from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' order by id desc)");
            if (Label80.Text == "")
            {
                Label80.Text = "0";
            }

            Label81.Text = Alib.idGetAFieldByQuery("select sum(convert(int, shgexpamountinbanksavings)) as SHGCashinBank from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' order by id desc)");
            if (Label81.Text == "")
            {
                Label81.Text = "0";
            }

            Label82.Text = Alib.idGetAFieldByQuery("select rid from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            if (Label82.Text == "")
            {
                Label82.Text = "0";
            }

            Label83.Text = (Convert.ToDouble(Label79.Text) + Convert.ToDouble(Label80.Text) + Convert.ToDouble(Label81.Text) + Convert.ToDouble(Label82.Text)).ToString();

            lbltotalgrants.Text = Alib.idGetAFieldByQuery("select sum(convert(int, shgtotgrants)) as SHGSavings from [vikasardo].[vikasa].meeting where id in (SELECT max(m.id) from [vikasardo].[vikasa].meeting m where shgid='" + shgid + "' and cdeid='" + cdeid + "'group by  m.date)");
            if (lbltotalgrants.Text == "0")
            {
                lbltotalgrants.Text = "NA";
            }

            lblshgname3.Text = shgname;
            lblshgloc.Text = v + "," + tq + "(Tq)";
            lblrep1.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='0'");
            lblrep2.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='1'");
            lblrep11.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='0'");
            lblrep22.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='1'");
            lblbankbranch1.Text = Alib.idGetAFieldByQuery("select branch from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblshgloanamt.Text = Alib.idGetAFieldByQuery("select shgloanamount from [vikasardo].[vikasa].shgexternalloansplit where loan_enq_num='" + loanenqid + "'");
            lblrsinwords.Text = rupees(Convert.ToInt32(lblamountrequired.Text));
            lblshgname4.Text = shgname;
            // 27/6/17   lblshgsplitamount.Text = Alib.idGetAFieldByQuery("select amount from [vikasardo].[vikasa].shgexternalloansplit where loan_enq_num='" + loanenqid + "'");
            // 27/6/17  lblamtinrs.Text = rupees(Convert.ToInt32(lblshgsplitamount.Text));
            lblrep111.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='0'");
            lblrep222.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='1'");
            lblbankbranch11.Text = Alib.idGetAFieldByQuery("select branch from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblrep33.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='0'");
            lblrep34.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='1'");
            lblbankbranch12.Text = Alib.idGetAFieldByQuery("select branch from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblshgname5.Text = shgname;
            lblrep55.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='0'");
            lblrep56.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='1'");
            lblshgloanreqamt.Text = Alib.idGetAFieldByQuery("select shgloanamount from [vikasardo].[vikasa].shgexternalloansplit where loan_enq_num='" + loanenqid + "'");
            lblshgname43.Text = shgname;
            Label78.Text = lblsbaccno.Text = Alib.idGetAFieldByQuery("select savingAccount from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblshgname90.Text = shgname;
            lblbankbranch15.Text = Alib.idGetAFieldByQuery("select branch from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblsavaccnum23.Text = Alib.idGetAFieldByQuery("select savingAccount from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblshgsbaccnum23.Text = Alib.idGetAFieldByQuery("select savingAccount from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblsbacccopendate.Text = Alib.idGetAFieldByQuery("select savingAccOpenDate from [vikasardo].[vikasa].[shg] where shgid='" + shgid + "'");
            lblrep88.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='0'");
            lblrep99.Text = Alib.idGetAFieldByQuery("select name from [vikasardo].[vikasa].representative where shgid='" + shgid + "' and memberActionID='1'");
            //  lblshgsbaccnum89.Text = Alib.idGetAFieldByQuery("select savingAccount from shg where shgid='" + shgid + "'");
            lblshgloanamount12.Text = Alib.idGetAFieldByQuery("select shgloanamount from [vikasardo].[vikasa].shgexternalloansplit where loan_enq_num='" + loanenqid + "'");

            //
            string cdmid = Alib.idGetAFieldByQuery("select cdmid from [vikasardo].[vikasa].cde where cdeid='" + cdeid + "'");
            string cdoid = Alib.idGetAFieldByQuery("select cdoid from [vikasardo].[vikasa].cde where cdeid='" + cdeid + "'");
            Label37.Text = Label48.Text = lblcde.Text = Label12.Text = Alib.idGetAFieldByQuery("select cdename from [vikasardo].[vikasa].cde where cdeid='" + cdeid + "'");
            Label44.Text = lbldisbursedcdm.Text = lblcdm.Text = Label10.Text = Alib.idGetAFieldByQuery("select cdmname from [vikasardo].[vikasa].cdm where cdmid='" + cdmid + "'");
            Label35.Text = Label46.Text = lblcdo.Text = Label11.Text = Alib.idGetAFieldByQuery("select cdoname from [vikasardo].[vikasa].cdo where cdoid='" + cdoid + "'");

            lblreg.Text = Alib.idGetAFieldByQuery("select region from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lbltq.Text = Alib.idGetAFieldByQuery("select taluka from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblbranch90.Text = Alib.idGetAFieldByQuery("select branch from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lbccodate.Text = Alib.idGetAFieldByQuery("select savingAccOpenDate from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");

            //Sponser Data
            lblbankbranch67.Text = Alib.idGetAFieldByQuery("select branch from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblshgname78.Text = shgname;
            Random rn = new Random();
            //lbldisbursedProposal.Text = Label41.Text = lblnum.Text = spnum;
            string appr = Session["approved"].ToString();
            if (appr == "approved")
            {
                lbldisbursedProposal.Text = Label41.Text = lblnum.Text = spnum;
                Session["approved"] = "";
            }


            Label84.Text = lblproposal9999.Text = lblproposedate999.Text = lblproposaldate999.Text = lblprposaldate99.Text = Label27.Text = lblproposaldatesixthpage.Text = lbldateempty.Text = lbldatesecond.Text = lbldisbursedDate.Text = Label39.Text = lbldate23.Text = Alib.idGetAFieldByQuery("select proposaldate from [vikasardo].[vikasa].shgexternalloansplit where loan_enq_num='" + loanenqid + "'");
            lblshgname1.Text = shgname;
            lblshgloanamt90.Text = Alib.idGetAFieldByQuery("select shgloanamount from [vikasardo].[vikasa].shgexternalloansplit where loan_enq_num='" + loanenqid + "'");
            lblrsinwords12.Text = rupees(Convert.ToInt32(lblshgloanamt90.Text));
            lblbankbranch78.Text = Alib.idGetAFieldByQuery("select branch from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblshgname45.Text = shgname;
            lblshgamount6.Text = Alib.idGetAFieldByQuery("select shgloanamount from [vikasardo].[vikasa].shgexternalloansplit where loan_enq_num='" + loanenqid + "'");
            lblshgamtinrs.Text = rupees(Convert.ToInt32(lblshgamount6.Text));
            lblshgloanamt56.Text = Alib.idGetAFieldByQuery("select shgloanamount from [vikasardo].[vikasa].shgexternalloansplit where loan_enq_num='" + loanenqid + "'");
            lblamtinrupis.Text = rupees(Convert.ToInt32(lblshgamount6.Text));
            Label45.Text = lbldisbusedcdmregion.Text = lblreg56.Text = Alib.idGetAFieldByQuery("select region from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            Label47.Text = Label36.Text = lbltaluk12.Text = Alib.idGetAFieldByQuery("select taluka from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            Label49.Text = Label38.Text = lblbranch67.Text = Alib.idGetAFieldByQuery("select branch from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");
            lblshgname87.Text = shgname;
            lbltaluk.Text = Alib.idGetAFieldByQuery("select taluka from [vikasardo].[vikasa].shg where shgid='" + shgid + "'");

            //End Sponser Data
            lblfinancialto.Text = Alib.idGetAFieldByQuery("select top 1 date from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' order by id desc");
            dataload_DISBURSED();
            dataload_SHG_MEMBER_WISE_TOTAL_FINANCIAL_REPORT();
            dataload_SHG_FINANCIAL_REPORT_FROM_TO();
            //shgwisecalculate();
        }
    }

    private void dataload1()
    {

        string qur = "SELECT m.name, m.dob, m.fatherorhusband, m.address, s.village from [vikasardo].[vikasa].members m inner join [vikasardo].[vikasa].shg s on m.shgid=s.shgid where m.shgid='" + shgid + "'";
        SqlConnection con = new SqlConnection(Alib.conStr);
        con.Open();
        SqlCommand cmd = new SqlCommand(qur, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable ds = new DataTable();
        da.Fill(ds);
        int num = ds.Rows.Count;

        int bal = 20 - num;
        for (int i = 0; i < bal; i++)
        {
            DataRow dr = ds.Rows[2];
            ds.Rows.Add(ds);
        }
        listcart.DataSource = ds;
        listcart.DataBind();
        con.Close();

    }

    public string rupees(Int64 rupees)
    {
        string result = "";
        Int64 res;
        if ((rupees / 10000000) > 0)
        {
            res = rupees / 10000000;
            rupees = rupees % 10000000;
            result = result + ' ' + rupeestowords(res) + " Crore";
        }
        if ((rupees / 100000) > 0)
        {
            res = rupees / 100000;
            rupees = rupees % 100000;
            result = result + ' ' + rupeestowords(res) + " Lakh(s)";
        }
        if ((rupees / 1000) > 0)
        {
            res = rupees / 1000;
            rupees = rupees % 1000;
            result = result + ' ' + rupeestowords(res) + " Thousand";
        }
        if ((rupees / 100) > 0)
        {
            res = rupees / 100;
            rupees = rupees % 100;
            result = result + ' ' + rupeestowords(res) + " Hundred";
        }
        if ((rupees % 10) > 0)
        {
            res = rupees % 100;
            result = result + " " + rupeestowords(res);
        }
        result = result + ' ' + " Rupees only";
        return result;
    }
    public string rupeestowords(Int64 rupees)
    {
        string result = "";
        if ((rupees >= 1) && (rupees <= 10))
        {
            if ((rupees % 10) == 1) result = "One";
            if ((rupees % 10) == 2) result = "Two";
            if ((rupees % 10) == 3) result = "Three";
            if ((rupees % 10) == 4) result = "Four";
            if ((rupees % 10) == 5) result = "Five";
            if ((rupees % 10) == 6) result = "Six";
            if ((rupees % 10) == 7) result = "Seven";
            if ((rupees % 10) == 8) result = "Eight";
            if ((rupees % 10) == 9) result = "Nine";
            if ((rupees % 10) == 0) result = "Ten";
        }
        if (rupees > 9 && rupees < 20)
        {
            if (rupees == 11) result = "Eleven";
            if (rupees == 12) result = "Twelve";
            if (rupees == 13) result = "Thirteen";
            if (rupees == 14) result = "Forteen";
            if (rupees == 15) result = "Fifteen";
            if (rupees == 16) result = "Sixteen";
            if (rupees == 17) result = "Seventeen";
            if (rupees == 18) result = "Eighteen";
            if (rupees == 19) result = "Nineteen";
        }
        if (rupees > 20 && (rupees / 10) == 2 && (rupees % 10) == 0) result = "Twenty";
        if (rupees > 20 && (rupees / 10) == 3 && (rupees % 10) == 0) result = "Thirty";
        if (rupees > 20 && (rupees / 10) == 4 && (rupees % 10) == 0) result = "Forty";
        if (rupees > 20 && (rupees / 10) == 5 && (rupees % 10) == 0) result = "Fifty";
        if (rupees > 20 && (rupees / 10) == 6 && (rupees % 10) == 0) result = "Sixty";
        if (rupees > 20 && (rupees / 10) == 7 && (rupees % 10) == 0) result = "Seventy";
        if (rupees > 20 && (rupees / 10) == 8 && (rupees % 10) == 0) result = "Eighty";
        if (rupees > 20 && (rupees / 10) == 9 && (rupees % 10) == 0) result = "Ninty";

        if (rupees > 20 && (rupees / 10) == 2 && (rupees % 10) != 0)
        {
            if ((rupees % 10) == 1) result = "Twenty One";
            if ((rupees % 10) == 2) result = "Twenty Two";
            if ((rupees % 10) == 3) result = "Twenty Three";
            if ((rupees % 10) == 4) result = "Twenty Four";
            if ((rupees % 10) == 5) result = "Twenty Five";
            if ((rupees % 10) == 6) result = "Twenty Six";
            if ((rupees % 10) == 7) result = "Twenty Seven";
            if ((rupees % 10) == 8) result = "Twenty Eight";
            if ((rupees % 10) == 9) result = "Twenty Nine";
        }
        if (rupees > 20 && (rupees / 10) == 3 && (rupees % 10) != 0)
        {
            if ((rupees % 10) == 1) result = "Thirty One";
            if ((rupees % 10) == 2) result = "Thirty Two";
            if ((rupees % 10) == 3) result = "Thirty Three";
            if ((rupees % 10) == 4) result = "Thirty Four";
            if ((rupees % 10) == 5) result = "Thirty Five";
            if ((rupees % 10) == 6) result = "Thirty Six";
            if ((rupees % 10) == 7) result = "Thirty Seven";
            if ((rupees % 10) == 8) result = "Thirty Eight";
            if ((rupees % 10) == 9) result = "Thirty Nine";
        }
        if (rupees > 20 && (rupees / 10) == 4 && (rupees % 10) != 0)
        {
            if ((rupees % 10) == 1) result = "Forty One";
            if ((rupees % 10) == 2) result = "Forty Two";
            if ((rupees % 10) == 3) result = "Forty Three";
            if ((rupees % 10) == 4) result = "Forty Four";
            if ((rupees % 10) == 5) result = "Forty Five";
            if ((rupees % 10) == 6) result = "Forty Six";
            if ((rupees % 10) == 7) result = "Forty Seven";
            if ((rupees % 10) == 8) result = "Forty Eight";
            if ((rupees % 10) == 9) result = "Forty Nine";
        }
        if (rupees > 20 && (rupees / 10) == 5 && (rupees % 10) != 0)
        {
            if ((rupees % 10) == 1) result = "Fifty One";
            if ((rupees % 10) == 2) result = "Fifty Two";
            if ((rupees % 10) == 3) result = "Fifty Three";
            if ((rupees % 10) == 4) result = "Fifty Four";
            if ((rupees % 10) == 5) result = "Fifty Five";
            if ((rupees % 10) == 6) result = "Fifty Six";
            if ((rupees % 10) == 7) result = "Fifty Seven";
            if ((rupees % 10) == 8) result = "Fifty Eight";
            if ((rupees % 10) == 9) result = "Fifty Nine";
        }
        if (rupees > 20 && (rupees / 10) == 6 && (rupees % 10) != 0)
        {
            if ((rupees % 10) == 1) result = "Sixty One";
            if ((rupees % 10) == 2) result = "Sixty Two";
            if ((rupees % 10) == 3) result = "Sixty Three";
            if ((rupees % 10) == 4) result = "Sixty Four";
            if ((rupees % 10) == 5) result = "Sixty Five";
            if ((rupees % 10) == 6) result = "Sixty Six";
            if ((rupees % 10) == 7) result = "Sixty Seven";
            if ((rupees % 10) == 8) result = "Sixty Eight";
            if ((rupees % 10) == 9) result = "Sixty Nine";
        }
        if (rupees > 20 && (rupees / 10) == 7 && (rupees % 10) != 0)
        {
            if ((rupees % 10) == 1) result = "Seventy One";
            if ((rupees % 10) == 2) result = "Seventy Two";
            if ((rupees % 10) == 3) result = "Seventy Three";
            if ((rupees % 10) == 4) result = "Seventy Four";
            if ((rupees % 10) == 5) result = "Seventy Five";
            if ((rupees % 10) == 6) result = "Seventy Six";
            if ((rupees % 10) == 7) result = "Seventy Seven";
            if ((rupees % 10) == 8) result = "Seventy Eight";
            if ((rupees % 10) == 9) result = "Seventy Nine";
        }
        if (rupees > 20 && (rupees / 10) == 8 && (rupees % 10) != 0)
        {
            if ((rupees % 10) == 1) result = "Eighty One";
            if ((rupees % 10) == 2) result = "Eighty Two";
            if ((rupees % 10) == 3) result = "Eighty Three";
            if ((rupees % 10) == 4) result = "Eighty Four";
            if ((rupees % 10) == 5) result = "Eighty Five";
            if ((rupees % 10) == 6) result = "Eighty Six";
            if ((rupees % 10) == 7) result = "Eighty Seven";
            if ((rupees % 10) == 8) result = "Eighty Eight";
            if ((rupees % 10) == 9) result = "Eighty Nine";
        }
        if (rupees > 20 && (rupees / 10) == 9 && (rupees % 10) != 0)
        {
            if ((rupees % 10) == 1) result = "Ninty One";
            if ((rupees % 10) == 2) result = "Ninty Two";
            if ((rupees % 10) == 3) result = "Ninty Three";
            if ((rupees % 10) == 4) result = "Ninty Four";
            if ((rupees % 10) == 5) result = "Ninty Five";
            if ((rupees % 10) == 6) result = "Ninty Six";
            if ((rupees % 10) == 7) result = "Ninty Seven";
            if ((rupees % 10) == 8) result = "Ninty Eight";
            if ((rupees % 10) == 9) result = "Ninty Nine";
        }
        return result;
    }
    protected void btnprint_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "abc", "PrintPanel();", true);
    }

    private void loadnamesandsign()
    {

        string qur = "SELECT NAME from [vikasardo].[vikasa].members where shgid='" + shgid + "'";
        SqlConnection con = new SqlConnection(Alib.conStr);
        con.Open();
        SqlCommand cmd = new SqlCommand(qur, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable ds = new DataTable();
        da.Fill(ds);

        int num = ds.Rows.Count;
        if (num != 20)
        {
            int bal = 20 - num;
            for (int i = 0; i < bal; i++)
            {
                DataRow dr = ds.Rows[1];
                ds.Rows.Add(ds);
                ListView1.DataSource = ds;
                ListView1.DataBind();
            }
        }
        else
        {
            ListView1.DataSource = ds;
            ListView1.DataBind();
        }
        con.Close();

    }

    private void dataload3()
    {

        string qur = "SELECT NAME from [vikasardo].[vikasa].members where shgid='" + shgid + "'";
        SqlConnection con = new SqlConnection(Alib.conStr);
        con.Open();
        SqlCommand cmd = new SqlCommand(qur, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable ds = new DataTable();
        da.Fill(ds);
        int num = ds.Rows.Count;
        if (num != 20)
        {
            int bal = 20 - num;
            for (int i = 0; i < bal; i++)
            {
                DataRow dr = ds.Rows[1];
                ds.Rows.Add(ds);
                ListView2.DataSource = ds;
                ListView2.DataBind();
            }
        }
        else
        {
            ListView2.DataSource = ds;
            ListView2.DataBind();
        }
        con.Close();

    }
    private void dataload4()
    {

        string qur = "SELECT NAME,fatherORhusband,adhaarNum,savingAccNum,pan,mobile,dob,caste,address from [vikasardo].[vikasa].members where shgid='" + shgid + "'";
        SqlConnection con = new SqlConnection(Alib.conStr);
        con.Open();
        SqlCommand cmd = new SqlCommand(qur, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable ds = new DataTable();
        da.Fill(ds);

        int num = ds.Rows.Count;
        if (num != 20)
        {
            int bal = 20 - num;
            for (int i = 0; i < bal; i++)
            {
                DataRow dr = ds.Rows[1];
                ds.Rows.Add(ds);
                ListView3.DataSource = ds;
                ListView3.DataBind();
            }
        }
        else
        {
            ListView3.DataSource = ds;
            ListView3.DataBind();
        }
        con.Close();

    }
    private void dataload5()
    {

        //string qur = "SELECT NAME,savingAccNum,select village from shg where shgid='" + shgid + "',select reason from shgexternalloansplit where loan_enq_num='" + loanenqid + "' from members where shgid='" + shgid + "'";
        string qur = "select m.name, m.savingAccNum, s.village from [vikasardo].[vikasa].[members] m inner join [vikasardo].[vikasa].[shg] s on m.shgid = s.shgid where m.shgid='" + shgid + "'";
        SqlConnection con = new SqlConnection(Alib.conStr);
        con.Open();
        SqlCommand cmd = new SqlCommand(qur, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable ds = new DataTable();
        da.Fill(ds);

        int num = ds.Rows.Count;
        if (num != 20)
        {
            int bal = 20 - num;
            for (int i = 0; i < bal; i++)
            {
                DataRow dr = ds.Rows[1];
                ds.Rows.Add(ds);
                ListView4.DataSource = ds;
                ListView4.DataBind();
            }
        }
        else
        {
            ListView4.DataSource = ds;
            ListView4.DataBind();
        }
        con.Close();

    }

    //---------------------Prashanth's work from 14th July----------------------//

    private void dataload_SHG_MEMBER_WISE_TOTAL_FINANCIAL_REPORT()
    {
        string qur = "select name, totalsavings, interest, penalty, lending, convert(float, lending)- convert(float, lendbal) as repay, lendbal from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and meetings= (select top 1 meetings from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' order by id desc)";
        SqlConnection con = new SqlConnection(Alib.conStr);
        con.Open();
        SqlCommand cmd = new SqlCommand(qur, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable ds = new DataTable();
        da.Fill(ds);

        int num = ds.Rows.Count;
        if (num != 20)
        {
            int bal = 20 - num;
            for (int i = 0; i < bal; i++)
            {
                DataRow dr = ds.Rows[1];
                ds.Rows.Add(ds);
                ListView5.DataSource = ds;
                ListView5.DataBind();
            }
        }
        else
        {
            ListView5.DataSource = ds;
            ListView5.DataBind();
        }
        con.Close();
    }

    //public void shgwisecalculate()
    //{
        
    //}

    private void dataload_DISBURSED()
    {

        //string qur = "SELECT NAME,savingAccNum,select village from shg where shgid='" + shgid + "',select reason from shgexternalloansplit where loan_enq_num='" + loanenqid + "' from members where shgid='" + shgid + "'";
        string qur = "select m.name, m.savingAccNum from [vikasardo].[vikasa].members m inner join [vikasardo].[vikasa].shg s on m.shgid=s.shgid where m.shgid='" + shgid + "'";
        SqlConnection con = new SqlConnection(Alib.conStr);
        con.Open();
        SqlCommand cmd = new SqlCommand(qur, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable ds = new DataTable();
        da.Fill(ds);

        int num = ds.Rows.Count;
        if (num != 20)
        {
            int bal = 20 - num;
            for (int i = 0; i < bal; i++)
            {
                DataRow dr = ds.Rows[1];
                ds.Rows.Add(ds);
                ListView6.DataSource = ds;
                ListView6.DataBind();
            }
        }
        else
        {
            ListView6.DataSource = ds;
            ListView6.DataBind();
        }
        con.Close();

    }

    public void dataload_SHG_FINANCIAL_REPORT_FROM_TO()
    {

        lblrp23.Text = Label67.Text = Alib.idGetAFieldByQuery("select sum(convert(int, s.membershipfee)) from (select distinct(date), membershipfee from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and cdeid='" + cdeid + "' and date<>'0') s");
        if (lblrp23.Text == "")
        {
            lblrp23.Text = Label67.Text = "0";
        }


        Label50.Text = Label68.Text = Alib.idGetAFieldByQuery("select sum(convert(int, savings)) as SHGSavings from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' and date<>'0'");
        if (Label50.Text == "")
        {
            Label50.Text = Label68.Text = "0";
        }

        Label51.Text = Label69.Text = Alib.idGetAFieldByQuery("select sum(convert(int, interest)) as SHGInterest from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' and date<>'0'");
        if (Label51.Text == "")
        {
            Label51.Text = Label69.Text = "0";
        }

        Label52.Text = Label70.Text = Alib.idGetAFieldByQuery("select sum(convert(int, penalty)) as SHGpENALTY from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' and date<>'0'");
        if (Label52.Text == "")
        {
            Label52.Text = Label70.Text = "0";
        }

        Label53.Text = Label71.Text = Alib.idGetAFieldByQuery("SELECT sum(convert(int, shgtotbankinterest)) as total from [vikasardo].[vikasa].meeting where id in (SELECT max(m.id) from [vikasardo].[vikasa].meeting m where shgid='" + shgid + "' and cdeid='" + cdeid + "' and date<>'0' group by m.date)");
        if (Label53.Text == "")
        {
            Label53.Text = Label71.Text = "0";
        }

        Label54.Text = Label72.Text = Alib.idGetAFieldByQuery("select sum(convert(int, shgtotgrants)) as SHGSavings from [vikasardo].[vikasa].meeting where id in (SELECT max(m.id) from [vikasardo].[vikasa].meeting m where shgid='" + shgid + "' and cdeid='" + cdeid + "' and date<>'0' group by m.date)");
        if (Label54.Text == "")
        {
            Label54.Text = Label72.Text = "0";
        }

        Label55.Text = Label73.Text = Alib.idGetAFieldByQuery("select sum(convert(int, s.shgotherssavings)) from (select distinct(date), shgotherssavings from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and cdeid='" + cdeid + "' and date<>'0') s");
        if (Label55.Text == "")
        {
            Label55.Text = Label73.Text = "0";
        }

        Label57.Text = Label74.Text = (Convert.ToDouble(lblrp23.Text) + Convert.ToDouble(Label50.Text) + Convert.ToDouble(Label51.Text) + Convert.ToDouble(Label52.Text) + Convert.ToDouble(Label53.Text) + Convert.ToDouble(Label54.Text) + Convert.ToDouble(Label55.Text)).ToString();


        Label58.Text = Alib.idGetAFieldByQuery("select sum(convert(int, lendbal)) as SHG_InternalLending from [vikasardo].[vikasa].meeting where cdeid='" + cdeid + "' and shgid='" + shgid + "' and meetings=(select top 1 meetings from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc)");
        if (Label58.Text == "")
        {
            Label58.Text = "0";
        }

        Label59.Text = Alib.idGetAFieldByQuery("select sum(convert(int, shgexpamountinbanksavings)) as SHGCashinBank from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc)");
        if (Label59.Text == "")
        {
            Label59.Text = "0";
        }

        Label61.Text = Label60.Text = "0";


        Label28.Text = Label62.Text = Alib.idGetAFieldByQuery("select sum(convert(int, s.shgexp)) from (select distinct(date), shgexp from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and cdeid='" + cdeid + "' and date<>'0') s");
        if (Label28.Text == "")
        {
            Label28.Text = Label62.Text = "0";
        }

        Label63.Text = Alib.idGetAFieldByQuery("select sum(convert(int, shgexpcashinhand)) as SHGCashinHand from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' and id=(select top 1 id from [vikasardo].[vikasa].meeting where shgid='" + shgid + "' and cdeid='" + cdeid + "' order by id desc)");
        if (Label63.Text == "")
        {
            Label63.Text = "0";
        }

        Label64.Text = Alib.idGetAFieldByQuery("select sum(convert(int, s.shgexpothers)) from (select distinct(date), shgexpothers from [vikasardo].[vikasa].[meeting] where shgid='" + shgid + "' and cdeid='" + cdeid + "' and date<>'0') s");
        if (Label64.Text == "")
        {
            Label64.Text = "0";
        }

        Label66.Text = (Convert.ToDouble(Label58.Text) + Convert.ToDouble(Label63.Text) + Convert.ToDouble(Label59.Text) + Convert.ToDouble(Label62.Text) + Convert.ToDouble(Label64.Text)).ToString();
        if (Label66.Text == "")
        {
            Label66.Text = "0";
        }

        Label75.Text = (Convert.ToDouble(Label74.Text) - Convert.ToDouble(Label28.Text)).ToString();
        if (Label75.Text == "")
        {
            Label75.Text = "0";
        }
    }
}