using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sactioned : System.Web.UI.Page
{
    int index = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //    select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from shgexternalloansplit where id in (SELECT max(id) from shgexternalloansplit group by  loan_enq_num) and cdeid='" + cdeid + "' and approvedstate='1' and status='2'
            //Glib.LoadRequest("select id,referenceID,loan_enq_num,shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type,amount,sanctionedDate,disburseimg,meetingimg from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by  loan_enq_num) and status='13'", gvsprovider);
            //Glib.LoadRequest("select id,referenceID,loan_number,shgname,shgid,cdeid,approvedloanamount,momdate,reason,bankbrach,split_type,amount,sanctionedDate,disburseimg,meetingimg from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by  loan_enq_num) and status='13'", gvsprovider);
            Glib.LoadRequest("select id,referenceID,loan_enq_num,loan_number,shgname,shgid,cdeid,approvedloanamount,momdate,reason,bankbrach,split_type,amount,sanctionedDate,disburseimg,meetingimg from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by  loan_enq_num) and status='13'", gvsprovider);

        }


    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        //string qur = "Select top 10 shgname as tmp from [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] where shgname like '" + pre + "%' and status='13' union Select top 10 bankbrach as tmp from [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] where bankbrach like '" + pre + "%' and status='13'";
        string qur = "select distinct shgname as tmp from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from[vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by  loan_enq_num) and status = '13' and shgname like '" + pre + "%' and status = '13' union Select top 10 bankbrach as tmp from[vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] where bankbrach like '" + pre + "%' and status = '13'";
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }
    }
    protected void btnsrch_Click(object sender, EventArgs e)
    {
        if (txtsrch.Text == "")
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter SHG Name or Branch',text: '',timer: 1000,showConfirmButton: false})", true);
            Glib.LoadRequest("select id,referenceID,loan_enq_num,loan_number,shgname,shgid,cdeid,approvedloanamount,momdate,reason,bankbrach,split_type,amount,sanctionedDate,disburseimg,meetingimg from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by  loan_enq_num) and status='13'", gvsprovider);
            return;
        }
        else
        {
            // Glib.LoadRequest("select id,referenceID,loan_enq_num,shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type,amount,sanctionedDate,disburseimg,meetingimg from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by loan_enq_num) and status='13' and shgname like '" + txtsrch.Text + "%' or bankbrach like '" + txtsrch.Text + "%' ", gvsprovider);
            Glib.LoadRequest("select id,referenceID,loan_enq_num,loan_number,shgname,shgid,cdeid,approvedloanamount,momdate,reason,bankbrach,split_type,amount,sanctionedDate,disburseimg,meetingimg from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by loan_enq_num) and status='13' and (shgname like '" + txtsrch.Text + "%' or bankbrach like '" + txtsrch.Text + "%' )", gvsprovider);
        }
    }
    protected void lnkapprove_Click(object sender, EventArgs e)
    {
        string loanrefid = "";
        LinkButton link = sender as LinkButton;
        if (link.Text == "")
        {

        }
        else
        {
            //Looping through each Gridview row to find exact Row 
            //of the Grid from where the SelectedIndex change event is fired.
            foreach (GridViewRow row in gvsprovider.Rows)
            {
                //Finding Dropdown control  
                Control ctrl = row.FindControl("lnkapprove") as LinkButton;
                if (ctrl != null)
                {
                    LinkButton link1 = (LinkButton)ctrl;

                    //Comparing ClientID of the LinkButton with sender
                    //  if(ddl1.SelectedIndex==0
                    if (link.ClientID == link1.ClientID)
                    {
                        if (link.Text != "")
                        {
                            string shgname = row.Cells[6].Text;
                            var ctrlId = row.FindControl("Button1") as Button;
                            //lblid.Text = row.Cells[6].Text;
                            lblid.Text = ctrlId.Text;
                            string cdeid = row.Cells[8].Text;
                            string shgloan = row.Cells[9].Text;
                            string loanenqno = row.Cells[4].Text;
                            loanrefid = row.Cells[3].Text;
                            //float splitamt = float.Parse(row.Cells[13].Text);
                            float splitamt;
                            int membcount = Convert.ToInt32(Alib.idGetAFieldByQuery("select count(memberid) from [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] where  loan_enq_num='" + loanenqno + "'"));

                            string confirmValue = Request.Form["confirm_value"];
                            if (confirmValue == "Yes")
                            {
                                Alib.idExecute("update [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval set status='14',referenceID='" + loanrefid + "' where shgid='" + lblid.Text + "' and shgname='" + shgname + "' and approvedloanamount='" + shgloan + "' and cdeid='" + cdeid + "' and loan_enq_num='" + loanenqno + "'");
                                Alib.idExecute("update [vikasardo].[vikasa].shgexternalloansplit set status='14' where shgid='" + lblid.Text + "' and referenceID='" + loanrefid + "' and shgname='" + shgname + "' and cdeid='" + cdeid + "' and loan_enq_num='" + loanenqno + "'");
                                int mid = 1 + Convert.ToInt32(Alib.idGetAFieldByQuery("select top 1 meetings from [vikasardo].[vikasa].meeting where shgid='" + lblid.Text + "' order by id desc"));
                                int checkmid = mid - 1;
                                int membid;
                                SqlConnection conn = new SqlConnection(Alib.conStr);
                                SqlCommand cmd = null;
                                SqlDataReader reader1 = null;
                                cmd = new SqlCommand("select memberid,amount from [vikasardo].[vikasa].[shgexternalloansplit_forfinalapproval] where loan_enq_num='" + loanenqno + "'", conn);
                                conn.Open();
                                reader1 = cmd.ExecuteReader();
                                if (reader1.HasRows)
                                {
                                    while (reader1.Read())
                                    {
                                        splitamt = Convert.ToInt32(reader1["amount"].ToString());
                                        membid = Convert.ToInt32(reader1["memberid"].ToString());
                                        Alib.idExecute("update [vikasardo].[vikasa].meeting set bankloan=bankloan+" + splitamt + ",bankloanpending=bankloanpending+" + splitamt + " where shgid='" + lblid.Text + "' and cdeid='" + cdeid + "' and meetings='" + checkmid + "' and memberid='" + membid + "'");
                                    }
                                }
                                conn.Close();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Approved successfully..!',text: '',timer: 2000,showConfirmButton: false},function(){location.href='sactioned.aspx'})", true);
                                //Glib.LoadRequest("select id,referenceID,loan_enq_num,shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type,amount,sanctionedDate,disburseimg,meetingimg from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by  loan_enq_num) and status='13'", gvsprovider);
                                //Glib.LoadRequest("select id,referenceID,loan_number,shgname,shgid,cdeid,approvedloanamount,momdate,reason,bankbrach,split_type,amount,sanctionedDate,disburseimg,meetingimg from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by  loan_enq_num) and status='13'", gvsprovider);
                                Glib.LoadRequest("select id,referenceID,loan_enq_num,loan_number,shgname,shgid,cdeid,approvedloanamount,momdate,reason,bankbrach,split_type,amount,sanctionedDate,disburseimg,meetingimg from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by  loan_enq_num) and status='13'", gvsprovider);

                                // update to spc table
                                //  Alib.idExecute("INSERT INTO [vikasardo].[spc] (cdeid,shgid,shgrefno,shg,village,members,sbaccno,atlaccno,typeofac,loanaccdate,term,emi,loanamt,totrepay,lastrepay,repaydate,challan,balance,status,spc) Values ('" + cdeid + "','" + lblid.Text + "','" + loanrefid + "','" + shgname + "','" + bankbranch + "','" + totmembers + "','" + shgaccnum + "','" + shgaccnum + "','" + accType + "','" + System.DateTime.Now.ToString("dd/MM/yyyy") + "','" + terms + "','" + emi + "','" + shgloanamount + "','0','0','" + System.DateTime.Now + "','0','" + shgloanamount + "','0','0')");


                            }
                        }
                        else
                        {
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked No..!!')", true);
                        }
                    }
                }
            }
        }

    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (txtsrch.Text == "")
        {
            gvsprovider.PageIndex = e.NewPageIndex;
            Glib.LoadRequest("select id,referenceID,loan_enq_num,loan_number,shgname,shgid,cdeid,approvedloanamount,momdate,reason,bankbrach,split_type,amount,sanctionedDate,disburseimg,meetingimg from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by  loan_enq_num) and status='13'", gvsprovider);
        }
        else
        {
            gvsprovider.PageIndex = e.NewPageIndex;
            Glib.LoadRequest("select id,referenceID,loan_enq_num,loan_number,shgname,shgid,cdeid,approvedloanamount,momdate,reason,bankbrach,split_type,amount,sanctionedDate,disburseimg,meetingimg from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by loan_enq_num) and status='13' and (shgname like '" + txtsrch.Text + "%' or bankbrach like '" + txtsrch.Text + "%') ", gvsprovider);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        index = SHGSactioned.shgId;
        Glib.LoadRequest("SELECT member.name as Name, loanSplit.bankbrach as BankBranch ,  loanSplit.split_type as SplitType, loanSplit.amount as amount FROM [vikasa].[shgexternalloansplit] loanSplit INNER JOIN  [vikasa].[members] member ON member.memberID = loanSplit.memberId where loanSplit.shgid='" + index + "'", GridView1);
    }

    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("shgid"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            string qur = "SELECT member.name as Name, loanSplit.bankbrach as BankBranch ,  loanSplit.split_type as SplitType, loanSplit.amount as amount FROM [vikasa].[shgexternalloansplit] loanSplit INNER JOIN  [vikasa].[members] member ON member.memberID = loanSplit.memberId where loanSplit.shgid='" + index + "'";
            try
            {
                GridView1.DataSource = Alib.getData(qur);
                GridView1.DataBind();
                SHGSactioned.shgId = index;

                memberGridInSanctioned.Visible = true;
            }
            catch (Exception ex)
            {

            }
        }
    }



    protected void lnkrej_Click(object sender, EventArgs e)
    {
        string loanrefid = "";
        LinkButton link = sender as LinkButton;
        if (link.Text == "")
        {

        }
        else
        {
            //Looping through each Gridview row to find exact Row 
            //of the Grid from where the SelectedIndex change event is fired.
            foreach (GridViewRow row in gvsprovider.Rows)
            {
                //Finding Dropdown control  
                Control ctrl = row.FindControl("lnkrej") as LinkButton;
                if (ctrl != null)
                {
                    LinkButton link1 = (LinkButton)ctrl;

                    //Comparing ClientID of the LinkButton with sender
                    //  if(ddl1.SelectedIndex==0
                    if (link.ClientID == link1.ClientID)
                    {
                        if (link.Text != "")
                        {
                            string shgname = row.Cells[6].Text;
                            //lblid.Text = row.Cells[6].Text;
                            var ctrlId = row.FindControl("Button1") as Button;
                            lblid.Text = ctrlId.Text;
                            string cdeid = row.Cells[8].Text;
                            string shgloan = row.Cells[9].Text;
                            string loanenqno = row.Cells[4].Text;
                            loanrefid = row.Cells[3].Text;
                            int sancRejected = Convert.ToInt32(Alib.idGetAFieldByQuery("Select [isSancRejected] FROM [vikasa].[shgexternalloansplit] where loan_enq_num='" + loanenqno + "'"));

                            string confirmValue = Request.Form["confirm_value"];
                            if (confirmValue == "Yes" && sancRejected == 0)
                            {
                                Alib.idExecute("update [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval set status='7' where shgid='" + lblid.Text + "' and shgname='" + shgname + "' and approvedloanamount='" + shgloan + "' and cdeid='" + cdeid + "' and loan_enq_num='" + loanenqno + "' and referenceID='" + loanrefid + "'");
                                Alib.idExecute("update [vikasardo].[vikasa].shgexternalloansplit set status='7',rejreason='Reason:RejectedbyHO',rejbyid='HO',rejbyrole='HO' where shgid='" + lblid.Text + "' and shgname='" + shgname + "' and cdeid='" + cdeid + "' and loan_enq_num='" + loanenqno + "'");

                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({ confirmButtonText:'Ok',title:'',text:'Approved Successfully..!',type:'success'})", true);
                                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Rejected Successfully..!!')", true);
                                //Glib.LoadRequest("select id,referenceID,loan_enq_num,shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by  loan_enq_num) and status='13'", gvsprovider);
                                //Glib.LoadRequest("select id,referenceID,loan_number,shgname,shgid,cdeid,approvedloanamount,momdate,reason,bankbrach,split_type from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by  loan_enq_num) and status='13'", gvsprovider);
                                Glib.LoadRequest("select id,referenceID,loan_enq_num,loan_number,shgname,shgid,cdeid,approvedloanamount,momdate,reason,bankbrach,split_type,amount,sanctionedDate,disburseimg,meetingimg from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by  loan_enq_num) and status='13'", gvsprovider);

                                //updating column [isSancRejected] as 1
                                Alib.idExecute("update [vikasa].[shgexternalloansplit] set [isSancRejected] = 1 where loan_enq_num='" + loanenqno + "'");
                            }
                            else
                            {

                                Alib.idExecute("update [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval set status='50' where shgid='" + lblid.Text + "' and shgname='" + shgname + "' and approvedloanamount='" + shgloan + "' and cdeid='" + cdeid + "' and loan_enq_num='" + loanenqno + "' and referenceID='" + loanrefid + "'");
                                Alib.idExecute("update [vikasardo].[vikasa].shgexternalloansplit set status='50',rejreason='Reason:RejectedbyHO',rejbyid='HO',rejbyrole='HO' where shgid='" + lblid.Text + "' and shgname='" + shgname + "' and cdeid='" + cdeid + "' and loan_enq_num='" + loanenqno + "'");

                                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Rejected Successfully..!!')", true);
                                Glib.LoadRequest("select id,referenceID,loan_enq_num,loan_number,shgname,shgid,cdeid,approvedloanamount,momdate,reason,bankbrach,split_type,amount,sanctionedDate,disburseimg,meetingimg from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit_forfinalapproval group by  loan_enq_num) and status='13'", gvsprovider);

                                //updating column [isSancRejected] as 1
                                Alib.idExecute("update [vikasa].[shgexternalloansplit] set [isSancRejected] = 2 where loan_enq_num='" + loanenqno + "'");

                                //Update status into "", for permanent reject
                            }
                        }
                        else
                        {
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked No..!!')", true);
                        }
                    }
                }
            }
        }
    }
}