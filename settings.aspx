﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="settings.aspx.cs" Inherits="settigs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="assets/js/jquery.min.js"></script>
    <link href="assets/css/sweetalert.css" rel="stylesheet" />
    <script src="assets/js/sweetalert.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

    <link href="assets/js/jquery.timepicker.css" rel="stylesheet" />
    <script src="assets/js/jquery.timepicker.js"></script>
    <script src="assets/js/jquery.timepicker.min.js"></script>
    <style type="text/css">
        .img-zoom {
            width: 30px;
            -webkit-transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            -ms-transition: all .2s ease-in-out;
        }

        .hiddencol {
            display: none;
        }

        .transition {
            -webkit-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
        }
    </style>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.1.5"
        media="screen" />
    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#bfs5').addClass('active');
            $('#id3').addClass('active');
            $('.img-zoom').hover(function () {
                $(this).addClass('transition');
            }, function () {
                $(this).removeClass('transition');
            });
            $('.fancybox').fancybox();
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    overlay: {
                        speedOut: 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS: 'fancybox-custom',
                closeClick: true,
                openEffect: 'none',
                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        css: {
                            'background': 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });
        });
    </script>
    <style type="text/css">
        .fancybox-custom .fancybox-skin {
            box-shadow: 0 0 50px #222;
        }

        .navbar-inverse {
            background-color: #797979;
            border-color: #797979;
            width: 100%;
        }

        .hiddencol {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblid" runat="server" Visible="false"></asp:Label>
    <asp:HiddenField ID="hiddenlastrepay" runat="server" />
    <div class="row">
        <div class="col-sm-12">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav" runat="server" id="listdata1">
                            <li><a href="mcde.aspx">CDE</a></li>
                            <li><a href="mcdo.aspx">CDO</a></li>
                            <li><a href="mcdm.aspx">CDM</a></li>
                            <li><a href="mbranch.aspx">Branch</a></li>
                            <li><a href="mdistricts.aspx">District</a></li>
                            <li><a href="meducation.aspx">Education</a></li>
                            <li><a href="mgovtcards.aspx">Govt Card</a></li>
                            <li><a href="mloantype.aspx">Loan Type</a></li>
                            <li><a href="moccupation.aspx">Occupation</a></li>
                            <li><a href="mregions.aspx">Region</a></li>
                            <li><a href="leaves.aspx">Leaves</a></li>
                            <li class="active"><a href="settings.aspx">Controls</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
            <!--/ .page title -->
        </div>
    </div>
    <%--<div class="row">
        <div class="col-sm-12 ">
            <p class="text-muted page-title-alt">
                <button type="button" id="Button2" style="float: right;" runat="server" class="btn btn-default waves-effect waves-light">
                    <span class="btn-label"><i class="fa fa-plus"></i>
                    </span>Create New</button>
            </p>
        </div>
    </div>--%>
    <br />
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <label>HO Number</label>
                <asp:TextBox ID="txtnum" runat="server" class="form-control"></asp:TextBox>
            </div>
            <asp:Button ID="btnupdate" runat="server" Text="Update" CssClass="btn btn-default btn-custom" Height="32" Width="80" Style="padding: 2px;" OnClick="btnupdate_Click" />
            <asp:Button ID="btncan" runat="server" Text="Cancel" CssClass="btn btn-danger btn-custom" Height="32" Width="80" Style="padding: 2px;" OnClick="btncan_Click" />
        </div>
        <div class="col-lg-4">
            <label>Username</label>
            <div class="form-group">
                <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
            </div>
            <label>Password</label>
            <div class="form-group">
                <asp:TextBox ID="TextBox2" runat="server" class="form-control"></asp:TextBox>
            </div>
            <br />
            <asp:Button ID="Button1" runat="server" Text="Update" CssClass="btn btn-default btn-custom" Height="32" Width="80" Style="padding: 2px;" OnClick="Button1_Click" />
        </div>
    </div>
    <br />
    <div class="col-lg-12 card-box">
        <div class="table-responsive">
            <asp:GridView ID="gvsprovider" runat="server" Caption="Loan Enquiry List" EmptyDataText="No Data Available" EmptyDataRowStyle-CssClass="text-danger" EmptyDataRowStyle-Font-Size="Medium" CssClass="table table-bordered table-hover table-striped"
                AutoGenerateColumns="false" OnRowCommand="gvsprovider_RowCommand">
                <Columns>
                    <asp:ButtonField CommandName="edi" ControlStyle-CssClass="btn btn-default btn-custom" ButtonType="Button"
                        Text="Edit" HeaderText="Edit"></asp:ButtonField>
                    <asp:BoundField DataField="id" HeaderText="Sno"></asp:BoundField>
                    <asp:BoundField DataField="num" HeaderText="Contact"></asp:BoundField>
                </Columns>
                <PagerStyle CssClass="gridview"></PagerStyle>
            </asp:GridView>
        </div>
    </div>
    <div class="col-lg-12 card-box">
        <div class="table-responsive">
            <asp:GridView ID="GridView1" runat="server" Caption="User Credentials" EmptyDataRowStyle-CssClass="text-danger" EmptyDataRowStyle-Font-Size="Medium" CssClass="table table-bordered table-hover table-striped">
                <Columns>
                    <asp:ButtonField CommandName="edi" ControlStyle-CssClass="btn btn-default btn-custom" ButtonType="Button"
                        Text="Edit" HeaderText="Edit"></asp:ButtonField>
                </Columns>
                <PagerStyle CssClass="gridview"></PagerStyle>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

