﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class settigs : System.Web.UI.Page
{
    int index = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Glib.LoadRequest("Select * from HO", gvsprovider);
            TextBox1.Text = Alib.idGetAFieldByQuery("Select username from users");
            TextBox2.Text = Alib.idGetAFieldByQuery("Select password from users");
        }
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("edi"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[1].Text).ToString();
            lblid.Text = id;
            txtnum.Text = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
        }
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        Alib.idExecute("Update HO set num='" + txtnum.Text + "' where id='" + lblid.Text + "'");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Successfully Updated',text: '',timer: 2000,showConfirmButton: false})", true);
        Glib.LoadRequest("Select * from HO", gvsprovider);
    }
    protected void btncan_Click(object sender, EventArgs e)
    {
        txtnum.Text = "";
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Alib.idExecute("Update users set username='"+TextBox1.Text+"', password='"+TextBox2.Text+"'");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Successfully Updated',text: '',timer: 2000,showConfirmButton: false})", true);
        TextBox1.Text = Alib.idGetAFieldByQuery("Select username from users");
        TextBox2.Text = Alib.idGetAFieldByQuery("Select password from users");
    }
}