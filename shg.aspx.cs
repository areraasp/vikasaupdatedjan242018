﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class shg : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].shg where status='3' order by shgid desc", gvsprovider);
            Glib.LoadDDL(ddldistrict, "select distinct name from [vikasardo].[vikasa].districts", "name", "name");
            Glib.LoadDDL(ddlregion, "select distinct name from [vikasardo].[vikasa].regions", "name", "name");
            Glib.LoadDDL(ddlbranch, "select distinct name from [vikasardo].[vikasa].bankBranch", "name", "name");
        }
    }
    protected void btncreatenew_ServerClick(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
        btnsubmit.Text = "Submit";

    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (txtname.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Please Enter Name',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (ddltype.SelectedItem.Text == "--Select--")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Select Any Type: Men/ Women',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (ddlregion.SelectedItem.Text == "--Select--")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Select Any Region',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (ddldistrict.SelectedItem.Text == "--Select--")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Select Any District',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txt_taluk.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Taluk Name',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtvillage.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Village Name',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtsavingaccnum.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Saving Account Number',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtmeetdate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Meeting Date',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtmeetingtime.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Meeting Time',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtmeetplace.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Meeting Place',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtformdate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Formation Date',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtaccopendate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Account Open Date',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtmembers.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Total Members',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtreason.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Reason',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (btnsubmit.Text.Equals("Submit"))
        {
            Alib.idInsertInto("[vikasardo].[vikasa].shg",
                "name", txtname.Text,
                "type", ddltype.SelectedItem.Text,
                "village", txtvillage.Text,
                "taluka", txt_taluk.Text,
                "region", ddlregion.SelectedItem.Text,
                "district", ddldistrict.SelectedItem.Text,
                "savingAccount", txtsavingaccnum.Text,
                "meetingDate", txtmeetdate.Text,
                "annualmeetDate", txtannualmeetdate.Text,
                "annualIncome", txtannualincome.Text,
                "totalMember", txtmembers.Text,
                "meetingPlace", txtmeetplace.Text,
                "meetingTime", txtmeetingtime.Text,
                "formationDate", txtformdate.Text,
                "savingAccOpenDate", txtaccopendate.Text,
                "reason", txtreason.Text,
                "branch", ddlbranch.SelectedItem.Text);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Created successfully..!',text: '',timer: 2000,showConfirmButton: false},function(){location.href='shg.aspx'})", true);
        }
        else if (btnsubmit.Text.Equals("Update"))
        {
            Alib.idExecute("update [vikasardo].[vikasa].[shg] set name='" + txtname.Text + "', type='" + ddltype.SelectedItem.Text + "', village='" + txtvillage.Text + "', taluka='" + txt_taluk.Text + "', region='" + ddlregion.SelectedItem.Text + "', district='" + ddldistrict.SelectedItem.Text + "', savingAccount='" + txtsavingaccnum.Text + "', meetingDate='" + txtmeetdate.Text + "', annualmeetDate= '" + txtannualmeetdate.Text + "', annualIncome= '" + txtannualincome.Text + "', totalMember='" + txtmembers.Text + "', meetingPlace='" + txtmeetplace.Text + "', meetingTime='" + txtmeetingtime.Text + "', formationDate='" + txtformdate.Text + "', savingAccOpenDate='" + txtaccopendate.Text + "', reason='" + txtreason.Text + "', branch='" + ddlbranch.SelectedItem.Text + "' where shgid='" + lblid.Text + "'");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Updated successfully..!',text: '',timer: 2000,showConfirmButton: false},function(){location.href='shg.aspx'})", true);
        }
        else if (btnsubmit.Text.Equals("Reject"))
        {
            Alib.idExecute("update [vikasardo].[vikasa].shg set reason='" + txtreason.Text + "', status='5', rejectedby='HO', rejectedbyname='HO' where shgid='" + lblid.Text + "'");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Rejected.!',text: '',timer: 2000,showConfirmButton: false},function(){location.href='shg.aspx'})", true);
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].shg where status='3'  order by shgid desc", gvsprovider);
        }
        Glib.LoadRequest("Select * from [vikasardo].[vikasa].shg where status='3' order by shgid desc", gvsprovider);
        clean();
    }

    public void clean()
    {
        btnsubmit.Text = "Submit";
        txtname.Text = txt_taluk.Text = txtvillage.Text = txtsavingaccnum.Text = txtmeetdate.Text = txtmeetingtime.Text = txtmeetplace.Text = txtformdate.Text = txtaccopendate.Text = txtmembers.Text = txtreason.Text = txtannualmeetdate.Text = txtannualincome.Text = "";

        ddltype.SelectedItem.Text = "--Select--";
        Glib.LoadDDL(ddldistrict, "select distinct name from [vikasardo].[vikasa].districts", "name", "name");
        Glib.LoadDDL(ddlregion, "select distinct name from [vikasardo].[vikasa].regions", "name", "name");
        Glib.LoadDDL(ddlbranch, "select distinct name from [vikasardo].[vikasa].bankBranch", "name", "name");
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        string qur = "Select top 10 name as tmp from [vikasardo].[vikasa].[shg] where name like '" + pre + "%' and status='3' union Select top 10 branch as tmp from [vikasardo].[vikasa].shg where branch like '" + pre + "%' and status='3'";
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = -1;
    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select * from [vikasardo].[vikasa].shg where status='3' order by shgid desc", gvsprovider);
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Del"))
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[4].Text).ToString();
            lblid.Text = id;
            Alib.idExecute("delete from [vikasardo].[vikasa].shg where shgid='" + id + "'");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Deleted successfully..!',text: '',timer: 2000,showConfirmButton: false},function(){location.href='shg.aspx'})", true);
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].shg where status='3' order by shgid desc", gvsprovider);
        }
        if (e.CommandName.Equals("edi"))
        {
            btnsubmit.Text = "Update";
            MultiView1.ActiveViewIndex = 0;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[4].Text).ToString();
            lblid.Text = id;
            txtname.Text = HttpUtility.HtmlDecode(rows.Cells[5].Text).ToString();
            ddltype.SelectedItem.Text = HttpUtility.HtmlDecode(rows.Cells[6].Text).ToString();
            txtvillage.Text = HttpUtility.HtmlDecode(rows.Cells[7].Text).ToString();
            txt_taluk.Text = HttpUtility.HtmlDecode(rows.Cells[8].Text).ToString();
            ddlregion.SelectedItem.Text = HttpUtility.HtmlDecode(rows.Cells[9].Text).ToString();
            ddldistrict.SelectedItem.Text = HttpUtility.HtmlDecode(rows.Cells[10].Text).ToString();
            txtsavingaccnum.Text = HttpUtility.HtmlDecode(rows.Cells[11].Text).ToString();
            txtmeetdate.Text = HttpUtility.HtmlDecode(rows.Cells[12].Text).ToString();
            txtannualmeetdate.Text = HttpUtility.HtmlDecode(rows.Cells[13].Text).ToString();
            txtannualincome.Text = HttpUtility.HtmlDecode(rows.Cells[14].Text).ToString();
            txtmembers.Text = HttpUtility.HtmlDecode(rows.Cells[15].Text).ToString();
            txtmeetplace.Text = HttpUtility.HtmlDecode(rows.Cells[16].Text).ToString();
            txtmeetingtime.Text = HttpUtility.HtmlDecode(rows.Cells[17].Text).ToString();
            txtformdate.Text = HttpUtility.HtmlDecode(rows.Cells[18].Text).ToString();
            txtaccopendate.Text = HttpUtility.HtmlDecode(rows.Cells[19].Text).ToString();
            txtreason.Text = HttpUtility.HtmlDecode(rows.Cells[20].Text).ToString();
            ddlbranch.SelectedItem.Text = HttpUtility.HtmlDecode(rows.Cells[21].Text).ToString();
        }
        if (e.CommandName.Equals("apr"))
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[4].Text).ToString();

            lblid.Text = id;
            Alib.idExecute("Update [vikasardo].[vikasa].shg set status='4' where shgid=" + id);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Approved successfully..!',text: '',timer: 2000,showConfirmButton: false},function(){location.href='shg.aspx'})", true);
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].shg  where status='3' order by shgid desc", gvsprovider);

            // -----------------Message Send Starts-----------------------//

            string cdmnum = Alib.idGetAFieldByQuery("Select m.cdmphone from [vikasardo].[vikasa].[cdm] m inner join [vikasardo].[vikasa].[cde] e on e.cdmid=m.cdmid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + id + "'");
            string cdmname = Alib.idGetAFieldByQuery("Select m.cdmname from [vikasardo].[vikasa].[cdm] m inner join [vikasardo].[vikasa].[cde] e on e.cdmid=m.cdmid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + id + "'");
            string shgname = Alib.idGetAFieldByQuery("Select name from [vikasardo].[vikasa].[shg] where shgid ='" + id + "'");
            string cdoname = Alib.idGetAFieldByQuery("Select o.cdoname from [vikasardo].[vikasa].[cdo] o inner join [vikasardo].[vikasa].[cde] e on e.cdoid=o.cdoid inner join [vikasardo].[vikasa].[shg] s on e.cdeid=s.cdeid where shgid ='" + id + "'");
            string cdename = Alib.idGetAFieldByQuery("Select e.cdename from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shg] s on e.cdeid= s.cdeid where shgid ='" + id + "'");
            string wish = "Dear " + cdmname + ",\n HO Panel has approved your requested SHG-" + shgname + ", Inform related CDE to keep SHG activities regularly,\n Regards,\n HO Panel";
            SqlConnection con = null;
            SqlDataReader rdr = null;
            try
            {
                WebClient Client = new WebClient();
                Uri uri = new Uri("http://52.74.106.47/sendsms/?username=vikasavrdo@gmail.com&password=vikasa&text=" + wish + "&route=0&from=VIKASA&to=" + cdmnum);
                Stream data = Client.OpenRead(uri);
                StreamReader reader = new StreamReader(data);
                string s = reader.ReadToEnd();
                data.Close();
                reader.Close();
            }
            catch (SqlException ex)
            {
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                    con.Dispose();
                }
                if (rdr != null)
                    rdr.Dispose();
            }

            //-----------------Message Send Ends-------------------------//

        }
        if (e.CommandName.Equals("rej"))
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[4].Text).ToString();
            lblid.Text = id;

            MultiView1.ActiveViewIndex = 0;
            btnsubmit.Text = "Reject";
            txtname.Text = HttpUtility.HtmlDecode(rows.Cells[5].Text).ToString();
            ddltype.SelectedItem.Text = HttpUtility.HtmlDecode(rows.Cells[6].Text).ToString();
            txtvillage.Text = HttpUtility.HtmlDecode(rows.Cells[7].Text).ToString();
            txt_taluk.Text = HttpUtility.HtmlDecode(rows.Cells[8].Text).ToString();
            ddlregion.SelectedItem.Text = HttpUtility.HtmlDecode(rows.Cells[9].Text).ToString();
            ddldistrict.SelectedItem.Text = HttpUtility.HtmlDecode(rows.Cells[10].Text).ToString();
            txtsavingaccnum.Text = HttpUtility.HtmlDecode(rows.Cells[11].Text).ToString();
            txtmeetdate.Text = HttpUtility.HtmlDecode(rows.Cells[12].Text).ToString();
            txtannualmeetdate.Text = HttpUtility.HtmlDecode(rows.Cells[13].Text).ToString();
            txtannualincome.Text = HttpUtility.HtmlDecode(rows.Cells[14].Text).ToString();
            txtmembers.Text = HttpUtility.HtmlDecode(rows.Cells[15].Text).ToString();
            txtmeetplace.Text = HttpUtility.HtmlDecode(rows.Cells[16].Text).ToString();
            txtmeetingtime.Text = HttpUtility.HtmlDecode(rows.Cells[17].Text).ToString();
            txtformdate.Text = HttpUtility.HtmlDecode(rows.Cells[18].Text).ToString();
            txtaccopendate.Text = HttpUtility.HtmlDecode(rows.Cells[19].Text).ToString();
            txtreason.Text = HttpUtility.HtmlDecode(rows.Cells[20].Text).ToString();
            ddlbranch.SelectedItem.Text = HttpUtility.HtmlDecode(rows.Cells[21].Text).ToString();


        }
    }
    protected void btnsrch_Click(object sender, EventArgs e)
    {
        if (txtsrch.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter SHG Name or Branch',text: '',timer: 1000,showConfirmButton: false})", true);
            return;
        }
        else
        {
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].[shg] where name like '%" + txtsrch.Text.Trim() + "%' or branch like '%" + txtsrch.Text.Trim() + "%' and status='3' order by shgid desc", gvsprovider);
        }
    }
}